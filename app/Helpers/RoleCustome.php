<?php 


    /**
        $role_adminManager = '1','2';
        $role_secretary = '3';
        $role_designer = '4';
        $role_printer = '5';
        $role_finisher = '6';
    */
    
    function check_access_of_user($menu)
    {
       return return_access($menu);
    }

    function return_access($module){
        $module_list= App\Shop\Modules\Modules::get()->toArray();
        $role_permission=array();
        $menu_access = array();
        $menu = explode(",",session()->get('userData')->roleId);
        foreach($module_list as $data)
        {
            
            $roles=explode(',',$data['permited_roles']);
            
            if(in_array($menu[0],$roles)){
                $menu_access[$data['module_name']]=true;
            }
            else{
                $menu_access[$data['module_name']]=false;
            }
            
        }
        //print_r($role_permission);
        /*$menu_access =[
            '1' => [
                'customers' => true,
                'financies' => true,
                'products' => true,
                'jobs' => true,
                'suppliers' => true,
                'staffs' => true,
                'tasks' => true,
                'reports'=>true,
            ],
            '2' => [
                'customers' => true,
                'financies' => true,
                'products' => true,
                'jobs' => true,
                'suppliers' => true,
                'staffs' => true,
                'tasks' => true,
                'reports'=>true,
            ],
            '3' => [
                'customers' => true,
                'financies' => true,
                'products' => true,
                'jobs' => false,
                'suppliers' => true,
                'staffs' => false,
                'tasks' => true,
                'reports'=>true,
            ],
            '4' => [
                'customers' => false,
                'financies' => false,
                'products' => true,
                'jobs' => true,
                'suppliers' => true,
                'staffs' => false,
                'tasks' => true,
                'reports'=>true,
            ],
            '5' => [
                'customers' => false,
                'financies' => false,
                'products' => false,
                'jobs' => false,
                'suppliers' => true,
                'staffs' => false,
                'tasks' => true,
                'reports'=>true,
            ],
            '6' => [
                'customers' => false,
                'financies' => false,
                'products' => false,
                'jobs' => false,
                'suppliers' => true,
                'staffs' => false,
                'tasks' => false,
                'reports'=>true,
            ],
        ];*/
        
        $grant=false;
        foreach ($menu_access as $key =>$value) {
            
            //print_r($menu);
            if($key==$module && $menu_access[$key]== true){
                $grant=true;
            }
        }

        return $grant;
    }
    function check_permission($menu,$function)
    {
        // echo $menu;
        //echo"<pre>";
        $role_id =session()->get('userData')->roleId;
        $module_id= App\Shop\Modules\Modules::where('module_name',$menu)->get('id');
        //print_r($module_id);exit;
        $permission=App\Shop\RolePermissions\RolePermissions::where('module_id',$module_id[0]->id)
                                                            ->where('role_id',$role_id)
                                                            ->get();
        //print_r($permission);exit;
        $result=0;
        if($function == 'view')
        {
            if($permission[0]->view_permission == 1){
                $result = 1;
            }
            else{
                $result = 0;
            }
        }
        elseif($function == 'edit')
        {
            if($permission[0]->edit_permission == 1){
                $result = 1;
            }
            else{
                $result = 0;
            }
        }
        elseif($function == 'add')
        {
            if($permission[0]->add_permission == 1){
                $result = 1;
            }
            else{
                $result = 0;
            }
        }
        elseif($function == 'delete')
        {
            if($permission[0]->delete_permission == 1){
                $result = 1;
            }
            else{
                $result = 0;
            }
        }
        //echo $result;
         return $result;
        
    }