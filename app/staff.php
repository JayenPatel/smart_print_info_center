<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    //
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('staffid');
            $table->string('firstName'); //varchar(255)
            $table->string('lastName');
            $table->string('comment');
            $table->intval('roleId');
            $table->enum('status');
            $table->timestamps('startDate');
            $table->timestamps('endDate');

        });
  
    }
}
