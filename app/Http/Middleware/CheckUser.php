<?php
namespace App\Http\Middleware;
use Closure;
use Session;
class CheckUser
{
    public function handle($request, Closure $next)
    {
        if (!Session::get('userData')) {
            return redirect('/');
        }
        return $next($request);
    }
}
?>
