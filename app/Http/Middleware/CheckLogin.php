<?php
namespace App\Http\Middleware;
use Closure;
use Session;
class CheckLogin
{
    public function handle($request, Closure $next)
    {
        if (Session::get('userData')) {
            return redirect('/admin/dashboard');
        }
        return $next($request);
    }
}
?>
