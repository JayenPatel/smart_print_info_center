<?php

namespace App\Http\Controllers\Admin\FinanciesControllers;

use App\Exports\InvoiceExport;
use App\Exports\JobExport;
use App\Http\Controllers\Controller;
use App\Shop\Financies\Financies;
use App\Shop\Jobs\Jobs;
use DateTime;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use Mail;
use PDF;
ini_set('memory_limit', '-1');

class invoiceController extends Controller
{
    private $excel;
    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }
    public function index(Request $request)
    {
        try {
            $data = Financies::getInvoiceList();
            return view('admin.Financies.Invoice.invoice_list', ['list' => $data]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function create($id='')
    {
        try {
            $data = Jobs::getCategory();
           // $invoice['details'] = Jobs::getJobRecord($id)[0];
            $invoice=[];
            if($id !='') {
                $invoice['addedJob'] = Jobs::getJobRecord($id);
                $invoice['customer_id'] = Jobs::getJobRecord($id)[0]->customer_id;
                $jobs = Financies::getCustomerWiseJob($invoice['customer_id']);
                
                $invoice['jobs'] = [];
                foreach ($jobs as $key => $value) {
                    if ($value->job_id != $id) {
                        $invoice['jobs'][] = $value;
                    }
                }
            }
         // echo"<pre>";  print_r($invoice['jobs']);exit;
            return view('admin.Financies.Invoice.invoice_create',['invoice' => $data, 'data' => $invoice]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function edit($id)
    {
        try {

            $data = Jobs::getCategory();
            $invoice['details'] = Financies::getInvoiceDetails($id)[0];
            $invoice['addedJob'] = Financies::getAddedInvoiceDetails($id);
            $jobs = Financies::getCustomerWiseJob($invoice['details']->customer_id);
            // dd($jobs);
            $invoice['jobs']=[];
            foreach ($jobs as $key => $value) {
                if ($value->invoice_id == '') {
                    $invoice['jobs'][] = $value;
                }
            }
            return view('admin.Financies.Invoice.invoice_create', ['invoice' => $data, 'data' => $invoice]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function customerData(Request $request)
    {
        $data = $request->all();
        $data['jobs'] = Financies::getCustomerWiseJob($data['customer_id']);
        
        foreach ($data['jobs'] as $key => $job) {
            $data['jobs'][$key]->extra = Financies::getExtraById($job->job_id);
        }
        return response()->json(['status' => 'success', 'data' => $data, 'message' => 'Record Added Successfully']);

    }

    public function addInvoice(Request $request)
    {
        $data = $request->all();
        $data['invoice_date'] = new DateTime('today');
        $jobs = json_decode($data['jobs']);

        unset($data['jobs']);
        if($data['invoice_id'] !=''){
            Financies::updateData($data,$data['invoice_id']);
            $id = $data['invoice_id'];
        }else{
            unset($data['invoice_id']);
            $id = Financies::insertData($data);
            return response()->json(['status' => 'success', 'data' => $data, 'message' => 'Record Added Successfully']);
        }

        $jobsData = Financies::getCustomerWiseJob($data['customer_id']);
        foreach ($jobsData as $key => $value) {
            $job['invoice_id'] = null;
            Jobs::updateData($value->job_id, $job);
        }
        foreach ($jobs as $key => $value) {
            $job['invoice_id'] = $id;
            Jobs::updateData($value->job_id, $job);
            return response()->json(['status' => 'success', 'data' => $data, 'message' => 'Record Updated Successfully']);
        }

    }
    public function exportData()
    {
        return $this->excel->download(new InvoiceExport(''), 'Invoice'.'.xlsx');
    }


    public function delete(Request $request)
    {

        // dd($request->all());
        $data = \DB::delete('DELETE FROM invoice WHERE invoice_id='.$request->id);

         return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);

    }

    public function bill(Request $request)
    {
      
            return view('admin.Financies.Invoice.bill');
      
    }
  


}
