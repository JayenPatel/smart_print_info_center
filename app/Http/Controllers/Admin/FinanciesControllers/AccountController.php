<?php

namespace App\Http\Controllers\Admin\FinanciesControllers;

use App\Http\Controllers\Controller;
use App\Shop\Jobs\Jobs;
use Illuminate\Http\Request;
use log;

class AccountController extends Controller
{
    public function index(Request $request){
    	try{
    		return view('admin.Financies.Account.accounts_list');
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function create(){
    	try{
            $data = Jobs::getCategory();
            return view('admin.Financies.Account.account_create', ['data' => $data]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function edit(){
    	try{
    		$account = 1;
    		return view('admin.Financies.Account.account_create',['account' => $account]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
}
