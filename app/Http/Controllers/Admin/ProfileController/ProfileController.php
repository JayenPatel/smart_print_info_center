<?php

namespace App\Http\Controllers\Admin\ProfileController;

use App\Http\Controllers\Controller;
use App\Shop\Staffs\Staffs;
use Illuminate\Http\Request;
use log;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        try {
            return view('admin.profile.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function create()
    {
        try {
            $this->detail['role'] = Staffs::getRole();
            return view('admin.profile.create')->with(['data' => $this->detail]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function edit($id)
    {
        $this->detail['staff'] = Staffs::getOneRecord($id);
        $this->detail['staff']['roleId'] = explode(",", $this->detail['staff']['roleId']);
        $this->detail['role'] = Staffs::getRole();
        return view('admin.profile.edit')->with(['data' => $this->detail]);
    }

    public function delete(Request $request)
    {
        Staffs::deleteData($request->all()['id']);
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $data['status'] = 1;
        if( $data['password']  !=''){
            $data['password'] = md5($data['password']);
        }else{
           unset($data['password']);
        }
        if (request('profile_pic')) {
            $fileName = time() . '.' . request('profile_pic')->extension();
            request('profile_pic')->move(public_path('images'), $fileName);
            $data['profile_pic'] = $fileName;
        }
        if (!isset($data['staffId'])) {
            $data['createdDate'] = date("Y-m-d h:i:s");
            $data['createdBy'] = 1;
            $id = Staffs::insertData($data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);
        } else {
            $data['modifyDate'] = date("Y-m-d h:i:s");
            $data['modifyBy'] = 1;
            $id = Staffs::updateData($data);
            return response()->json(['status' => 'success', 'id' => $data['staffId'], 'message' => 'Record Updated Successfully']);
        }

    }

    public function getStaffData(Request $request)
    {
        $data= Staffs::getStaff();
        return response()->json(['status' => 'success', 'data' => $data]);

    }

}
