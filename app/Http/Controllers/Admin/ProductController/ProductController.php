<?php

namespace App\Http\Controllers\Admin\ProductController;

use Illuminate\Http\Request;
use log;
use App\Http\Controllers\Controller;
use App\Shop\Products\Products;
use App\Shop\Products\Customers;
use DateTime;

ini_set('memory_limit', '-1');

class ProductController extends Controller
{
    public function index(Request $request)
    {
        try {
            // dd('e');
            return view('admin.products.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function create(Request $request)
    {
        try {
            $customer = Products::getCustomer();
            $category= Products::getCategory();
            return view('admin.products.create', ['customer' => $customer,'category'=> $category]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function edit($id)
    {
        try {
            $product = Products::getProductData($id)[0];
            $customer = Products::getCustomer();
            $category = Products::getCategory();
                //dd($customer);exit;
                return view('admin.products.create', ['product' => $product, 'category' => $category,'customer' => $customer]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        //print_r($data);exit;
           
        if(isset($data['id']) && $data['id']!= "") {
            unset($data['_token']);
            $data['status'] = 1;
            $id = Products::updateData($data['id'],$data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Updated Successfully']);

        }else{
            $data['status'] = 1;
            unset($data['_token']);
            $id = Products::insertData($data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);

        }
        //print_r($data);exit;
    }

    public function getProductData(Request $request)
    {
        $data = Products::getProductData();
        //print_r($data);
        return response()->json(['status' => 'success', 'data' => $data]);

    }

    public function delete(Request $request)
    {

        $data = Products::deleteData($request->all()['id']);
     
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);

    }
}
