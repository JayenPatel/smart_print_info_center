<?php

namespace App\Http\Controllers\Admin\SuppliersController;

use App\Http\Controllers\Controller;
use App\Shop\Customers\Customers;
use Illuminate\Http\Request;
use log;

class SupplierController extends Controller
{
    public function index(Request $request)
    {
        try {
            // dd('e');
            return view('admin.suppliers.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function create(Request $request)
    {
        try {
            $category = Customers::getSupplierCategory();

            return view('admin.suppliers.create', ['category' => $category]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function edit($id)
    {
        try {
            $customer = Customers::getSupplierData($id)[0];
            $category = Customers::getSupplierCategory();
            //   $alternate_address = ;
            if ($customer->alternate_address != 0) {
                return view('admin.suppliers.create', ['customer' => $customer, 'category' => $category]);

            } else {
                //dd($customer);exit;
                return view('admin.suppliers.create', ['customer' => $customer, 'category' => $category]);
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function Address()
    {
        try {
            return view('admin.suppliers.address');
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        if($data['supplier_id'] != '') {
            $data['status'] = 1;
            /* $data['modifyDate'] = new DateTime('today');
             $data['modifyBy'] = 1;*/
            $id = Customers::updateSupplierData($data['supplier_id'],$data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Updated Successfully']);

        }else{
            $data['status'] = 1;
            /*$data['createdDate'] = new DateTime('today');
            $data['createdBy'] = 1;*/
            $id = Customers::insertSupplierData($data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);

        }
        //print_r($data);exit;
    }

    public function getSupplierData(Request $request)
    {
        $data = Customers::getSupplierData();
        return response()->json(['status' => 'success', 'data' => $data]);

    }

    public function delete(Request $request)
    {

        $data = Customers::deleteSupplierData($request->all()['id']);
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);

    }
}
