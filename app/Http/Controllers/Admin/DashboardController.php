<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Shop\Dashboard\Dashboard;
use Illuminate\Http\Request;
use Log;
ini_set('memory_limit', '-1');

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        try {
//            dd(session()->get('userData')->firstName);exit();
            return view('admin.dashboard');
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function getDashboardData(Request $request)
    {
        $data = $request->all();
        $months = array(
            "Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"
        );
        $formattedMonthArray = array(
            "01" => "Jan", "02" => "Feb", "03" => "Mar", "04" => "Apr",
            "05" => "May", "06" => "Jun", "07" => "Jul", "08" => "Aug",
            "09" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec",
        );
        $result = [];
        foreach ($formattedMonthArray as $key => $month) {
            $print['name']= 'Print Job';
            $print['data'][]=   Dashboard::getDashboard($key,$data['year'],'P');

            $calender['name']= 'Calender Job';
            $calender['data'][]=   Dashboard::getDashboard($key,$data['year'],'C');

            $web['name']= 'Web Job';
            $web['data'][]=   Dashboard::getDashboard($key,$data['year'],'W');

            $other['name']= 'Other Job';
            $other['data'][]=   Dashboard::getDashboard($key,$data['year'],'X');
            if($key == 12){
                $result[] = $print;
                $result[] = $calender;
                $result[] = $web;
                $result[] = $other;
            }
        }
        $final = [];
        $final['month'] = $months;
        $final['data'] = $result;
        return response()->json(['status' => 'success', 'final' => $final, 'message' => '']);

    }
}
