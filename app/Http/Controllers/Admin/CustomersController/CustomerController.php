<?php

namespace App\Http\Controllers\Admin\CustomersController;

use App\Http\Controllers\Controller;
use App\Shop\Customers\Customers;
use DateTime;
use Illuminate\Http\Request;
use log;

ini_set('memory_limit', '-1');

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        try {
            // dd('e');
            return view('admin.customers.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function create(Request $request)
    {
        try {
            $category = Customers::getCategory();

            return view('admin.customers.create', ['category' => $category]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function edit($id)
    {
        try {
            $customer = Customers::getCustomerData($id)[0];
            $category = Customers::getCategory();
            //   $alternate_address = ;
            if ($customer->alternate_address != 0) {
                $alternate_address = Customers::getAlternateAddress($customer->alternate_address)[0];
            //    dd($alternate_address);
                return view('admin.customers.create', ['customer' => $customer, 'category' => $category, 'alternate_address' => $alternate_address]);

            } else {
                //dd($customer);exit;
                return view('admin.customers.create', ['customer' => $customer, 'category' => $category]);
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function Address()
    {
        try {
            return view('admin.customers.address');
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        
        //dd($data);exit;
        $alternate_address= array();
        $data['alternate_address'] = json_decode($data['alternate_address']);
          //echo "<pre>"; print_r($data['alternate_address']);exit;
        foreach ($data['alternate_address'] as $key=>$item) {
            $alternate_address[$item->name] = $item->value;
            //array_push($alternate_address, $new);
        }

        if($data['alternateAddress'] =='' && $alternate_address['address1'] !='') {
            $id = Customers::insertAddressData($alternate_address);
            $data['alternate_address'] = $id;
        }else if($data['alternateAddress'] !=''&& $alternate_address['address1'] !=''){
            $id = Customers::updateAddressData($data['alternateAddress'],$alternate_address);
            $data['alternate_address'] = $data['alternateAddress'];
        }else{
            unset($data['alternate_address']);
        }
        unset($data['_token'],$data['alternateAddress']);
        if($data['customer_id'] != '') {
            $data['status'] = 1;
           /* $data['modifyDate'] = new DateTime('today');
            $data['modifyBy'] = 1;*/
            $id = Customers::updateData($data['customer_id'],$data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Updated Successfully']);

        }else{
            $data['status'] = 1;
            /*$data['createdDate'] = new DateTime('today');
            $data['createdBy'] = 1;*/
            $id = Customers::insertData($data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);

        }
        //print_r($data);exit;
    }

    public function getCustomerData(Request $request)
    {
        $data = Customers::getCustomerData();
        return response()->json(['status' => 'success', 'data' => $data]);

    }

    public function delete(Request $request)
    {

        $data = Customers::deleteData($request->all()['id']);
     
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);

    }
}
