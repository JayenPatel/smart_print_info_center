<?php

namespace App\Http\Controllers\Admin\TasksController;

use App\Http\Controllers\Controller;
use App\Shop\Staffs\Staffs;
use App\Shop\Tasks\Tasks;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tasks.task');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staff = Staffs::get();
        return view('admin.tasks.create',['staff' => $staff]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        if (!isset($data['task_id'])) {
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['createdBy'] = 1;
            $id = Tasks::insertData($data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);
        } else {
            $data['updated_at'] = date("Y-m-d h:i:s");
            $data['modifyBy'] = 1;
            $id = Tasks::updateData($data);
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Updated Successfully']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['task'] = Tasks::getOneRecord($id);
        $data['staff'] = Staffs::get();
        //dd($data['task']);
        return view('admin.tasks.edit')->with(['data' => $data]);
    }

    public function delete(Request $request)
    {
        Tasks::deleteData($request->all()['id']);
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }

    public function getTaskData(Request $request)
    {
        $data= Tasks::getTask();
        return response()->json(['status' => 'success', 'data' => $data]);

    }
}
