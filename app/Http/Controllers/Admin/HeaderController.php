<?php

namespace App\Http\Controllers\Admin;

use App\Exports\JobExport;
use App\Exports\OtherExport;
use App\Http\Controllers\Controller;
use App\Shop\Jobs\Jobs;
use Illuminate\Http\Request;
use Log;
use Maatwebsite\Excel\Excel;
ini_set('memory_limit', '-1');

class HeaderController extends Controller
{
    private $excel;
    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }
    public function printJob(Request $request)
    {
        try {
            $job = Jobs::getPrintWithCustomerJob();
            // echo "<pre>";  print_r($job);exit;
            return view('admin.header.print_job', ['list' => $job]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function calendar(Request $request)
    {
        try {
            $job = Jobs::getCalenderWithCustomerJob();
            return view('admin.header.calendar', ['list' => $job]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function otherJob(Request $request)
    {
        try {
            $job = Jobs::getOtherWithCustomerJob();
            return view('admin.header.other_job', ['list' => $job]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function webJob(Request $request)
    {
        try {
            $job = Jobs::getWebWithCustomerJob();
            return view('admin.header.web_job', ['list' => $job]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function quotes(Request $request)
    {
        try {
            $job = Jobs::getQuotesWithCustomerJob();
            return view('admin.header.quotes', ['list' => $job]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $d =[];
        if($data['type'] == 'prog_delivered' && $data['value'] == 1){
            $d['status'] = 'D';
            $d['prog_type'] = 1;
            $d['prog_proof'] = 1;
            $d['prog_confirmed'] = 1;
            $d['prog_film'] = 1;
            $d['prog_printed'] = 1;
            $d['prog_finished'] = 1;
            $d['prog_delivered'] = 1;
        }else if($data['type'] == 'prog_finished' && $data['value'] == 1){
                $d['prog_type'] = 1;
                $d['prog_proof'] = 1;
                $d['prog_confirmed'] = 1;
                $d['prog_film'] = 1;
                $d['prog_printed'] = 1;
                $d['prog_finished'] = 1;
            }
        else if($data['type'] == 'prog_printed' && $data['value'] == 1){
            $d['prog_type'] = 1;
            $d['prog_proof'] = 1;
            $d['prog_confirmed'] = 1;
            $d['prog_film'] = 1;
            $d['prog_printed'] = 1;
        } else if($data['type'] == 'prog_film' && $data['value'] == 1){
            $d['prog_type'] = 1;
            $d['prog_proof'] = 1;
            $d['prog_confirmed'] = 1;
            $d['prog_film'] = 1;
        } else if($data['type'] == 'prog_confirmed' && $data['value'] == 1){
            $d['prog_type'] = 1;
            $d['prog_proof'] = 1;
            $d['prog_confirmed'] = 1;
        }
        else if($data['type'] == 'prog_proof' && $data['value'] == 1){
            $d['prog_type'] = 1;
            $d['prog_proof'] = 1;
        }
        Jobs::updateHeaders($data,$d);
        return response()->json(['status' => 'success', 'message' => 'Record Updated Successfully']);
    }

    public function exportJobs($type)
    {
        return $this->excel->download(new JobExport($type), $type.'.xlsx');
    }
    public function exportJobsPDF($type)
    {
        return $this->excel->download(new JobExport($type), $type.'.pdf',"Dompdf");
    }
    public function exportothers($type)
    {
        return $this->excel->download(new OtherExport($type), $type.'.xlsx');
    }
    public function exportothersPDF($type)
    {
        return $this->excel->download(new OtherExport($type), $type.'.pdf',"Dompdf");
    }
}
