<?php

namespace App\Http\Controllers\Admin\ReportsController;

use App\Http\Controllers\Controller;
use App\Shop\Customers\Customers;
use App\Shop\Products\Products;
use App\Shop\Jobs\Jobs;
use App\Shop\Staffs\Staffs;
use App\Shop\Suppliers\Supplier;
use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use log;
class ReportController extends Controller
{
    public function userreport(Request $request)
    {
        try {
            // dd('e');
            return view('admin.Reports.User.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function getuserData(Request $request)
    {
        if(isset($request->todate)||isset($request->fromdate)||isset($request->county)||isset($request->postcode))
        {
            if(isset($request->fromdate)){
                $dateF=$request->fromdate;
            }
            else{
                $dateF=Carbon::now()->subMonth();
            }
            if(isset($request->county)){
                $county=$request->county;
            }
            else{
                $county='';
            }
            if(isset($request->postcode)){
                $postcode=$request->postcode;
            }
            else{
                $postcode="";
            }
            if(isset($request->todate)){
                $dateT=$request->todate;
            }
            else{
                $dateT=Carbon::now();
            }
            $data = Customers::getCustomerReport($dateF, $dateT,$county,$postcode);
        }
        else
        {
            $data = Customers::getCustomerReport();
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }
    public function productreport(Request $request)
    {
        try {
            // dd('e');
            return view('admin.Reports.Product.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function getproductData(Request $request)
    {
        //print_r($request->all());
        if(isset($request->todate)||isset($request->fromdate) || $request->customer_id!="" || $request->category_id!="")
        {
            //print_r($request->all());
            if(isset($request->fromdate)){
                $dateF=$request->fromdate;
            }
            else{
                $dateF=Carbon::now()->subMonth();
            }
            if(isset($request->todate)){
                $dateT=$request->todate;
            }
            else{
                $dateT=Carbon::now();
            }
            if($request->category_id!=''){
                $category_id=$request->category_id;
            }
            else{
                $category_id="";
            }
            if($request->customer_id!=''){
                $customer_id=$request->customer_id;
            }
            else{
                $customer_id="";
            }
            $data = Products::getProductReport($dateF, $dateT,$customer_id,$category_id);
        }
        else
        {
            $data = Products::getProductReport();
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function printreport(Request $request)
    {
        try {
            // dd('e');
            return view('admin.Reports.PrintJob.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function getprintData(Request $request)
    {
        if(isset($request->todate)||isset($request->fromdate)|| $request->category!="" || $request->status!="")
        {
            if(isset($request->fromdate)){
                $dateF=$request->fromdate;
            }
            else{
                $dateF=Carbon::now()->subMonth();
            }
           
            if(isset($request->todate)){
                $dateT=$request->todate;
            }
            else{
                $dateT=Carbon::now();
            }
            if(isset($request->category)){
                $category=$request->category;
            }
            else{
                $category='';
            }
            if(isset($request->status)){
                $status=$request->status;
            }
            else{
                $status='';
            }
            $data = Jobs::getPrintReport($dateF, $dateT,$category,$status);
        }
        else
        {
            $data = Jobs::getPrintReport();
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function webreport(Request $request)
    {
        try {
            // dd('e');
            return view('admin.Reports.WebJob.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function getwebData(Request $request)
    {
        if(isset($request->todate)||isset($request->fromdate)|| $request->category!="" || $request->status!="")
        {
            if(isset($request->fromdate)){
                $dateF=$request->fromdate;
            }
            else{
                $dateF=Carbon::now()->subMonth();
            }
           
            if(isset($request->todate)){
                $dateT=$request->todate;
            }
            else{
                $dateT=Carbon::now();
            }
            if(isset($request->category)){
                $category=$request->category;
            }
            else{
                $category='';
            }
            if(isset($request->status)){
                $status=$request->status;
            }
            else{
                $status='';
            }
            $data = Jobs::getWebReport($dateF, $dateT,$category,$status);
        }
        else
        {
            $data = Jobs::getWebReport();
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }
    public function calenderreport(Request $request)
    {
        try {
            // dd('e');
            return view('admin.Reports.CalenderJob.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function getcalenderData(Request $request)
    {
        if(isset($request->todate)||isset($request->fromdate)|| $request->category!="" || $request->status!="")
        {
            if(isset($request->fromdate)){
                $dateF=$request->fromdate;
            }
            else{
                $dateF=Carbon::now()->subMonth();
            }
           
            if(isset($request->todate)){
                $dateT=$request->todate;
            }
            else{
                $dateT=Carbon::now();
            }
            if(isset($request->category)){
                $category=$request->category;
            }
            else{
                $category='';
            }
            if(isset($request->status)){
                $status=$request->status;
            }
            else{
                $status='';
            }
            $data = Jobs::getCalenderReport($dateF, $dateT,$category,$status);
        }
        else
        {
            $data = Jobs::getCalenderReport();
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }
    public function otherreport(Request $request)
    {
        try {
            // dd('e');
            return view('admin.Reports.OtherJob.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function getotherData(Request $request)
    {
        if(isset($request->todate)||isset($request->fromdate)|| $request->category!="" || $request->status!="")
        {
            if(isset($request->fromdate)){
                $dateF=$request->fromdate;
            }
            else{
                $dateF=Carbon::now()->subMonth();
            }
           
            if(isset($request->todate)){
                $dateT=$request->todate;
            }
            else{
                $dateT=Carbon::now();
            }
            if(isset($request->category)){
                $category=$request->category;
            }
            else{
                $category='';
            }
            if(isset($request->status)){
                $status=$request->status;
            }
            else{
                $status='';
            }
            $data = Jobs::getOtherReport($dateF, $dateT,$category,$status);
        }
        else
        {
            $data = Jobs::getOtherReport();
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function supplierreport(Request $request)
    {
        try {
            // dd('e');
            return view('admin.Reports.Supplier.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function getsupplierData(Request $request)
    {
        if(isset($request->todate)||isset($request->fromdate)||isset($request->county)||isset($request->postcode))
        {
            if(isset($request->fromdate)){
                $dateF=$request->fromdate;
            }
            else{
                $dateF=Carbon::now()->subMonth();
            }
            if(isset($request->county)){
                $county=$request->county;
            }
            else{
                $county='';
            }
            if(isset($request->postcode)){
                $postcode=$request->postcode;
            }
            else{
                $postcode="";
            }
            if(isset($request->todate)){
                $dateT=$request->todate;
            }
            else{
                $dateT=Carbon::now();
            }
            $data = Supplier::getSupplierReport($dateF, $dateT,$county,$postcode);
        }
        else
        {
            $data = Supplier::getSupplierReport();
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function staffreport(Request $request)
    {
        try {
            // dd('e');
            return view('admin.Reports.Staff.list');
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function getstaffData(Request $request)
    {
        if(isset($request->todate)||isset($request->fromdate))
        {
            if(isset($request->fromdate)){
                $dateF=$request->fromdate;
            }
            else{
                $dateF=Carbon::now()->subMonth();
            }
            if(isset($request->todate)){
                $dateT=$request->todate;
            }
            else{
                $dateT=Carbon::now();
            }
            $data = Staffs::getStaffReport($dateF, $dateT);
        }
        else
        {
            $data = Staffs::getStaffReport();
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }
}
