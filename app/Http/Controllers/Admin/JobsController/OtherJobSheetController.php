<?php

namespace App\Http\Controllers\Admin\JobsController;

use App\Http\Controllers\Controller;
use App\Shop\Jobs\Jobs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use log;

class OtherJobSheetController extends Controller
{
    public function index(Request $request){
    	try{
            $job = Jobs::getOtherJob();
    		return view('admin.jobs.other_job_sheet_list', ['list' => $job]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function create(){
    	try{
            $job = Jobs::getLastRecord();
            $data = Jobs::getOtherCategory();
            if (isset($job)) {
                $data['job_id'] = (filter_var($job->job_id, FILTER_SANITIZE_NUMBER_INT) + 1) . 'X';
            } else {
                $data['job_id'] = (00001) . 'X';
            }
            $data['reg_date'] = Carbon::now()->toDateTimeString();
    		return view('admin.jobs.other_job_sheet_create', ['job' => $data]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function submit(Request $request)
    {
        $data = $request->all();
        $data['order_taken_by'] = 1;
        $data['extra'] = json_decode($data['extra']);
        $other = [];
        $other['comment'] = $data['comment'];
        $extra =[];
        foreach ($data['extra'] as $key=> $item) {
            $extra[] = (Array) $item;
        }
        //echo"<pre>";  print_r($extra);exit;

        unset($data['comment'], $data['extra'], $data['description_extra'], $data['vat_extra'], $data['net_price_extra']);
        $data['type'] = 'X';
        if ($data['status'] == 'Q') {
            $data['job_id'] = (filter_var($data['job_id'], FILTER_SANITIZE_NUMBER_INT) ) . 'Q';
        }

        $vatextra=[
    		'vat' => $request->vat_extra,
    		'net_price' => $request->net_price_extra,
    		'description' => $request->description_extra
    	   ];

        $job = Jobs::getotherJobById($data['job_id']);
        if (isset($job[0])) {
            $id = Jobs::updateData($data['job_id'], $data);
            $other['job_id'] = $data['job_id'];
            Jobs::updateotherData($data['job_id'], $other);
            Jobs::deleteExtraById($data['job_id']);
            $vatextra['job_id'] = $data['job_id'];
            Jobs::extraVatrecord($vatextra);

            foreach ($extra as $key=> $item) {
                $item['job_id'] = $data['job_id'];
                Jobs::insertExtraData($item);
            }
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Updated Successfully']);

        } else {
            $id = Jobs::insertData($data);
            $other['job_id'] = $data['job_id'];
            Jobs::insertotherData($other);
            Jobs::deleteExtraById($data['job_id']);
            $vatextra['job_id'] = $id;
            Jobs::extraVatrecord($vatextra);
            
            foreach ($extra as $key=> $item) {
                $item['job_id'] = $data['job_id'];
                Jobs::insertExtraData($item);
            }
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);
        }

    }
    public function edit($id){
    	try{
            //$data = Jobs::getWorkCategory();
            $data = Jobs::getOtherCategory();
            $other = Jobs::getotherJobById($id);
            $other[0]->extra = Jobs::getExtraById($id);
            $data['job_id'] = $other[0]->job_id;
            $data['reg_date'] = $other[0]->reg_date;
            //echo"<pre>";  print_r($data);exit;

            return view('admin.jobs.other_job_sheet_create', ['job' => $data, 'other' => $other[0]]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function delete(Request $request)
    {

        // dd($id);
        $data = Jobs::where('job_id',$request->id)->delete();
        // dd($data);
        // $data->delete();
        
         return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
        // return back();

    }
}
