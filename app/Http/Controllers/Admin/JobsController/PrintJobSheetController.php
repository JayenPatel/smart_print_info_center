<?php

namespace App\Http\Controllers\Admin\JobsController;

use App\Http\Controllers\Controller;
use App\Shop\Jobs\Jobs;
use App\Shop\Jobtitles\Jobtitles;
use App\Shop\Financies\Financies;
use Carbon\Carbon;
use Illuminate\Http\Request;
use log;
use Illuminate\Support\Facades\Auth;

class PrintJobSheetController extends Controller
{
    public function index(Request $request)
    {
        
        try {
            $job = Jobs::getPrintJob();
        //   dd($job);
            return view('admin.jobs.print_job_sheet_list', ['list' => $job]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function create()
    {
        try {
            $job = Jobs::getLastRecord();
            $data = Jobs::getCategory();
            if (isset($job)) {
                $data['job_id'] = (filter_var($job->job_id, FILTER_SANITIZE_NUMBER_INT) + 1) . 'P';
            } else {
                $data['job_id'] = (00001) . 'P';
            }
            $data['reg_date'] = Carbon::now()->toDateTimeString();
            $data['vat_data'] = Jobs::getVatRecord();
             //echo "<pre>";print_r($job->job_id);exit;
            return view('admin.jobs.print_job_sheet_create', ['job' => $data]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function submit(Request $request)
    {
        $data = $request->all();
        $data['order_taken_by'] = 1;

        $data['printing'] = json_decode($data['printing']);
        $data['extra'] = json_decode($data['extra']);
        $printing = [];
        foreach ($data['printing'] as $item) {
            $printing[$item->name] = $item->value;
        }
        $extra =[];
        foreach ($data['extra'] as $key=> $item) {
            $extra[] = (Array) $item;
        }
        
        if($data['vat']!=0){
            $vat['vat_setting'] = $data['vat_setting'];
            Jobs::updatVateData('1',$vat);
        }
        unset($data['printing'], $data['extra'], $data['description_extra'], $data['vat_extra'], $data['net_price_extra'],$data['vat_setting']);
        $data['type'] = 'P';
        if ($data['status'] == 'Q') {
            $data['job_id'] = (filter_var($data['job_id'], FILTER_SANITIZE_NUMBER_INT) ) . 'Q';
        }
        $vatextra=[
    		'vat' => $request->vat_extra,
    		'net_price' => $request->net_price_extra,
    		'description' => $request->description_extra
    	   ];
        $job = Jobs::getPrintJobById($data['job_id']);
        if (isset($job[0])) {
        
            $id = Jobs::updateData($data['job_id'], $data);
            $printing['job_id'] = $data['job_id'];
            Jobs::updatePrintData($data['job_id'], $printing);
            Jobs::deleteExtraById($data['job_id']);
           
            // Jobs::updateextraVatrecord($data['job_id'],$vatextra);
            $vatextra['job_id'] = $data['job_id'];
            Jobs::extraVatrecord($vatextra);

            foreach ($extra as $key=> $item) {
                    $item['job_id'] = $data['job_id'];
                Jobs::insertExtraData($item);
            }
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Updated Successfully']);

        } else {

            $id = Jobs::insertData($data);
            $printing['job_id'] = $data['job_id'];
            Jobs::insertPrintData($printing);
            Jobs::deleteExtraById($data['job_id']);
            $vatextra['job_id'] = $id;
            
            Jobs::extraVatrecord($vatextra);

            foreach ($extra as $key=> $item) {
                $item['job_id'] = $data['job_id'];
                Jobs::insertExtraData($item);
            }
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);
        }

    }

    public function edit($id,Request $request)
    {
        try {
            if(isset($request->reprint))
            {
                $job = Jobs::getLastRecord();
                $data = Jobs::getCategory();
                $print = Jobs::getPrintJobById($id);
                //echo"<pre>";  echo $request->reprint;exit;

                $print[0]->extra = Jobs::getExtraById($id);
                if (isset($job)) {
                    $data['job_id'] = (filter_var($job->job_id, FILTER_SANITIZE_NUMBER_INT) + 1) . 'P';
                } else {
                    $data['job_id'] = (00001) . 'P';
                }
                $data['reg_date'] = Carbon::now()->toDateTimeString();
                $data['vat_data'] = Jobs::getVatRecord();
                return view('admin.jobs.print_job_sheet_create', ['job' => $data, 'print' => $print[0]]);
            }
            else{
                $data = Jobs::getCategory();
                $print = Jobs::getPrintJobById($id);
                //echo"<pre>";  echo $request->reprint;exit;

                $print[0]->extra = Jobs::getExtraById($id);
                $data['job_id'] = $print[0]->job_id;
                $data['reg_date'] = $print[0]->reg_date;
                $data['vat_data'] = Jobs::getVatRecord();
                return view('admin.jobs.print_job_sheet_create', ['job' => $data, 'print' => $print[0]]);
            }
            
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function reprintedit(Request $request,$id)
    {

        try {
            $data = Jobs::getCategory();
            $print = Jobs::getPrintJobById($id);
           // echo"<pre>";  print_r($print);exit;

            $print[0]->extra = Jobs::getExtraById($id);
            $data['job_id'] = $print[0]->job_id;
            $data['reg_date'] = $print[0]->reg_date;
            $data['vat_data'] = Jobs::getVatRecord();
            return view('admin.jobs.print_job_sheet_create', ['job' => $data, 'print' => $print[0]]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function customerprintData(Request $request)
    {
        $data = $request->all();
        $data['jobs'] = Jobs::getCustomerWisePrintJob($data['customer_id']);
        
        foreach ($data['jobs'] as $key => $job) {
            $data['jobs'][$key]->extra = Financies::getExtraById($job->job_id);
        }
        return response()->json(['status' => 'success', 'data' => $data, 'message' => 'Record Added Successfully']);

    }

    public function delete(Request $request)
    {

        // dd($id);
        $data = Jobs::where('job_id',$request->id)->delete();
        // dd($data);
        // $data->delete();
        
         return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
        // return back();

    }
    public function addjobtitle(Request $request)
    {
        $data=$request->all();
        Jobtitles::insertjobtitleData($data);
        return response()->json(['status' => 'success', 'message' => 'Record Added Successfully']);
    }
    public function removejobtitle(Request $request)
    {
        $data=$request->all();

        $data=Jobtitles::getjobtitleData($data['type'],$data['job_title']);

        //print_r($data);
        if(!empty($data))
        {
            Jobtitles::find($data[0]->id)->delete();
        //Category::
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['status' => 'error', 'message' => 'NO DATA FOUND.']);
        }
        
    }
    
  
}
