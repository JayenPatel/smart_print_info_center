<?php

namespace App\Http\Controllers\Admin\JobsController;

use App\Http\Controllers\Controller;
use App\Shop\Jobs\Jobs;
use App\Shop\Category\Category;
use App\Shop\Colours\Colours;
use Carbon\Carbon;
use Illuminate\Http\Request;
use log;

class CalendarSheetController extends Controller
{
    public function index(Request $request)
    {
        try {
            $job = Jobs::getCalenderJob();
            return view('admin.jobs.calender_sheet_list', ['list' => $job]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function create()
    {
        try {

            $job = Jobs::getLastRecord();
            $data = Jobs::getCategoryCalender();
            if (isset($job)) {
                $data['job_id'] = (filter_var($job->job_id, FILTER_SANITIZE_NUMBER_INT) + 1) . 'C';
            } else {
                $data['job_id'] = (00001) . 'C';
            }
            $data['reg_date'] = Carbon::now()->toDateTimeString();
            return view('admin.jobs.calender_sheet_create', ['job' => $data]);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function submit(Request $request)
    {
        $data = $request->all();
        $data['order_taken_by'] = 1;

        //$data['calender'] = [];
        $data['extra'] = json_decode($data['extra']);
        $calender = [];
        if (isset($data['envelope_qty'])) {
            $calender['envelope_qty'] = $data['envelope_qty'];
        } else {
            $calender['envelope_qty'] = 0;
        }
        if (isset($data['logo'])) {
            $calender['logo'] = $data['logo'];
        } else {
            $calender['logo'] = 0;
        }
        if (isset($data['menu_on_back'])) {
            $calender['menu_on_back'] = $data['menu_on_back'];
        } else {
            $calender['menu_on_back'] = 0;
        }
        if (isset($data['comments'])) {
            $calender['comments'] = $data['comments'];
        } else {
            $calender['comments'] = '';
        }
        $extra = [];
        foreach ($data['extra'] as $key => $item) {
            $extra[] = (array)$item;
        }
        //echo"<pre>";  print_r($extra);exit;

        unset($data['extra'], $data['comments'], $data['logo'], $data['envelope_qty'], $data['menu_on_back'], $data['description_extra'], $data['vat_extra'], $data['net_price_extra']);
        $data['type'] = 'C';
        if ($data['status'] == 'Q') {
            $data['job_id'] = (filter_var($data['job_id'], FILTER_SANITIZE_NUMBER_INT)) . 'Q';
        }

        $vatextra=[
    		'vat' => $request->vat_extra,
    		'net_price' => $request->net_price_extra,
    		'description' => $request->description_extra
    	   ];

        $job = Jobs::getCalenderJobById($data['job_id']);
        // print_r($job);exit;

        if (isset($job[0])) {
            $cal = Jobs::getOnlyCalenderJob($data['job_id']);
           // print_r($cal);exit;
            $id=  Jobs::updateData($data['job_id'], $data);
            if (count($cal) !==0) {
                $calender['job_id'] = $data['job_id'];
                Jobs::updateCalenderData($data['job_id'], $calender);
            } else {
                $calender['job_id'] = $data['job_id'];
                Jobs::insertCalenderData($calender);
            }
            $calender['job_id'] = $data['job_id'];
            Jobs::deleteExtraById($data['job_id']);
            $vatextra['job_id'] = $data['job_id'];
            Jobs::extraVatrecord($vatextra);
            foreach ($extra as $key => $item) {
                $item['job_id'] = $data['job_id'];
                Jobs::insertExtraData($item);
            }
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Updated Successfully']);

        } else {
            $id = Jobs::insertData($data);
            $calender['job_id'] = $data['job_id'];
            Jobs::insertCalenderData($calender);
            Jobs::deleteExtraById($data['job_id']);
            $vatextra['job_id'] = $id;
            
            Jobs::extraVatrecord($vatextra);
            foreach ($extra as $key => $item) {
                $item['job_id'] = $data['job_id'];
                Jobs::insertExtraData($item);
            }
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);
        }

    }

    public function edit($id)
    {
        try {
            
            $data = Jobs::getCategory();
            $calender = Jobs::getCalenderJobById($id);
            $calender[0]->extra = Jobs::getExtraById($id);
            $data['job_id'] = $calender[0]->job_id;
            $data['reg_date'] = $calender[0]->reg_date;
            //echo"<pre>";  print_r($data);exit;
            return view('admin.jobs.calender_sheet_create', ['job' => $data, 'calender' => $calender[0]]);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function delete(Request $request)
    {

        // dd($id);
        $data = Jobs::where('job_id',$request->id)->delete();
        // dd($data);
        // $data->delete();
        
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
        // return back();

    }
    public function addcategory(Request $request)
    {
        $data=$request->all();
        Category::insertcategoryData($data);
        return response()->json(['status' => 'success', 'message' => 'Record Added Successfully']);
    }
    public function removecategory(Request $request)
    {
        $data=$request->all();

        $data=Category::getcategoryData($data['type'],$data['name']);

        //print_r($data);
        if(!empty($data))
        {
            Category::find($data[0]->id)->delete();
        //Category::
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['status' => 'error', 'message' => 'Something wen wrong.']);
        }
        
    }
    public function addcolour(Request $request)
    {
        $data=$request->all();
        Colours::insertcolourData($data);
        return response()->json(['status' => 'success', 'message' => 'Record Added Successfully']);
    }
    public function removecolour(Request $request)
    {
        $data=$request->all();

        $data=Colours::getcolourData($data['type'],$data['colour']);

        //print_r($data);
        if(!empty($data))
        {
            Colours::find($data[0]->id)->delete();
        //Category::
        return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['status' => 'error', 'message' => 'NO DATA FOUND.']);
        }
        
    }
    
}
