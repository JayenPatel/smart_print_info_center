<?php

namespace App\Http\Controllers\Admin\JobsController;

use App\Http\Controllers\Controller;
use App\Shop\Jobs\Jobs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use log;

class WebJobSheetController extends Controller
{
    public function index(Request $request){
    	try{
            $job = Jobs::getWebJob();
            return view('admin.jobs.web_job_sheet_list', ['list' => $job]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function create(){
        try {
            $job = Jobs::getLastRecord();
            $data = Jobs::getWorkCategory();
            if (isset($job)) {
                $data['job_id'] = (filter_var($job->job_id, FILTER_SANITIZE_NUMBER_INT) + 1) . 'W';
            } else {
                $data['job_id'] = (00001) . 'W';
            }
            $data['reg_date'] = Carbon::now()->toDateTimeString();
    		return view('admin.jobs.web_job_sheet_create', ['job' => $data]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }
    public function submit(Request $request)
    {
        $data = $request->all();
        $data['order_taken_by'] = 1;

        $data['web'] = json_decode($data['web']);
        $data['extra'] = json_decode($data['extra']);
        $web = [];
        foreach ($data['web'] as $item) {
            $web[$item->name] = $item->value;
        }
        $extra =[];
        foreach ($data['extra'] as $key=> $item) {
            $extra[] = (Array) $item;
        }
        //echo"<pre>";  print_r($extra);exit;

        unset($data['web'], $data['extra'], $data['description_extra'], $data['vat_extra'], $data['net_price_extra']);
        $data['type'] = 'W';
        if ($data['status'] == 'Q') {
            $data['job_id'] = (filter_var($data['job_id'], FILTER_SANITIZE_NUMBER_INT) ) . 'Q';
        }

        $vatextra=[
    		'vat' => $request->vat_extra,
    		'net_price' => $request->net_price_extra,
    		'description' => $request->description_extra
    	   ];

        $job = Jobs::getWebJobById($data['job_id']);
        if (isset($job[0])) {
            $id = Jobs::updateData($data['job_id'], $data);
            $web['job_id'] = $data['job_id'];
            Jobs::updateWebData($data['job_id'], $web);
            Jobs::deleteExtraById($data['job_id']);
            $vatextra['job_id'] = $data['job_id'];
            Jobs::extraVatrecord($vatextra);

            foreach ($extra as $key=> $item) {
                $item['job_id'] = $data['job_id'];
                Jobs::insertExtraData($item);
            }
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Updated Successfully']);

        } else {
            $id = Jobs::insertData($data);
            $web['job_id'] = $data['job_id'];
            Jobs::insertWebData($web);
            Jobs::deleteExtraById($data['job_id']);
            $vatextra['job_id'] = $id;
            Jobs::extraVatrecord($vatextra);


            foreach ($extra as $key=> $item) {
                $item['job_id'] = $data['job_id'];
                Jobs::insertExtraData($item);
            }
            return response()->json(['status' => 'success', 'id' => $id, 'message' => 'Record Added Successfully']);
        }

    }

    public function edit($id)
    {
        try {
            $data = Jobs::getWorkCategory();
            $webjob = Jobs::getWebJobById($id);
            $webjob[0]->extra = Jobs::getExtraById($id);
            $data['job_id'] = $webjob[0]->job_id;
            $data['reg_date'] = $webjob[0]->reg_date;
            //echo"<pre>";  print_r($data);exit;

            return view('admin.jobs.web_job_sheet_create', ['job' => $data, 'web' => $webjob[0]]);
    	}catch(Exception $e){
    		log::error($e);
    	}
    }

    public function delete(Request $request)
    {

        // dd($id);
        $data = Jobs::where('job_id',$request->id)->delete();
        // dd($data);
        // $data->delete();
        
         return response()->json(['status' => 'success', 'message' => 'Record Deleted Successfully']);
        // return back();

    }
  
}
