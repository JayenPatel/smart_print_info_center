<?php

namespace App\Http\Controllers\Admin\RoleController;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Shop\Modules\Modules;
use App\Shop\Roles\Roles;
use App\Shop\RolePermissions\RolePermissions;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * 
     * @Aayushi
     * Display User Role permission
     * 
     */
    public function index(Request $request,$id)
    {
        try {
            
            $roles=Roles::get();
            if(isset($id) || $id!= ''){
                $modules=DB::table('modules')
                        ->leftjoin('role_permission as m','m.module_id', '=', 'modules.id')
                        ->where('m.role_id',$id)
                        ->get();

            }
            else{
                $modules=DB::table('modules')
                            ->leftjoin('role_permission as m','m.module_id', '=', 'modules.id')
                            ->where('m.role_id',1)
                            ->get();
            }
            if(isset($id))
            {
                $role_id=$id;
            }
            else{
                $role_id=1;
            }
            return view('admin.roles.create', ['roles' => $roles,'modules'=>$modules,'role_id'=>$id]);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    /**
     * 
     * @Aayushi 
     * For store/update user permission
     * 
     */
    public function store(Request $request,$id)
    {
       $request_data=$request->all();
        try {
            
            $module_name=Modules::get('module_name')->toArray();
            foreach($module_name as $name)
            {
                if(isset($request_data[$name['module_name']]))
                {
                    $ids=RolePermissions::where('role_id',$id)
                    ->where('module_id',$request_data[$name['module_name']])
                    ->get('id')
                    ->toArray();
                    
                    if(isset($request_data[$name['module_name'].'view']))
                    {
                        $view_permission=1;
                    }
                    else{
                        $view_permission=0;
                    }
                    if(isset($request_data[$name['module_name'].'add']))
                    {
                        $add_permission=1;
                    }
                    else{
                        $add_permission=0;
                    }
                    if(isset($request_data[$name['module_name'].'edit']))
                    {
                        $edit_permission=1;
                    }
                    else{
                        $edit_permission=0;
                    }
                    if(isset($request_data[$name['module_name'].'delete']))
                    {
                        $delete_permission=1;
                    }
                    else{
                        $delete_permission=0;
                    }
                    $data=RolePermissions::find($ids[0]['id']);
                    $data->view_permission=$view_permission;
                    $data->add_permission=$add_permission;
                    $data->edit_permission=$edit_permission;
                    $data->delete_permission=$delete_permission;
                    $data->save();
                    $data=RolePermissions::find($ids[0]['id']);
                    if($data->view_permission == 0 && $data->delete_permission == 0 && $data->edit_permission == 0 && $data->add_permission == 0)
                    {
                        $module = Modules::find($data->module_id);
                        $role_id=explode(',',$module->permited_roles);
                        if(in_array($id,$role_id))
                        {
                            $key=array_search($id, $role_id);
                            echo $key;
                            unset($role_id[$key]);
                        }
                        $roles =implode(',',$role_id);
                        $module->permited_roles=$roles;
                        $module->save();
                    }
                    if($data->view_permission == 1 || $data->delete_permission == 1 || $data->edit_permission == 1 || $data->add_permission == 1)
                    {
                        $module = Modules::find($data->module_id);
                        $role_id=explode(',',$module->permited_roles);
                        if(!in_array($id,$role_id))
                        {
                            array_push($role_id,$id);
                        }
                        $roles =implode(',',$role_id);
                        $module->permited_roles=$roles;
                        $module->save();
                    }
                }
                
            }
            return response()->json(['status' => 'success', 'role_id' => $id, 'message' => 'Record Updated Successfully']);
          
        } catch (Exception $e) {
            log::error($e);
        }
    }
}
