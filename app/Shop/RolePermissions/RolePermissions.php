<?php

namespace App\Shop\RolePermissions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class RolePermissions extends Model
{
    protected $table = 'role_permission';
    //
}
