<?php
namespace App\Shop\Staffs;

use App\Shop\Addresses\Address;
use App\Shop\Orders\Order;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Staffs extends Model {

    protected $table = 'staff';

  public static function getRole(){
      $value = DB::table('role')->get();
      return $value;
    }
    public static function getStaff(){
      $value = DB::table('staff')->get();
      return $value;
    }

  public static function insertData($data){
      DB::table('staff')->insert($data);
      $id = DB::getPdo()->lastInsertId();
      return $id;
  }

  public static function updateData($data){
    DB::table('staff')
      ->where('staffId', $data['staffId'])
      ->update($data);
  }

  public static function deleteData($id){
    DB::table('staff')->where('staffId', '=', $id)->delete();
  }
   public static function getOneRecord($id){
    $value1 = DB::table('staff')->where('staffId', $id)->get()->toArray();
    $value= (array) $value1[0];
    return $value;
  }
  public static function getStaffReport($from='',$to='')
  {
    $to_date=Carbon::parse($to)->addDays(1);
    if($from!='' && $to!='')
    {
      $value = DB::table('staff')
                  ->whereBetween('createdDate',[$from,$to_date])
                  ->get()->toArray();
    }
    else{
      $value = DB::table('staff')->get();
    }
    
    return $value;
  }
  public static function getStaffForExcel()
  {
    $value = DB::table('staff')
    ->leftJoin('role as r','r.roleId', '=', 'staff.roleId')
    ->get();
    return $value;
  }
}
