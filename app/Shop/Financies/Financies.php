<?php

namespace App\Shop\Financies;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Financies extends Model
{
    public static function getCustomerWiseJob($id)
    {
            return DB::table('job')->join('customer', 'customer.customer_id', 'job.customer_id')->
            where('job.customer_id', $id)->orderBy('job.job_id', 'desc')->
            select('job.*', 'customer.business_name')->get()->toArray();
    }

    public static function getInvoiceList()
    {
        return DB::table('invoice')->join('customer', 'customer.customer_id', 'invoice.customer_id')->orderBy('invoice_id', 'desc')->select('invoice.*', 'customer.business_name')->get()->toArray();

    }

    public static function getExtraById($id)
    {
        return DB::table('extra')->join('job', 'extra.job_id', 'job.job_id')->where('job.job_id', $id)->orderBy('extra.job_id', 'desc')->get('extra.*')->toArray();
    }

    public static function insertData($data)
    {
        DB::table('invoice')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }
    public static function updateData($data,$id)
    {
        DB::table('invoice')->where('invoice_id', $id)->update($data);
    }
    public static function getInvoiceDetails($id){
        return DB::table('invoice')->join('customer', 'customer.customer_id', 'invoice.customer_id')->
        where('invoice.invoice_id', $id)->select('invoice.*', 'customer.business_name')->get()->toArray();
    }
    public static function getAddedInvoiceDetails($id){
        return DB::table('job')->where('job.invoice_id', $id)->orderBy('job.job_id', 'desc')->get()->toArray();
    }
    public static function getCustomers($status=''){
        if($status ==''){
            $data['customer'] = DB::table('customer')->where('business_name', '!=', '')->orderBy('customer_id', 'desc')->get()->toArray();
        }
        return $data;
    }

}

?>
