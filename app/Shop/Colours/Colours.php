<?php

namespace App\Shop\Colours;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

use Illuminate\Database\Eloquent\Model;

class Colours extends Model
{
    use SoftDeletes;
    protected $table = 'colours';

    public static function insertcolourData($data)
    {
        DB::table('colours')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }
    public static function getcolourData($type,$name)
    {
        $id=DB::table('colours')
                ->where('type',$type)
                ->where('colour',$name)
                ->get('id')
                ->toArray();
        return $id;
    }
}
