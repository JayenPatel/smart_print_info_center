<?php

namespace App\Shop\Customers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class Customers extends Model
{
    protected $table = 'customer';
    public static function getCustomerData($id = 0)
    {

        if ($id == 0) {
            $value = DB::table('customer')->orderBy('customer_id', 'desc')->get()->toArray();
        } else {
            $value = DB::table('customer')->where('customer_id', $id)->get()->toArray();
        }
        return $value;
    }
    public static function getCustomerReport($fromdate='',$todate='',$county='',$postcode='')
    {
        $to_date=Carbon::parse($todate)->addDays(1);
        if($county!='' && $postcode!='')
        {
            $value = DB::table('customer')
            ->where('county',$county)
            ->where('postcode',$postcode)
            ->whereBetween('created_at',[$fromdate,$to_date])
            ->orderBy('customer_id', 'desc')
            ->get()
            ->toArray();
        }
        elseif($county!='')
        {
            $value = DB::table('customer')
            ->where('county',$county)
            ->whereBetween('created_at',[$fromdate,$to_date])
            ->orderBy('customer_id', 'desc')
            ->get()
            ->toArray();
        }
        elseif($postcode!='')
        {
            $value = DB::table('customer')
            ->where('county',$county)
            ->where('postcode',$postcode)
            ->whereBetween('created_at',[$fromdate,$to_date])
            ->orderBy('customer_id', 'desc')
            ->get()
            ->toArray();
        }
        else if($fromdate!='' && $to_date!='')
        {
            $value = DB::table('customer')
            ->whereBetween('created_at',[$fromdate,$to_date])
            ->orderBy('customer_id', 'desc')
            ->get()
            ->toArray();
        }
        else{
            $value = DB::table('customer')
            ->orderBy('customer_id', 'desc')
            ->get()
            ->toArray();
        }
       
        return $value;
    }
    public static function getSupplierData($id = 0)
    {

        if ($id == 0) {
            $value = DB::table('supplier')->orderBy('supplier_id', 'desc')->get()->toArray();
        } else {
            $value = DB::table('supplier')->where('supplier_id', $id)->get()->toArray();
        }
        return $value;
    }

    public static function getCategory()
    {
        $value = DB::table('customer')->where('business_type', '!=', '')->orderBy('business_type', 'desc')->groupBy('business_type')->get('business_type')->toArray();
        return $value;
    }

    public static function getSupplierCategory()
    {
        $value = DB::table('supplier')->where('business_type', '!=', '')->orderBy('business_type', 'desc')->groupBy('business_type')->get('business_type')->toArray();
        return $value;
    }

    public static function getAlternateAddress($id)
    {

        $value = DB::table('alternate_address')->where('id', $id)->get()->toArray();
        return $value;
    }

    public static function insertData($data)
    {
        DB::table('customer')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }

    public static function insertSupplierData($data)
    {
        DB::table('supplier')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }

    public static function insertAddressData($data)
    {
        DB::table('alternate_address')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }

    public static function updateAddressData($id, $data)
    {
        DB::table('alternate_address')
            ->where('id', $id)
            ->update($data);
    }

    public static function updateSupplierData($id, $data)
    {
        DB::table('supplier')
            ->where('supplier_id', $id)
            ->update($data);
    }

    public static function updateData($id, $data)
    {
        DB::table('customer')
            ->where('customer_id', $id)
            ->update($data);
    }

    public static function deleteData($id)
    {
        DB::table('customer')->where('customer_id', '=', $id)->delete();
    }

    public static function deleteSupplierData($id)
    {
        DB::table('supplier')->where('supplier_id', '=', $id)->delete();
    }

    public static function getCustomerForExcel()
    {
        return DB::table('customer')
                        ->orderBy('customer_id', 'desc')
                        ->get()
                        ->toArray();
    }
}
