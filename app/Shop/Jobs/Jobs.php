<?php

namespace App\Shop\Jobs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Jobs extends Model
{

    
protected $table = 'job';


    public static function getCustomerWisePrintJob($id)
    {
            return DB::table('job')
                        ->join('customer', 'customer.customer_id', 'job.customer_id')
                        ->where('job.customer_id', $id)
                        ->where('job.type', 'P')
                        ->orderBy('job.job_id', 'desc')
                        ->select('job.*', 'customer.business_name')->get()->toArray();
    }
    public static function getPrintReport($from='',$to='',$category='',$status='')
    {
        $to_date=Carbon::parse($to)->addDays(1);
        if($category!='' && $status!='')
        {
            $value= DB::table('printing')
                    ->rightJoin('job', 'printing.job_id', 'job.job_id')
                    ->where('job.category',$category)
                    ->where('job.status',$status)
                    ->where('job.type', 'P')
                    ->whereBetween('job.reg_date',[$from,$to_date])
                    ->orderBy('printing.job_id', 'desc')
                    ->get()->toArray();
        }
        else if($category!='')
        {
            $value= DB::table('printing')
            ->rightJoin('job', 'printing.job_id', 'job.job_id')
            ->where('job.category',$category)
            ->where('job.type', 'P')
            ->whereBetween('job.reg_date',[$from,$to_date])
            ->orderBy('printing.job_id', 'desc')
            ->get()->toArray();
        }
        else if($status!='')
        {
            $value= DB::table('printing')
                    ->rightJoin('job', 'printing.job_id', 'job.job_id')
                    ->where('job.status',$status)
                    ->where('job.type', 'P')
                    ->whereBetween('job.reg_date',[$from,$to_date])
                    ->orderBy('printing.job_id', 'desc')
                    ->get()->toArray();
        }
        else if($from!='' && $to!=''){
            $value= DB::table('printing')
            ->rightJoin('job', 'printing.job_id', 'job.job_id')
            ->where('job.type', 'P')
            ->whereBetween('job.reg_date',[$from,$to_date])
            ->orderBy('printing.job_id', 'desc')
            ->get()->toArray();
        }
        else{
            $value= DB::table('printing')
                    ->rightJoin('job', 'printing.job_id', 'job.job_id')
                    ->where('job.type', 'P')
                    ->orderBy('printing.job_id', 'desc')
                    ->get()->toArray();
        }
        return $value;
    }
    public static function getWebReport($from='',$to='',$category='',$status='')
    {
        $to_date=Carbon::parse($to)->addDays(1);
        if($category!='' && $status!='')
        {
            $value= DB::table('web')
                    ->rightJoin('job', 'web.job_id', 'job.job_id')
                    ->where('job.category',$category)
                    ->where('job.status',$status)
                    ->where('job.type', 'W')
                    ->whereBetween('job.reg_date',[$from,$to_date])
                    ->orderBy('web.job_id', 'desc')
                    ->get()->toArray();
        }
        else if($category!='')
        {
            $value= DB::table('web')
            ->rightJoin('job', 'web.job_id', 'job.job_id')
            ->where('job.category',$category)
            ->where('job.type', 'W')
            ->whereBetween('job.reg_date',[$from,$to_date])
            ->orderBy('web.job_id', 'desc')
            ->get()->toArray();
        }
        else if($status!='')
        {
            $value= DB::table('web')
                    ->rightJoin('job', 'web.job_id', 'job.job_id')
                    ->where('job.status',$status)
                    ->where('job.type', 'W')
                    ->whereBetween('job.reg_date',[$from,$to_date])
                    ->orderBy('web.job_id', 'desc')
                    ->get()->toArray();
        }
        else if($from!='' && $to!=''){
            $value= DB::table('web')
            ->rightJoin('job', 'web.job_id', 'job.job_id')
            ->where('job.type', 'W')
            ->whereBetween('job.reg_date',[$from,$to_date])
            ->orderBy('web.job_id', 'desc')
            ->get()->toArray();
        }
        else{
            $value= DB::table('web')
                    ->rightJoin('job', 'web.job_id', 'job.job_id')
                    ->where('job.type', 'W')
                    ->orderBy('web.job_id', 'desc')
                    ->get()->toArray();
        }
        return $value;
    }
    public static function getCalenderReport($from='',$to='',$category='',$status='')
    {
        $to_date=Carbon::parse($to)->addDays(1);
        if($category!='' && $status!='')
        {
            $value= DB::table('calendar')
                    ->rightJoin('job', 'calendar.job_id', 'job.job_id')
                    ->where('job.category',$category)
                    ->where('job.status',$status)
                    ->where('job.type', 'C')
                    ->whereBetween('job.reg_date',[$from,$to_date])
                    ->orderBy('calendar.job_id', 'desc')
                    ->get()->toArray();
        }
        else if($category!='')
        {
            $value= DB::table('calendar')
            ->rightJoin('job', 'calendar.job_id', 'job.job_id')
            ->where('job.category',$category)
            ->where('job.type', 'C')
            ->whereBetween('job.reg_date',[$from,$to_date])
            ->orderBy('calendar.job_id', 'desc')
            ->get()->toArray();
        }
        else if($status!='')
        {
            $value= DB::table('calendar')
                    ->rightJoin('job', 'calendar.job_id', 'job.job_id')
                    ->where('job.status',$status)
                    ->where('job.type', 'C')
                    ->whereBetween('job.reg_date',[$from,$to_date])
                    ->orderBy('calendar.job_id', 'desc')
                    ->get()->toArray();
        }
        else if($from!='' && $to!=''){
            $value= DB::table('calendar')
            ->rightJoin('job', 'calendar.job_id', 'job.job_id')
            ->where('job.type', 'C')
            ->whereBetween('job.reg_date',[$from,$to_date])
            ->orderBy('calendar.job_id', 'desc')
            ->get()->toArray();
        }
        else{
            $value= DB::table('calendar')
                    ->rightJoin('job', 'calendar.job_id', 'job.job_id')
                    ->where('job.type', 'C')
                    ->orderBy('calendar.job_id', 'desc')
                    ->get()->toArray();
        }
        return $value;
    }
    public static function getOtherReport($from='',$to='',$category='',$status='')
    {
        $to_date=Carbon::parse($to)->addDays(1);
        if($category!='' && $status!='')
        {
            $value= DB::table('otherjob')
                    ->rightJoin('job', 'otherjob.job_id', 'job.job_id')
                    ->where('job.category',$category)
                    ->where('job.status',$status)
                    ->where('job.type', 'X')
                    ->whereBetween('job.reg_date',[$from,$to_date])
                    ->orderBy('otherjob.job_id', 'desc')
                    ->get()->toArray();
        }
        else if($category!='')
        {
            $value= DB::table('otherjob')
            ->rightJoin('job', 'otherjob.job_id', 'job.job_id')
            ->where('job.category',$category)
            ->where('job.type', 'X')
            ->whereBetween('job.reg_date',[$from,$to_date])
            ->orderBy('calendar.job_id', 'desc')
            ->get()->toArray();
        }
        else if($status!='')
        {
            $value= DB::table('otherjob')
                    ->rightJoin('job', 'otherjob.job_id', 'job.job_id')
                    ->where('job.status',$status)
                    ->where('job.type', 'X')
                    ->whereBetween('job.reg_date',[$from,$to_date])
                    ->orderBy('otherjob.job_id', 'desc')
                    ->get()->toArray();
        }
        else if($from!='' && $to!=''){
            $value= DB::table('otherjob')
            ->rightJoin('job', 'otherjob.job_id', 'job.job_id')
            ->where('job.type', 'X')
            ->whereBetween('job.reg_date',[$from,$to_date])
            ->orderBy('otherjob.job_id', 'desc')
            ->get()->toArray();
        }
        else{
            $value= DB::table('otherjob')
                    ->rightJoin('job', 'otherjob.job_id', 'job.job_id')
                    ->where('job.type', 'X')
                    ->orderBy('otherjob.job_id', 'desc')
                    ->get()->toArray();
        }
        return $value;
    }
    public static function getLastRecord()
    {
        return DB::table('job')->orderBy('job_id', 'desc')->first();
    }
    public static function getJobRecord($id)
    {
        return DB::table('job')->where('job_id', $id)->orderBy('job_id', 'desc')->get()->toArray();
    }
    public static function getPrintJob()
    {
        return DB::table('printing')->rightJoin('job', 'printing.job_id', 'job.job_id')->where('job.type', 'P')->orderBy('printing.job_id', 'desc')->get()->toArray();
    }

    public static function getCalenderJob()
    {
        return DB::table('calendar')->rightJoin('job', 'calendar.job_id', 'job.job_id')->where('job.type', 'C')->orderBy('calendar.job_id', 'desc')->get()->toArray();
    }

    public static function getWebJob()
    {
        return DB::table('web')->rightJoin('job', 'web.job_id', 'job.job_id')->where('job.type', 'W')->orderBy('web.job_id', 'desc')->get()->toArray();
    }
    public static function getOtherJob()
    {
        return DB::table('otherjob')->rightJoin('job', 'otherjob.job_id', 'job.job_id')->where('job.type', 'X')->orderBy('otherjob.job_id', 'desc')->get()->toArray();
    }

    public static function getOnlyCalenderJob($id)
    {
        return DB::table('calendar')->where('calendar.job_id', $id)->orderBy('calendar.job_id', 'desc')->get()->toArray();
    }

    public static function getPrintJobById($id)
    {
        return DB::table('printing')->rightJoin('job', 'printing.job_id', 'job.job_id')->where('job.job_id', $id)->orderBy('printing.job_id', 'desc')->get()->toArray();
    }

    public static function getCalenderJobById($id)
    {
        return DB::table('calendar')->rightJoin('job', 'calendar.job_id', 'job.job_id')->where('job.job_id', $id)->orderBy('job.job_id', 'desc')->get()->toArray();
    }

    public static function getWebJobById($id)
    {
        return DB::table('web')->rightJoin('job', 'web.job_id', 'job.job_id')->where('job.job_id', $id)->orderBy('job.job_id', 'desc')->get()->toArray();
    }

    public static function getotherJobById($id)
    {
        return DB::table('otherjob')->rightJoin('job', 'otherjob.job_id', 'job.job_id')->where('job.job_id', $id)->orderBy('job.job_id', 'desc')->get()->toArray();
    }

    public static function getExtraById($id)
    {
        return DB::table('extra')->join('job', 'extra.job_id', 'job.job_id')->where('job.job_id', $id)->orderBy('extra.job_id', 'desc')->get('extra.*')->toArray();
    }

    public static function getCalenderCategory()
    {
        $data['customer'] = DB::table('customer')->where('business_name', '!=', '')->orderBy('customer_id', 'desc')->get()->toArray();
        $data['category'] = DB::table('category')->where('type','calender')->where('status',1)->whereNull('deleted_at')->get()->toArray();
        $data['colors'] = DB::table('colours')->where('type','calender')->whereNull('deleted_at')->get()->toArray();
        $data['job_title'] = DB::table('jobtitle')->groupBy('job_title')->where('type','calender')->whereNotNull('job_title')->whereNull('deleted_at')->get('job_title')->toArray();
        return $data;
    }

    public static function getWorkCategory()
    {
        $data['customer'] = DB::table('customer')->where('business_name', '!=', '')->orderBy('customer_id', 'desc')->get()->toArray();
        $data['category'] = DB::table('category')->where('type','web')->where('status',1)->whereNull('deleted_at')->get()->toArray();
        $data['colors'] = DB::table('colours')->where('type','web')->whereNull('deleted_at')->get()->toArray();
        $data['accoun_type'] = DB::table('web')->groupBy('accoun_type')->where('accoun_type', '!=', '')->get('accoun_type')->toArray();

        return $data;
    }

    public static function getOtherCategory()
    {
        $data['customer'] = DB::table('customer')->where('business_name', '!=', '')->orderBy('customer_id', 'desc')->get()->toArray();
        $data['category'] = DB::table('category')->where('type','other')->where('status',1)->whereNull('deleted_at')->get()->toArray();
        $data['colors'] = DB::table('colours')->where('type','other')->whereNull('deleted_at')->get()->toArray();
        $data['job_title'] = DB::table('jobtitle')->groupBy('job_title')->where('type','other')->whereNotNull('job_title')->whereNull('deleted_at')->get('job_title')->toArray();
        return $data;
    }

    public static function getCategory()
    {
        $data['customer'] = DB::table('customer')->where('business_name', '!=', '')->orderBy('customer_id', 'desc')->get()->toArray();

        $data['category'] = DB::table('category')->where('type','print')->where('status',1)->whereNull('deleted_at')->get('name')->toArray();
        $data['job_title'] = DB::table('jobtitle')->groupBy('job_title')->where('type','print')->whereNotNull('job_title')->whereNull('deleted_at')->get('job_title')->toArray();
        $data['design_unfolded_size'] = DB::table('printing')->groupBy('design_unfolded_size')->where('design_unfolded_size', '!=', '')->get('design_unfolded_size')->toArray();
        $data['design_folded_size'] = DB::table('printing')->groupBy('design_folded_size')->where('design_folded_size', '!=', '')->get('design_folded_size')->toArray();
        $data['prepress_setupsize'] = DB::table('printing')->groupBy('prepress_setupsize')->where('prepress_setupsize', '!=', '')->get('prepress_setupsize')->toArray();
        $data['prepress_setup_type'] = DB::table('printing')->groupBy('prepress_setup_type')->where('prepress_setup_type', '!=', '')->get('prepress_setup_type')->toArray();
        $data['prepress_emailto'] = DB::table('printing')->groupBy('prepress_emailto')->where('prepress_emailto', '!=', '')->get('prepress_emailto')->toArray();
        $data['press_printwhere'] = DB::table('printing')->groupBy('press_printwhere')->where('press_printwhere', '!=', '')->get('press_printwhere')->toArray();
        $data['paper_type'] = DB::table('printing')->groupBy('paper_type')->where('paper_type', '!=', '')->get('paper_type')->toArray();
        $data['paper_brand'] = DB::table('printing')->groupBy('paper_brand')->where('paper_brand', '!=', '')->get('paper_brand')->toArray();
        $data['paper_supplier'] = DB::table('printing')->groupBy('paper_supplier')->where('paper_supplier', '!=', '')->get('paper_supplier')->toArray();
        $data['paper_colour'] = DB::table('colours')->where('type','print')->whereNull('deleted_at')->get('colour as paper_colour')->toArray();
        $data['paper_size'] = DB::table('printing')->groupBy('paper_size')->where('paper_size', '!=', '')->get('paper_size')->toArray();
        $data['finish_binding'] = DB::table('printing')->groupBy('finish_binding')->where('finish_binding', '!=', '')->get('finish_binding')->toArray();
        $data['finish_finish'] = DB::table('printing')->groupBy('finish_finish')->where('finish_finish', '!=', '')->get('finish_finish')->toArray();
        $data['finish_delivery'] = DB::table('printing')->groupBy('finish_delivery')->where('finish_delivery', '!=', '')->get('finish_delivery')->toArray();

        
        $data['colour'] = DB::table('colours')->where('type','print')->whereNull('deleted_at')->get()->toArray();
      
        return $data;
    }

    public static function getCategoryCalender()
    {
        $data['customer'] = DB::table('customer')->where('business_name', '!=', '')->orderBy('customer_id', 'desc')->get()->toArray();

        $data['category'] = DB::table('category')->where('type','calender')->where('status',1)->whereNull('deleted_at')->get('name')->toArray();
        $data['design_unfolded_size'] = DB::table('printing')->groupBy('design_unfolded_size')->where('design_unfolded_size', '!=', '')->get('design_unfolded_size')->toArray();
        $data['design_folded_size'] = DB::table('printing')->groupBy('design_folded_size')->where('design_folded_size', '!=', '')->get('design_folded_size')->toArray();
        $data['prepress_setupsize'] = DB::table('printing')->groupBy('prepress_setupsize')->where('prepress_setupsize', '!=', '')->get('prepress_setupsize')->toArray();
        $data['prepress_setup_type'] = DB::table('printing')->groupBy('prepress_setup_type')->where('prepress_setup_type', '!=', '')->get('prepress_setup_type')->toArray();
        $data['prepress_emailto'] = DB::table('printing')->groupBy('prepress_emailto')->where('prepress_emailto', '!=', '')->get('prepress_emailto')->toArray();
        $data['press_printwhere'] = DB::table('printing')->groupBy('press_printwhere')->where('press_printwhere', '!=', '')->get('press_printwhere')->toArray();
        $data['paper_type'] = DB::table('printing')->groupBy('paper_type')->where('paper_type', '!=', '')->get('paper_type')->toArray();
        $data['paper_brand'] = DB::table('printing')->groupBy('paper_brand')->where('paper_brand', '!=', '')->get('paper_brand')->toArray();
        $data['paper_supplier'] = DB::table('printing')->groupBy('paper_supplier')->where('paper_supplier', '!=', '')->get('paper_supplier')->toArray();
        $data['paper_colour'] = DB::table('printing')->groupBy('paper_colour')->where('paper_colour', '!=', '')->get('paper_colour')->toArray();
        $data['paper_size'] = DB::table('printing')->groupBy('paper_size')->where('paper_size', '!=', '')->get('paper_size')->toArray();
        $data['finish_binding'] = DB::table('printing')->groupBy('finish_binding')->where('finish_binding', '!=', '')->get('finish_binding')->toArray();
        $data['finish_finish'] = DB::table('printing')->groupBy('finish_finish')->where('finish_finish', '!=', '')->get('finish_finish')->toArray();
        $data['finish_delivery'] = DB::table('printing')->groupBy('finish_delivery')->where('finish_delivery', '!=', '')->get('finish_delivery')->toArray();
        $data['job_title'] = DB::table('jobtitle')->groupBy('job_title')->where('type','calender')->whereNotNull('job_title')->whereNull('deleted_at')->get('job_title')->toArray();
        
        $data['colour'] = DB::table('colours')->where('type','calender')->whereNull('deleted_at')->get()->toArray();
      
        return $data;
    }

    public static function insertData($data)    
    {
        DB::table('job')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }

    public static function insertExtraData($data)
    {
        DB::table('extra')->insert($data);
    }

    public static function insertotherData($data)
    {
        DB::table('otherjob')->insert($data);
    }


    public static function updateData($id, $data)
    {
        DB::table('job')->where('job_id', $id)->update($data);
    }

    public static function updatePrintData($id, $data)
    {
        DB::table('printing')->where('job_id', $id)->update($data);
    }

    public static function updateotherData($id, $data)
    {
        DB::table('otherjob')->where('job_id', $id)->update($data);
    }

    public static function updateWebData($id, $data)
    {
        DB::table('web')->where('job_id', $id)->update($data);
    }

    public static function updateCalenderData($id, $data)
    {
        DB::table('calendar')->where('job_id', $id)->update($data);
    }

    public static function insertPrintData($data)
    {
        DB::table('printing')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }

    public static function insertWebData($data)
    {
        DB::table('web')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }

    public static function insertCalenderData($data)
    {
        DB::table('calendar')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }

    public static function deleteJobData($id)
    {
        DB::table('job')->where('job_id', '=', $id)->delete();
    }

    public static function deleteExtraById($id)
    {
        return DB::table('extra')->where('extra.job_id', $id)->delete();
    }

    public static function getPrintWithCustomerJob()
    {
        return DB::table('job')->join('customer', 'customer.customer_id', 'job.customer_id')->where('job.job_id','like','%P')->where('job.type', 'P')->orderBy('job.job_id', 'desc')->select('job.*','customer.business_name')->get()->toArray();
    }
    public static function getCalenderWithCustomerJob()
    {
        return DB::table('job')->join('customer', 'customer.customer_id', 'job.customer_id')->where('job.job_id','like','%C')->where('job.type', 'C')->orderBy('job.job_id', 'desc')->select('job.*','customer.business_name')->get()->toArray();
    }
    public static function getOtherWithCustomerJob()
    {
        return DB::table('job')->join('customer', 'customer.customer_id', 'job.customer_id')->where('job.job_id','like','%X')->where('job.type', 'X')->orderBy('job.job_id', 'desc')->select('job.*','customer.business_name')->get()->toArray();
    }
    public static function getWebWithCustomerJob()
    {
        return DB::table('job')->join('customer', 'customer.customer_id', 'job.customer_id')->where('job.job_id','like','%W')->where('job.type', 'W')->orderBy('job.job_id', 'desc')->select('job.*','customer.business_name')->get()->toArray();
    }
    public static function getQuotesWithCustomerJob()
    {
        return DB::table('job')->join('customer', 'customer.customer_id', 'job.customer_id')->where('job.job_id','like','%Q')->orderBy('job.job_id', 'desc')->select('job.*','customer.business_name')->get()->toArray();
    }
    public static function updateHeaders($data,$res)
    {
        $res[$data['type']] = $data['value'];
        DB::table('job')->where('job_id', $data['id'])->update($res);
    }

    public static function getPrintForExcel()
    {
        return DB::table('printing')->rightJoin('job', 'printing.job_id', 'job.job_id')
            ->join('customer', 'customer.customer_id', 'job.customer_id')->
            where('job.type', 'P')->orderBy('printing.job_id', 'desc')->
            select('job.*','printing.*','customer.business_name')->get()->toArray();
    }
    public static function getCalenderForExcel()
    {
        return DB::table('calendar')->rightJoin('job', 'calendar.job_id', 'job.job_id')
            ->join('customer', 'customer.customer_id', 'job.customer_id')->
            where('job.type', 'C')->orderBy('calendar.job_id', 'desc')->
            select('job.*','calendar.*','customer.business_name')->get()->toArray();
    }
    public static function getWebForExcel()
    {
        return DB::table('web')->rightJoin('job', 'web.job_id', 'job.job_id')
            ->join('customer', 'customer.customer_id', 'job.customer_id')->
            where('job.type', 'W')->orderBy('web.job_id', 'desc')->
            select('job.*','web.*','customer.business_name')->get()->toArray();
    }
    public static function getOtherForExcel()
    {
        return DB::table('otherjob')->rightJoin('job', 'otherjob.job_id', 'job.job_id')
            ->join('customer', 'customer.customer_id', 'job.customer_id')->
            where('job.type', 'X')->orderBy('otherjob.job_id', 'desc')->
            select('job.*','otherjob.*','customer.business_name')->get()->toArray();
    }

    public static function updatVateData($id, $data)
    {
        DB::table('site_settings')->where('setting_id', $id)->update($data);
    }
    

    public static function getVatRecord()
    {
        return DB::table('site_settings')->first();
    }

    public static function extraVatrecord($vatextra)
    {
        return DB::table('extra')->insert($vatextra);
        
    }

    public static function updateextraVatrecord($id,$vatextra)
    {
        return DB::table('extra')->where(['job_id'=> $id])->update($vatextra);
    }
    
}
