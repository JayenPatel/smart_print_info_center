<?php

namespace App\Shop\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'category';

    public static function insertcategoryData($data)
    {
        DB::table('category')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }
    public static function getcategoryData($type,$name)
    {
        $id=DB::table('category')
                ->where('type',$type)
                ->where('name',$name)
                ->get('id')
                ->toArray();
        return $id;
    }
}
