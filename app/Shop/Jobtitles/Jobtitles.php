<?php

namespace App\Shop\Jobtitles;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Jobtitles extends Model
{
    use SoftDeletes;
    protected $table = 'jobtitle';

    public static function insertjobtitleData($data)
    {
        DB::table('jobtitle')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }
    public static function getjobtitleData($type,$name)
    {
        $id=DB::table('jobtitle')
                ->where('type',$type)
                ->where('job_title',$name)
                ->get('id')
                ->toArray();
        return $id;
    }
}
