<?php

namespace App\Shop\Roles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class Roles extends Model
{
    protected $table = 'role';
}
