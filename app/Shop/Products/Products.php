<?php

namespace App\Shop\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class Products extends Model
{
    protected $table = 'products';
    public static function getProductData($id = 0)
    {

        if ($id == 0) {
            $value = DB::table('products')
                    ->leftJoin('customer as cm','cm.customer_id', '=', 'products.customer_id')
                    ->where('products.status',1)
                    ->orderBy('products.id', 'desc')
                    ->get()->toArray();
        } else {
            $value = DB::table('products')
                    ->leftJoin('customer as cm','cm.customer_id', '=', 'products.customer_id')
                    ->where('products.status',1)
                    ->where('id', $id)
                    ->get()
                    ->toArray();
        }
        return $value;
    }
    public static function getProductReport($fromdate='',$todate='',$customer_id='',$category_id='')
    {
        $to_date=Carbon::parse($todate)->addDays(1);
        if($customer_id!='' && $category_id!='')
        {
            $value= DB::table('products')
                ->leftJoin('customer as cm','cm.customer_id', '=', 'products.customer_id')
                ->leftJoin('category as ca','ca.id', '=', 'products.category_id')
                ->where('products.customer_id','LIKE',"%".$customer_id."%")
                ->where('products.category_id','LIKE',"%".$category_id."%")
                ->where('products.status',1)
                ->whereBetween('products.created_at',[$fromdate,$to_date])
                ->orderBy('products.id', 'desc')
                ->get()
                ->toArray();
        }
        else if($customer_id!=''){
            $value= DB::table('products')
                ->leftJoin('customer as cm','cm.customer_id', '=', 'products.customer_id')
                ->leftJoin('category as ca','ca.id', '=', 'products.category_id')
                ->where('products.customer_id','LIKE',"%".$customer_id."%")
                ->where('products.status',1)
                ->whereBetween('products.created_at',[$fromdate,$to_date])
                ->orderBy('products.id', 'desc')
                ->get()
                ->toArray();
        }
        else if($category_id!='')
        {
            $value= DB::table('products')
                ->leftJoin('customer as cm','cm.customer_id', '=', 'products.customer_id')
                ->leftJoin('category as ca','ca.id', '=', 'products.category_id')
                ->where('products.category_id','LIKE',"%".$category_id."%")
                ->where('products.status',1)
                ->whereBetween('products.created_at',[$fromdate,$to_date])
                ->orderBy('products.id', 'desc')
                ->get()
                ->toArray();
        }
        elseif($fromdate!=''&& $todate!='')
        {
            $value= DB::table('products')
            ->leftJoin('customer as cm','cm.customer_id', '=', 'products.customer_id')
            ->leftJoin('category as ca','ca.id', '=', 'products.category_id')
            ->where('products.status',1)
            ->whereBetween('products.created_at',[$fromdate,$to_date])
            ->orderBy('products.id', 'desc')
            ->get()
            ->toArray();
        }
        else{
            $value= DB::table('products')
                ->leftJoin('customer as cm','cm.customer_id', '=', 'products.customer_id')
                ->leftJoin('category as ca','ca.id', '=', 'products.category_id')
                ->where('products.status',1)
                ->orderBy('products.id', 'desc')
                ->get()
                ->toArray();
        }
        return $value;
    }

    public static function getCustomer()
    {
        $value = DB::table('customer')->orderBy('business_name', 'desc')->get()->toArray();
        return $value;
    }

    public static function getCategory()
    {
        $value = DB::table('category')->orderBy('name', 'desc')->get()->toArray();
        return $value;
    }

    public static function insertData($data)
    {
        DB::table('products')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }


    public static function updateData($id, $data)
    {
        DB::table('products')
            ->where('id', $id)
            ->update($data);
    }

    public static function deleteData($id)
    {
        DB::table('products')->where('id', '=', $id)->delete();
    }

    

    public static function getProductForExcel()
    {
        return DB::table('products')
                        ->leftJoin('customer as cm','cm.customer_id', '=', 'products.customer_id')
                        ->leftJoin('category as ca','ca.id', '=', 'products.category_id')
                        ->where('products.status',1)
                        ->whereMonth('products.created_at',Carbon::now()->month)
                        ->orderBy('products.id', 'desc')
                        ->get()
                        ->toArray();
                        return $value;
    }
}
