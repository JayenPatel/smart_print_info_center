<?php

namespace App\Shop\Modules;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class Modules extends Model
{
    use SoftDeletes;
}
