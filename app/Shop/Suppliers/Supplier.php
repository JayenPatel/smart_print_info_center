<?php

namespace App\Shop\Suppliers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class Supplier extends Model
{
    protected $table = 'supplier';
    public static function getSupplierReport($fromdate='',$todate='',$county='',$postcode='')
    {
        $to_date=Carbon::parse($todate)->addDays(1);
        if($county!='' && $postcode!='')
        {
            $value = DB::table('supplier')
            ->where('county',$county)
            ->where('postcode',$postcode)
            ->whereBetween('created_at',[$fromdate,$to_date])
            ->orderBy('supplier_id', 'desc')
            ->get()
            ->toArray();
        }
        elseif($county!='')
        {
            $value = DB::table('supplier')
            ->where('county',$county)
            ->whereBetween('created_at',[$fromdate,$to_date])
            ->orderBy('supplier_id', 'desc')
            ->get()
            ->toArray();
        }
        elseif($postcode!='')
        {
            $value = DB::table('supplier')
            ->where('county',$county)
            ->where('postcode',$postcode)
            ->whereBetween('created_at',[$fromdate,$to_date])
            ->orderBy('custsupplier_idomer_id', 'desc')
            ->get()
            ->toArray();
        }
        else if($fromdate!='' && $todate!='')
        {
            $value = DB::table('supplier')
            ->whereBetween('created_at',[$fromdate,$to_date])
            ->orderBy('supplier_id', 'desc')
            ->get()
            ->toArray();
        }
        else{
            $value = DB::table('supplier')
            ->orderBy('supplier_id', 'desc')
            ->get()
            ->toArray();
        }
       
        return $value;
    }
    public static function getSupplierForExcel()
    {
        return DB::table('supplier')
                ->orderBy('supplier_id', 'desc')
                ->get()
                ->toArray();
    }
}
