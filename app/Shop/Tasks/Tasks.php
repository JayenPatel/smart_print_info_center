<?php
namespace App\Shop\Tasks;

use App\Shop\Addresses\Address;
use App\Shop\Orders\Order;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Nicolaslopezj\Searchable\SearchableTrait;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Tasks extends Model {

    protected $table = 'task';

  public static function getRole(){
      $value = DB::table('role')->get();
      return $value;
    }
    public static function getTask(){
      $value = DB::table('task')->leftJoin('staff','staff.staffId','task.task_assign_to')->get();
      return $value;
    }

  public static function insertData($data){
      DB::table('task')->insert($data);
      $id = DB::getPdo()->lastInsertId();
      return $id;
  }

  public static function updateData($data){
    DB::table('task')
      ->where('task_id', $data['task_id'])
      ->update($data);
  }

  public static function deleteData($id){
    DB::table('task')->where('task_id', '=', $id)->delete();
  }
   public static function getOneRecord($id){
    $value1 = DB::table('task')->where('task_id', $id)->get()->toArray();
    $value= (array) $value1[0];
    return $value;
  }

}
