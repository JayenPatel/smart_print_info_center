<?php
namespace App\Shop\Auth;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Auth extends Model {

  public static function loginData($data){
    $value = DB::table('staff')->where('loginName', $data['loginName'])->where('password', $data['password'])->first();
    return $value;
  }

}
