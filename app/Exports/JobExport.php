<?php

namespace App\Exports;

use App\Shop\Jobs\Jobs;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStyles;

class JobExport implements  FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    protected $type;

    function __construct($type)
    {
        $this->type = $type;
    }

    public function styles(Worksheet $sheet)
    {
        return [

            1    => ['font' => ['bold' => true,'italic' => true,'size' => 14],
            ],
        ];
    }
    

    public function collection()
    {
        if($this->type =='printing'){
            $data = Jobs::getPrintForExcel();
        }
        if($this->type =='web'){
            $data = Jobs::getWebForExcel();
        }
        if($this->type =='calender'){
            $data = Jobs::getCalenderForExcel();
        }
        if($this->type =='other'){
            $data = Jobs::getOtherForExcel();
        }
        $data_array = [];
        if (isset($data[0])) {
            foreach ($data as $item) {

                $data_array[] = array(
                    'Job_id' => $item->job_id,
                    'customer_name' => $item->business_name,
                    'status' => $item->status,
                    'quantity' => $item->quantity,
                    'colours' => $item->colours,
                    'colour1' => $item->colour1,
                    'colour2' => $item->colour2,
                    'colour3' => $item->colour3,
                    'net_price' => $item->net_price,
                    'to_delivery_address' => $item->to_delivery_address,
                    'vat' => $item->vat,
                );
            }
        }
      /*  dd($data);
        exit;*/
        return collect($data_array);

    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setTitle('Smart Print');
            },
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getColumnDimension('A')->setAutoSize(false);
                $event->sheet->getColumnDimension('J')->setAutoSize(false);
                $event->sheet
                    ->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getPageSetup()->setFitToPage(true);
                $cellRange = 'A2:K2'; // All headers
                //$event->sheet->setCellValue('A1:W1', 'Smart Print');
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->getColor()
                         ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('353B84');
                $event->sheet->setAutoFilter($cellRange);
                $cellRange1 = 'A1:J1'; 
                $event->sheet->getDelegate()->mergeCells('A1:K1');
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(20);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->getColor()
                         ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('353B84');
                
            },
        ];
    }

    public function headings(): array
    {
        return [['Smart Print - Job'],["Job id", "Customer Name","status", "quantity","colours","colour1","colour2","colour3","net_price","to_delivery_address","vat"]];
    }

}

?>
