<?php

namespace App\Exports;

use App\Shop\Financies\Financies;
use App\Shop\Jobs\Jobs;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InvoiceExport implements FromCollection, WithHeadings
{
    protected $type;

    function __construct($type)
    {
        $this->type = $type;
    }

    public function collection()
    {
        $data = Financies::getInvoiceList();

        $data_array = [];
        if (isset($data[0])) {
            foreach ($data as $item) {

                $data_array[] = array(
                    'invoice_id' => $item->invoice_id,
                    'customer_name' => $item->business_name,
                    'invoice_date' => $item->invoice_date,
                    'net_delivery_price' => $item->net_delivery_price,
                    'delivery_vat' => $item->delivery_vat,
                    'deposit' => $item->deposit,
                    'discount' => $item->discount,
                    'comments' => $item->comments
                );
            }
        }
//          dd($data);
//          exit;
        return collect($data_array);

    }
    public function headings(): array
    {
        return ["Invoice id", "Customer Name","Invoice Date", "Delivery Price","Vat","Deposit","Discount","Comments"];
    }
}

?>
