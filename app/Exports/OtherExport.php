<?php

namespace App\Exports;

use App\Shop\Customers\Customers;
use App\Shop\Products\Products;
use App\Shop\Suppliers\Supplier;
use App\Shop\Staffs\Staffs;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;

class OtherExport implements FromCollection, WithHeadings ,WithStyles, ShouldAutoSize, WithEvents
{
    protected $type;

    function __construct($type)
    {
        $this->type = $type;
    }
    public function styles(Worksheet $sheet)
    {
        return [

            1    => ['font' => ['bold' => true,'italic' => true,'size' => 20],
            ],
        ];
    }
    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setTitle('Smart Print');
            },
            AfterSheet::class    => function(AfterSheet $event) {
                
                $event->sheet->getColumnDimension('A')->setAutoSize(false);
                $event->sheet
                ->getPageSetup()
                ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getPageSetup()->setFitToPage(true);
                $cellRange = 'A2:G2'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->getColor()
                         ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('353B84');
                $cellRange1 = 'A1:G1'; 
                $event->sheet->getDelegate()->mergeCells('A1:G1');
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(20);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->getColor()
                         ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFill()
                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                            ->getStartColor()->setRGB('353B84');
                
                
            },
        ];
    }
    public function collection()
    {
        if($this->type =='customer'){
            $data = Customers::getCustomerForExcel();
            $data_array = [];
            if (isset($data[0])) {
                foreach ($data as $item) {

                    $data_array[] = array(
                        'customer_id' => $item->customer_id,
                        'customer_name' => $item->first_name." ".$item->first_name,
                        'business_type' => $item->business_type,
                        'business_name' => $item->business_name,
                        'email' => $item->email,
                        'county' => $item->county,
                    );
                }
            }
        }
        if($this->type =='product'){
            $data = Products::getproductForExcel();
            $data_array = [];
            if (isset($data[0])) {
                foreach ($data as $item) {

                    $data_array[] = array(
                        'product_id' => $item->id,
                        'customer_name' => $item->first_name." ".$item->surname,
                        'category_name' => $item->name,
                        'product_name' => $item->product_name,
                        'product_description' => $item->business_type,
                        'price' => $item->price,
                    );
                }
            }
        }
        if($this->type =='supplier'){
            $data = Supplier::getsupplierForExcel();
            $data_array = [];
            if (isset($data[0])) {
                foreach ($data as $item) {

                    $data_array[] = array(
                        'supplier_id' => $item->supplier_id,
                        'Supplier_name' => $item->first_name." ".$item->surname,
                        'business_type' => $item->business_type,
                        'business_name' => $item->business_name,
                        'county' => $item->county,
                    );
                }
            }
        }
        if($this->type =='staff'){
            $data = Staffs::getStaffForExcel();
            $data_array = [];
            if (isset($data[0])) {
                foreach ($data as $item) {

                    $data_array[] = array(
                        'staff_id' => $item->staffId,
                        'Staff_name' => $item->firstName." ".$item->lastName,
                        'loginName' => $item->loginName,
                        'comment' => $item->comment,
                        'role' => $item->roleName,
                        'startDate' =>$item->startDate,
                        'endDate' =>$item->endDate,
                    );
                }
            }
        }
        
      /*  dd($data);
        exit;*/
        return collect($data_array);

    }
    public function headings(): array
    {
        if($this->type =='customer'){
            return [['Smart Print - Customer'],["#", "Customer Name", "Business Type","Business Name","Email","County"]];
        }
        if ($this->type=='product')
        {
            return [['Smart Print - Customer'],["#", "Customer Name","Category Name","Product Name", "Product Description","Price"]];
        }
        if($this->type =='supplier'){
            return [['Smart Print - Supplier'],["#", "Supplier Name", "Business Type","Business Name","County"]];
        }
        if($this->type =='staff'){
            return [['Smart Print - Staff'],["#", "Staff Name", "Login Name","Comment","Role","Start Date","End Date"]];
        }
    }

}

?>
