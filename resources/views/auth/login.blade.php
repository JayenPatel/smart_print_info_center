<!DOCTYPE html>
<html>
<head>
	<title>Smart Printer-Admin Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<link rel="stylesheet" href="{{ asset('css/admin.min.css') }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		html{height: 100%;}
	</style>
</head>
<body>
	<section class="admin-login">
		<div class="row justify-content-center">
		  <div class="box__">
		    <div class="login-logo">
		    	<img src="{{ asset('authImage/SPIC_logo.png')}}" style="width: 100%;">
		    </div>
		    <div style="margin: 60px 0 20px 0" class="formLogin">
		    	<form method="POST" id="formLogin" >
                    {{ csrf_field() }}
		    		<div class="login-input-box">
				    	<i class="fa fa-envelope" aria-hidden="true"></i>
				    	<input type="email" name="email" placeholder="Enter User Name" class="login-input required">
				    </div>
				    <div class="login-input-box">
				    	<i class="fa fa-lock" aria-hidden="true" style="font-size: 24px;"></i>
				    	<input type="password" name="password" placeholder="Enter Password" class="login-input required">
				    </div>
				    <div class="text-center" style="margin-top: 35px;">
				    	<button type="button" id="submit" class="login_btn">Login</button>
				    </div>
		    	</form>
		    </div>
		  </div>
		</div>
	</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
<script type="text/javascript">
    $("input").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});
    $("body").on("click","#submit",function(){
        timeValidation = false;
        var isValidate=true;
        $(".formLogin .required").each(function() {
            if(this.value==""){
                isValidate=false;
                $(this).css("border-bottom","1px dashed #CC0000");
            }else{
                $(this).css("border","");
            }
        });
        if(isValidate){
            var post_data =$("#formLogin").serializeArray()
            $.ajax({
                url: '/admin/doLogin', //this is your uri
                type: 'POST', //this is your method
                data:post_data,
                dataType: 'json',
                success: function(response){
                    if (response['status'] == 'success') {
                            toastr.success("Login Successfully");
                            setTimeout(function () {
                            window.location = "/admin/dashboard";
                                }, 1000);
                        } else {
                            toastr.error("Please username and password are worng!");
                        }
                }
            });
        }else{
            toastr.error("Please enter username password.");
        }
    });
</script>
</body>
</html>
