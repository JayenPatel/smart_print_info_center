@extends('admin.layouts.app')
@section('title','Edit Staff - Smart Print')
@section('content')
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                @if(isset($staffs)) Edit @else New @endif Staff
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <form class="form" id="staffForm" method="post" class="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input id="staffId" name="staffId" value="{!! $data['staff']['staffId'] ?: old('name')  !!}"
                       type="hidden" class="form-control">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Profile Image<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="file" name="profile_pic" class="form-control input "/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">First Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="text" name="firstName" class="form-control input required"
                                   value="{!! $data['staff']['firstName'] ?: old('name')  !!}"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Surname<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="text" name="lastName" class="form-control input required"
                                   value="{!! $data['staff']['lastName'] ?: old('name')  !!}"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Password:</label>
                        <div class="col-lg-4">
                            <input type="password" id="password" name="password" class="form-control input"
                                   value="" />
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Retry Password:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="password" id="re-password" class="form-control input" value=""

                                />
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Start date<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input class="form-control input date required" name="startDate" type="text" id="startDate"
                                   value="{!! $data['staff']['startDate'] ?: old('name')  !!}"/>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Login Name:<span>*</span></label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" id="loginName" name="loginName" class="form-control input required" value="{!! $data['staff']['loginName'] ?: old('name')  !!}" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">End date<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" id="endDate" name="endDate" class="form-control input required date"
                                       value="{!! $data['staff']['endDate'] ?: old('name')  !!}"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>

                        <label class="col-lg-2 col-form-label text-right">Comments:</label>
                        <div class="col-lg-4">
                            <textarea class="form-control input" name="comment" id="exampleTextarea" rows="3"> {!! $data['staff']['comment'] ?: old('name')  !!} </textarea>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Roles:<span>*</span></label>
                        <!-- <div class="col-lg-5"> -->
                        <div class="col-lg-4">
                            <div class="checkbox-list">
                                @foreach($data['role'] as $value)
                                <label class="checkbox checkboc_bottom">
                                    <input type="checkbox" name="roleId[]" value="{{$value->roleId}}"
                                           class="required roleId" @if( in_array($value->roleId,$data['staff']['roleId']))
                                    checked="checked" @endif/> {{$value->roleName}}
                                    <span></span>
                                </label>
                                @endforeach
                            </div>
                        </div>
                        <!-- </div> -->
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-10">
                            <a class="btn btn__bg" href="{{ route('admin.staffs.list') }}"> Cancel</a>
                            <button type="button" id="submitStaff" class="btn btn__bg mr-2 "> Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--end::Container-->

    <style type="text/css">
        .checkboc_bottom {
            margin-bottom: 20px !important;
            font-size: 16px !important;
            color: #353B84 !important;
        }
    </style>

    @endsection
    @push('script')

    <script src="/js/pages/staffs/staffs.js"></script>
    <script type="text/javascript">
        var startDate = new Date($('#startDate').val());
        $('#startDate').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        }).datepicker('setDate', new Date(startDate));
        var endDate = new Date($('#endDate').val());
        $('#endDate').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        }).datepicker('setDate', new Date(endDate));
    </script>

    @endpush
