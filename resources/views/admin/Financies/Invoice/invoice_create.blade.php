@extends('admin.layouts.app')
@section('title','Create Invoice - Smart Print')
@section('content')
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style type="text/css">
    .dis-flex {
        display: flex;
    }

    th {
        background-color: #353B84 !important;
        text-align: center !important;
    }

    td {
        text-align: center !important;
        border: 0.1px solid #353B84 !important;
        background-color: #f0eff5ba;
        color: #353B84 !important;
    }

    h1 {
        style="background-color:LightGray;
     
    }

  
</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                @if(isset($invoice)) Edit @else New @endif Invoice
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
    <div class=" container ">
        <div class="card card-custom gutter-b example example-compact">
            <div class="form-group col-lg-4 mt-3">
                <div class="row">
                    <div class="col-lg-3">
                        <label class=" col-form-label text-right">Customer:</label>
                    </div>
                    <div class="col-lg-9 selectOption">
                        <input class="form-control input optionInput" type="text" style="display: none"/>
                        <select data-live-search='true' class="form-control selectpicker selectedOption required"
                                id="customer_id"
                                name="customer_id">
                            <option></option>
                            @foreach($invoice['customer'] as $category)
                            @if(isset($data['details']) && $category->customer_id  && $category->address1 == $data['details']->customer_id)
                            <option selected="selected" value="{{$category->customer_id }}" invoice="{{$category->address1 }}">{{
                                $category->business_name
                                }}
                            </option>
                            @elseIf(isset($data['customer_id']) && $category->customer_id && $category->address1 == $data['customer_id'])
                            <option selected="selected" value="{{$category->customer_id }}" invoice="{{$category->address1 }}">{{
                                $category->business_name
                                }}
                            </option>
                            @else
                            <option value="{{$category->customer_id}}" invoice="{{$category->address1 }}">{{ $category->business_name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    

                    <div class="col-12">
                        <div class="row">
                            <div class="col-3">
                            <a class="btn btn-primary" data-toggle="modal"  data-target="#myModal">PDF</a>

                            </div>
                        </div>
                    </div>
                            
                            
                                
                    
                </div>
            </div>
            <div class="card-body " id="table_row">
                <!--begin: Datatable-->
                <table class="table table-separate table-head-custom table-checkable datatable1" id="datatable1">
                    <thead>
                    <tr>
                        <th>Job ID</th>
                        <th>Quantity</th>
                        <th>Job Category</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="5">No Job Found</td>
                    </tr>
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--begin::Card-->
        <div class="card card-custom gutter-b example example-compact">
            <!--begin::Form-->
            <form class="form">
                <div class="card-body" >
                    <table class="table table-separate table-head-custom table-checkable" id="datatable2" style="display: none">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Job ID</th>
                            <th>Quantity</th>
                            <th>Description</th>
                            <th>Vat</th>
                            <th>Net Price</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-8 col-sm-6">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-sm-12 col-form-label">Invoice Comments</label>
                                        <div class="col-md-5 col-sm-12">
                                            <textarea class="form-control input_hightlight input" id="invoiceComment"
                                                      rows="3">@if(isset($data['details'])) {{$data['details']->comments}} @endif</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-sm-12 col-form-label">Discount applied</label>
                                        <div class="col-md-5 col-sm-12">
                                            <input class="form-control input vat_per_discount" name="vat_discount" onchange="calculateAmount()" type="number" value="<?php if (isset($data['details'])) {
                                                echo $data['details']->discount;
                                            } ?>"
                                                   id="discountApplied"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group row align-items-center">
                                        <label class="col-md-3 col-sm-12 col-form-label">Show Discount</label>
                                        <div class="col-md-5 col-sm-12">
                                            <label class="checkbox checkbox-single">
                                                <input type="checkbox" name="show_discount" value="1" class="checkable" @if(isset($data['details']) &&
                                                       $data['details']->show_discount ==1) checked @endif id="showDiscount" >
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <input type="hidden" name="invoice_id" id="invoice_id" value="<?php if (isset($data['details'])) {
                                echo $data['details']->invoice_id;
                            } ?>">
                            <label class="col-form-label">17.5% is added to delivery.</label>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Delivery:</label>
                                        <div class="col-md-5">
                                            <input onchange="calculateAmount()" class="form-control input" type="number" value="<?php if (isset($data['details'])) {
                                                echo $data['details']->net_delivery_price;
                                            } ?>" id="delivery"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <label class="col-3 col-form-label">Subtotal:</label>
                                        <label class="col-5 col-form-label" id="totalSubTotal">0.00</label>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="row">
                                        <label class="col-3 col-form-label">Vat:</label>
                                        <label class="col-5 col-form-label" id="totalVat">0.00</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <label class="col-3 col-form-label">Total:</label>
                                        <label class="col-5 col-form-label" id="totalAmount">0.00</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <label class="col-3 col-form-label">Deposit:</label>
                                        <label class="col-5 col-form-label"></label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <label class="col-3 col-form-label">Balance:</label>
                                        <label class="col-5 col-form-label">0.00</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-3">
                                            <button type="button" class="btn btn__bg mr-2" id="submitBtn">Submit
                                            </button>
                                        </div>
                                        <div class="col-5">
                                            <a class="btn btn__bg" href="{{ route('admin.invoices.list') }}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
<div class="modal fade" id="myModal" role="dialog">
<!-- <link href="{{ url('css/style.bundle.css') }}" rel="stylesheet" type="text/css"/> -->
     <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
              <!-- <h4 class="modal-title">Invoice PDF</h4> --> 

            <img alt="Logo" src="{{url('authImage/0001.jpg')}}" class="center" />
              
         
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

   <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-12">
    		<div class="invoice-title">
    			<h2 id="heading">Invoice</h2><h5 class="pull-right">VAT NO 162 8634 93</h5>
    		</div>
        </div>
      </div>
    		
          <div class="container">
    		<div class="row">
    			<div class="col-xs-6 col-md-6">
    				<address>
    				<strong>Billed To:</strong><br>
                    <span id="pop-customer-address"></span>
    				</address>
    			</div>
                    <div class="col-xs-6 col-md-6 text-right ">
                     <div class="col-xs-12 col-md-12">
                                <strong>Order No :</strong>
                                <span>01</span>
                            </div>
                
                        <div class="col-xs-12 col-md-12">
                                <strong>Customer ID :</strong>
                                <span id="pop-customer-id">00</span>
                                </div>
                        <div class="col-xs-12 col-md-12">
                            <strong>Invoice Date :</strong>
                            <span>{{ date('Y-m-d') }}</span>
                        </div>
                </div> 
    		</div>
    	</div>
    </div>
               
    				
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
                <table style="width:100%" id="print-table">
                    <tr class="quantity_item">
                        <th>Quantity</th>
                        <th>Job Description</th>
                        <th>Amount</th>
                    </tr>
                      
                        <tr class="quantity_subitem quantity_subitem1 table-tr">      
                            <td id="totalQuentityPop"></td>
                            <td id="totalDescriptionPop"></td>
                            <td id="amountPop"></td>
                        </tr>

                        <tr class="quantity_subitem">      
                            <td></td>
                            <td class="divery_charge d-flex flex-row-reverse">Discount:</td>
                            <td class="text-right"><input type="number" name="vat_extra_discount" onchange="calculateAmount()" class="vat_radio vat_per_discount" id="discountAppliedPop"/></td>
                        </tr>
                        <tr class="quantity_subitem">      
                            <td></td>
                            <td class="divery_charge d-flex flex-row-reverse">Delivery charge</td>
                            <td id="vat_per_radio" class="text-right">0.00</td>
                        </tr>
                        <tr class="quantity_subitem">      
                            <td></td>
                            <td class="divery_charge d-flex flex-row-reverse">Subtotal</td>
                            <td id="totalSubTotalPop" class="text-right">0.00</td>
                          
                        </tr>
                        <tr class="quantity_subitem">      
                            <td></td>
                            <td class="divery_charge d-flex flex-row-reverse">VAT 20%</td>
                            <td id="totalVatPop" class="text-right"></td>
                        </tr>
                        <tr class="quantity_subitem">      
                            <td></td>
                            <td class="divery_charge d-flex flex-row-reverse">Total</td>
                            <td class="text-right">0.00</td>
                        </tr>
                        <tr class="quantity_subitem">      
                            <td></td>
                            <td class="divery_charge d-flex flex-row-reverse">Deposit (Paid)</td>
                            <td class="text-right">0.00</td>
                        </tr>
                        <tr class="quantity_subitem">      
                            <td></td>
                            <td class="divery_charge d-flex flex-row-reverse"><b>TOTAL AMOUNT DUE</b></td>
                            <td id="totalAmountPop" class="text-right"></td>
                        </tr>
                    
                </table>
                <div class="payment_method mt-4">
                   <div class="payment_box1">
                     <h6>Payment Method</h6>
                   </div>

                   <div class="payment_box3">
                     <p>Cheques  | Please make all cheques payable to SMART PRINT</p>
                   </div>

                   <div class="payment_box3">
                     <p>Bank Transfer  |  BARCLAYS BANK  Account Name: Smart Print  Account No: 83498484  Sort Code: 20-92-63</p>
                   </div>

                </div>
                <div class="payment_note_method mt-4">
                   <div class="payment_box2">
                     <p>The above goods remain in the property of Smart Print & Media ltd. until full settlement of this invoice is received.<br>
                        Goods will not be dispatched, unless full payment is received in our office.<br>
                        Any claims must be notified within three working days in writing.<br>
                        All orders must be accompanied by 50% deposit.</p>
                   </div>
                </div>
                <div class="thank_u">
                THANK YOU FOR YOUR BUSINESS
                </div>
                <div class="invoice_last"><b>7 NORBRECK PARADE, LONDON NW10 7HR<br>
                                TEL: 020 8838 2747 | EMAIL: ADMIN@SMARTPRINTLTD.CO.UK | COMPANY REG. NO. 08523409<br>
                                SMART PRINT & MEDIA LTD. T/A SMART PRINT</b></div>
    		</div>
    	</div>
    </div>
    <div class="modal-footer">
         
          <button onclick="pdfbtn()"  class="btn btn-primary">Download</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
</div>
      
    </div>
  </div>

        </div>
        <!--end::Card-->

    </div>
    <!--end::Container-->
    <div id="elementH"></div>
    @endsection

    @push('script')
    <script type="text/javascript">
        var job_list = [];
        var added_job = [];
        
    </script>
    @endpush
    @push('script')
    <script src="/js/pages/financies/financies.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

    <script>
        var doc = new jsPDF();


        var specialElementHandlers = {
            '#elementH': function (element, renderer) {
                return true;
            }
        };

        function pdfbtn(){   
            doc.fromHTML($('#myModal').html(), 15, 15, {
                'width':170,
                'elementHandlers': specialElementHandlers
            });
            doc.save('sample-file.pdf');
        }

   </script>

    @endpush
                
 
  

                                

                               





                 




                   
                   
    					
                   
