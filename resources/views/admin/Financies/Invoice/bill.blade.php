
<style>
.col-xs-7 {
   
  margin-right:100px;
    margin-top:20px;
    
}
.col-xs-8{
    text-align:center;
    margin-left:600px;
    margin-top:-190px;
    
   
}
.date {
    float: right;
    margin-top: 118px;
    margin-right: 197px;
}
.purchase_method1{
    border:3px solid black;
    margin-top:80px;
    
}
.purchase_method2 {
    border: 1px solid black;
    margin-top: 33px;
    width: 392px;
    margin-left: 0px;
}
.purchase_method3 {
    border: 1px solid black;
    margin-top: 33px;
    width: 197px;
    margin-left: 0px;
}
.purchase_method4 {
    border: 1px solid black;
    margin-top: 95px;
    width: 781px;
    margin-left: 15px;
    height: 61px;
}
.purchase_method5 {
    border: 1px solid black;
    margin-top: -61px;
    width: 196px;
    margin-left: 796px;
    height: 61px;
}

.purchase_method6 {
    border: 1px solid black;
    margin-top: 11px;
    width: 250px;
    margin-left: 15px;
    height: 31px;
}
.purchase_method8 {
    border: 1px solid black;
    margin-top: 0px;
    width: 250px;
    margin-left: 15px;
    height: 31px;
}
.purchase_method9 {
    border: 1px solid black;
    width: 728px;
    margin-left: 265px;
    height: 62px;
    margin-bottom: 41px;
    margin-top: -62px;
}
.purchase_method7 {
    
    border: 1px solid black;
    margin-top: -2px;
    width: 303px;
    margin-left: 454px;
    height: 43px;
    text-align: center;
    font-size: 25px;
}

.purchase_box1 {
    margin-left: 40px;
}
.purchase_box2 {
    margin-left: 40px;

}
.purchase_box7 {
    border: 1px solid black;
    margin-top: -43px;
    width: 221px;
    margin-left: 756px;
    height: 43px;
    text-align: center;
    font-size: 27px;
}
tr.quantity_item th {
    text-align: center;
    padding: 7px;
    color: black;
}
.purchase-table {
    border: 1px solid black;
    margin-top: -10px;
    width: 979px;
    height: 509px;
}
.quantity_subitem{
    text-align:center;
    padding:7px;
}

.purchase_last {
    font-size: 107%;
    padding: 104px;
    text-align: center;
    margin-right: 238px;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  
}
.purchase-address {
    margin-right: 143px;
    margin-top:35px;
}
.quantity_subitem td {
    padding: 7px;
    font-size: 15px;
}
tr.quantity_subitem.quantity_subitem1 td {
    padding-bottom: 450px;
    font-size: 15px;
}


tr.quantity_subitem td {
    border-top: 0px !important;
    border-bottom: 0px !important;
}
.center1 {
    width: 45%;
    display: block;
    margin-left: auto;
    margin-right: auto;
}
.d_address {
    margin-right: 48px;
}
.col-xs-6{
   
    margin-top :50px;
}
</style>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-xs-12">
    <img alt="Logo" src="{{url('authImage/0001.jpg')}}" class="center1" />
    		<div class="invoice-title">
    			<h2>P u r c h a s e  o r d e r</h2>
                <div class="date">
    					<strong>Date :</strong>
    					10/12/21
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<strong>Supplier Name:</strong><br>
    					John Smith<br>
    			</div>
    			<div class="col-xs-7">
    					<strong>Delivery Address:</strong><br>
                        <div class="d_address">
                            John Smith<br>
                            1234 Main<br>
                            abc Apt. 4B<br>
                        </div>
    			</div>
                <div class="col-xs-8">
    					<div class="purchase-address">
                            <strong>Invoice Address:</strong><br>
                            <div class="d_address">
                                John Smith<br>
                                1234 Main<br>
                                abc Apt. 4B<br>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
            <div class="col-md-2">
                <div class="purchase_method2">
                   <div class="purchase_box1">
                     <b>Order Number</b><br><br>
                     <b>122</b>
                   </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="purchase_method2">
                   <div class="purchase_box1">
                     <b>Supplier ID</b><br><br>
                     <b>122</b>
                   </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="purchase_method2">
                   <div class="purchase_box1">
                     <b>Order Date</b><br><br>
                     <b>122</b>
                   </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="purchase_method2">
                   <div class="purchase_box1">
                     <b>Delivery Date</b><br><br>
                     <b>122</b>
                   </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="purchase_method3">
                   <div class="purchase_box1">
                        <b>Delivery Method</b><br><br>
                        <b>122</b>
                   </div>
                </div>
            </div>
            <div class="purchase_method4">
                <div class="purchase_box2">
                    <b>Delivery Terms</b><br><br>
                    <b>122</b>
                </div>
            </div>
            <div class="purchase_method5">
                <div class="purchase_box1">
                    <b>Payment Terms</b><br><br>
                    <b>122</b>
                </div>
            </div>
            
            <div class="purchase_method6">
                <div class="purchase_box1">
                    <b>Our Reference:</b>
                </div>
            </div>
            <div class="purchase_method8">
                <div class="purchase_box1">
                    <b>Your Reference:</b>
                </div>
            </div>
            <div class="purchase_method9">
                <div class="purchase_box1">
                    <b>Your Reference:</b>
                </div>
            </div>
    </div>
    <div class="row">
    	<div class="col-md-11">
    		<div class="purchase-table">
                <table style="width:100%">
                    <tr class="quantity_item">
                        <th>Quantity</th>
                        <th>Job Description</th>
                        <th>Amount</th>
                        <th>Unit</th>
                        <th>Unite Price(exc VAT)</th>
                        <th>Total Price(exc VAT)</th>
                     
                    </tr>
                    <tr class="quantity_subitem quantity_subitem1 table-tr">      
                        <td>100</td>
                        <td>Business cards (VAT 20%)</td>
                        <td>£50.00</td>
                        <td>£50.00</td>
                        <td>£50.00</td>
                        <td>£50.00</td>
                    </tr>
                </table>

                <!-- <div class="total-order"> -->
                <div class="purchase_method7">
                      <b>Total Order:</b><br>
                </div>
                <div class="purchase_box7">
                   <b>120</b><br>
                </div>
    		</div>
    	</div>
    </div>
    <div class="purchase_last"><b>7 NORBRECK PARADE, LONDON NW10 7HR<br>
                                TEL: 020 8838 2747 | EMAIL: ADMIN@SMARTPRINTLTD.CO.UK | COMPANY REG. NO. 08523409<br>
                                SMART PRINT & MEDIA LTD. T/A SMART PRINT</b></div>
    		</div>
</div>

    	
    					

                   









    					
    					
                
    				
    					
                           
                           
    				
                        








  
               
               
        
 
                   


                       
                     
                    
                   
                 
               

                   
                  
                    




                        



                      



               
                        
                    




      

            
         




            
            
        
    



        
        
        
        

    
               
    				
    
    	


                    

             
               
                
              
    	
               
    			
    			
    		  
               
    			
    	   
               
    			
    			
    				
    			
    				
				
               
                

    				
    				
               
                
		
    				
    				
    				
               
                
