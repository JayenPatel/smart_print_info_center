@extends('admin.layouts.app')
@section('title','Create Account - Smart Print')
@section('content')

<style type="text/css">
    th{
            background-color: #353B84 !important;
            text-align: center !important;
            color: white !important;
        }
        td{
            text-align: center !important;
            border: 0.1px solid #353B84 !important;
            background-color: #f0eff5ba;
            color: #353B84 !important;
        }
        .radio-m{
            margin-left: 200px;
        }
        .radio-m-d{
            margin-left: 220px;
        }
        @media only screen and (max-width: 425px) {
            .box-payDetail{
                margin: 0px 10px;
            }
            .datebtn{
                margin: 5px 0 0 11px;
            }
            .radio-m{
                margin-left: 150px !important;
            }
            .radio-m-d{
                margin-left: 175px !important;
            }
        }
</style>

<!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                    @if(isset($account)) Edit @else New @endif Account
                </h5>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <div class="d-flex flex-column-fluid">
        <div class=" container ">
            <div class="form-group col-lg-4 mt-3">
                <div class="row">
                    <div class="col-lg-3">
                        <label class=" col-form-label text-right">Customer:</label>
                    </div>
                    <div class="col-lg-9 selectOption">
                        <input class="form-control input optionInput" type="text" style="display: none"/>
                        <select data-live-search='true' class="form-control selectpicker selectedOption required"
                                id="customer_id"
                                name="customer_id">
                            <option></option>
                            @foreach($data['customer'] as $category)
                            @if(isset($data['details']) && $category->customer_id == $data['details']->customer_id)
                            <option selected="selected" value="{{$category->customer_id }}">{{
                                $category->business_name
                                }}
                            </option>
                            @else
                            <option value="{{$category->customer_id}}">{{ $category->business_name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title" style="font-size: 20px;color: #353B84!important;">
                        <strong>Outstanding Final Invoices:0</strong>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table table-bordered table-checkable dataTable no-footer dtr-inline" id="accountsDT">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Type</th>
                                <th>Discription</th>
                                <th>Credit</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>TOTAL CREDIT:</td>
                                <td>£0.00</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <!--end: Datatable-->
                </div>
                <div class="card-body">
                    <div class="form-group row align-items-center">
                        <div class="form-group col-md-6">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <strong style="font-size: 13px;">Credit/Payment</strong>
                                </div>
                                <div class="col-8">
                                    <!-- Button trigger modal-->
                                    <button type="button" class="btn btn__bg" data-toggle="modal" data-target="#exampleModalCenter">
                                        New Entry (for Payment/credit)
                                    </button>

                                    <!-- Modal-->
                                    <div class="modal fade" id="exampleModalCenter" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header acc_popup">
                                                    <h5 class="modal-title" style="color: white; margin: 0 auto;">Enter/Update Payment/Credit</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <i aria-hidden="true" class="ki ki-close" style="color: white!important;"></i>
                                                    </button>
                                                </div>
                                                <!-- <div class="acc_popup"></div> -->
                                                <div style="padding: 25px 0">
                                                    <div id="isDeposit">
                                                        <div class="text-center" style="color: black;font-weight: 700">Is it Deposit?</div>
                                                        <div class="form-group text-center">
                                                            <div class="radio-inline">
                                                                <label class="radio">
                                                                    <input type="radio" value="yes" name="deposit" /> Yes
                                                                    <span></span>
                                                                </label>
                                                                <label class="radio">
                                                                    <input type="radio" value="no" name="deposit" /> No
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="text-center" style="padding-bottom: 22px;">
                                                            <button type="button" class="btn btn__bg font-weight-bold">Save</button>
                                                            <button type="button" class="btn btn__bg font-weight-bold" id="pop1" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group text-center" id="deposit_typ">
                                                        <div class="radio-inline">
                                                            <label class="radio">
                                                                <input type="radio" value="payment" checked name="deposit_typ" /> Payment
                                                                <span></span>
                                                            </label>
                                                            <label class="radio">
                                                                <input type="radio" value="creditnote" name="deposit_typ" /> CreditNote
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                        <div class="row justify-content-center" id="paymentTYPE" style="margin-top: 10px;">
                                                            <label class="col-md-4 col-form-label text-right">Payment Type:</label>
                                                            <div class="col-md-6">
                                                                <select class="form-control input">
                                                                    <option>Choose one</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="text-center" style="padding-bottom: 22px;margin-top: 22px;">
                                                            <button type="button" class="btn btn__bg font-weight-bold">Save</button>
                                                            <button type="button" class="btn btn__bg font-weight-bold" id="pop2" name="pop2">Cancel</button>
                                                        </div>
                                                    </div>
                                                    <div class="box-payDetail" id="payDetail">
                                                        <div class="row">
                                                            <label class="col-md-4 col-form-label text-right" style="margin-left: 8px;">Address 1:</label>
                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-md-4 col-form-label text-right" style="margin-left: 8px;">Amount:</label>
                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-md-4 col-form-label text-right" style="margin-left: 8px;">Pay Date:</label>
                                                            <div class="col-md-5">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input" id="time-holder" readonly />
                                                                </div>
                                                            </div>
                                                            <button class="btn btn__bg datebtn" id="time" style="height: 37px;">Today</button>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-md-4 col-form-label text-right" style="margin-left: 8px;">Description:</label>
                                                            <div class="col-md-7">
                                                                <div class="input-group">
                                                                    <textarea class="form-control input" rows="3"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center" style="padding-bottom: 22px;margin-top: 22px;">
                                                            <button type="button" class="btn btn__bg font-weight-bold">Save</button>
                                                            <button type="button" class="btn btn__bg font-weight-bold" id="pop3" name="pop3">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- <div class="text-center" style="padding-bottom: 22px;">
                                                    <button type="button" class="btn btn__bg font-weight-bold">Save</button>
                                                    <button type="button" class="btn btn__bg font-weight-bold" data-dismiss="modal">Cancel</button>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end model -->
                                </div>
                            </div>

                        </div>
                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="col-6"></div>
                                <div class="col-6 highlight" style="padding: 10px 25px;">
                                    <strong>Amount to pay: £0.00 </strong><button class="btn btn__bg">PAY</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
@endsection

@push('script')
    <script type="text/javascript">
        $('input[name="deposit"]').click(function(e) {
          if(e.target.value === 'no') {
            $('#deposit_typ').show();
            $('#isDeposit').hide();

          } else {
            $('#deposit_typ').hide();
            $('#payDetail').hide();
          }
        })
        $('#deposit_typ').hide();

        $('input[name="deposit_typ"]').click(function(e) {
          if(e.target.value === 'creditnote') {
            $('#payDetail').show();
            $('#deposit_typ').hide();
          } else {
            $('#payDetail').hide();
            $('#deposit_typ').show();
          }
        })
        $('#payDetail').hide();

        $('button[name="pop3"]').click(function(){
            $("#deposit_typ").show();
            $("#payDetail").hide();
        });
        $('button[name="pop2"]').click(function(){
            $("#isDeposit").show();
            $("#deposit_typ").hide();
        });

        // btn click get date
        $(function(){
            $('#time').click(function(){
                var time = new Date();
                let formatted_date = time.getDate() + "/" + (time.getMonth() + 1) + "/" + time.getFullYear()
                $('#time-holder').val(formatted_date);
            });
        });

        // dataTable
        $("#accountsDT").DataTable({
            scrollY: "50vh",
            scrollX: !0,
            scrollCollapse: !0,
        });
    </script>
@endpush
