<style type="text/css">
    .text-muted{
        color: white !important;
    }
    .menu-txt-color{
        color: white !important;
    }
    .menu-m{
        margin-top: 3px;
    }
    .menu-m-5{
        margin-left: 5px;
    }
    input:disabled{
        background-color: rgba(255, 255, 255, 0.5) !important;
    }
    .checkbox>input:disabled:checked~span {
        background-color: gray !important;
    }
    .menu-m-7{
        margin-left: 7px;
    }
    .icon-m-c{
        padding: 17px 18px !important;
    }
    @media only screen and (max-width: 768px) {
        .text-muted{
            color: #353B84 !important;
        }
        .menu-txt-color{
            color: #353B84 !important;
        }
        .header-top-menu{
            margin: 0 !important;
        }
        .menu-m-7{
            margin: 0 !important;
        }
        .menu-m-5{
            margin: 0 !important;
        }
    }

</style>
{!! Session('success') !!}

<!--begin::Wrapper-->
<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
    <!--begin::Header-->
    <div id="kt_header" class="header  header-fixed " >
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                <!--begin::Header Menu-->
                <div id="kt_header_menu" class="header-menu header-menu-mobile  header-menu-layout-default " >
                    <!--begin::Header Nav-->
                    <ul class="menu-nav ">
                        <li class="menu-item">
                            <div class="menu-m">
                                <a href="{{ route('admin.header.print-job') }}" class="menu-link header-top-menu">
                                    <span class="menu-text"><i class="fa fa-print" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                                </a>
                                <span class="menu-txt-color">Print Job</span>
                            </div>
                        </li>
                        <li class="menu-item">
                            <div class="menu-m">
                                <a href="{{ route('admin.header.calendar') }}" class="menu-link header-top-menu icon-m-c">
                                    <span class="menu-text"><i class="fa fa-calendar" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                                </a>
                            <span class="menu-txt-color">Calendar</span>
                            </div>
                        </li>
                        <li class="menu-item">
                            <div class="menu-m">
                                <a href="{{ route('admin.header.other-job') }}" class="menu-link header-top-menu">
                                    <span class="menu-text"><i class="fa fa-users" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                                </a>
                            <span class="menu-txt-color">Other Job</span>
                            </div>
                        </li>
                        <li class="menu-item">
                            <div class="menu-m">
                                <a href="{{ route('admin.header.web-job') }}" class="menu-link header-top-menu" style="padding: 17px 21px !important">
                                    <span class="menu-text"><i class="fa fa-user" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                                </a>
                            <span class="menu-txt-color menu-m-5">Web Job</span>
                            </div>
                        </li>
                        <li class="menu-item">
                            <div class="menu-m">
                                <a href="{{ route('admin.header.quotes') }}" class="menu-link header-top-menu">
                                    <span class="menu-text"><i class="fa fa-quote-left" aria-hidden="true"></i></span><i class="menu-arrow"></i>
                                </a>
                            <span class="menu-txt-color menu-m-7">Quotes</span>
                            </div>
                        </li>
                    </ul>
                    <!--end::Header Nav-->
                </div>
                <!--end::Header Menu-->
            </div>
            <!--end::Header Menu Wrapper-->

            <!--begin::Topbar-->
            <div class="topbar">
                <div class="topbar-item">
                    <!--begin::Languages-->
                    <div class="dropdown">
                        <!--begin::Toggle-->
                        <div class="topbar-item">

                        </div>
                        <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px" style="border: none !important;">
                            <div class="btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2" style="border: none !important;cursor: pointer;">
                                <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1"></span>
                                <span style="color: #9a9bc7!important" class="font-weight-bolder font-size-base d-none d-md-inline mr-3">Pofile</span>
                                <div class="symbol symbol-35 symbol-light-success">
                                    <div class="symbol-label" style="background-image:url('{{ asset('headerImage/100_5.png')  }}')"></div>
                                    <i class="symbol-badge bg-success"></i>
                                </div>
                            </div>
                            <!-- <div class="btn btn-dropdown btn-lg mr-1">
                                <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Logout</span><i class="menu-arrow" style="color: white !important;"></i>
                            </div> -->
                        </div>
                        <!--end::Toggle-->

                        <!--begin::Dropdown-->
                        <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                            <!--begin:Header-->
                            <div class="d-flex flex-column flex-center py-10 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url('{{ asset('headerImage/bg-1.jpg') }}')">
                                <h4 class="text-white font-weight-bold">
                                    Welcome {{session()->get('userData')->firstName}} {!! session()->get('userData')->lastName !!}
                                </h4>
                                <div class="symbol symbol-70 mr-5">
                                    <div class="symbol-label" style="background-image:url('{{ asset('headerImage/100_5.png')  }}')"></div>
                                    <i class="symbol-badge bg-success"></i>
                                    <hr>
                                    <a href="{{url('admin/profile/edit/'.session()->get('userData')->staffId)}}" style="font-weight: bold;color: white;">Edit Profile</a>
                                </div>
                            </div>
                            <!--end:Header-->
                            <!--begin:Nav-->
                            <div class="row row-paddingless">
                                <!--begin:Item-->
                                <div class="col-12">
                                    <span class="d-block py-10 px-5 text-center bg-hover-light border-right border-bottom">
                                        <!-- <span class="d-block text-dark-75 font-weight-bold font-size-h6 mt-2 mb-1">Title</span> -->
                                        <span class="d-block text-dark-50 font-size-lg">
                                            <?php echo date('l'); ?>, <?php echo date('d'); ?> <?php echo date('F'); ?> <?php echo date('Y'); ?></span>
                                        <a href="{{url('/logout')}}" class="btn btn-success btn-sm font-weight-bold font-size-sm mt-2">Logout</a>
                                    </span>
                                </div>
                                <!--end:Item-->
                            </div>
                        <!--end:Nav-->
                        </div>
                        <!--end::Dropdown-->
                    </div>
                <!--end::Languages-->
                    <!-- <div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2 btn-bg" id="kt_quick_user_toggle">
                        <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Logout</span>
                    </div> -->
                </div>
            </div>
            <!--end::Topbar-->
        </div>
        <!--end::Container-->
    </div>
<!--end::Header-->

    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
