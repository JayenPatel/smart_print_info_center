@extends('admin.layouts.app')
@section('title','Create Web Job Sheet - Smart Print')
@section('content')
<style type="text/css">
</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                @if(isset($web)) Edit @else New @endif Web jobsheet
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->

<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <div class="card-body">
                <form class="form" id="jobForm" method="post">
                    <div class="row mb-10">
                        <label class="col-lg-3 col-form-label lable_highlight"><strong>Job Id:
                                {{$job['job_id']}}</strong></label>
                        <label class="col-lg-3 col-form-label lable_highlight"><strong>Reg.Date:
                                {{$job['reg_date']}} </strong></label>
                        <div class=" col-lg-2"></div>
                        <div class="form-group col-lg-4">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class=" col-form-label text-right">Customer:</label>
                                </div>
                                <div class="col-lg-9 selectOption">
                                    <input class="form-control input optionInput" type="text" style="display: none"/>
                                    <select class="form-control selectpicker selectedOption required" id="customer_id"
                                            name="customer_id">
                                        <option></option>
                                        @foreach($job['customer'] as $category)
                                        @if(isset($web) && $category->customer_id == $web->customer_id)
                                        <option selected="selected" value="{{$category->customer_id }}">{{ $category->business_name
                                            }}
                                        </option>
                                        @else
                                        <option value="{{$category->customer_id}}">{{ $category->business_name }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="reg_date" id="reg_date" value="{{$job['reg_date']}}">
                    <input type="hidden" name="job_id" id="job_id" value="{{$job['job_id']}}">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Category:</label>
                        <div class="col-lg-2 selectOption">
                            <input class="form-control input optionInput" type="text" style="display: none"/>
                            <select class="form-control selectpicker selectedOption required" id="category"
                                    name="category">
                                <option value="">Choose Category</option>
                                @foreach($job['category'] as $category)
                                @if(isset($web) && $category->name == $web->category)
                                <option selected="selected" value="{{$category->name }}">{{ $category->name }}
                                </option>
                                @else
                                <option value="{{$category->name}}">{{ $category->name }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2" id='normal'>
                            <a class="btn font-weight-bolder plus_minus_button  NewOptionCategory input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOptionCategory input btn-m">-</a>
                        </div>
                        <div class="col-lg-2" id='store' hidden="true">
                            <a class="btn font-weight-bolder plus_minus_button input btn-m saveOptionCategory">Add</a>
                            <a class="btn font-weight-bolder plus_minus_button input btn-m cancelOptionCategory">Cancel</a>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Status:</label>
                        <div class="col-lg-4">
                            <select class="form-control selectpicker required" id="status" name="status">

                                <option @if(isset($web) && $web->status =='A') selected @endif value="A">Active
                                </option>
                                <option @if(isset($web) && $web->status =='Q') selected @endif value="Q">Quote
                                </option>
                                <option @if(isset($web) && $web->status =='D') selected @endif value="D">Done
                                </option>
                                <option @if(isset($web) && $web->status =='C') selected @endif value="C">Cancelled
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Description:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" id="description" name="description"
                                       value="<?php if (isset($web)) {
                                           echo $web->description;
                                       } ?>" class="form-control input"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Quantity:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" name="quantity" value="<?php if (isset($web)) {
                                    echo $web->quantity;
                                } ?>" id="quantity" class="form-control required input"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Discount:</label>
                                    <div class="col-lg-3">
                                        <input class="form-control input_hightlight required"  maxlength="2" placeholder="%" id="number"
                                               value="<?php if (isset($web)) {
                                                   echo $web->discount;
                                               } ?>" name="discount" type="text"/>
                                    </div>
                                    <div class="col-lg-5"></div>
                     </div>
                    <div class="highlight">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="col-lg-6 col-form-label text-right"></label>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Net Price:</label>
                                    <div class="col-lg-3">
                                        <input class="form-control input_hightlight required" id="net_price"
                                               value="<?php if (isset($web)) {
                                                   echo $web->net_price;
                                               } ?>" name="net_price" type="text"/>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">VAT:</label>
                                      <div class="col-lg-3">
                                        <div class="radio-inline">
                                            
                                            <label class="radio">
                                                <input type="radio" name="vat" class="vat_radio vat_per_radio" data-vat_per="1" value="<?php if (isset($web)) {
                                                   echo $web->vat;
                                               } ?>" @if(isset($web) ) checked @endif/>
                                                <p class="vat_text"><?php if (isset($web)) {
                                                   echo $web->vat;
                                               } ?>%</p>
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" data-vat_per="0" class="vat_radio" name="vat" value="0" @if(isset($web) && $web->vat
                                                ==0) checked @endif/>
                                                0 %
                                                <span></span>
                                            </label>
                                            <input type="text" class="form-control input_hightlight" maxlength="2" value="<?php if (isset($web)) {
                                                   echo $web->vat;
                                               } ?>" style="display: none;" type="number" id="vat_cal_hidden" >
                                            </br><button type="button" class="btn btn-primary"  type="text" id="vat_btn_hidden">update</button>
                                            <button type="button" class="btn btn-primary"  type="text" id="vat_cancel_hidden">Cancel</button>
                                        </div>



                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Price Comments:</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control input_hightlight" name="comments" id="comments"
                                                  rows="3">@if(isset($web)) {{$web->comments}} @endif</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="highlight">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="col-form-label text-right lable_highlight"><strong>EXTRAS</strong></label>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Description:</label>
                                    <div class="col-lg-8">
                                        <input class="form-control input_hightlight character" name="description_extra"
                                               id="description_extra"
                                               type="text"/>
                                        <span class="form-text text-muted error" id="extra_description_error">Please enter description</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">VAT:</label>
                                    <div class="col-lg-3">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" name="vat_extra" class="vat_extra vat_per_radio" data-vat_per="1"/>
                                                <p class="vat_text_extra"> %</p>
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="vat_extra" data-vat_per="0" class="vat_extra" value="0"/>
                                                0 %
                                                <span></span>
                                            </label>
                                        </div>
                                        <input type="text"  class="form-control input_hightlight" value="" style="display: none;" maxlength="2"  id="vat_extra_hidden" >
                                        </br><button type="button" class="btn btn-primary"  type="text" id="vat_extra_btn_hidden">update</button>
                                            <button type="button" class="btn btn-primary"  type="text" id="vat_extra_cancel_hidden">Cancel</button>
                                              
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Net_Price:</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control input_hightlight numeric"
                                               name="net_price_extra" id="net_price_extra"/>
                                        <span class="form-text text-muted error" id="extra_net_price_error">Please enter net price</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button type="button" class="btn btn__bg mr-2 btn-m" id="add_extra">Add Extra
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row" id="table_row">
                                    <div class="col-lg-12">
                                        <table class="table table-separate table-head-custom table-checkable"
                                               id="datatable1">
                                            <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>VAT</th>
                                                <th>Net_Price</th>
                                                
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <form id="webForm">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">URL:</label>
                        <div class="col-lg-4">
                            <input type="text" value="<?php if (isset($web)) {
                                echo $web->url;
                            } ?>" name="url" id="url" class="form-control input"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Ftp:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input value="<?php if (isset($web)) {
                                    echo $web->ftp;
                                } ?>" name="ftp" id="ftp" type="text" class="form-control input"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Expiry date:</label>
                        <div class="col-lg-4">
                            <input value="<?php if (isset($web)) {
                                echo $web->name_expiry_date;
                            } ?>" name="name_expiry_date" id="name_expiry_date" type="text" class="form-control input"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Ftp Username:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input value="<?php if (isset($web)) {
                                    echo $web->ftp_username;
                                } ?>" name="ftp_username" id="ftp_username" type="text" class="form-control input"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Domain tag:</label>
                        <div class="col-lg-4">
                            <input value="<?php if (isset($web)) {
                                echo $web->domain_tag;
                            } ?>"  id="domain_tag" name="domain_tag" type="text" class="form-control input"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">P3 server address:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input value="<?php if (isset($web)) {
                                    echo $web->p3_server_address;
                                } ?>"  name="p3_server_address" id="p3_server_address" type="text" class="form-control input"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Hosting Company:</label>
                        <div class="col-lg-4">
                            <input value="<?php if (isset($web)) {
                                echo $web->hosting_company;
                            } ?>"  name="hosting_company" id="hosting_company" type="text" class="form-control input"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">P3 username:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input value="<?php if (isset($web)) {
                                    echo $web->p3_username;
                                } ?>"  name="p3_username" id="p3_username" type="text" class="form-control input"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Account Type:</label>
                        <div class="col-lg-4">
                            <select name="accoun_type" id="accoun_type" class="form-control selectpicker">
                                @foreach($job['accoun_type'] as $category)
                                @if(isset($web) && $category->accoun_type ==
                                $web->accoun_type)
                                <option selected="selected" value="{{$category->accoun_type }}">{{
                                    $category->accoun_type }}
                                </option>
                                @else
                                <option value="{{$category->accoun_type}}">{{
                                    $category->accoun_type }}
                                </option>
                                @endif
                                @endforeach
                            </select>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Comments:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input  value="<?php if (isset($web)) {
                                    echo $web->comments;
                                } ?>"  name="comments" id="comments" type="text" class="form-control input"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Account Username:</label>
                        <div class="col-lg-4">
                            <input  value="<?php if (isset($web)) {
                                echo $web->account_username;
                            } ?>" id="account_username" name="account_username" type="text" class="form-control input"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Expired:</label>
                        <div class="col-lg-4">
                            <label class="checkbox checkbox_top_margin">
                                <input name="expired"  @if(isset($web) && $web->expired ==1) checked @endif id="expired" type="checkbox" value="1" class="checkable input">
                                <span></span>
                            </label>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Account password:</label>
                        <div class="col-lg-4">
                            <input  value="<?php if (isset($web)) {
                                echo $web->account_password;
                            } ?>" id="account_password" name="account_password" type="text" class="form-control input"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-10">
                        <a class="btn btn__bg" href="{{ route('admin.jobs.web_job_sheets.list') }}"> Cancel</a>
                        <button id="submitWebJobBtn" class="btn btn__bg mr-2 ">@if(isset($customer)) Update @else Add @endif</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Container-->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.numeric').on('input', function (event) {
                this.value = this.value.replace(/[^0-9]/g, '');
            });
            $('.percent').on('input', function (event) {
                this.value = this.value.replace(/[^0-9\.\%]/g, '');
            });
            $('.character').on('input', function (event) {
                this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
            });
        });
    </script>
    @endsection
    @push('script')

    <script type="text/javascript">
        var data = <?= !empty($web) ? json_encode($web) : '""' ?>;
        if($('#name_expiry_date').val() !='')
        {
            $('#name_expiry_date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            }).datepicker('setDate', new Date($('#name_expiry_date').val()));
        }else {
            $('#name_expiry_date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
        }



        $("#number").keypress(function (e){
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
       }
    });

    $("#vat_cal_hidden").keypress(function (e){
       var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
   }
  });
  $("#vat_extra_hidden").keypress(function (e){
       var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
   }
  });

  $("body").on("click", ".NewOptionCategory", function() {
       
       $('#store').attr("hidden",false);
       $('#normal').attr("hidden",true);
       $(this).parent().parent().find('.selectOption').find('.optionInput').show();
       $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
   });
   $("body").on("click", ".cancelOptionCategory", function() {
       $('#store').attr("hidden",true);
       $('#normal').attr("hidden",false);
       $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
       $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
   });
 $("body").on("click", ".saveOptionCategory", function() {
       var val = $(this).parent().parent().find('.optionInput').val();
       $(this).parent().parent().find('.optionInput').val('');
       if ($.trim(val) != '') {
           console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
           $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
           $(this).parent().parent().find('.cancelOptionCategory').click();

           //var name=$('#categorytxt').val();
           console.log(val);
           var post_data = new FormData();
           post_data.append('type', 'web');
           post_data.append('name', val);
           post_data.append('status', 1);
           console.log(post_data);
           ajax_request('/admin/job/category', post_data, getCategoryDetails, null);
       } else {
           toast_error('Please Enter Valid Value');
       }

   });
   function getCategoryDetails(result)
   {
       if (result.status === "success") {
       toast_success(result.message);
   } else {
       toast_error(result.message);
   }
   }
   function removecategoryresponse(result)
    {
        if (result.status === "success") {
        toast_success(result.message);
        setTimeout(function() {
            location.reload();
        }, 1200)
        } else {
            toast_error(result.message);
        }
    }
   $("body").on("click", ".removeOptionCategory", function() {
            var value=$('#category').val(); 
            var post_data = new FormData();
           post_data.append('type', 'web');
           post_data.append('name', value);
           console.log(post_data);
           deleteswal('/admin/job/removecategory', post_data, removecategoryresponse, null);
           //ajax_request('/admin/job/removecategory', post_data, getCategoryDetails, null);
        //$(this).parent().parent().find('.selectedOption').selectpicker('val', '');

    });
    </script>
    <script src="/js/pages/jobs/jobs.js"></script>

    @endpush
