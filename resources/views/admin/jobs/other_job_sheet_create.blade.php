@extends('admin.layouts.app')
@section('title','Create Other Job Sheet - Smart Print')
@section('content')

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                @if(isset($otherjob)) Edit @else New @endif Other Job Sheet
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->

<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <div class="card-body">
                <form class="form" id="jobForm" method="post">
                    <div class="row mb-10">
                        <label class="col-lg-3 col-form-label lable_highlight"><strong>Job Id:
                                {{$job['job_id']}}</strong></label>
                        <label class="col-lg-3 col-form-label lable_highlight"><strong>Reg.Date:
                                {{$job['reg_date']}} </strong></label>
                        <div class=" col-lg-2"></div>
                        <div class="form-group col-lg-4">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class=" col-form-label text-right">Customer:</label>
                                </div>
                                <div class="col-lg-9 selectOption">
                                    <input class="form-control input optionInput" type="text" style="display: none"/>
                                    <select class="form-control selectpicker selectedOption required" id="customer_id"
                                            name="customer_id">
                                        <option></option>
                                        @foreach($job['customer'] as $category)
                                        @if(isset($other) && $category->customer_id == $other->customer_id)
                                        <option selected="selected" value="{{$category->customer_id }}">{{ $category->business_name
                                            }}
                                        </option>
                                        @else
                                        <option value="{{$category->customer_id}}">{{ $category->business_name }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="reg_date" id="reg_date" value="{{$job['reg_date']}}">
                    <input type="hidden" name="job_id" id="job_id" value="{{$job['job_id']}}">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Job Title:</label>
                        <div class="col-lg-5 selectOption">
                            <input class="form-control input optionInput" type="text" style="display: none"/>
                            <select class="form-control selectpicker selectedOption required" id="job_title"
                                    name="job_title">
                                    <option value="">Choose Job Title</option>
                                    @foreach($job['job_title'] as $category)
                                @if(isset($other) && $category->job_title == $other->job_title)
                                <option selected="selected" value="{{$category->job_title }}">{{ $category->job_title }}
                                </option>
                                @else
                                <option value="{{$category->job_title}}">{{ $category->job_title }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2" id='normaljobtitle'>
                            <a class="btn font-weight-bolder plus_minus_button  NewOptionJobtitle input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOptionJobtitle input btn-m">-</a>
                        </div>
                        <div class="col-lg-2" id='storejobtitle' hidden="true">
                            <a class="btn font-weight-bolder plus_minus_button input btn-m saveOptionJobtitle">Add</a>
                            <a class="btn font-weight-bolder plus_minus_button input btn-m cancelOptionJobtitle">Cancel</a>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Colour:</label>
                        <div class="col-lg-2 selectOption">
                            <input class="form-control input optionInput"  type="text" style="display: none"/>
                            <select class="form-control selectpicker selectedOption required" id="colours"
                                    name="colours">
                                    <option value="">Choose Colour</option>
                                @foreach($job['colors'] as $category)
                                @if(isset($other) && $category->colour == $other->colours)
                                <option selected="selected" value="{{$category->colour }}">{{ $category->colour }}
                                </option>
                                @else
                                <option value="{{$category->colour}}">{{ $category->colour }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2" id='normalcolour'>
                            <a class="btn font-weight-bolder plus_minus_button  NewOptionColour input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOptionColour input btn-m">-</a>
                        </div>
                        <div class="col-lg-2" id='storecolour' hidden="true">
                            <a class="btn font-weight-bolder plus_minus_button input btn-m saveOptionColour">Add</a>
                            <a class="btn font-weight-bolder plus_minus_button input btn-m cancelOptionColour">Cancel</a>
                        </div>
                    </div>  
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Category:</label>
                        <div class="col-lg-2 selectOption">
                            <input class="form-control input optionInput" id='category_txt' type="text" style="display: none"/>
                            <select class="form-control selectpicker selectedOption required" id="category_select"
                                    name="category">
                                <option value="">Choose Category</option>
                                @foreach($job['category'] as $category)
                                @if(isset($other) && $category->name == $other->category)
                                <option selected="selected" value="{{$category->name }}">{{ $category->name }}
                                </option>
                                @else
                                <option value="{{$category->name}}">{{ $category->name }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2" id='normal'>
                            <a class="btn font-weight-bolder plus_minus_button  NewOptionCategory input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOptionCategory input btn-m">-</a>
                        </div>
                        <div class="col-lg-2" id='store' hidden="true">
                            <a class="btn font-weight-bolder plus_minus_button input btn-m saveOptionCategory">Add</a>
                            <a class="btn font-weight-bolder plus_minus_button input btn-m cancelOptionCategory">Cancel</a>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Quantity:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="number" name="quantity" value="<?php if (isset($other)) {
                                    echo $other->quantity;
                                } ?>" id="quantity" class="form-control required input"/>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Description:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <textarea type="text" id="description" name="description"
                                       value="<?php if (isset($other)) {
                                           echo $other->description;
                                       } ?>" class="form-control input"><?php if (isset($other)) {
                                           echo $other->description;
                                       } ?></textarea>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Status:</label>
                        <div class="col-lg-4">
                            <select class="form-control selectpicker required" id="status" name="status">

                                <option @if(isset($other) && $other->status =='A') selected @endif value="A">Active
                                </option>
                                <option @if(isset($other) && $other->status =='Q') selected @endif value="Q">Quote
                                </option>
                                <option @if(isset($other) && $other->status =='D') selected @endif value="D">Done
                                </option>
                                <option @if(isset($other) && $other->status =='C') selected @endif value="C">Cancelled
                                </option>
                            </select>
                        </div><label class="col-lg-2 col-form-label text-right">Comment:</label>
                        <div class="col-lg-4">
                                        <textarea class="form-control input_hightlight" name="comment" id="comment"
                                                  rows="3">@if(isset($other)) {{$other->comment}} @endif</textarea>
                        </div>
                    </div>
                
                    <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Discount:</label>
                                    <div class="col-lg-3">
                                        <input class="form-control input_hightlight required"  maxlength="2" placeholder="%" id="number"
                                               value="<?php if (isset($other)) {
                                                   echo $other->discount;
                                               } ?>" name="discount" type="text"/>
                                    </div>
                                    <div class="col-lg-5"></div>
                     </div>
                    <div class="highlight">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="col-lg-6 col-form-label text-right"></label>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Net Price:</label>
                                    <div class="col-lg-3">
                                        <input class="form-control input_hightlight required" id="net_price"
                                               value="<?php if (isset($other)) {
                                                   echo $other->net_price;
                                               } ?>" name="net_price" type="number"/>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                                

                                <div class="form-group row vat_div">
                                    <label class="col-lg-4 col-form-label text-right">VAT:</label>
                                    <div class="col-lg-3">
                                        <div class="radio-inline">
                                            
                                            <label class="radio">
                                                <input type="radio" name="vat" class="vat_radio vat_per_radio" data-vat_per="1" value="<?php if (isset($other)) {
                                                   echo $other->vat;
                                               } ?>" @if(isset($other) ) checked @endif/>
                                                <p class="vat_text"><?php if (isset($other)) {
                                                   echo $other->vat;
                                               } ?>%</p>
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" data-vat_per="0" class="vat_radio" name="vat" value="0" @if(isset($other) && $other->vat
                                                ==0) checked @endif/>
                                                0 %
                                                <span></span>
                                            </label>
                                            <input class="form-control input_hightlight" maxlength="2" value="<?php if (isset($other)) {
                                                   echo $other->vat;
                                               } ?>" style="display: none;" type="text" id="vat_cal_hidden" >
                                            </br><button type="button" class="btn btn-primary"  type="text" id="vat_btn_hidden">update</button>
                                            <button type="button" class="btn btn-primary"  type="text" id="vat_cancel_hidden">Cancel</button>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Price Comments:</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control input_hightlight" name="comments" id="comments"
                                                  rows="3">@if(isset($other)) {{$other->comments}} @endif</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="highlight">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="col-form-label text-right lable_highlight"><strong>EXTRAS</strong></label>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Description:</label>
                                    <div class="col-lg-8">
                                        <input class="form-control input_hightlight character" name="description_extra"
                                               id="description_extra"
                                               type="text"/>
                                        <span class="form-text text-muted error" id="extra_description_error">Please enter description</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">VAT:</label>
                                    <div class="col-lg-3">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" name="vat_extra" class="vat_extra vat_per_radio" data-vat_per="1"/>
                                                <p class="vat_text_extra"> %</p>
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="vat_extra" data-vat_per="0" class="vat_extra" value="0"/>
                                                0 %
                                                <span></span>
                                            </label>
                                        </div>
                                        <input type="text"  class="form-control input_hightlight" value="" style="display: none;" maxlength="2" id="vat_extra_hidden" >
                                        </br><button type="button" class="btn btn-primary"  type="text" id="vat_extra_btn_hidden">update</button>
                                            <button type="button" class="btn btn-primary"  type="text" id="vat_extra_cancel_hidden">Cancel</button>
                                              
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Net_Price:</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control input_hightlight numeric"
                                               name="net_price_extra" id="net_price_extra"/>
                                        <span class="form-text text-muted error" id="extra_net_price_error">Please enter net price</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button type="button" class="btn btn__bg mr-2 btn-m" id="add_extra">Add Extra
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row" id="table_row">
                                    <div class="col-lg-12">
                                        <table class="table table-separate table-head-custom table-checkable"
                                               id="datatable1">
                                            <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>VAT</th>
                                                <th>Net_Price</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-10">
                        <a class="btn btn__bg" href="{{ route('admin.jobs.other_job_sheets.list') }}"> Cancel</a>
                        <button class="btn btn__bg mr-2 " id="submitOtherBtn">@if(isset($other)) Update @else Add @endif</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Container-->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    @endsection
    @push('script')

    <script type="text/javascript">
        var data = <?= !empty($web) ? json_encode($web) : '""' ?>;

        $("#number").keypress(function (e){
       var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
   }
  });
  $("#vat_cal_hidden").keypress(function (e){
       var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
   }
  });
  $("#vat_extra_hidden").keypress(function (e){
       var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
   }
  });
  $("body").on("click", ".NewOptionCategory", function() {
       
        $('#store').attr("hidden",false);
        $('#normal').attr("hidden",true);
        $(this).parent().parent().find('.selectOption').find('.optionInput').show();
        $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
    });
    $("body").on("click", ".cancelOptionCategory", function() {
        $('#store').attr("hidden",true);
        $('#normal').attr("hidden",false);
        $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
        $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
    });
  $("body").on("click", ".saveOptionCategory", function() {
        var val = $(this).parent().parent().find('.optionInput').val();
        $(this).parent().parent().find('.optionInput').val('');
        if ($.trim(val) != '') {
            console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
            $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
            $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
            $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
            $(this).parent().parent().find('.cancelOptionCategory').click();

            //var name=$('#categorytxt').val();
            console.log(val);
            var post_data = new FormData();
            post_data.append('type', 'other');
            post_data.append('name', val);
            post_data.append('status', 1);
            console.log(post_data);
            ajax_request('/admin/job/category', post_data, getCategoryDetails, null);
        } else {
            toast_error('Please Enter Valid Value');
        }

    });
    function getCategoryDetails(result)
    {
        if (result.status === "success") {
        toast_success(result.message);
    } else {
        toast_error(result.message);
    }
    }
    function removecategoryresponse(result)
    {
        if (result.status === "success") {
        toast_success(result.message);
        setTimeout(function() {
            location.reload();
        }, 1200)
        } else {
            toast_error(result.message);
        }
    }
    $("body").on("click", ".removeOptionCategory", function() {
            var value=$('#category_select').val(); 
            console.log(value);
            var post_data = new FormData();
           post_data.append('type', 'other');
           post_data.append('name', value);
           console.log(post_data);
           deleteswal('/admin/job/removecategory', post_data, removecategoryresponse, null);
           //ajax_request('/admin/job/removecategory', post_data, getCategoryDetails, null);
        //$(this).parent().parent().find('.selectedOption').selectpicker('val', '');

    });

    /**
 * 
 * Normal Colour ADD DELETE 
 * 
 */

$("body").on("click", ".NewOptionColour", function() {
       
       $('#storecolour').attr("hidden",false);
       $('#normalcolour').attr("hidden",true);
       $(this).parent().parent().find('.selectOption').find('.optionInput').show();
       $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
   });
   $("body").on("click", ".cancelOptionColour", function() {
       $('#storecolour').attr("hidden",true);
       $('#normalcolour').attr("hidden",false);
       $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
       $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
   });
 $("body").on("click", ".saveOptionColour", function() {
       var val = $(this).parent().parent().find('.optionInput').val();
       $(this).parent().parent().find('.optionInput').val('');
       if ($.trim(val) != '') {
           console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
           $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
           $(this).parent().parent().find('.cancelOptionColour').click();

           //var name=$('#categorytxt').val();
           console.log(val);
           var post_data = new FormData();
           post_data.append('type', 'other');
           post_data.append('colour', val);
           console.log(post_data);
           ajax_request('/admin/job/colour', post_data, getColourDetails, null);
       } else {
           toast_error('Please Enter Valid Value');
       }

   });
   function getColourDetails(result)
   {
       if (result.status === "success") {
       toast_success(result.message);
   } else {
       toast_error(result.message);
   }
   }
   function removecolourresponse(result)
    {
        if (result.status === "success") {
        toast_success(result.message);
        setTimeout(function() {
            location.reload();
        }, 1200)
        } else {
            toast_error(result.message);
        }
    }
   $("body").on("click", ".removeOptionColour", function() {
            var value=$('#colours').val(); 
            console.log(value);
            var post_data = new FormData();
           post_data.append('type', 'other');
           post_data.append('colour', value);
           console.log(post_data);
           deleteswal('/admin/job/removecolour', post_data, removecolourresponse, null);
    });

    /**
* 
* Jobtitle ADD DELETE 
* 
*/

$("body").on("click", ".NewOptionJobtitle", function() {
       
       $('#storejobtitle').attr("hidden",false);
       $('#normaljobtitle').attr("hidden",true);
       $(this).parent().parent().find('.selectOption').find('.optionInput').show();
       $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
   });
   $("body").on("click", ".cancelOptionJobtitle", function() {
       $('#storejobtitle').attr("hidden",true);
       $('#normaljobtitle').attr("hidden",false);
       $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
       $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
   });
 $("body").on("click", ".saveOptionJobtitle", function() {
       var val = $(this).parent().parent().find('.optionInput').val();
       $(this).parent().parent().find('.optionInput').val('');
       if ($.trim(val) != '') {
           console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
           $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
           $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
           $(this).parent().parent().find('.cancelOptionJobtitle').click();

           //var name=$('#categorytxt').val();
           console.log(val);
           var post_data = new FormData();
           post_data.append('type', 'other');
           post_data.append('job_title', val);
           console.log(post_data);
           ajax_request('/admin/job/jobtitle', post_data, getJobtitleDetails, null);
       } else {
           toast_error('Please Enter Valid Value');
       }

   });
   function getJobtitleDetails(result)
   {
       if (result.status === "success") {
       toast_success(result.message);
   } else {
       toast_error(result.message);
   }
   }
   function removejobtitleresponse(result)
    {
        if (result.status === "success") {
        toast_success(result.message);
        setTimeout(function() {
            location.reload();
        }, 1200)
        } else {
            toast_error(result.message);
        }
    }
   $("body").on("click", ".removeOptionJobtitle", function() {
            var value=$('#job_title').val(); 
            var post_data = new FormData();
           post_data.append('type', 'other');
           post_data.append('job_title', value);
           console.log(post_data);
           deleteswal('/admin/job/removejobtitle', post_data, removejobtitleresponse, null);
    });
    </script>
    <script src="/js/pages/jobs/jobs.js"></script>
    @endpush
