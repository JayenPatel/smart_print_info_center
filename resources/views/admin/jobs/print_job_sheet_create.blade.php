@extends('admin.layouts.app')
@section('title','Create Print Job - Smart Print')
@section('content')
<style type="text/css">
    @media only screen and (max-width: 425px) {
        .nav.nav-tabs.nav-tabs-line .nav-link {
            margin: 0 8px;
        }
    }
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                @if(isset($printjob)) Edit @else New @endif Print Jobsheet
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->

<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <div class="card-body">
                <form class="form" id="jobForm" method="post">
                    <div class="row mb-10">
                        <label class="col-lg-3 col-form-label lable_highlight"><strong>Job Id:
                                {{$job['job_id']}}</strong></label>
                        <label class="col-lg-3 col-form-label lable_highlight"><strong>Reg.Date:
                                {{$job['reg_date']}} </strong></label>
                        <div class=" col-lg-2"></div>
                        <div class="form-group col-lg-4">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class=" col-form-label text-right">Customer:</label>
                                </div>
                                <div class="col-lg-9 selectOption">
                                    <input class="form-control input optionInput" type="text" style="display: none" />
                                    <select class="form-control selectpicker selectedOption required" id="customer_id" name="customer_id">
                                        <option></option>
                                        @foreach($job['customer'] as $category)
                                        @if(isset($print) && $category->customer_id == $print->customer_id)
                                        <option selected="selected" value="{{$category->customer_id }}">{{ $category->business_name
                                            }}
                                        </option>
                                        @else
                                        <option value="{{$category->customer_id}}">{{ $category->business_name }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body " id="table_row">
                        <!--begin: Datatable-->
                        <table class="table table-separate table-head-custom table-checkable datatable2" style="border-color:#353B84;" id="datatable2">
                            <thead style="background-color:#353B84;">
                                <tr>
                                    <th>Job ID</th>
                                    <th>Status</th>
                                    <th>Quantity</th>
                                    <th>Job Category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody style="background-color:#F4F3F8;">
                                <tr>
                                    <td colspan="5">No Job Found</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--end: Datatable-->
                    </div>
                    <input type="hidden" name="reg_date" id="reg_date" value="{{$job['reg_date']}}">
                    <input type="hidden" id="myurl" value="{{url('admin/jobs/print_job_sheets/edit/')}}" />
                    <input type="hidden" name="job_id" id="job_id" value="{{$job['job_id']}}">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Job Title<span style="color:red;">*</span>:</label>
                        <div class="col-lg-5 selectOption">
                            <input class="form-control input optionInput" type="text" style="display: none" />
                            <select class="form-control selectpicker selectedOption required" id="job_title" name="job_title">
                                <option value="">Choose Job Title</option>
                                @foreach($job['job_title'] as $category)
                                @if(isset($print) && $category->job_title == $print->job_title)
                                <option selected="selected" value="{{$category->job_title }}">{{ $category->job_title }}
                                </option>
                                @else
                                <option value="{{$category->job_title}}">{{ $category->job_title }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2" id='normaljobtitle'>
                            <a class="btn font-weight-bolder plus_minus_button  NewOptionJobtitle input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOptionJobtitle input btn-m">-</a>
                        </div>
                        <div class="col-lg-2" id='storejobtitle' hidden="true">
                            <a class="btn font-weight-bolder plus_minus_button input btn-m saveOptionJobtitle">Add</a>
                            <a class="btn font-weight-bolder plus_minus_button input btn-m cancelOptionJobtitle">Cancel</a>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Category:</label>
                        <div class="col-lg-2 selectOption">
                            <input class="form-control input optionInput" type="text" style="display: none" />
                            <select class="form-control selectpicker selectedOption required" id="category" name="category">
                                <option value="">Choose Category</option>
                                @foreach($job['category'] as $category)
                                @if(isset($print) && $category->name == $print->category)
                                <option selected="selected" value="{{$category->name }}">{{ $category->name }}
                                </option>
                                @else
                                <option value="{{$category->name}}">{{ $category->name }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2" id='normal'>
                            <a class="btn font-weight-bolder plus_minus_button  NewOptionCategory input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOptionCategory input btn-m">-</a>
                        </div>
                        <div class="col-lg-2" id='store' hidden="true">
                            <a class="btn font-weight-bolder plus_minus_button input btn-m saveOptionCategory">Add</a>
                            <a class="btn font-weight-bolder plus_minus_button input btn-m cancelOptionCategory">Cancel</a>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Quantity<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="number" name="quantity" value="<?php if (isset($print)) {
                                                                                echo $print->quantity;
                                                                            } ?>" id="quantity" class="form-control required input" />
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Description:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <textarea type="text" id="description" name="description" value="<?php if (isset($print)) {
                                                                                                        echo $print->description;
                                                                                                    } ?>" class="form-control input"><?php if (isset($print)) {
                                                                                                                                            echo $print->description;
                                                                                                                                        } ?></textarea>
                            </div>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Colour:</label>
                        <div class="col-lg-2 selectOption">
                            <input class="form-control input optionInput" type="text" id='colourinput' style="display: none" />
                            <select class="form-control selectpicker selectedOption required" id="colours" name="colours">
                                <option value="">Choose Colour</option>
                                @foreach($job['colour'] as $category)
                                @if(isset($print) && $category->colour == $print->colours)
                                <option selected="selected" value="{{$category->colour }}">{{ $category->colour }}
                                </option>
                                @else
                                <option value="{{$category->colour}}">{{ $category->colour }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2" id='normalcolour'>
                            <a class="btn font-weight-bolder plus_minus_button  NewOptionColour input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOptionColour input btn-m">-</a>
                        </div>
                        <div class="col-lg-2" id='storecolour' hidden="true">
                            <a class="btn font-weight-bolder plus_minus_button input btn-m saveOptionColour">Add</a>
                            <a class="btn font-weight-bolder plus_minus_button input btn-m cancelOptionColour">Cancel</a>
                        </div>
                    </div>



                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Status:</label>
                        <div class="col-lg-4">
                            <select class="form-control selectpicker required" id="status" name="status">
                                <option @if(isset($print) && $print->status =='A') selected @endif value="A">Active
                                </option>
                                <option @if(isset($print) && $print->status =='Q') selected @endif value="Q">Quote
                                </option>
                                <option @if(isset($print) && $print->status =='D') selected @endif value="D">Done
                                </option>
                                <option @if(isset($print) && $print->status =='C') selected @endif value="C">Cancelled
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Discount:</label>
                        <div class="col-lg-3">
                            <input class="form-control input_hightlight required" placeholder="%" maxlength="2" id="number" value="<?php if (isset($print->discount)) {
                                                                                                                                        echo $print->discount;
                                                                                                                                    } ?>" name="discount" type="text" />
                        </div>
                        <div class="col-lg-5"></div>
                    </div>
                    <div class="highlight">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="col-lg-6 col-form-label text-right"></label>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Net Price:</label>
                                    <div class="col-lg-3">
                                        <input class="form-control input_hightlight required" id="net_price" value="<?php if (isset($print)) {
                                                                                                                        echo $print->net_price;
                                                                                                                    } ?>" name="net_price" type="text" />
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                                <div class="form-group row vat_div">
                                    <label class="col-lg-4 col-form-label text-right">VAT:</label>
                                    <div class="col-lg-3">
                                        <div class="radio-inline">

                                            <label class="radio">
                                                <input type="radio" name="vat" class="vat_radio vat_per_radio" data-vat_per="1" value="<?php if (isset($print)) {
                                                                                                                                            echo $print->vat;
                                                                                                                                        } ?>" @if(isset($print) ) checked @endif />
                                                <p class="vat_text"><?php if (isset($print)) {
                                                                        echo $print->vat;
                                                                    } ?>%</p>
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" data-vat_per="0" class="vat_radio" name="vat" value="0" @if(isset($print) && $print->vat
                                                ==0) checked @endif/>
                                                0 %
                                                <span></span>
                                            </label>
                                            <input name="vat_setting" class="form-control input_hightlight" value="<?php if (isset($print)) {
                                                                                                                        echo $print->vat;
                                                                                                                    } ?>" style="display: none;" type="text" maxlength="2" id="vat_cal_hidden">
                                            </br><button type="button" class="btn btn-primary" type="text" id="vat_btn_hidden">update</button>
                                            <button type="button" class="btn btn-primary" type="text" id="vat_cancel_hidden">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Price Comments:</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control input_hightlight" name="comments" id="comments" rows="3">@if(isset($print)) {{$print->comments}} @endif</textarea>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="highlight">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="col-form-label text-right lable_highlight"><strong>EXTRAS</strong></label>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Description:</label>
                                    <div class="col-lg-8">
                                        <input class="form-control input_hightlight character" name="description_extra" id="description_extra" type="text" />
                                        <span class="form-text text-muted error" id="extra_description_error">Please enter description</span>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">VAT:</label>
                                    <div class="col-lg-3">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" name="vat_extra" class="vat_extra vat_per_radio" data-vat_per="1" "/>
                                                <p class=" vat_text_extra"><?php echo $job['vat_data']->vat_setting; ?> %</p>
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="vat_extra" data-vat_per="0" class="vat_extra" value="0" @if(isset($print) && $print->vat
                                                ==0) checked @endif/>
                                                0 %
                                                <span></span>
                                            </label>
                                        </div>
                                        <input name="vat_setting" class="form-control input_hightlight" maxlength="2" value="<?php echo $job['vat_data']->vat_setting; ?>" style="display: none;" type="text" id="vat_extra_hidden">
                                        </br><button type="button" class="btn btn-primary" type="text" id="vat_extra_btn_hidden">update</button>
                                        <button type="button" class="btn btn-primary" type="text" id="vat_extra_cancel_hidden">Cancel</button>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label text-right">Net_Price:</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control input_hightlight numeric" name="net_price_extra" id="net_price_extra" />
                                        <span class="form-text text-muted error" id="extra_net_price_error">Please enter net price</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <button type="button" class="btn btn__bg mr-2 btn-m" id="add_extra">Add Extra
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row" id="table_row">
                                    <div class="col-lg-12">
                                        <table class="table table-separate table-head-custom table-checkable" id="datatable1">
                                            <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>VAT</th>
                                                    <th>Net_Price</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <!--  <div class="form-group row highlight">
                            <label class="col-lg-9 col-form-label text-right lable_highlight"><strong>Reprint Job
                                    id:</strong></label>
                            <div class="col-lg-3">
                                <button type="button" class="btn btn__bg" data-toggle="modal"
                                        data-target="#exampleModalCenter">
                                    Got it
                                </button>
                                <div class="modal fade" id="exampleModalCenter" data-backdrop="static" tabindex="-1"
                                     role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                        <div class="modal-content">
                                            <div style="padding: 25px 0">
                                                <div class="text-center" style="color: black;">No jobs found.</div>
                                            </div>
                                            <div class="text-center" style="padding-bottom: 22px;">
                                                <button type="button" class="btn btn__bg font-weight-bold"
                                                        data-dismiss="modal">Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->

                    </div>
                </form>

                <form id="printingForm">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs nav-tabs-line">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1">Design</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_2">Prepress</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_3">Press</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_4">Paper</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_5">Finishing</a>
                            </li>
                        </ul>
                        <div class="tab-content mt-5" id="myTabContent">
                            <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">


                                        Size:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" id="unfolded_input" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name="design_unfolded_size" id="design_unfolded_size">

                                            @foreach($job['design_unfolded_size'] as $size)
                                            @if(isset($print) && $size->design_unfolded_size == $print->category)
                                            <option selected="selected" value="{{$size->design_unfolded_size }}">{{
                                                $size->design_unfolded_size }}
                                            </option>
                                            @else
                                            <option value="{{$size->design_unfolded_size}}">{{
                                                $size->design_unfolded_size }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Folded Pages:</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <input type="text" name="design_folded_pages" value="<?php if (isset($print)) {
                                                                                                        echo $print->design_folded_pages;
                                                                                                    } ?>" id="design_folded_pages" class="form-control input" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Folded Size:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" id="folded_input" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name="design_folded_size" id="design_folded_size">

                                            @foreach($job['design_folded_size'] as $category)
                                            @if(isset($print) && $category->design_folded_size ==
                                            $print->design_folded_size)
                                            <option selected="selected" value="{{$category->design_folded_size }}">{{
                                                $category->design_folded_size }}
                                            </option>
                                            @else
                                            <option value="{{$category->design_folded_size}}">{{
                                                $category->design_folded_size }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Folds:</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <input type="text" name="design_folds" id="design_folds" value="<?php if (isset($print)) {
                                                                                                                echo $print->design_folds;
                                                                                                            } ?>" class="form-control input" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Print Sides:</label>
                                    <div class="col-lg-4">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" value="Single" name="design_double_side" @if(isset($print) && $print->design_double_side =='Single') checked @endif/>
                                                Single
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" value="Double" name="design_double_side" @if(isset($print) && $print->design_double_side =='Double') checked @endif/>
                                                Double
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Orientation:</label>
                                    <div class="col-lg-4">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" value="Portrait" name="design_orientation" @if(isset($print) && $print->design_orientation =='Portrait') checked @endif/>
                                                Portrait
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" value="Landscape" name="design_orientation" @if(isset($print) && $print->design_orientation =='Landscape') checked @endif/>
                                                Landscape
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Design comments:</label>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <textarea class="form-control input" name="design_comments" id="design_comments" rows="3">@if(isset($print)){{$print->design_comments}} @endif</textarea>
                                        </div>
                                        <!-- <span class="form-text text-muted">Please enter your address</span> -->
                                    </div>
                                    <div class="col-lg-2 btn-m">
                                        <div class="checkbox-list">
                                            <label class="checkbox checkboc_bottom">
                                                <input type="checkbox" @if(isset($print) && $print->design_artwork ==1) checked @endif value="1"
                                                name="design_artwork"
                                                id="design_artwork"/>
                                                Full
                                                Artwork Supplied
                                                <span></span>
                                            </label>
                                            <label class="checkbox checkboc_bottom">
                                                <input id="design_typesetting" value="1" name="design_typesetting" type="checkbox" @if(isset($print) && $print->design_typesetting ==1) checked @endif/> Typesetting
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="checkbox-list">
                                            <label class="checkbox checkboc_bottom">
                                                <input id="design_photography" name="design_photography" value="1" type="checkbox" @if(isset($print) && $print->design_photography ==1) checked @endif/> Photography
                                                <span></span>
                                            </label>
                                            <label class="checkbox checkboc_bottom">
                                                <input name="design_stockphotos" id="design_stockphotos" value="1" type="checkbox" @if(isset($print) && $print->design_stockphotos ==1) checked @endif/>
                                                Stockphotos
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="checkbox-list">
                                            <label class="checkbox checkboc_bottom">
                                                <input id="design_logo" value="1" @if(isset($print) && $print->design_logo ==1) checked @endif name="design_logo"
                                                type="checkbox"/>
                                                New
                                                Logo
                                                <span></span>
                                            </label>
                                            <label class="checkbox checkboc_bottom">
                                                <input name="design_scan" value="1" @if(isset($print) && $print->design_scan ==1) checked @endif id="design_scan"
                                                type="checkbox"/>
                                                Scan
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                @if(isset($print->job_id))
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Note:</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control input_hightlight" name="note" id="note" rows="3">@if(isset($print)) {{$print->note}} @endif</textarea>
                                    </div>
                                </div>
                                @endif


                            </div>
                            <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Setup Size:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" id="setup_input" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name="prepress_setupsize" id="prepress_setupsize">

                                            @foreach($job['prepress_setupsize'] as $category)
                                            @if(isset($print) && $category->prepress_setupsize ==
                                            $print->prepress_setupsize)
                                            <option selected="selected" value="{{$category->prepress_setupsize }}">{{
                                                $category->prepress_setupsize }}
                                            </option>
                                            @else
                                            <option value="{{$category->prepress_setupsize}}">{{
                                                $category->prepress_setupsize }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption  input btn-m">-</a>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Setup orientation:</label>
                                    <div class="col-lg-4">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" value="Portrait" name="prepress_orientation" @if(isset($print) && $print->prepress_orientation =='Portrait') checked @endif/>
                                                Portrait
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" value="Landscape" name="prepress_orientation" @if(isset($print) && $print->prepress_orientation =='Landscape') checked @endif/>
                                                Landscape
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Setup Type:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" id="setup_type_input" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name="prepress_setup_type" id="prepress_setup_type">

                                            @foreach($job['prepress_setup_type'] as $category)
                                            @if(isset($print) && $category->prepress_setup_type ==
                                            $print->prepress_setup_type)
                                            <option selected="selected" value="{{$category->prepress_setup_type }}">{{
                                                $category->prepress_setup_type }}
                                            </option>
                                            @else
                                            <option value="{{$category->prepress_setup_type}}">{{
                                                $category->prepress_setup_type }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Number of up:</label>
                                    <div class="col-lg-1">
                                        <div class="input-group">
                                            <input type="number" value="0" id="prepress_no_of_up" name="prepress_no_of_up" value="<?php if (isset($print)) {
                                                                                                                                        echo $print->prepress_no_of_up;
                                                                                                                                    } ?>" class="form-control input" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Type:</label>
                                    <div class="col-lg-4">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" value="Digital" name="prepress_dig_or_film" @if(isset($print) && $print->prepress_dig_or_film =='Digital')
                                                checked @endif/>
                                                Digital
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" value="Litho" name="prepress_dig_or_film" @if(isset($print) && $print->prepress_dig_or_film =='Litho')
                                                checked @endif/> Litho
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Film Where:</label>
                                    <div class="col-lg-4">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" value="Inhouse" name="prepress_where" @if(isset($print) && $print->prepress_dig_or_film =='Inhouse')
                                                checked @endif/>
                                                Inhouse
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" value="Bureau" name="prepress_where" @if(isset($print) && $print->prepress_dig_or_film =='Bureau')
                                                checked @endif/> Bureau
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" value="CTP" name="prepress_where" @if(isset($print) && $print->prepress_dig_or_film =='CTP') checked @endif/> CTP
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Platform:</label>
                                    <div class="col-lg-4">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" value="Mac" name="prepress_platform" @if(isset($print) && $print->prepress_platform =='Mac') checked
                                                @endif/> Mac
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" value="PC" name="prepress_platform" @if(isset($print) && $print->prepress_platform =='PC') checked
                                                @endif/> PC
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- <div class="col-lg-2"></div> -->
                                    <label class="col-lg-2 text-right"></label>
                                    <div class="col-lg-4">
                                        <div class="checkbox-inline">
                                            <!--  <label class="checkbox">
                                                <input type="checkbox" name="prepress_cd" value="1" @if(isset($print) &&
                                                       $print->prepress_cd ==1) checked @endif/>
                                                Burn
                                                CD
                                                <span></span>
                                            </label> -->
                                            <label class="checkbox">
                                                <input name="prepress_email" value="1" type="checkbox" @if(isset($print) && $print->prepress_email ==1) checked @endif/> Send email
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        @if(isset($print))
                                        <div class="row">
                                            <label class="col-lg-4 col-form-label text-right">Send Mail to:</label>
                                            <div class="col-lg-6 selectOption">
                                                @foreach($job['prepress_emailto'] as $category)
                                                @if(isset($print) && $category->prepress_emailto ==
                                                $print->prepress_emailto)
                                                <a href="mailto:{{$category->prepress_emailto }}">{{$category->prepress_emailto }}</a>
                                                @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-right">Email to:</label>
                                            <div class="col-lg-4 selectOption">
                                                <input class="form-control input optionInput" type="text" id="email_to_input" style="display: none" />
                                                <select class="form-control selectpicker selectedOption" name="prepress_emailto" id="prepress_emailto">

                                                    @foreach($job['prepress_emailto'] as $category)
                                                    @if(isset($print) && $category->prepress_emailto ==
                                                    $print->prepress_emailto)
                                                    <option selected="selected" value="{{$category->prepress_emailto }}">{{
                                                        $category->prepress_emailto }}
                                                    </option>
                                                    @else
                                                    <option value="{{$category->prepress_emailto}}">{{
                                                        $category->prepress_emailto }}
                                                    </option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                                <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="kt_tab_pane_3" role="tabpanel" aria-labelledby="kt_tab_pane_3">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Print Were:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" id="print_where_input" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name="press_printwhere" id="press_printwhere">

                                            @foreach($job['press_printwhere'] as $category)
                                            @if(isset($print) && $category->press_printwhere ==
                                            $print->press_printwhere)
                                            <option selected="selected" value="{{$category->press_printwhere }}">{{
                                                $category->press_printwhere }}
                                            </option>
                                            @else
                                            <option value="{{$category->press_printwhere}}">{{
                                                $category->press_printwhere }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">No. of Plates:</label>
                                    <div class="col-lg-1">
                                        <div class="input-group">
                                            <input type="number" id="press_no_of_plates" name="press_no_of_plates" value="<?php if (isset($print)) {
                                                                                                                                echo $print->press_no_of_plates;
                                                                                                                            } ?>" class="form-control input" />
                                        </div>
                                        <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Print Were:</label>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <input type="text" value="<?php if (isset($print)) {
                                                                            echo $print->prepress_where;
                                                                        } ?>" name="prepress_where" id="prepress_where" class="form-control input" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Machine Run:</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <input type="number" value="<?php if (isset($print)) {
                                                                            echo $print->press_machine_run;
                                                                        } ?>" name="press_machine_run" id="press_machine_run" class="form-control input" />
                                        </div>
                                        <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Press Comments:</label>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <textarea class="form-control input" name="press_comments" id="press_comments" value="<?php if (isset($print)) {
                                                                                                                                        echo $print->press_comments;
                                                                                                                                    } ?>" rows="3"></textarea>
                                        </div>
                                        <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="kt_tab_pane_4" role="tabpanel" aria-labelledby="kt_tab_pane_4">
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <label class="col-lg-4 col-form-label text-right">Paper type:</label>
                                            <div class="col-lg-4 selectOption">
                                                <input class="form-control input optionInput" type="text" id="paper_type_input" style="display: none" />
                                                <select class="form-control selectpicker selectedOption" name="paper_type" id="paper_type">

                                                    @foreach($job['paper_type'] as $category)
                                                    @if(isset($print) && $category->paper_type == $print->paper_type)
                                                    <option selected="selected" value="{{$category->paper_type }}">{{
                                                        $category->paper_type }}
                                                    </option>
                                                    @else
                                                    <option value="{{$category->paper_type}}">{{ $category->paper_type
                                                        }}
                                                    </option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                                <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <label class="col-lg-4 col-form-label text-right">Brand:</label>
                                            <div class="col-lg-4 selectOption">
                                                <input class="form-control input optionInput" type="text" id="brand_input" style="display: none" />
                                                <select class="form-control selectpicker selectedOption" name="paper_brand" id="paper_brand">

                                                    @foreach($job['paper_brand'] as $category)
                                                    @if(isset($print) && $category->paper_brand == $print->paper_brand)
                                                    <option selected="selected" value="{{$category->paper_brand }}">{{
                                                        $category->paper_brand }}
                                                    </option>
                                                    @else
                                                    <option value="{{$category->paper_brand}}">{{ $category->paper_brand
                                                        }}
                                                    </option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                                <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                                            </div>
                                            <div class="col-lg-4">
                                                <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                                <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Paper Colour:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name='paper_colour' id="paper_colour">
                                            <option value="">Select Paper Colour</option>
                                            @foreach($job['paper_colour'] as $category)

                                            @if(isset($print) && $category->paper_colour == $print->paper_colour)
                                            <option selected="selected" value="{{$category->paper_colour }}">{{
                                                $category->paper_colour }}
                                            </option>
                                            @else
                                            <option value="{{$category->paper_colour}}">{{ $category->paper_colour }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2" id='normalpapercolour'>
                                        <a class="btn font-weight-bolder plus_minus_button  NewOptionPaperColour input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button  removeOptionPaperColour input btn-m">-</a>
                                    </div>
                                    <div class="col-lg-2" id='storepapercolour' hidden="true">
                                        <a class="btn font-weight-bolder plus_minus_button input btn-m saveOptionPaperColour">Add</a>
                                        <a class="btn font-weight-bolder plus_minus_button input btn-m cancelOptionPaperColour">Cancel</a>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Thickness:</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <input type="text" name="paper_thickness" id="paper_thickness" class="form-control input" value="<?php if (isset($print)) {
                                                                                                                                                    echo filter_var($print->paper_thickness, FILTER_SANITIZE_NUMBER_INT);
                                                                                                                                                } ?>" />
                                        </div>
                                        <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="radio-inline">
                                            <label class="radio">
                                                <input type="radio" @if(isset($print) && preg_replace('/[^a-z-]/i', '' ,$print->paper_thickness) =='gsm') checked @endif id="gsm"
                                                value="gsm"/> gsm
                                                <span></span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" @if(isset($print) && preg_replace('/[^a-z-]/i', '' ,$print->paper_thickness) =='mic') checked @endif id="mic"
                                                value="mic"/> mic
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <label class="col-lg-2 col-form-label text-right">Size:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" id="size_input" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name="paper_size" id="paper_size">

                                            @foreach($job['paper_size'] as $category)
                                            @if(isset($print) && $category->paper_size == $print->paper_size)
                                            <option selected="selected" value="{{$category->paper_size }}">{{
                                                $category->paper_size }}
                                            </option>
                                            @else
                                            <option value="{{$category->paper_size}}">{{ $category->paper_size }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                    </div>

                                    <label class="col-lg-2 col-form-label text-right">Quantity:</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <input type="number" id="paper_quantity" name="paper_quantity" value="<?php if (isset($print)) {
                                                                                                                        echo $print->paper_quantity;
                                                                                                                    } ?>" class="form-control input" />
                                        </div>
                                        <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Supplier:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name='paper_supplier' id="paper_supplier">
                                            <option value="">Select Supplier </option>
                                            @foreach($job['paper_supplier'] as $category)

                                            @if(isset($print) && $category->paper_supplier == $print->paper_supplier)
                                            <option selected="selected" value="{{ $category->paper_supplier }}">{{
                                                 $category->paper_supplier }}
                                            </option>
                                            @else
                                            <option value="{{$category->paper_supplier}}">{{ $category->paper_supplier }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Comments:</label>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <textarea class="form-control input" name="paper_comments" id="paper_comments" rows="3"><?php if (isset($print)) {
                                                                                                                                        echo $print->paper_comments;
                                                                                                                                    } ?></textarea>
                                        </div>
                                        <!-- <span class="form-text text-muted">Please enter your address</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="kt_tab_pane_5" role="tabpanel" aria-labelledby="kt_tab_pane_5">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <label class="col-md-4 col-form-label text-right">Binding:</label>
                                            <div class="col-md-4 selectOption">
                                                <input class="form-control input optionInput" type="text" id="finish_binding" name="finish_binding" style="display: none" />
                                                <select class="form-control selectpicker selectedOption" id="binding_select">

                                                    @foreach($job['finish_binding'] as $category)
                                                    @if(isset($print) && $category->finish_binding ==
                                                    $print->finish_binding)
                                                    <option selected="selected" value="{{$category->finish_binding }}">
                                                        {{ $category->finish_binding }}
                                                    </option>
                                                    @else
                                                    <option value="{{$category->finish_binding}}">{{
                                                        $category->finish_binding }}
                                                    </option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                                <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <label class="col-md-4 col-form-label text-right">Finishing:</label>
                                            <div class="col-md-4 selectOption">
                                                <input class="form-control input optionInput" type="text" style="display: none" />
                                                <select class="form-control selectpicker selectedOption" id="finish_finish" name="finish_finish">

                                                    @foreach($job['finish_finish'] as $category)
                                                    @if(isset($print) && $category->finish_finish ==
                                                    $print->finish_finish)
                                                    <option selected="selected" value="{{$category->finish_finish }}">{{
                                                        $category->finish_finish }}
                                                    </option>
                                                    @else
                                                    <option value="{{$category->finish_finish}}">{{
                                                        $category->finish_finish }}
                                                    </option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                                <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Delivery by:</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" id="delivery_input" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name="finish_delivery" id="finish_delivery">
                                            @foreach($job['finish_delivery'] as $category)
                                            @if(isset($print) && $category->finish_delivery == $print->finish_delivery)
                                            <option selected="selected" value="{{$category->finish_delivery }}">{{
                                                $category->finish_delivery }}
                                            </option>
                                            @else
                                            <option value="{{$category->finish_delivery}}">{{ $category->finish_delivery
                                                }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                    </div>
                                    <label class="col-lg-2 col-form-label text-right">Boxing Required:</label>
                                    <div class="col-lg-2">
                                        <div class="checkbox-list">
                                            <label class="checkbox checkboc_bottom">
                                                <input name="finish_boxing" value="1" id="finish_boxing" @if(isset($print) && $print->finish_boxing ==1) checked @endif
                                                type="checkbox"/>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <!-- <label class="col-lg-2 col-form-label text-right">Delivery Address :</label>
                                    <div class="col-lg-2 selectOption">
                                        <input class="form-control input optionInput" type="text" id="delivery_input" style="display: none" />
                                        <select class="form-control selectpicker selectedOption" name="finish_delivery" id="finish_delivery">
                                            
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn font-weight-bolder plus_minus_button NewOption input btn-m">+</a>
                                        <a class="btn font-weight-bolder plus_minus_button removeOption input btn-m">-</a>
                                    </div> -->
                                    <label class="col-lg-2 col-form-label text-right">Finishing Comments:</label>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <textarea class="form-control input" name="finish_comments" id="finish_comments" rows="3"><?php if (isset($print)) {
                                                                                                                                            echo $print->finish_comments;
                                                                                                                                        } ?></textarea>
                                        </div>
                                        <!-- <span class="form-text text-muted">Please enter your address</span> -->
                                    </div>
                                    <!--  <div class="col-lg-2 btn-m">
                                        <div class="checkbox-list">
                                            <label class="checkbox checkboc_bottom">
                                                <input id="finish_cutting" value="1" @if(isset($print) && $print->finish_cutting
                                                ==1) checked @endif name="finish_cutting" type="checkbox"/>
                                                Cutting
                                                <span></span>
                                            </label>
                                            <label class="checkbox checkboc_bottom">
                                                <input name="finish_creasing" value="1" @if(isset($print) && $print->finish_creasing
                                                ==1) checked @endif id="finish_creasing" type="checkbox"/>
                                                Creasing
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="checkbox-list">
                                            <label class="checkbox checkboc_bottom">
                                                <input name="finish_folding" value="1" @if(isset($print) && $print->finish_folding
                                                ==1) checked @endif id="finish_folding" type="checkbox"/>
                                                Folding
                                                <span></span>
                                            </label>
                                            <label class="checkbox checkboc_bottom">
                                                <input name="finish_perforation" value="1" @if(isset($print) && $print->finish_perforation
                                                ==1) checked @endif id="finish_perforation"
                                                type="checkbox"/>
                                                Perforation
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="checkbox-list">
                                            <label class="checkbox checkboc_bottom">
                                                <input name="finish_numbering" value="1" id="finish_numbering"
                                                       @if(isset($print) && $print->finish_numbering ==1) checked @endif
                                                type="checkbox"/> Numbering
                                                <span></span>
                                            </label>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-10">
                        <a class="btn btn__bg" href="{{ route('admin.jobs.print_job_sheets.list') }}"> Cancel</a>
                        <button id="submitBtn" type="button" class="btn btn__bg mr-2 ">@if(isset($print)) Update
                            @else Add @endif
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--end::Container-->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    @endsection
    @push('script')

    <script type="text/javascript">
        var data = <?= !empty($print) ? json_encode($print) : '""' ?>;

        $("#number").keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        });
        $("#vat_cal_hidden").keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        });
        $("#vat_extra_hidden").keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        });

        $("body").on("click", ".NewOptionCategory", function() {

            $('#store').attr("hidden", false);
            $('#normal').attr("hidden", true);
            $(this).parent().parent().find('.selectOption').find('.optionInput').show();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
        });
        $("body").on("click", ".cancelOptionCategory", function() {
            $('#store').attr("hidden", true);
            $('#normal').attr("hidden", false);
            $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
        });
        $("body").on("click", ".saveOptionCategory", function() {
            var val = $(this).parent().parent().find('.optionInput').val();
            $(this).parent().parent().find('.optionInput').val('');
            if ($.trim(val) != '') {
                console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
                $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
                $(this).parent().parent().find('.cancelOptionCategory').click();

                //var name=$('#categorytxt').val();
                console.log(val);
                var post_data = new FormData();
                post_data.append('type', 'print');
                post_data.append('name', val);
                post_data.append('status', 1);
                console.log(post_data);
                ajax_request('/admin/job/category', post_data, getCategoryDetails, null);
            } else {
                toast_error('Please Enter Valid Value');
            }

        });

        function getCategoryDetails(result) {
            if (result.status === "success") {
                toast_success(result.message);
            } else {
                toast_error(result.message);
            }
        }

        function removecategoryresponse(result) {
            if (result.status === "success") {
                toast_success(result.message);
                setTimeout(function() {
                    location.reload();
                }, 1200)
            } else {
                toast_error(result.message);
            }
        }
        $("body").on("click", ".removeOptionCategory", function() {
            var value = $('#category').val();
            var post_data = new FormData();
            post_data.append('type', 'print');
            post_data.append('name', value);
            console.log(post_data);
            deleteswal('/admin/job/removecategory', post_data, removecategoryresponse, null);
        });
        /**
         * 
         * Normal Colour ADD DELETE 
         * 
         */

        $("body").on("click", ".NewOptionColour", function() {
            $('#storecolour').attr("hidden", false);
            $('#normalcolour').attr("hidden", true);
            $(this).parent().parent().find('.selectOption').find('.optionInput').show();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
        });
        $("body").on("click", ".cancelOptionColour", function() {
            $('#storecolour').attr("hidden", true);
            $('#normalcolour').attr("hidden", false);
            $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
        });
        $("body").on("click", ".saveOptionColour", function() {
            var val = $(this).parent().parent().find('.optionInput').val();
            $(this).parent().parent().find('.optionInput').val('');
            if ($.trim(val) != '') {
                console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
                $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
                $(this).parent().parent().find('.cancelOptionColour').click();

                //var name=$('#categorytxt').val();
                console.log(val);
                var post_data = new FormData();
                post_data.append('type', 'print');
                post_data.append('colour', val);
                console.log(post_data);
                ajax_request('/admin/job/colour', post_data, getColourDetails, null);
            } else {
                toast_error('Please Enter Valid Value');
            }

        });

        function getColourDetails(result) {
            if (result.status === "success") {
                toast_success(result.message);
            } else {
                toast_error(result.message);
            }
        }

        function removecolourresponse(result) {
            if (result.status === "success") {
                toast_success(result.message);
                setTimeout(function() {
                    location.reload();
                }, 1200)
            } else {
                toast_error(result.message);
            }
        }
        $("body").on("click", ".removeOptionColour", function() {
            var value = $('#colours').val();
            console.log(value);
            var post_data = new FormData();
            post_data.append('type', 'print');
            post_data.append('colour', value);
            console.log(post_data);
            deleteswal('/admin/job/removecolour', post_data, removecolourresponse, null);
            //ajax_request('/admin/job/removecategory', post_data, getCategoryDetails, null);
            //$(this).parent().parent().find('.selectedOption').selectpicker('val', '');


        });

        /**
         * 
         * Paper Colour ADD DELETE 
         * 
         */

        $("body").on("click", ".NewOptionPaperColour", function() {

            $('#storepapercolour').attr("hidden", false);
            $('#normalpapercolour').attr("hidden", true);
            $(this).parent().parent().find('.selectOption').find('.optionInput').show();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
        });
        $("body").on("click", ".cancelOptionPaperColour", function() {
            $('#storepapercolour').attr("hidden", true);
            $('#normalpapercolour').attr("hidden", false);
            $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
        });
        $("body").on("click", ".saveOptionPaperColour", function() {
            var val = $(this).parent().parent().find('.optionInput').val();
            $(this).parent().parent().find('.optionInput').val('');
            if ($.trim(val) != '') {
                console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
                $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
                $(this).parent().parent().find('.cancelOptionPaperColour').click();

                //var name=$('#categorytxt').val();
                console.log(val);
                var post_data = new FormData();
                post_data.append('type', 'print');
                post_data.append('colour', val);
                console.log(post_data);
                ajax_request('/admin/job/colour', post_data, getColourDetails, null);
            } else {
                toast_error('Please Enter Valid Value');
            }
        });

        function getColourDetails(result) {
            if (result.status === "success") {
                toast_success(result.message);
            } else {
                toast_error(result.message);
            }
        }

        function removecolourresponse(result) {
            if (result.status === "success") {
                toast_success(result.message);
                setTimeout(function() {
                    location.reload();
                }, 1200)
            } else {
                toast_error(result.message);
            }
        }
        $("body").on("click", ".removeOptionPaperColour", function() {
            var value = $('#paper_colour').val();
            var post_data = new FormData();
            post_data.append('type', 'print');
            post_data.append('colour', value);
            console.log(post_data);
            deleteswal('/admin/job/removecolour', post_data, removecolourresponse, null);
            //ajax_request('/admin/job/removecategory', post_data, getCategoryDetails, null);
            //$(this).parent().parent().find('.selectedOption').selectpicker('val', '');
        });


        /**
         * 
         * Jobtitle ADD DELETE 
         * 
         */

        $("body").on("click", ".NewOptionJobtitle", function() {

            $('#storejobtitle').attr("hidden", false);
            $('#normaljobtitle').attr("hidden", true);
            $(this).parent().parent().find('.selectOption').find('.optionInput').show();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
        });
        $("body").on("click", ".cancelOptionJobtitle", function() {
            $('#storejobtitle').attr("hidden", true);
            $('#normaljobtitle').attr("hidden", false);
            $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
        });
        $("body").on("click", ".saveOptionJobtitle", function() {
            var val = $(this).parent().parent().find('.optionInput').val();
            $(this).parent().parent().find('.optionInput').val('');
            if ($.trim(val) != '') {
                console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
                $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
                $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
                $(this).parent().parent().find('.cancelOptionJobtitle').click();

                //var name=$('#categorytxt').val();
                console.log(val);
                var post_data = new FormData();
                post_data.append('type', 'print');
                post_data.append('job_title', val);
                console.log(post_data);
                ajax_request('/admin/job/jobtitle', post_data, getJobtitleDetails, null);
            } else {
                toast_error('Please Enter Valid Value');
            }

        });

        function getJobtitleDetails(result) {
            if (result.status === "success") {
                toast_success(result.message);
            } else {
                toast_error(result.message);
            }
        }

        function removejobtitleresponse(result) {
            if (result.status === "success") {
                toast_success(result.message);
                setTimeout(function() {
                    location.reload();
                }, 1200)
            } else {
                toast_error(result.message);
            }
        }
        $("body").on("click", ".removeOptionJobtitle", function() {
            var value = $('#job_title').val();
            var post_data = new FormData();
            post_data.append('type', 'print');
            post_data.append('job_title', value);
            console.log(post_data);
            deleteswal('/admin/job/removejobtitle', post_data, removejobtitleresponse, null);
        });
    </script>
    <script src="/js/pages/jobs/jobs.js"></script>

    @endpush