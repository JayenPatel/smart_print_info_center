@php

$menu = session()->get('userData')->roleId;

@endphp

@extends('admin.layouts.app')
@section('title','View Web Job Sheet - Smart Print')
@section('content')
<style type="text/css">
    th {
        background-color: #353B84 !important;
        text-align: center !important;
    }

    td {
        text-align: center !important;
        border: 0.1px solid #353B84 !important;
        background-color: #f0eff5ba;
        color: #353B84 !important;
    }

    .btn__bg {
        background-color: #353B84 !important;
        color: white;
    }

    .btn__bg:hover {
        background-color: #353b84c9 !important;
        color: white;
    }
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                Web jobsheet
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
    <div class=" container ">
        <div class=" container ">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <!-- <h3 class="card-label">
                            Scrollable Table
                            <span class="d-block text-muted pt-2 font-size-sm">scrollable datatable with fixed height</span>
                        </h3> -->
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Dropdown-->
                        <div class="dropdown dropdown-inline mr-2">
                            <a href="{{ route('admin.header.export','web') }}" class="btn btn__bg font-weight-bolder">
                                <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/PenAndRuller.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
                                            <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
                                        </g>
                                    </svg><!--end::Svg Icon-->
                                </span> Export To Excel
                            </a>

                        </div>
                        <!--end::Dropdown-->

                        <!--begin::Button-->
                        <?php if (check_permission('jobs', 'add') == 1) { ?>
                            <a href="{{ route('admin.jobs.web_job_sheets.add') }}" class="btn btn__bg font-weight-bolder">
                                <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <circle fill="#000000" cx="9" cy="15" r="6" />
                                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                        </g>
                                    </svg><!--end::Svg Icon--></span> Create New
                            </a>
                        <?php } ?>
                        <!--end::Button-->
                    </div>
                </div>

                <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table table-separate table-head-custom table-checkable" id="webjobDT">
                        <thead>
                            <tr>
                                <th>Job Id</th>
                                <th>Status</th>
                                <th>Quantity</th>
                                <th>Job Category</th>
                                <?php if (check_permission('jobs', 'edit') == 1 || check_permission('jobs', 'delete') == 1) { ?>
                                    <th>Actions</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (check_permission('jobs', 'view') == 1) { ?>
                                @foreach($list as $data)
                                <tr>
                                    <td>{{$data->job_id}}</td>
                                    <td>{{$data->status}}</td>
                                    <td>{{$data->quantity}}</td>
                                    <td>{{$data->category}}</td>
                                    <?php if (check_permission('jobs', 'edit') == 1 || check_permission('jobs', 'delete') == 1) { ?>
                                        <td>
                                            <?php if (check_permission('jobs', 'edit') == 1) { ?>
                                                <a href="{{ route('admin.jobs.web_job_sheets.edit', $data->job_id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details"> <span class="svg-icon svg-icon-md"> <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path>
                                                                <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </a>
                                            <?php } ?>
                                            <?php if (check_permission('jobs', 'delete') == 1) { ?>
                                                <a onclick="deleteCustomer('{{  $data->job_id  }}')"><i class="far fa-trash-alt"></i></a>
                                            <?php } ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                @endforeach
                            <?php } ?>
                        </tbody>


                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
    </div>
    <!--end::Container-->

    @endsection

    @push('script')
    <script type="text/javascript">
        $("#webjobDT").DataTable({
            scrollY: "50vh",
            scrollX: !0,
            scrollCollapse: !0,
        })

        function deleteCustomer(id) {
            var post_data = new FormData();
            post_data.append('id', id);
            deleteswal('/admin/jobs/web_job_sheets/delete', post_data, deleteResponse, null);
        }

        function deleteResponse(result) {
            if (result.status == "success") {
                toast_success(result.message);
                location.reload();


            } else {
                toast_error(result.message);
            }
        }
    </script>
    @endpush