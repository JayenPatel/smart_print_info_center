@extends('admin.layouts.app')
@section('title','View Quotes - Smart Print')
@section('content')
<!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                    Quotes
                </h5>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->

<style type="text/css">
	th{
			background-color: #353B84 !important;
			text-align: center !important;
		}
		td{
			text-align: center !important;
			border: 0.1px solid #353B84 !important;
			background-color: #f0eff5ba;
			color: #353B84 !important;
		}
		.checkbox>span {
		    background-color: #caccce !important;
		}
</style>

<div class="d-flex flex-column-fluid">
<div class="container">
<!--begin::Dashboard-->
	<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-separate table-head-custom table-checkable" id="quotes">
		            <thead>
		                <tr>
		                    <th style="color: white !important;padding-left: 5px !important;">Date</th>
							<th style="color: white !important;">Customer</th>
							<th style="color: white !important;">Job Id</th>
							<th style="color: white !important;">Qty</th>
							<th style="color: white !important;">Description</th>
							<th style="color: white !important;">Status</th>
							<th style="color: white !important;">Type/Design</th>
							<th style="color: white !important;">Proof</th>
							<th style="color: white !important;">Confirmed</th>
							<th style="color: white !important;">Film/Plate</th>
							<th style="color: white !important;">Printing</th>
							<th style="color: white !important;">Finished</th>
							<th style="color: white !important;">Delivered</th>
							<th style="color: white !important;">Job Sheet</th>
							<th style="color: white !important;">Invoice</th>
							<th style="color: white !important;">Account</th>
		                </tr>
		            </thead>
                    <tbody>
                    @foreach($list as $data)
                    <tr>
                        <td>{{$data->reg_date}}</td>
                        <td>{{$data->business_name}}</td>
                        <td>{{$data->job_id}}</td>
                        <td>{{$data->quantity}}</td>
                        <td>{{$data->description}}</td>
                        <td>{{$data->status}}</td>
                        <td style="text-align: center;">
                            <label class="checkbox checkbox-single">
                                <input type="checkbox" data-id="{{$data->job_id}}" name="prog_type"  @if(isset($data) &&
                                       $data->prog_type == 1) checked disabled @endif id="prog_type" value="1" class="checkable">
                                <span></span>
                            </label>
                        </td>
                        <td style="text-align: center;">
                            <label class="checkbox checkbox-single">
                                <input type="checkbox"  data-id="{{$data->job_id}}"  name="prog_proof" id="prog_proof" value="1" class="checkable"  @if(isset($data) &&
                                       $data->prog_proof == 1) checked disabled @endif>
                                <span></span>
                            </label>
                        </td>
                        <td style="text-align: center;">
                            <label class="checkbox checkbox-single">
                                <input type="checkbox" data-id="{{$data->job_id}}"  name="prog_confirmed" id="prog_confirmed" value="1"  @if(isset($data) &&
                                       $data->prog_confirmed == 1) checked disabled @endif class="checkable">
                                <span></span>
                            </label>
                        </td>
                        <td style="text-align: center;">
                            <label class="checkbox checkbox-single">
                                <input type="checkbox"  data-id="{{$data->job_id}}" name="prog_film" id="prog_film" value="1" class="checkable"   @if(isset($data) &&
                                       $data->prog_film == 1) checked disabled @endif >
                                <span></span>
                            </label>
                        </td>
                        <td style="text-align: center;">
                            <label class="checkbox checkbox-single">
                                <input type="checkbox"  data-id="{{$data->job_id}}" name="prog_printed" id="prog_printed"  value="1" class="checkable"  @if(isset($data) &&
                                       $data->prog_printed == 1) checked disabled @endif >
                                <span></span>
                            </label>
                        </td>
                        <td style="text-align: center;">
                            <label class="checkbox checkbox-single">
                                <input type="checkbox"  data-id="{{$data->job_id}}"  @if(isset($data) &&
                                       $data->prog_finished == 1) checked disabled @endif  name="prog_finished" id="prog_finished"   value="1" class="checkable">
                                <span></span>
                            </label>
                        </td>
                        <td style="text-align: center;">
                            <label class="checkbox checkbox-single">
                                <input type="checkbox"  data-id="{{$data->job_id}}"  @if(isset($data) &&
                                       $data->prog_delivered == 1) checked disabled @endif  name="prog_delivered" id="prog_delivered" value="1" class="checkable">
                                <span></span>
                            </label>
                        </td>
                        <?php if($data->type == 'W') {?>
                            <td><a  href="{{ route('admin.jobs.web_job_sheets.edit', $data->job_id) }}">Go</a></td>
                        <?php }?>
                        <?php if($data->type == 'C') {?>
                            <td><a  href="{{ route('admin.jobs.calendar.edit', $data->job_id) }}">Go</a></td>
                        <?php }?>
                        <?php if($data->type == 'P') {?>
                            <td><a  href="{{ route('admin.jobs.print_job_sheets.edit', $data->job_id) }}">Go</a></td>
                        <?php }?>
                        <?php if($data->type == 'X') {?>
                            <td><a  href="{{ route('admin.jobs.other_job_sheets.edit', $data->job_id) }}">Go</a></td>
                        <?php }?>

                        <td>
                           <a href="{{ route('admin.header.quotes') }}">N/A</a>
                        </td>
                        <td>100</td>
                    </tr>
                    @endforeach

                    </tbody>

		        		</table>
				<!--end: Datatable-->
			</div>
		</div>
		<!--end::Card-->
</div>
<!--end::Dashboard-->
@endsection
@push('script')
	<script type="text/javascript">
        $("body").on("click", ".checkable", function () {
            var id = $(this).attr('data-id');
            var type = $(this).attr('name');
            var value = 0;
            if ($(this).prop("checked") == true) {
                value = 1;
            }
            console.log(id,type,value);
            var post_data = new FormData();
            post_data.append('id', id);
            post_data.append('type', type);
            post_data.append('value', value);
            ajax_request('/admin/update', post_data, submit_response, null);
        });
        function submit_response(result){
            if (result.status == "success") {
                toast_success(result.message);
                location.reload();

            } else {
                toast_error(result.message);
            }
        }
	$("#quotes").DataTable({
			scrollY: "50vh",
			scrollX: !0,
			scrollCollapse: !0,
	})
	</script>
@endpush
