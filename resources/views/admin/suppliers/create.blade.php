@extends('admin.layouts.app')
@section('title','View Suppliers - Smart Print')
@section('content')
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
          <div class="col-3">
                     <a class="btn btn-primary" data-toggle="modal"  data-target="#myModal">Purchase Order</a>
          </div>

    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                Suppliers
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <form class="form" id="supplierForm" method="post" class="form" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Business Type<span>*</span>:</label>
                        <input class="form-control input" name="supplier_id" type="text" id="supplier_id"
                               value="<?php if (isset($customer)) {
                                   echo $customer->supplier_id;
                               } ?>" style="display: none"/>
                        <div class="col-lg-2 selectOption">
                            <input class="form-control input optionInput" type="text" style="display: none"/>
                            <select name='business_type' class="form-control selectpicker selectedOption  required"
                                    id="business_type">
                                <option value="">Choose one</option>
                                @foreach($category as $category)
                                    @if(isset($customer) && $category->business_type == $customer->business_type)
                                        <option selected="selected" value="{{$category->business_type }}">{{ $category->business_type }}
                                        </option>
                                    @else
                                        <option value="{{$category->business_type}}">{{ $category->business_type }}</option>
                                    @endif
                                @endforeach

                            </select>
                        </div>
                        <div class="col-lg-2">
                            <a class="btn font-weight-bolder plus_minus_button  NewOption input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOption input btn-m">-</a>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Web:</label>
                        <div class="col-lg-4">
                            <input type="text" name='web' class="form-control input"
                                   value="<?php if (isset($customer)) {
                                       echo $customer->web;
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Business Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($customer)) {
                                    echo $customer->business_name;
                                } ?>"
                                       name='business_name' class="form-control input required"/>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Postcode<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input name='postcode' value="<?php if (isset($customer)) {
                                    echo $customer->postcode;
                                } ?>" type="text" onKeyUP="this.value = this.value.toUpperCase();"  class="form-control input required" id="postcode"/>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">First Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="text" value="<?php if (isset($customer)) {
                                echo $customer->first_name;
                            } ?>" name='first_name'
                                   class="form-control input required"/>
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Address1<span>*</span>:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" name='address1' value="<?php if (isset($customer)) {
                                    echo $customer->address1;
                                } ?>" class="form-control input" id="autoAddress"/>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Last Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="text" value="<?php if (isset($customer)) {
                                echo $customer->surname;
                            } ?>" name='surname'
                                   class="form-control input required"/>
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Address2:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($customer)) {
                                    echo $customer->address2;
                                } ?>" name='address2'
                                       class="form-control input"/>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Telephone<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="text" name='telephone' value="<?php if (isset($customer)) {
                                echo $customer->telephone;
                            } ?>"
                                   class="form-control input required"/>
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Area:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($customer)) {
                                    echo $customer->area;
                                } ?>" name='area'
                                       class="form-control input"/>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                       <label class="col-lg-2 col-form-label text-right">Mobile:</label>
                        <div class="col-lg-4">
                            <input name='mobile' value="<?php if (isset($customer)) {
                                echo $customer->mobile;
                            } ?>"
                                   type="text" class="form-control input"/>
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Town:<span>*</span></label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($customer)) {
                                    echo $customer->city;
                                } ?>" name='city'
                                       class="form-control input"/>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Email:</label>
                        <div class="col-lg-4">
                            <input name='email' value="<?php if (isset($customer)) {
                                echo $customer->email;
                            } ?>" type="text"
                                   class="form-control input"/>
                            <span class="form-text text-muted"></span>
                        </div>                        
                        <label class="col-lg-2 col-form-label text-right">Country<span>*</span>:</label>
                        <div class="col-lg-4">
                            <select name='county' data-id="<?php if (isset($customer)) {
                                echo $customer->county;
                            } ?>" id="country"
                                    class="form-control selectpicker required">
                                <!-- <option value="">Choose one</option>
                                 <option value="India" @if(isset($customer)) selected @endif>India</option>
                                 <option value="America">America</option>-->
                            </select>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-lg-2 col-form-label text-right">Secondary Email:</label>
                          <div class="col-lg-4">
                             <input name='secondary_email' value="<?php if (isset($customer)) {
                                echo $customer->secondary_email;
                               } ?>" type="text"
                                   class="form-control input"/>
                              <span class="form-text text-muted"></span>
                         </div>
                        <label class="col-lg-2 col-form-label text-right">Status:</label>
                        <div class="col-lg-4">
                            <select class="form-control selectpicker" name="supplier_status">
                                <option @if(isset($customer) && $customer->supplier_status =='Active') selected @endif
                                    value="Active">Active
                                </option>
                                <option @if(isset($customer) && $customer->supplier_status =='Passive') selected @endif value="Passive">Passive
                                </option>
                            </select>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-2 col-form-label text-right">Comments:</label>
                         <div class="col-lg-4">
                            <textarea name='comments' value="<?php if (isset($customer)) {
                                echo $customer->comments;
                            } ?>"
                                      class="form-control input"
                                      rows="3"><?php if (isset($customer)) {
                                    echo $customer->comments;
                                } ?></textarea>
                            <span class="form-text text-muted"></span>
                         </div>
                    </div>
                       
                   <!--  <div class="form-group row">
                        
                    </div> -->
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-7">
                            <a class="btn btn__bg" href="{{ route('admin.customers.list') }}"> Cancel</a>
                            <button class="btn btn__bg mr-2 " id="submitSupplier" type="button">@if(isset($customer))
                                Update @else Add @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="modal fade" id="myModal" role="dialog">
              <!-- <link href="{{ url('css/style.bundle.css') }}" rel="stylesheet" type="text/css"/> -->
               <div class="modal-dialog modal-lg">
        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                    <!-- <h4 class="modal-title">Invoice PDF</h4> --> 
                    <img alt="Logo" src="{{url('authImage/0001.jpg')}}" class="center" />
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                    

                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="purchase_method2">
                            <div class="purchase_box1">
                                <b>Order Number</b><br><br>
                                <b>122</b>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="purchase_method10">
                            <div class="purchase_box1">
                                <b>Supplier ID</b><br><br>
                                <b>122</b>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="purchase_method11">
                            <div class="purchase_box1">
                                <b>Order Date</b><br><br>
                                <b>122</b>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="purchase_method12">
                            <div class="purchase_box1">
                                <b>Delivery Date</b><br><br>
                                <b>122</b>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="purchase_method3">
                            <div class="purchase_box1">
                                    <b>Delivery Method</b><br><br>
                                    <b>122</b>
                            </div>
                            </div>
                        </div>
                         <!-- <div class="purchase_method4">
                            <div class="purchase_box2">
                                <b>Delivery Terms</b><br><br>
                                <b>122</b>
                            </div>
                        </div> -->
                        <!-- <div class="purchase_method5">
                            <div class="purchase_box1">
                                <b>Payment Terms</b><br><br>
                                <b>122</b>
                            </div>
                        </div>
                        
                        <div class="purchase_method6">
                            <div class="purchase_box1">
                                <b>Our Reference:</b>
                            </div>
                        </div>
                        <div class="purchase_method8">
                            <div class="purchase_box1">
                                <b>Your Reference:</b>
                            </div>
                        </div>
                        <div class="purchase_method9">
                            <div class="purchase_box1">
                                <b>Your Reference:</b>
                            </div>
                        </div>  -->
                   </div>
            	</div>
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-11">
                            <div class="purchase-table">
                                <table style="width:100%">
                                    <tr class="quantity_item1">
                                        <th>Quantity</th>
                                        <th>Job Description</th>
                                        <th>Amount</th>
                                        <th>Unit</th>
                                        <th>Unite Price(exc VAT)</th>
                                        <th>Total Price(exc VAT)</th>
                                    
                                    </tr>
                                    <tr class="quantity_subitem quantity_subitem1 table-tr">      
                                        <td>100</td>
                                        <td>Business cards (VAT 20%)</td>
                                        <td>£50.00</td>
                                        <td>£50.00</td>
                                        <td>£50.00</td>
                                        <td>£50.00</td>
                                    </tr>
                                </table>

                                <!-- <div class="total-order"> -->
                                <div class="purchase_method7">
                                    <b>Total Order:</b><br>
                                </div>
                                <div class="purchase_box7">
                                <b>120</b><br>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                

                   
                </div>
                
          
                
        </div>
    </div>
        </div>
    </div>
    <!--end::Container-->


    @endsection

    <!-- <div id="add_branch" class="modal fade bs-example-modal-lg in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 style="text-align:center" class="modal-title pull-center" id="myModalLabel"> SELECT ADDRESS</h4> </div>
                <div class="modal-body">
                    <div id="show_address"></div>
                </div>
            </div>
        </div>
    </div> -->
    @push('script')

    <script src="/js/countries.js"></script>
    <script src="/js/pages/suppliers/suppliers.js"></script>
    <script>
        populateCountries("country");
        populateCountries("country1");
    </script>
    @endpush
