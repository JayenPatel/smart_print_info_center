@extends('admin.layouts.app')
@section('title','Role Permission - Smart Print')
@section('content')
<style type="text/css">
.headrow{
    background-color:#353B84; 
}
    .role_header{
        background-color:#353B84; 
        text-align: center;
        text-transform: uppercase;
        color: #FFFFFF !important;
        font-weight: 900 !important;
    }
    @media only screen and (max-width: 425px) {
        .nav.nav-tabs.nav-tabs-line .nav-link {
            margin: 0 8px;
        }
    }
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                Role Permission
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->

<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <div class="card-body">
                <form class="form" id="roleForm"  method="post">
                    <div class="row mb-10">
                        <div class="form-group col-lg-4">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class=" col-form-label text-right">Roles:</label>
                                </div>
                                <div class="col-lg-9 selectOption">
                                    <input class="form-control input optionInput" type="text" style="display: none"/>
                                    <select class="form-control selectpicker selectedOption required" onchange='permission(this)' id="roleId"
                                            name="roleId">
                                        @foreach($roles as $role)
                                        
                                        <option value="{{$role->roleId}}" <?php if($role->roleId == $role_id){?>selected<?php } ?>>{{ $role->roleName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="form-group col-lg-4"></div>
                        <div class="form-group col-lg-4"></div>
                        <div class="form-group col-lg-4"></div>    

                        <br/>
                        <div class="form-group formBody col-lg-12">
                            <div class="row headrow">
                                <div class="col-lg-4">
                                    <label class="col-form-label text-right role_header">Module Name </label>
                                </div>
                                <div class="col-lg-2">
                                    <label class=" col-form-label text-right role_header">View </label>
                                </div>
                                <div class="col-lg-2">
                                    <label class=" col-form-label text-right role_header">Add </label>
                                </div>
                                <div class="col-lg-2">
                                    <label class=" col-form-label text-right role_header">Edit </label>
                                </div>
                                <div class="col-lg-2">
                                    <label class=" col-form-label text-right role_header">Delete </label>
                                </div>
                            </div>
                            @foreach($modules as $module)
                            <div class="row">
                                <div class="col-lg-4">
                                    <label class=" col-form-label text-right">{{ucwords($module->module_name)}}</label>
                                    <input type="hidden" name='{{$module->module_name}}' value='{{$module->module_id}}'>
                                </div>
                                <div class="col-lg-2">
                                <label class=" col-form-label text-right"></label>
                                    <input type='checkbox' id='{{$module->module_name."view"}}'<?php if($module->view_permission == '1'){?>checked<?php } ?> name='{{$module->module_name."view"}}'/>
                                </div>
                                <div class="col-lg-2">
                                <label class=" col-form-label text-right"></label>
                                <input type='checkbox' id='{{$module->module_name."add"}}' <?php if($module->add_permission == '1'){?>checked<?php } ?>  name='{{$module->module_name."add"}}'/>
                                </div>
                                <div class="col-lg-2">
                                <label class=" col-form-label text-right"></label>
                                <input type='checkbox' <?php if($module->edit_permission == '1'){?>checked<?php } ?> id='{{$module->module_name."edit"}}'  name='{{$module->module_name."edit"}}'/>
                                </div>
                                <div class="col-lg-2">
                                <label class=" col-form-label text-right"></label>
                                <input type='checkbox' <?php if($module->delete_permission == '1'){?>checked<?php } ?> id='{{$module->module_name."delete"}}'  name='{{$module->module_name."delete"}}'/>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-10">
                        <!-- <a class="btn btn__bg" href="{{ route('admin.jobs.print_job_sheets.list') }}"> Cancel</a> -->
                        <button id="submitpermission" type="button" class="btn btn__bg mr-2 "> Update
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--end::Container-->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    @endsection
    @push('script')
    <script src="/js/pages/roles/roles.js"></script>

    @endpush

                            
                               
                    
