@extends('admin.layouts.app')
@section('title','Create Customer - Smart Print')
@section('content')

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                @if(isset($customer)) Edit @else New @endif Customer
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <form class="form" id="customerForm" method="post" class="form" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Business Type<span>*</span>:</label>
                        <input class="form-control input" name="customer_id" type="text" id="customer_id" value="<?php if (isset($customer)) {
                                                                                                                        echo $customer->customer_id;
                                                                                                                    } ?>" style="display: none" />
                        <input class="form-control input" name="alternateAddress" type="text" id="alternate_address" value="<?php if (isset($customer)) {
                                                                                                                                echo $customer->alternate_address;
                                                                                                                            } ?>" style="display: none" />
                        <div class="col-lg-2 selectOption">
                            <input class="form-control input optionInput" type="text" style="display: none" />
                            <select name='business_type' class="form-control selectpicker selectedOption  required" id="business_type">
                                <option value="">Choose one</option>
                                @foreach($category as $category)
                                @if(isset($customer) && $category->business_type == $customer->business_type)
                                <option selected="selected" value="{{$category->business_type }}">{{ $category->business_type }}
                                </option>
                                @else
                                <option value="{{$category->business_type}}">{{ $category->business_type }}</option>
                                @endif
                                @endforeach

                            </select>
                        </div>
                        <div class="col-lg-2">
                            <a class="btn font-weight-bolder plus_minus_button  NewOption input btn-m">+</a>
                            <a class="btn font-weight-bolder plus_minus_button  removeOption input btn-m">-</a>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Web:</label>
                        <div class="col-lg-4">
                            <input type="text" name='web' class="form-control input" value="<?php if (isset($customer)) {
                                                                                                echo $customer->web;
                                                                                            } ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Business Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($customer)) {
                                                                echo $customer->business_name;
                                                            } ?>" name='business_name' class="form-control input required" />
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Postcode<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input name='postcode' id="postcode" value="<?php if (isset($customer)) {
                                                                                echo $customer->postcode;
                                                                            } ?>" type="text" class="form-control input required" onKeyUP="this.value = this.value.toUpperCase();" />
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">First Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="text" value="<?php if (isset($customer)) {
                                                            echo $customer->first_name;
                                                        } ?>" name='first_name' class="form-control input required" />
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Address1<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" name='address1' id="autoAddress" value="<?php if (isset($customer)) {
                                                                                                echo $customer->address1;
                                                                                            } ?>" class="form-control input" />
                            </div>
                            <span class="form-text text-muted"></span>
                            <span>Auto complete address fill postcode</span>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Last Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="text" value="<?php if (isset($customer)) {
                                                            echo $customer->surname;
                                                        } ?>" name='surname' class="form-control input required" />
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Address2:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($customer)) {
                                                                echo $customer->address2;
                                                            } ?>" name='address2' class="form-control input" />
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Telephone<span style="color:red;">*</span>:</label>
                        <div class="col-lg-4">
                            <input type="text" name='telephone' value="<?php if (isset($customer)) {
                                                                            echo $customer->telephone;
                                                                        } ?>" class="form-control input required" />
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Area:</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($customer)) {
                                                                echo $customer->area;
                                                            } ?>" name='area' class="form-control input" />
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Fax:</label>
                        <div class="col-lg-4">
                            <input type="text" name='fax' value="<?php if (isset($customer)) {
                                                                        echo $customer->fax;
                                                                    } ?>" class="form-control input" />
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Town:<span>*</span></label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($customer)) {
                                                                echo $customer->city;
                                                            } ?>" name='city' class="form-control input" />
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Mobile:</label>
                        <div class="col-lg-4">
                            <input name='mobile' value="<?php if (isset($customer)) {
                                                            echo $customer->mobile;
                                                        } ?>" type="text" class="form-control input" />
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Country<span>*</span>:</label>
                        <div class="col-lg-4">
                            <select name='county' data-id="<?php if (isset($customer)) {
                                                                echo $customer->county;
                                                            } ?>" id="country" class="form-control selectpicker required">
                                <!-- <option value="">Choose one</option>
                                 <option value="India" @if(isset($customer)) selected @endif>India</option>
                                 <option value="America">America</option>-->
                            </select>
                            <span class="form-text text-muted"></span>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Email:</label>
                        <div class="col-lg-4">
                            <input name='email' value="<?php if (isset($customer)) {
                                                            echo $customer->email;
                                                        } ?>" type="text" class="form-control input" />
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Comments:</label>
                        <div class="col-lg-4">
                            <textarea name='comments' value="<?php if (isset($customer)) {
                                                                    echo $customer->comments;
                                                                } ?>" class="form-control input" rows="3"><?php if (isset($customer)) {
                                                                            echo $customer->comments;
                                                                        } ?></textarea>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Secondary Email:</label>
                        <div class="col-lg-4">
                            <input name='secondary_email' value="<?php if (isset($customer)) {
                                                                        echo $customer->secondary_email;
                                                                    } ?>" type="text" class="form-control input" />
                            <span class="form-text text-muted"></span>
                        </div>
                        <label class="col-lg-2 col-form-label text-right">Status:</label>
                        <div class="col-lg-4">
                            <select class="form-control selectpicker" name="customer_status">
                                <option @if(isset($customer) && $customer->customer_status =='Active') selected @endif
                                    value="Active">Active
                                </option>
                                <option @if(isset($customer) && $customer->customer_status =='Passive') selected @endif value="Passive">Passive
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-7">
                            <a class="btn btn__bg" href="{{ route('admin.customers.list') }}"> Cancel</a>
                            <button class="btn btn__bg mr-2 " id="submitCustomer" type="button">@if(isset($customer))
                                Update @else Add @endif
                            </button>
                        </div>
                        <div class="col-lg-3">
                            <label class="col-form-label"><strong>Alternative Delivery Address:</strong><a data-toggle="modal" data-target="#addressModal" style="margin-left: 10px;"><i class="fa fa-plus-square" aria-hidden="true" style="color: #353B84;font-size: 25px;"></i></a></label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--end::Container-->

    <div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Alternate Address</h4>
                </div>
                <div class="modal-body">
                    <div class=" container ">
                        <form class="form" id="addressForm" method="post">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">Postcode<span>*</span>:</label>
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                                <input type="text" value="<?php if (isset($alternate_address)) {
                                                                                echo $alternate_address->postCode;
                                                                            } ?>" name="postCode" id="postCode" onkeyup="this.value = this.value.toUpperCase();" class="form-control required input" />
                                            </div>
                                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                                        </div>
                                        <label class="col-lg-2 col-form-label text-right">Address1:<span>*</span></label>
                                        <div class="col-lg-4">
                                            <input type="text" value="<?php if (isset($alternate_address)) {
                                                                            echo $alternate_address->address1;
                                                                        } ?>" name="address1" id="address1" class="form-control required input" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">Address2:</label>
                                        <div class="col-lg-4">
                                            <input type="text" name="address2" id="address2" value="<?php if (isset($alternate_address)) {
                                                                                                        echo $alternate_address->address2;
                                                                                                    } ?>" class="form-control input" />
                                        </div>
                                        <label class="col-lg-2 col-form-label text-right">Town:<span>*</span></label>
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                                <input type="text" name="town" id="town" value="<?php if (isset($alternate_address)) {
                                                                                                    echo $alternate_address->town;
                                                                                                } ?>" class="form-control input" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">Area:</label>
                                        <div class="col-lg-4">
                                            <input type="text" value="<?php if (isset($alternate_address)) {
                                                                            echo $alternate_address->area;
                                                                        } ?>" name="area" id="area" class="form-control input" />
                                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                                        </div>

                                        <label class="col-lg-2 col-form-label text-right">Country<span>*</span>:</label>
                                        <div class="col-lg-4">
                                            <select id="country1" name="country" data-id="<?php if (isset($alternate_address)) {
                                                                                                echo $alternate_address->country;
                                                                                            } ?>" class="form-control selectpicker">
                                                <!-- <option>Choose one</option>
                                                 <option @if(isset($customer)) selected @endif>India</option>
                                                 <option>America</option>-->
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-custom">
                                <div class="card-body">`
                                    <div class="form-group row">

                                        <label class="col-lg-2 col-form-label text-right">Postcode<span>*</span>:</label>
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                                <input type="text" value="<?php if (isset($alternate_address)) {
                                                                                echo $alternate_address->postCode2;
                                                                            } ?>" name="postCode2" onkeyup="this.value = this.value.toUpperCase();" id="postCode2" class="form-control required input" />
                                            </div>
                                            <!-- <span class="form-text text-muted">Please enter your postcode</span> -->
                                        </div>
                                        <label class="col-lg-2 col-form-label text-right">Address1:<span>*</span></label>
                                        <div class="col-lg-4">
                                            <input type="text" value="<?php if (isset($alternate_address)) {
                                                                            echo $alternate_address->addrs1;
                                                                        } ?>" name="addrs1" id="addrs1" class="form-control required input" />
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">Address2:</label>
                                        <div class="col-lg-4">
                                            <input type="text" name="addrs2" id="addrs2" value="<?php if (isset($alternate_address)) {
                                                                                                    echo $alternate_address->addrs2;
                                                                                                } ?>" class="form-control input" />
                                        </div>
                                        <label class="col-lg-2 col-form-label text-right">Town:<span>*</span></label>
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                                <input type="text" name="town2" id="town2" value="<?php if (isset($alternate_address)) {
                                                                                                        echo $alternate_address->town2;
                                                                                                    } ?>" class="form-control input" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">Area:</label>
                                        <div class="col-lg-4">
                                            <input type="text" value="<?php if (isset($alternate_address)) {
                                                                            echo $alternate_address->area2;
                                                                        } ?>" name="area2" id="area2" class="form-control input" />
                                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                                        </div>
                                        <label class="col-lg-2 col-form-label text-right">Country<span>*</span>:</label>
                                        <div class="col-lg-4">
                                            <select id="country2" name="country" data-id="<?php if (isset($alternate_address)) {
                                                                                                echo $alternate_address->country;
                                                                                            } ?>" class="form-control required selectpicker">
                                                <!-- <option>Choose one</option>
                                                 <option @if(isset($customer)) selected @endif>India</option>
                                                 <option>America</option>-->
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">Comment1:</label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <textarea type="text" id="comment1" name="comment1" value="<?php if (isset($alternate_address)) {
                                                                                                                echo $alternate_address->comment1;
                                                                                                            } ?>" class="form-control input" /></textarea>
                                            </div>
                                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-right">Comment2:</label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <textarea type="text" id="comment2" name="comment2" value="<?php if (isset($alternate_address)) {
                                                                                                                echo $alternate_address->comment2;
                                                                                                            } ?>" class="form-control input" /></textarea>
                                            </div>
                                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeModal" class="btn btn__bg" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn__bg mr-2" id="submitAddress">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div id="add_branch" class="modal fade bs-example-modal-lg in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 style="text-align:center" class="modal-title pull-center" id="myModalLabel"> SELECT ADDRESS</h4>
                </div>
                <div class="modal-body">
                    <div id="show_address"></div>
                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('script')

    <script src="/js/countries.js"></script>
    <script src="/js/pages/customers/customers.js"></script>
    <script>
        populateCountries("country");
        populateCountries("country1");
        populateCountries("country2");
    </script>
    @endpush