@extends('admin.layouts.app')
@section('title','Create Customer - Smart Print')
@section('content')

<!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                   @if(isset($customer)) Edit @else New @endif Customer                           
                </h5>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <div class="d-flex flex-column-fluid">
        <div class=" container ">

            <div class="card card-custom">
				<!--begin::Form-->
				<form class="form">
					<div class="card-body">
						
						<div class="form-group row">
							<label class="col-lg-2 col-form-label text-right">Address1:</label>
							<div class="col-lg-4">
								<input type="text" class="form-control input"/>
								<!-- <span class="form-text text-muted">Please enter your address</span> -->
							</div>
							<label class="col-lg-2 col-form-label text-right">Town:<span>*</span></label>
							<div class="col-lg-4">
								<div class="input-group">
									<input type="text" class="form-control input"/>
								</div>
								<!-- <span class="form-text text-muted">Please enter your postcode</span> -->
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-2 col-form-label text-right">Address2:</label>
							<div class="col-lg-4">
								<input type="text" class="form-control input"/>
								<!-- <span class="form-text text-muted">Please enter your address</span> -->
							</div>
							<label class="col-lg-2 col-form-label text-right">Country<span>*</span>:</label>
							<div class="col-lg-4">
								<select class="form-control selectpicker">
									<option>Choose one</option>
									<option @if(isset($customer)) selected @endif>India</option>
									<option>America</option>
								</select>
								<!-- <span class="form-text text-muted">Please enter your postcode</span> -->
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-2 col-form-label text-right">Area:</label>
							<div class="col-lg-4">
								<input type="text" class="form-control input"/>
								<!-- <span class="form-text text-muted">Please enter your address</span> -->
							</div>
							<label class="col-lg-2 col-form-label text-right">Postcode<span>*</span>:</label>
							<div class="col-lg-4">
								<div class="input-group">
									<input type="text" class="form-control input"/>
								</div>
								<!-- <span class="form-text text-muted">Please enter your postcode</span> -->
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-10">
								<a class="btn btn__bg" href="{{ route('admin.customers.create') }}"> Cancel</a>
                                <button class="btn btn__bg mr-2 ">Add</button>
							</div>
						</div>
					</div>
				</form>
			</div>
        </div>
        <!--end::Container-->
@endsection