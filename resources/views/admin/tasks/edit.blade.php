@extends('admin.layouts.app')
@section('title','Create Staff - Smart Print')
@section('content')
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                Add Task
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <form class="form" id="taskForm" method="post" class="form" enctype="multipart/form-data">
                {{ csrf_field() }}                
                <input id="task_id" name="task_id" value="{!! $data['task']['task_id'] ?: old('name')  !!}"
                       type="hidden" class="form-control">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Title<span style="color:red;">*</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" name="task_name" class="form-control input required" value="{!! $data['task']['task_name'] ?: old('name')  !!}"/>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Description<span style="color:red;">*</span>:</label>
                        <div class="col-lg-10">
                            <textarea class="form-control input required" name="task_description" id="exampleTextarea" rows="3">{!! $data['task']['task_description'] ?: old('name')  !!}</textarea>
                            <!-- <span class="form-text text-muted">Please enter your address</span> -->
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Assign To<span>*</span>:</label>
                        <div class="col-lg-4">
                            <select id="country1" name="task_assign_to"
                                    data-id="<?php if (isset($alternate_address)) {
                                        echo $alternate_address->country;
                                    } ?>"
                                    class="form-control required selectpicker">
                                    <option>Choose one</option>
                                    @foreach($data['staff'] as $staffs)
                                    @if($staffs->staffId == $data['task']['task_assign_to'])
                                        <option selected="selected" value="{{$staffs->staffId}}">{{$staffs->firstName}} {{$staffs->lastName}}</option>
                                    @else
                                        <option  value="{{$staffs->staffId}}">{{$staffs->firstName}} {{$staffs->lastName}}</option>
                                    @endif
                                    @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-10">
                            <a class="btn btn__bg" href="{{ route('admin.tasks.list') }}"> Cancel</a>
                            <button type="button" id="submitTask" class="btn btn__bg mr-2 ">
                                @if(isset($staffs)) Update
                                @else Add @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--end::Container-->

    <style type="text/css">
        .checkboc_bottom {
            margin-bottom: 20px !important;
            font-size: 16px !important;
            color: #353B84 !important;
        }
    </style>

    @endsection

@push('script')
    <script src="/js/pages/tasks/tasks.js"></script>
    <script type="text/javascript">
        $(".date").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

    </script>
@endpush
