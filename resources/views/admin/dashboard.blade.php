@extends('admin.layouts.app')
@section('title','Dashboard - Smart Print')
@section('content')

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                Dashboard
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->

<div class="d-flex flex-column-fluid">
    <div class="container">
        <!--begin::Dashboard-->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <!-- <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">
                                Column Chart
                            </h3>
                        </div>
                    </div> -->
                    <div class="card-body">
                        <select class="selectpicker" id="year" onchange="changeYear()">
                            <?php
                            $curent_date = date('Y');
                            for ($i = '2001'; $i <= $curent_date; $i++) {
                                ?>
                                <option <?php if ($curent_date == $i) {
                                    echo 'selected';
                                } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <!--begin::Chart-->
                        <div id="dashboardChart">
                        </div>
                        <!--end::Chart-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
        </div>
    </div>
    <!--end::Dashboard-->
    @endsection


    @push('script')
    <script type="text/javascript">
        changeYear();
        function changeYear() {
            var post_data = new FormData();
            post_data.append('year', $('#year').val());
            ajax_request('/admin/getDashboardData', post_data, submit_response, null);
        }
        function submit_response(res) {
            var e = {
                series:res['final']['data'],
                chart: {
                    type: "bar",
                    height: 350
                },
                plotOptions: {
                    bar: {
                        horizontal: !1,
                        columnWidth: "55%",
                        endingShape: "rounded"
                    }
                },
                dataLabels: {
                    enabled: !1
                },
                stroke: {
                    show: !0,
                    width: 2,
                    colors: ["transparent"]
                },
                xaxis: {
                    categories: res['final']['month']
                },
                yaxis: {
                    title: {
                        text: "Count"
                    }
                },
                fill: {
                    opacity: 1
                },
                tooltip: {
                    y: {
                        formatter: function (e) {
                            return "Total Count :" + e + ""
                        }
                    }
                },
                colors: [primary, success, warning, info]
            };
            new ApexCharts(document.querySelector("#dashboardChart"), e).render()
        }

    </script>
    @endpush
