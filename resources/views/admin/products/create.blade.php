@extends('admin.layouts.app')
@section('title','Create Product - Smart Print')
@section('content')

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-end flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                @if(isset($product)) Edit @else New @endif Product
            </h5>
            <!--end::Page Title-->
        </div>
        <!--end::Info-->
    </div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
    <div class=" container ">

        <div class="card card-custom">
            <!--begin::Form-->
            <form class="form" id="productForm" method="post" class="form" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="card-body">
                    <div class="form-group row">
                    <label class="col-lg-2 col-form-label text-right">Bussiness Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-6 selectOption">
                            <input class="form-control input optionInput" type="text" style="display: none"/>
                            <select name='customer_id' class="form-control selectpicker selectedOption  required"
                                    id="customer_id">
                                <option value="">Choose one</option>
                                @foreach($customer as $customer)
                                @if(isset($product) && $customer->customer_id  == $product->customer_id )
                                <option selected="selected" value="{{$customer->customer_id}}">{{ $customer->business_name }}
                                </option>
                                @else
                                <option value="{{$customer->customer_id}}">{{ $customer->business_name }}</option>
                                @endif
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-lg-2 col-form-label text-right">Category Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-6 selectOption">
                            <input class="form-control input optionInput" type="text" style="display: none"/>
                            <select name='category_id' class="form-control selectpicker selectedOption  required"
                                    id="category_id">
                                <option value="">Choose one</option>
                                @foreach($category as $category )
                                @if(isset($product) && $category->id  == $product->category_id )
                                <option selected="selected" value="{{$category->id }}">{{ $category->name }}
                                </option>
                                @else
                                <option value="{{$category->id}}">{{ $category->name }}</option>
                                @endif
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Product Name<span style="color:red;">*</span>:</label>
                        <div class="col-lg-6">
                            <input type='hidden' name='id' value='<?php if (isset($product)) {
                                    echo $product->id;
                                } ?>'/>
                            <div class="input-group">
                                <input type="text" value="<?php if (isset($product)) {
                                    echo $product->product_name;
                                } ?>"
                                       name='product_name' class="form-control input required"/>
                            </div>
                            <span class="form-text text-muted"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Description <span style="color:red;">*</span>:</label>
                        <div class="col-lg-6">
                        <textarea class="form-control input" name="product_description" id="exampleTextarea" rows="3">
                                <?php if(isset($product)){
                                    echo $product->product_description; 
                                }
                                ?>
                        </textarea>
                            <!-- <span class="form-text text-muted"></span> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Product Price<span style="color:red;">*</span>:</label>
                        <div class="col-lg-6">
                            <input type="number" min='1' value="<?php if (isset($product)) {
                                echo $product->price;
                            } ?>" name='price'
                                   class="form-control input required"/>
                            <span class="form-text text-muted"></span>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Status:</label>
                        <div class="col-lg-6">
                            <select class="form-control selectpicker" name="status">
                                <option @if(isset($product) && $product->status =='1') selected @endif
                                    value="Active">Active
                                </option>
                                <option @if(isset($product) && $product->status =='0') selected @endif value="Passive">Passive
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-7">
                            <a class="btn btn__bg" href="{{ route('admin.products.list') }}"> Cancel</a>
                            <button class="btn btn__bg mr-2 " id="submitproduct" type="button">@if(isset($product))
                                Update @else Add @endif
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--end::Container-->


@endsection

    @push('script')

    <script src="/js/pages/product/products.js"></script>
    @endpush
