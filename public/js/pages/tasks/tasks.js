getTaskData();
var table = '';
init();

function init() {
    table = $("#taskDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}
$("body").on("click", "#submitTask", function() {
    var isValidate = true;
    $("#taskForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            $("#taskForm").find('.dropdown-toggle').css("border", "2px dashed #CC0000");
        } else {
            $(this).css("border", "");
            $("#taskForm").find('.dropdown-toggle').css("border", "");
        }
    });
    if (isValidate == true) {
        var post_data = new FormData($('#taskForm')[0]);
        ajax_request('/admin/tasks/store', post_data, submit_response, null);
    } else {
        toast_error("Please enter all required fields");
    }
});


function submit_response(result) {
    if (result.status == "success") {
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/tasks"
        }, 1200)
    } else {
        toast_error(result.message);
    }
}

function getTaskData() {
    var post_data = {};
    ajax_request('/admin/tasks/getTaskData', post_data, getTaskDetails, null);
}

function getTaskDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    var edit_permission=$('#editpermission').val();
    var delete_permission=$('#deletepermission').val();
    $(result['data']).each(function(index, data) {
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.task_name + '</td>' +
            '<td>' + data.task_description + '</td>' +
            '<td>' + data.firstName + data.lastName + '</td>';
            if(edit_permission == 1 || delete_permission == 1){
                html += '<td>' ;
            }
            if(edit_permission == 1){
            html += '<a onclick="editTask(' + data.task_id + ')" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">' +
            '<span class="svg-icon svg-icon-md">' +
            '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' +
            '</span>' +
            '</a>' ;
        }
        if(delete_permission == 1){
            html += '<a  style="display:none;" onclick="deleteTask(' + data.task_id + ')" class="btn btn-sm btn-clean btn-icon hide_delete" title="Delete">' +
            '<span class="svg-icon svg-icon-md">' +
            '<i class="far fa-trash-alt"></i>' +
            '</span>' +
            '</a>' ;
        }
        if(edit_permission == 1 || delete_permission == 1){
            html += '</td>' ;
        }
            html +='</tr>';
    });
    $('.tbody').html(html);
    init();
}

function editTask(id) {
    window.location.href = "/admin/tasks/edit/" + id;
}

function deleteTask(id) {
    var post_data = new FormData();
    post_data.append('id', id);
    deleteswal('/admin/tasks/delete', post_data, deleteResponse, null);
}

function deleteResponse(result) {
    if (result.status == "success") {
        toast_success(result.message);
        getTaskData();
    } else {
        toast_error(result.message);
    }
}