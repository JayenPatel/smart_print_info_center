getProductData();
var table = '';
var alternetAddress = [];
init();

function init() {
    //alternetAddress = $('#addressForm').serializeArray();
    table = $("#ProductDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}
$("body").on("click", ".NewOption", function() {
    $(this).parent().find('.NewOption').html('Add');
    $(this).parent().find('.removeOption').html('Cancel');

    $(this).parent().find('.NewOption').addClass('saveOption');
    $(this).parent().find('.NewOption').removeClass('NewOption');

    $(this).parent().find('.removeOption').addClass('cancelOption');
    $(this).parent().find('.removeOption').removeClass('removeOption');
    $(this).parent().parent().find('.selectOption').find('.optionInput').show();
    $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
});
$("body").on("click", ".cancelOption", function() {
    $(this).parent().find('.saveOption').html('+');
    $(this).parent().find('.cancelOption').html('-');

    $(this).parent().find('.saveOption').addClass('NewOption');
    $(this).parent().find('.saveOption').removeClass('saveOption');

    $(this).parent().find('.cancelOption').addClass('removeOption');
    $(this).parent().find('.cancelOption').removeClass('cancelOption');

    $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
    $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
});
$("body").on("click", ".saveOption", function() {
    var val = $(this).parent().parent().find('.optionInput').val();
    $(this).parent().parent().find('.optionInput').val('');
    if ($.trim(val) != '') {
        $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
        $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
        $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
        $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
        $(this).parent().parent().find('.cancelOption').click();
    } else {
        toast_error('Please Enter Valid Value');
    }

});
$("body").on("click", ".removeOption", function() {
    $(this).parent().parent().find('.selectedOption').selectpicker('val', '');
});

$("body").on("click", "#submitproduct", function() {
    var isValidate = true;
    $("#productForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#productForm')[0]);
        ajax_request('/admin/products/store', post_data, submit_product_response, null);
    } else {
        toast_error("Please enter all required fields");
    }
});

function submit_product_response(result) {
    if (result.status === "success") {
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/products"
        }, 1200)
    } else {
        toast_error(result.message);
    }
}

function getProductData() {
    var post_data = {};
    ajax_request('/admin/products/getProductData', post_data, getProductDetails, null);
}

function getProductDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    $(result['data']).each(function(index, data) {
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.business_name + '</td>' +
            '<td>' + data.product_name + '</td>' +
            '<td>' + data.product_description + '</td>' +
            '<td>$' + data.price + '</td>' +
            '<td>' +
            '<a onclick="editProduct(' + data.id + ')" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">' +
            '<span class="svg-icon svg-icon-md">' +
            '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' +
            '</span>' +
            '</a>' +

            '<a style="display:none;" onclick="deleteProduct(' + data.id + ')"  class="btn btn-sm btn-clean btn-icon hide_delete" title="Delete">' +
            '<span class="svg-icon svg-icon-md">' +
            '<i class="far fa-trash-alt"></i>' +
            '</span>' +
            '</a>' +
            '</td>' +
            '</tr>';
    });
    $('.tbody').html(html);
    init();
}


function editProduct(id) {
    window.location.href = "/admin/products/edit/" + id;
}

function deleteProduct(id) {
    var post_data = new FormData();
    post_data.append('id', id);
    deleteswal('/admin/products/delete', post_data, deleteResponse, null);
}

function deleteResponse(result) {
    if (result.status == "success") {
        toast_success(result.message);
        getProductData();
        location.reload();

    } else {
        toast_error(result.message);
    }
}