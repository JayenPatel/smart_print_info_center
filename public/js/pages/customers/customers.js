getCustomerData();
var table = '';
var alternetAddress = [];
init();

function init() {
    alternetAddress = $('#addressForm').serializeArray();
    table = $("#customerDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}
$("body").on("click", ".NewOption", function() {
    $(this).parent().find('.NewOption').html('Add');
    $(this).parent().find('.removeOption').html('Cancel');

    $(this).parent().find('.NewOption').addClass('saveOption');
    $(this).parent().find('.NewOption').removeClass('NewOption');

    $(this).parent().find('.removeOption').addClass('cancelOption');
    $(this).parent().find('.removeOption').removeClass('removeOption');
    $(this).parent().parent().find('.selectOption').find('.optionInput').show();
    $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
});
$("body").on("click", ".cancelOption", function() {
    $(this).parent().find('.saveOption').html('+');
    $(this).parent().find('.cancelOption').html('-');

    $(this).parent().find('.saveOption').addClass('NewOption');
    $(this).parent().find('.saveOption').removeClass('saveOption');

    $(this).parent().find('.cancelOption').addClass('removeOption');
    $(this).parent().find('.cancelOption').removeClass('cancelOption');

    $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
    $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
});
$("body").on("click", ".saveOption", function() {
    var val = $(this).parent().parent().find('.optionInput').val();
    $(this).parent().parent().find('.optionInput').val('');
    if ($.trim(val) != '') {
        $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
        $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
        $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
        $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
        $(this).parent().parent().find('.cancelOption').click();
    } else {
        toast_error('Please Enter Valid Value');
    }

});
$("body").on("click", ".removeOption", function() {
    $(this).parent().parent().find('.selectedOption').selectpicker('val', '');
});

$("body").on("click", "#submitCustomer", function() {
    var isValidate = true;
    $("#customerForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#customerForm')[0]);
        post_data.append('alternate_address', JSON.stringify(alternetAddress));
        ajax_request('/admin/customers/store', post_data, submit_customer_response, null);
    } else {
        toast_error("Please enter all required fields");
    }
});
$("body").on("click", "#submitAddress", function() {
    var isValidate = true;
    $("#customerForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        alternetAddress = $('#addressForm').serializeArray();
        toast_success("Address Saved Successfully");
        $('#closeModal').click();
    } else {
        toast_error("Please enter all required fields");
    }
});

function submit_customer_response(result) {
    if (result.status === "success") {
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/customers/list"
        }, 1200)
    } else {
        toast_error(result.message);
    }
}

function getCustomerData() {
    var post_data = {};
    ajax_request('/admin/customers/getCustomerData', post_data, getCustomerDetails, null);
}

function getCustomerDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    var edit_permission=$('#editpermission').val();
    var delete_permission=$('#deletepermission').val();
    $(result['data']).each(function(index, data) {
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.business_name + '</td>' +
            '<td>' + data.county + '</td>' +
            '<td>' + data.postcode + '</td>' ;
    if(edit_permission == 1 || delete_permission == 1){
        html += '<td>' ;
    }
        
    if(edit_permission == 1){
            html += '<a onclick="editCustomer(' + data.customer_id + ')" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">' +
            '<span class="svg-icon svg-icon-md">' +
            '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' +
            '</span>' +
            '</a>';
    }
    if(delete_permission == 1){
        html +='<a style="display:none;" onclick="deleteCustomer(' + data.customer_id + ')"  class="btn btn-sm btn-clean btn-icon hide_delete" title="Delete">' +
            '<span class="svg-icon svg-icon-md">' +
            '<i class="far fa-trash-alt"></i>' +
            '</span>' +
            '</a>' ;
        }
        if(edit_permission == 1 || delete_permission == 1){
            html += '</td>' ;
        }
            html +='</tr>';
        
    });
    $('.tbody').html(html);
    init();
}


function editCustomer(id) {
    window.location.href = "/admin/customers/edit/" + id;
}

function deleteCustomer(id) {
    var post_data = new FormData();
    post_data.append('id', id);
    deleteswal('/admin/customers/delete', post_data, deleteResponse, null);
}

function deleteResponse(result) {
    if (result.status == "success") {
        toast_success(result.message);
        getCustomerData();
        location.reload();

    } else {
        toast_error(result.message);
    }
}


$("#postcode").on('keydown', function(event) {
    $("#show_address").html("");
    if (event.keyCode == 13 || event.keyCode == 9) {
        if ($("#postcode").val() != "") {
            $("#show_address").html("<center><h4>Loading...</h4><center>");
            $('#add_branch').modal('show');
            var postcode = $("#postcode").val();
            $.ajax({
                url: "https://api.getAddress.io/find/" + postcode + "?api-key=UnXxCxBrtEWV5W9okbD_Xg27485",
                type: "GET",
                contentType: "JSON",
                success: function(data) {
                    $("#show_address").html("");
                    var create_html = "";
                    $.each(data, function(index, element) {
                        if (index == "addresses") {
                            for (var i = 0; i < element.length; i++) {
                                var temp_length = element[i].length;
                                var temp_name = "";
                                while (temp_length--) {
                                    if (element[i].charAt(temp_length) == ",") {
                                        if (element[i].charAt(temp_length - 2) == ",") {
                                            temp_name += "";
                                        } else {
                                            if (element[i].charAt(temp_length) == " ") {
                                                console.log("space" + element[i].charAt(temp_length));
                                            } else {
                                                temp_name += element[i].charAt(temp_length);
                                            }
                                        }
                                    } else {
                                        temp_name += element[i].charAt(temp_length);
                                    }
                                }
                                temp_name = temp_name.split("").reverse().join("").trim();
                                temp_name = temp_name.replace("'", ' ');
                                create_html += '<div class="row add_list"><div class="col-lg-10 col-lg-offset-2"><input class="btn btn-info select_address" type="button" onclick="set_address(' + "'" + temp_name.toString() + "'" + ');" value="SELECT" name="' + temp_name.toString() + '"><span>' + temp_name + '</span></div></div>';
                            }
                        }
                    });
                    $("#show_address").html(create_html);
                },
                error: function(error) {
                    $("#show_address").html("<h4>No Data Found.</h4>");
                }
            });
        }
        itype = $(this).prop('type');
        if (itype !== 'submit') {
            currentTabindex = $(this).attr('tabindex');
            if (currentTabindex) {
                nextInput = $('input[tabindex^="' + (parseInt(currentTabindex) + 1) + '"]');
                if (nextInput.length) {
                    nextInput.focus();
                    return false;
                }
            }
        }
    }
});

$("#postCode").on('keydown', function(event) {
    $("#show_address").html("");
    if (event.keyCode == 13 || event.keyCode == 9) {
        if ($("#postCode").val() != "") {
            $("#show_address").html("<center><h4>Loading...</h4><center>");
            $('#add_branch').modal('show');
            var postcode = $("#postCode").val();
            $.ajax({
                url: "https://api.getAddress.io/find/" + postcode + "?api-key=UnXxCxBrtEWV5W9okbD_Xg27485",
                type: "GET",
                contentType: "JSON",
                success: function(data) {
                    $("#show_address").html("");
                    var create_html = "";
                    $.each(data, function(index, element) {
                        if (index == "addresses") {
                            for (var i = 0; i < element.length; i++) {
                                var temp_length = element[i].length;
                                var temp_name = "";
                                while (temp_length--) {
                                    if (element[i].charAt(temp_length) == ",") {
                                        if (element[i].charAt(temp_length - 2) == ",") {
                                            temp_name += "";
                                        } else {
                                            if (element[i].charAt(temp_length) == " ") {
                                                console.log("space" + element[i].charAt(temp_length));
                                            } else {
                                                temp_name += element[i].charAt(temp_length);
                                            }
                                        }
                                    } else {
                                        temp_name += element[i].charAt(temp_length);
                                    }
                                }
                                temp_name = temp_name.split("").reverse().join("").trim();
                                temp_name = temp_name.replace("'", ' ');
                                create_html += '<div class="row add_list"><div class="col-lg-10 col-lg-offset-2"><input class="btn btn-info select_address" type="button" onclick="set_address1(' + "'" + temp_name.toString() + "'" + ');" value="SELECT" name="' + temp_name.toString() + '"><span>' + temp_name + '</span></div></div>';
                            }
                        }
                    });
                    $("#show_address").html(create_html);
                },
                error: function(error) {
                    $("#show_address").html("<h4>No Data Found.</h4>");
                }
            });
        }
        itype = $(this).prop('type');
        if (itype !== 'submit') {
            currentTabindex = $(this).attr('tabindex');
            if (currentTabindex) {
                nextInput = $('input[tabindex^="' + (parseInt(currentTabindex) + 1) + '"]');
                if (nextInput.length) {
                    nextInput.focus();
                    return false;
                }
            }
        }
    }
});

$("#postCode2").on('keydown', function(event) {
    $("#show_address").html("");
    if (event.keyCode == 13 || event.keyCode == 9) {
        if ($("#postCode2").val() != "") {
            $("#show_address").html("<center><h4>Loading...</h4><center>");
            $('#add_branch').modal('show');
            var postcode = $("#postCode2").val();
            $.ajax({
                url: "https://api.getAddress.io/find/" + postcode + "?api-key=UnXxCxBrtEWV5W9okbD_Xg27485",
                type: "GET",
                contentType: "JSON",
                success: function(data) {
                    $("#show_address").html("");
                    var create_html = "";
                    $.each(data, function(index, element) {
                        if (index == "addresses") {
                            for (var i = 0; i < element.length; i++) {
                                var temp_length = element[i].length;
                                var temp_name = "";
                                while (temp_length--) {
                                    if (element[i].charAt(temp_length) == ",") {
                                        if (element[i].charAt(temp_length - 2) == ",") {
                                            temp_name += "";
                                        } else {
                                            if (element[i].charAt(temp_length) == " ") {
                                                console.log("space" + element[i].charAt(temp_length));
                                            } else {
                                                temp_name += element[i].charAt(temp_length);
                                            }
                                        }
                                    } else {
                                        temp_name += element[i].charAt(temp_length);
                                    }
                                }
                                temp_name = temp_name.split("").reverse().join("").trim();
                                temp_name = temp_name.replace("'", ' ');
                                create_html += '<div class="row add_list"><div class="col-lg-10 col-lg-offset-2"><input class="btn btn-info select_address" type="button" onclick="set_address2(' + "'" + temp_name.toString() + "'" + ');" value="SELECT" name="' + temp_name.toString() + '"><span>' + temp_name + '</span></div></div>';
                            }
                        }
                    });
                    $("#show_address").html(create_html);
                },
                error: function(error) {
                    $("#show_address").html("<h4>No Data Found.</h4>");
                }
            });
        }
        itype = $(this).prop('type');
        if (itype !== 'submit') {
            currentTabindex = $(this).attr('tabindex');
            if (currentTabindex) {
                nextInput = $('input[tabindex^="' + (parseInt(currentTabindex) + 1) + '"]');
                if (nextInput.length) {
                    nextInput.focus();
                    return false;
                }
            }
        }
    }
});

function set_address(address) {
    $('#add_branch').modal('hide');
    $("#autoAddress").val(address);
}

function set_address1(address) {
    $('#add_branch').modal('hide');
    $("#address1").val(address);
}

function set_address2(address) {
    $('#add_branch').modal('hide');
    $("#addrs1").val(address);
}