
$("body").on("click", "#submitStaff", function () {
    var isValidate = true;
    $("#staffForm .required").each(function () {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000" );
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#staffForm')[0]);
        ajax_request('/admin/staff/store',post_data,submit_response ,null);
    }else{
        toast_error("Please enter all required fields");
    }
});
function submit_response(result){
    if(result.status=="success"){
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/staff"
        },1200)
    }else{
        toast_error(result.message);
    }
}
function getCustomerData(){
   var post_data={};
    ajax_request('/admin/staff/store',post_data,getCustomerDetails ,null);
}
function getCustomerDetails(data){

}
