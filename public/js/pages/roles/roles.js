function permission(selectObject) {
    var value = selectObject.value;  
    window.location = value;
  }
  $("body").on("click", "#submitpermission", function() {
    var isValidate = true;
    $("#roleForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var data = new FormData($('#roleForm')[0]);
        var role_id= $('#roleId').val();
        //console.log(role_id); 
         ajax_request(role_id, data, submit_role_response, null);
    } else {
        toast_error("Please enter all required fields");
    }
});
function submit_role_response(result)
{
    if (result.status === "success") {
        toast_success(result.message);
        setTimeout(function() {
            location.reload();
        }, 1200)
        } else {
            toast_error(result.message);
        }
}