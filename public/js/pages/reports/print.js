getPrintData();
var table = '';
var alternetAddress = [];
init();

function init() {
    //alternetAddress = $('#addressForm').serializeArray();
    table = $("#printDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}

function getPrintData() {
    var post_data = {};
    ajax_request('/admin/reports/print', post_data, getPrintDetails, null);
}

function getPrintDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    $(result['data']).each(function(index, data) {
        if(data.status == 'A')
        {
            data.status='Active';
        }
        if(data.status == 'D')
        {
            data.status='Done';
        }
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.job_title + '</td>' +
            '<td>' + data.description + '</td>' +
            '<td>' + data.status + '</td>' +
            '<td>' + data.quantity + '</td>' +
            '<td>' + data.category + '</td>' +
            '</tr>';
    });
    $('.tbody').html(html);
    init();
}
$("body").on("click", "#submitPrint", function() {
    var isValidate = true;
    $("#PrintForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#PrintForm')[0]);
        ajax_request('/admin/reports/print', post_data, getPrintDetails, null);
    } else {
        toast_error("Please enter all required fields");
    }
});

