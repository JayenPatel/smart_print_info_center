getSupplierData();
var table = '';
var alternetAddress = [];
init();

function init() {
    //alternetAddress = $('#addressForm').serializeArray();
    table = $("#supplierDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}

function getSupplierData() {
    var post_data = {};
    ajax_request('/admin/reports/supplier', post_data, getSupplierDetails, null);
}

function getSupplierDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    $(result['data']).each(function(index, data) {
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.first_name+ " "+data.surname + '</td>' +
            '<td>' + data.business_name + '</td>' +
            '<td>' + data.county + '</td>' +
            '<td>' + data.postcode + '</td>' +
            '<td>' + data.created_at + '</td>' +
            '</tr>';
    });
    $('.tbody').html(html);
    init();
}
$("body").on("click", "#submitsupplier", function() {
    var isValidate = true;
    $("#supplierForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#supplierForm')[0]);
        ajax_request('/admin/reports/supplier', post_data, getSupplierDetails, null);
    } else {
        toast_error("Please enter all required fields");
    }
});

