getOtherData();
var table = '';
var alternetAddress = [];
init();

function init() {
    //alternetAddress = $('#addressForm').serializeArray();
    table = $("#otherDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}

function getOtherData() {
    var post_data = {};
    ajax_request('/admin/reports/other', post_data, getOtherDetails, null);
}

function getOtherDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    $(result['data']).each(function(index, data) {
        if(data.status == 'A')
        {
            data.status='Active';
        }
        if(data.status == 'D')
        {
            data.status='Done';
        }
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.job_title + '</td>' +
            '<td>' + data.description + '</td>' +
            '<td>' + data.status + '</td>' +
            '<td>' + data.quantity + '</td>' +
            '<td>' + data.category + '</td>' +
            '</tr>';
    });
    $('.tbody').html(html);
    init();
}
$("body").on("click", "#submitother", function() {
    var isValidate = true;
    $("#otherForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#otherForm')[0]);
        ajax_request('/admin/reports/other', post_data, getOtherDetails, null);
    } else {
        toast_error("Please enter all required fields");
    }
});

