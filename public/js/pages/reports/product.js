getProductData();
var table = '';
init();

function init() {
    table = $("#productDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}
function getProductData() {
    var post_data = {};
    ajax_request('/admin/reports/product', post_data, getProductDetails, null);
}

function getProductDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    $(result['data']).each(function(index, data) {
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.business_name + '</td>' +
            '<td>' + data.name + '</td>' +
            '<td>' + data.product_name + '</td>' +
            '<td>' + data.product_description + '</td>' +
            '<td>$' + data.price + '</td>' +
            '</tr>';
    });
    $('.tbody').html(html);
    init();
}
$("body").on("click", "#submitProduct", function() {
    var isValidate = true;
    $("#ProductForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#ProductForm')[0]);
        ajax_request('/admin/reports/product', post_data, getProductDetails, null);
    } else {
        toast_error("Please enter all required fields");
    }
});

