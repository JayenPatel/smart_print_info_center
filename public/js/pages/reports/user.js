getCustomerData();
var table = '';
var alternetAddress = [];
init();

function init() {
    //alternetAddress = $('#addressForm').serializeArray();
    table = $("#customerDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}

function getCustomerData() {
    var post_data = {};
    ajax_request('/admin/reports/user', post_data, getCustomerDetails, null);
}

function getCustomerDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    $(result['data']).each(function(index, data) {
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.first_name+ " "+data.surname + '</td>' +
            '<td>' + data.email + '</td>' +
            '<td>' + data.business_name + '</td>' +
            '<td>' + data.county + '</td>' +
            '<td>' + data.postcode + '</td>' +
            '<td>' + data.created_at + '</td>' +
            '</tr>';
    });
    $('.tbody').html(html);
    init();
}
$("body").on("click", "#submitCustomer", function() {
    var isValidate = true;
    $("#customerForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#customerForm')[0]);
        ajax_request('/admin/reports/user', post_data, getCustomerDetails, null);
    } else {
        toast_error("Please enter all required fields");
    }
});

