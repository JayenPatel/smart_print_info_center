getStaffData();
var table = '';
var alternetAddress = [];
init();

function init() {
    //alternetAddress = $('#addressForm').serializeArray();
    table = $("#staffDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}

function getStaffData() {
    var post_data = {};
    ajax_request('/admin/reports/staff', post_data, getStaffDetails, null);
}

function getStaffDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    $(result['data']).each(function(index, data) {
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.firstName+ " "+data.lastName + '</td>' +
            '<td>' + data.loginName + '</td>' +
            '<td>' + data.comment + '</td>' +
            '</tr>';
    });
    $('.tbody').html(html);
    init();
}
$("body").on("click", "#submitstaff", function() {
    var isValidate = true;
    $("#staffForm .required").each(function() {
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000");
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
    if (isValidate) {
        var post_data = new FormData($('#staffForm')[0]);
        ajax_request('/admin/reports/staff', post_data, getStaffDetails, null);
    } else {
        toast_error("Please enter all required fields");
    }
});

