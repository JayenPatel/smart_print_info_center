function deleteswal(url, request_data, response_function_ajax, element) {
    swal({
        title: "Are you sure Want to Delete?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        closeModal: true
    }).then((willDelete) => {
        if (willDelete) {
            ajax_request(url, request_data, response_function_ajax, element);
        }
    });
}

function ajax_request(URL, request_data, response_function, element) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: URL,
        data: request_data,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function(result) {
            response_function(result, element);
        },
        error: function(error) {
            console.log(error)
            response_function(undefined, element);
        }
    });
}

function toast_success(messsage) {
    setTimeout(function() {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr.success(messsage, 'success');
    }, 500);
}

function toast_error(messsage) {
    setTimeout(function() {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr.error(messsage, 'Error');
    }, 500);
}