var totalVat = 0;
var totalSubTotal = 0;
var totalQuentityPop = 0;
var amountPop = 0;

var totalDescriptionPop = 0;
var totalAmount = 0;
// console.log(added_job, job_list);

setAddedTableData();
setTableData();
calculateAmount();

$("body").on("change", "#delivery", function() {
    $('#vat_per_radio').html($(this).val());
    // $("#vat_per_radio").val($(this).val());
    $(".vat_extra").val($(this).val());

});
$("body").on("change", "#discountApplied", function() {
    $(".vat_per_discount").val($(this).val());
    $(".vat_extra_discount").val($(this).val());

});

$("body").on("change", "#discountAppliedPop", function() {
    $(".vat_per_discount").val($(this).val());
    $(".vat_discount").val($(this).val());

});

$("body").on("change", "#customer_id", function() {
    var customer_id = $(this).val();
    var post_data = new FormData();
    post_data.append('customer_id', customer_id);
    $("#pop-customer-id").html($(this).val());
    $("#pop-customer-address").html($('option:selected', this).attr('invoice'));
    ajax_request('/admin/financies/invoices/customerData', post_data, submit_customerData, null);
});
$("body").on("click", ".addJobs", function() {
    var index = $(this).attr('data-index');
    var data = job_list[index];
    job_list.splice(index, 1);

    added_job.push(data);
    $(this).parent().parent().remove()
    var table = $('#datatable2 tbody');
    table.html('');
    setAddedTableData();
    setTableData();
    console.log(job_list, added_job);
    calculateAmount();
    $('#datatable2').show();
});
$("body").on("click", ".removeJobs", function() {

    var index = $(this).attr('data-index');
    var data = added_job[index];
    added_job.splice(index, 1);
    job_list.push(data);
    console.log(index, data, job_list, added_job)
    $(this).parent().parent().remove();
    $(".removepdf").remove();
    $('#datatable2 tbody').html('');
    setAddedTableData();
    setTableData();
    calculateAmount();
    console.log(job_list);

    if (added_job.length == 0) {
        $('#datatable2').hide();
    }

});




$("body").on("click", "#submitBtn", function() {

    if (added_job.length == 0) {
        toast_error('Please Select At Least One Job')
        return false;
    }
    $(this).attr('disabled', true);
    var delivery = $('#delivery').val();
    var showDiscount = 0;

    if ($('#showDiscount').is(":checked")) {
        showDiscount = 1;
    }
    var invoiceComment = $('#invoiceComment').val();
    var discountApplied = $('#discountApplied').val();
    var customer_id = $('#customer_id').val();
    var invoice_id = $('#invoice_id').val();

    var post_data = new FormData();
    post_data.append('net_delivery_price', delivery);
    post_data.append('delivery_vat', 17.5);
    post_data.append('deposit', 0);
    post_data.append('discount', discountApplied);
    post_data.append('show_discount', showDiscount);
    post_data.append('comments', invoiceComment);
    post_data.append('customer_id', customer_id);
    post_data.append('invoice_id', invoice_id);
    post_data.append('jobs', JSON.stringify(added_job));
    ajax_request('/admin/financies/invoices/addInvoice', post_data, submit_invoices, null);
});


function submit_invoices(result) {
    if (result.status == "success") {
        toast_success(result.message);
        //return false;
        setTimeout(function() {
            window.location = "/admin/financies/invoices"
        }, 1200)
    } else {
        $('#submitBtn').attr('disabled', false)

        toast_error(result.message);
    }
}

function setTableData() {

    var table = $('#datatable1 tbody');
    table.html('');
    // console.log(job_list)

    $(job_list).each(function(index, data) {
        if(data.status == 'A')
        {
            data.status='Active';
        }
        if(data.status == 'D')
        {
            data.status='Done';
        }
        table.append("<tr><td>" + data.job_id + "</td><td>" + data.quantity + "</td><td>" + data.category + "</td><td>" + data.status + "</td><td><button class='addJobs btn btn__bg' data-index='" + index + "' >Add</button>" +
            "</td></tr>");
    });
}

function setAddedTableData() {
    $(added_job).each(function(index, data) {
        $('#datatable2').show();
        var html = '';
        if(data.status == 'A')
        {
            data.status='Active';
        }
        if(data.status == 'D')
        {
            data.status='Done';
        }
        html += "<tr><td><button class='removeJobs btn btn__bg' data-index='" + index + "' >Remove</button></td><td>" + data.job_id + "</td><td>" + data.quantity + "</td><td>";
        html += data.description;
        $(data.extra).each(function(i, e) {
            html += '<br>' + e.description;
        });
        html += "</td><td>";
        html += data.vat + "%";
        $(data.extra).each(function(i, e) {
            html += '<br>' + e.vat + "%";
            if (e.vat != null) {
                totalVat = totalVat + parseFloat(e.vat)
            }
        });
        html += "</td><td>";
        html += data.net_price;
        $(data.extra).each(function(i, e) {
            html += '<br>' + e.net_price;
        });



        html += "</td><td>" + data.status + "</td></tr>"
        $('#datatable2 tbody').append(html);
    });

}

function submit_customerData(res) {
    console.log(res['jobs'])
    var table = $('#datatable1 tbody');
    table.html('');
    job_list = [];
    added_job = [];
    $('#datatable2').hide();
    job_list = res['data']['jobs'];
    setTableData();
    console.log(added_job, job_list);

    if (job_list.length == 0) {
        table.append("<td colspan='5'>No Job Found</td>");
        $('#datatable2').hide();
        $('#datatable2 tbody').html('');
    }
}


function calculateAmount() {
    totalVat = 0;
    totalSubTotal = 0;
    totalQuentityPop = 0;
    amountPop = 0;

    totalDescriptionPop = 0;
    totalAmount = 0;

    var discount = parseFloat($('#discountApplied').val());
    var discount = parseFloat($('#discountAppliedPop').val());

    var delivery = parseFloat($('#delivery').val());
    if (!discount) {
        discount = 0;
    }
    if (!delivery) {
        delivery = 0;
    }
    var ty = '';
    $(added_job).each(function(index, data) {
        // console.log("=============");
        // console.log(data);
        totalVat = totalVat + parseFloat(data.vat);
        amountPop = 0;
        ty = index;
        $(data.extra).each(function(i, e) {
            totalVat = totalVat + parseFloat(e.vat);
            totalSubTotal = totalSubTotal + parseFloat(e.net_price) // peli net price 
            totalDescriptionPop = data.description + "<br>" + e.description;
            if (e.net_price != null) {
                amountPop = amountPop + parseFloat(e.net_price)
            }

            // totalDescription = totalDescription + parseFloat(data.description)


        });
        totalSubTotal = totalSubTotal + parseFloat(data.net_price) // biji net price
        amountPop = amountPop + parseFloat(data.net_price)
        totalQuentityPop = data.quantity;
        // totalDescription = totalDescription + parseFloat(data.description) // biji net price


    });
    var vat = totalVat * 100 / totalSubTotal
    totalAmount = vat + totalSubTotal + delivery - discount;

    $('#totalAmount').html(totalAmount.toFixed(2));
    $('#totalSubTotal').html(totalSubTotal.toFixed(2));
    $('#totalVat').html(vat.toFixed(2));

    $('#totalAmountPop').html(totalAmount.toFixed(2));
    $('#totalSubTotalPop').html(totalSubTotal.toFixed(2));

    if (totalQuentityPop != 0 || amountPop != 0 || totalDescriptionPop != 0) {
        console.log("append");

        var append_html = "<tr class='removepdf'>";
        append_html += "<td>" + totalQuentityPop + "</td>";
        append_html += "<td>" + totalDescriptionPop + "</td>";
        append_html += "<td>" + amountPop + "</td>";
        append_html += "</tr>";


        $("#print-table .quantity_item").after(append_html);
    }

    totalQuentityPop = 0;
    amountPop = 0;
    totalDescriptionPop = null;

    $('#totalVatPop').html(vat.toFixed(2));


    // $('#totalAmount').html(totalAmount.toFixed(2));
    // $('#totalSubTotal').html(totalSubTotal.toFixed(2));
    // $('#totalVat').html(vat.toFixed(2));

    // $('#totalAmountPop').html(totalAmount.toFixed(2));
    // $('#totalSubTotalPop').html(totalSubTotal.toFixed(2));
    // $('#totalQuentityPop').html(totalQuentityPop.toFixed(2));
    // $('#amountPop').html(amountPop.toFixed(2));


    // $('#totalDescriptionPop').html(totalDescriptionPop.toFixed(2));
    // $('#totalVatPop').html(vat.toFixed(2));



}