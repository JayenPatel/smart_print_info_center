var extra = [];
$("body").on("change", "#customer_id", function() {
    var customer_id = $(this).val();
    var post_data = new FormData();
    post_data.append('customer_id', customer_id);
    $("#pop-customer-id").html($(this).val());
    $("#pop-customer-address").html($('option:selected', this).attr('invoice'));
    ajax_request('/admin/financies/invoices/customerprintData', post_data, submit_customerData, null);
});
function submit_customerData(res) {
    console.log(res['jobs'])
    var table = $('#datatable2 tbody');
    table.html('');
    job_list = [];
    added_job = [];
    //$('#datatable2').hide();
    job_list = res['data']['jobs'];
    setTableData();
    console.log(added_job, job_list);

    if (job_list.length == 0) {
        table.append("<td colspan='5'>No Job Found</td>");
        // $('#datatable2').hide();
        // $('#datatable2 tbody').html('');
    }
}
function setTableData() {

    var table = $('#datatable2 tbody');
    table.html('');
    
    // console.log(job_list)

    $(job_list).each(function(index, data) {
        var myurl = $("#myurl").val()+"/"+data.job_id;
        console.log(myurl);
        table.append("<tr><td>" + data.job_id + "</td><td>" + data.status + "</td><td>" + data.quantity + "</td><td>" + data.category + "</td>" +
            "<td><a value='reprint' class='btn btn__bg'  href='"+myurl+"?reprint=1'>Reprint Job</a></td></tr>");
    });
}
$(document).ready(function() {
    $("#vat_cal_hidden").hide();
    if ($("body").find("#job_id").val() != "" || $("body").find("#job_id").val() != 0) {
        if ($("body").find(".vat_per_radio").val() != 0 && $(".vat_per_radio").is(":checked")) { $("#vat_cal_hidden").show(); } else { $("#vat_cal_hidden").hide(); }
    }

    $("#vat_extra_hidden").hide();
    if ($("body").find("#job_id").val() != "" || $("body").find("#job_id").val() != 0) {
        if ($("body").find(".vat_per_radio").val() != 0 && $(".vat_per_radio").is(":checked")) { $("#vat_extra_hidden").show(); } else { $("#vat_extra_hidden").hide(); }
    }

    setExtraInTable();
    console.log($('#select_colour').val())
    $('#colour_1').hide();
    $('#colour_11').hide();
    $('#colour_2').hide();
    $('#colour_22').hide();
    $('#colour_3').hide();
    $('#colour_33').hide();
    value = $('#select_colour').val();
    if (value == 1) {
        $('#colour_1').show();
        $('#colour_11').show();
    } else if (value == 2) {
        $('#colour_1').show();
        $('#colour_11').show();
        $('#colour_2').show();
        $('#colour_22').show();
    } else if (value == 3) {
        $('#colour_1').show();
        $('#colour_11').show();
        $('#colour_2').show();
        $('#colour_22').show();
        $('#colour_3').show();
        $('#colour_33').show();
    } else if (value == "CMYK") {
        $('#colour_1').hide();
        $('#colour_11').hide();
        $('#colour_2').hide();
        $('#colour_22').hide();
        $('#colour_33').hide();
        $('#colour_3').hide();
    } else {

    }
    // VAT1
    $("body").on("click", ".vat_radio", function() {
        if ($(this).data('vat_per') == 1) {
            $("#vat_cal_hidden").show();
            $("#vat_btn_hidden").show();
            $("#vat_cancel_hidden").show();

        } else {
            $("#vat_cal_hidden").hide();
            $("#vat_btn_hidden").hide();
            $("#vat_cancel_hidden").hide();

        }
    });
    $("#vat_btn_hidden").on("click", function() {
        $(".vat_per_radio").val($("#vat_cal_hidden").val());
        $(".vat_text").text($("#vat_cal_hidden").val() + " %");
        $(".vat_extra").val($("#vat_cal_hidden").val());
        //$(".vat_extra").text($(this).val()+" %");
    });

    $(document).ready(function() {
        $("#vat_btn_hidden").click(function() {
            $("#vat_cal_hidden").hide();
            $("#vat_btn_hidden").hide();
            $("#vat_cancel_hidden").hide();

        });
        $("#vat_cal_hidden").hide();
        $("#vat_btn_hidden").hide();
        $("#vat_cancel_hidden").hide();

    });
    $(document).ready(function() {
        $("#vat_cancel_hidden").click(function() {
            $("#vat_cal_hidden").hide();
            $("#vat_btn_hidden").hide();
            $("#vat_cancel_hidden").hide();


        });
        $("#vat_cal_hidden").hide();
        $("#vat_btn_hidden").hide();
        $("#vat_cancel_hidden").hide();

    });


    // VAT2 
    $("body").on("click", ".vat_extra", function() {
        if ($(this).data('vat_per') == 1) {
            $("#vat_extra_hidden").show();
            $("#vat_extra_btn_hidden").show();
            $("#vat_extra_cancel_hidden").show();

        } else {
            $("#vat_extra_hidden").hide();
            $("#vat_extra_btn_hidden").hide();
            $("#vat_extra_cancel_hidden").hide();
        }
    });

    // $("body").on("change", "#vat_extra_hidden", function() {
    //     $(".vat_per_radio").val($(this).val());
    //     $(".vat_text_extra").text($(this).val() + " %");
    //     $(".vat_extra").val($(this).val());
    //     //$(".vat_extra").text($(this).val()+" %");
    // });

    $("#vat_extra_btn_hidden").on("click", function() {
        $(".vat_per_radio").val($("#vat_extra_hidden").val());
        $(".vat_text_extra").text($("#vat_extra_hidden").val() + " %");
        $(".vat_extra").val($("#vat_cal_hidden").val());
        //$(".vat_extra").text($(this).val()+" %");
    });

    $(document).ready(function() {
        $("#vat_extra_btn_hidden").click(function() {
            $("#vat_extra_hidden").hide();
            $("#vat_extra_btn_hidden").hide();
            $("#vat_extra_cancel_hidden").hide();

        });
        $("#vat_extra_hidden").hide();
        $("#vat_extra_btn_hidden").hide();





    });

    $(document).ready(function() {
        $("#vat_extra_cancel_hidden").click(function() {
            $("#vat_extra_hidden").hide();
            $("#vat_extra_btn_hidden").hide();
            $("#vat_extra_cancel_hidden").hide();

        });
        $("#vat_extra_hidden").hide();
        $("#vat_extra_btn_hidden").hide();
        $("#vat_extra_cancel_hidden").hide();


    });














    $("body").on("click", ".NewOption", function() {
        $(this).parent().find('.NewOption').html('Add');
        $(this).parent().find('.removeOption').html('Cancel');

        $(this).parent().find('.NewOption').addClass('saveOption');
        $(this).parent().find('.NewOption').removeClass('NewOption');

        // $('#store').attr("hidden",false);
        // $('#normal').attr("hidden",true);
        $(this).parent().find('.removeOption').addClass('cancelOption');
        $(this).parent().find('.removeOption').removeClass('removeOption');
        $(this).parent().parent().find('.selectOption').find('.optionInput').show();
        $(this).parent().parent().find('.selectOption').find('.selectedOption').hide();
    });
    $("body").on("click", ".cancelOption", function() {
        $(this).parent().find('.saveOption').html('+');
        $(this).parent().find('.cancelOption').html('-');

        $(this).parent().find('.saveOption').addClass('NewOption');
        $(this).parent().find('.saveOption').removeClass('saveOption');

        $(this).parent().find('.cancelOption').addClass('removeOption');
        $(this).parent().find('.cancelOption').removeClass('cancelOption');
        // $('#store').attr("hidden",true);
        // $('#normal').attr("hidden",false);
        $(this).parent().parent().find('.selectOption').find('.optionInput').hide();
        $(this).parent().parent().find('.selectOption').find('.selectedOption').show();
    });
    $("body").on("click", ".saveOption", function() {
        var val = $(this).parent().parent().find('.optionInput').val();
        $(this).parent().parent().find('.optionInput').val('');
        if ($.trim(val) != '') {
            console.log($(this).parent().parent().find('.selectOption').find('.selectedOption').html());
            $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('destroy');
            $(this).parent().parent().find('.selectOption').find('.selectedOption').append('<option value="' + val + '">' + val + '</option>');
            $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker();
            $(this).parent().parent().find('.selectOption').find('.selectedOption').selectpicker('val', val);
            $(this).parent().parent().find('.cancelOption').click();
        } else {
            toast_error('Please Enter Valid Value');
        }

    });
    $("body").on("click", ".removeOption", function() {
        $(this).parent().parent().find('.selectedOption').selectpicker('val', '');
    });


    $('.numeric').on('input', function(event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('.percent').on('input', function(event) {
        this.value = this.value.replace(/[^0-9\.\%]/g, '');
    });
    $('.character').on('input', function(event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });
    var thickness_radio = $('input[name="thickness_radio"]:checked').val();
    if (thickness_radio == 'mic') {
        $('#thickness_input').val('mic');
    } else if (thickness_radio == 'gsm') {
        $('#thickness_input').val('gsm');
    }
    $('#gsm').click(function() {
        $('#thickness_input').val('gsm');
    });
    $('#mic').click(function() {
        $('#thickness_input').val('mic');
    });
    /*$('.category_plus').click(function () {
        $('.category_plus').text('Add');
        $('.category_minus').text('Cancel');
        $('#Category').parent().hide();
        $('#category_input').show();
    });
    $('.category_minus').click(function () {
        $('.category_plus').text('+');
        $('.category_minus').text('-');
        $('#Category').parent().show();
        $('#category_input').hide();
    });

    $('.unfolded_plus').click(function () {
        $('.unfolded_plus').text('Add');
        $('.unfolded_minus').text('Cancel');
        $('#unfolded_select').parent().hide();
        $('#unfolded_input').show();
    });
    $('.unfolded_minus').click(function () {
        $('.unfolded_plus').text('+');
        $('.unfolded_minus').text('-');
        $('#unfolded_select').parent().show();
        $('#unfolded_input').hide();
    });

    $('.folded_plus').click(function () {
        $('.folded_plus').text('Add');
        $('.folded_minus').text('Cancel');
        $('#folded_select').parent().hide();
        $('#folded_input').show();
    });
    $('.folded_minus').click(function () {
        $('.folded_plus').text('+');
        $('.folded_minus').text('-');
        $('#folded_select').parent().show();
        $('#folded_input').hide();
    });*/

    /*$('.setup_plus').click(function () {
        $('.setup_plus').text('Add');
        $('.setup_minus').text('Cancel');
        $('#setup_select').parent().hide();
        $('#setup_input').show();
    });
    $('.setup_minus').click(function () {
        $('.setup_plus').text('+');
        $('.setup_minus').text('-');
        $('#setup_select').parent().show();
        $('#setup_input').hide();
    });
*/
    /*
        $('.setup_type_plus').click(function () {
            $('.setup_type_plus').text('Add');
            $('.setup_type_minus').text('Cancel');
            $('#setup_type_select').parent().hide();
            $('#setup_type_input').show();
        });
        $('.setup_type_minus').click(function () {
            $('.setup_type_plus').text('+');
            $('.setup_type_minus').text('-');
            $('#setup_type_select').parent().show();
            $('#setup_type_input').hide();
        });

        $('.email_to_plus').click(function () {
            $('.email_to_plus').text('Add');
            $('.email_to_minus').text('Cancel');
            $('#email_to_select').hide();
            $('#email_to_input').show();
        });
        $('.email_to_minus').click(function () {
            $('.email_to_plus').text('+');
            $('.email_to_minus').text('-');
            $('#email_to_select').show();
            $('#email_to_input').hide();
        });

        $('.print_where_plus').click(function () {
            $('.print_where_plus').text('Add');
            $('.print_where_minus').text('Cancel');
            $('#print_where_select').hide();
            $('#print_where_input').show();
        });
        $('.print_where_minus').click(function () {
            $('.print_where_plus').text('+');
            $('.print_where_minus').text('-');
            $('#print_where_select').show();
            $('#print_where_input').hide();
        });

        $('.paper_type_plus').click(function () {
            $('.paper_type_plus').text('Add');
            $('.paper_type_minus').text('Cancel');
            $('#paper_type_select').hide();
            $('#paper_type_input').show();
        });
        $('.paper_type_minus').click(function () {
            $('.paper_type_plus').text('+');
            $('.paper_type_minus').text('-');
            $('#paper_type_select').show();
            $('#paper_type_input').hide();
        });

        $('.brand_plus').click(function () {
            $('.brand_plus').text('Add');
            $('.brand_minus').text('Cancel');
            $('#brand_select').hide();
            $('#brand_input').show();
        });
        $('.brand_minus').click(function () {
            $('.brand_plus').text('+');
            $('.brand_minus').text('-');
            $('#brand_select').show();
            $('#brand_input').hide();
        });

        $('.paper_color_plus').click(function () {
            $('.paper_color_plus').text('Add');
            $('.paper_color_minus').text('Cancel');
            $('#paper_color_select').hide();
            $('#paper_color_input').show();
        });
        $('.paper_color_minus').click(function () {
            $('.paper_color_plus').text('+');
            $('.paper_color_minus').text('-');
            $('#paper_color_select').show();
            $('#paper_color_input').hide();
        });*/
    if (typeof(data.extra)) {
        $(data.extra).each(function(index, data) {
            extra.push({ description: data.description, vat: data.vat, net_price: data.net_price });
            setExtraInTable();
        });
    }
    $('#add_extra').click(function() {
        $('#extra_description_error').hide();
        $('#extra_vat_error').hide();
        $('#extra_net_price_error').hide();
        var description = $('#description_extra').val();
        var vat = $('.vat_extra:checked').val();
        var net_price = $('#net_price_extra').val();
        if (description == "") {
            $('#extra_description_error').show();
        } else if (vat == "") {
            $('#extra_vat_error').show();
        } else if (net_price == "") {
            $('#extra_net_price_error').show();
        } else if (description != "" && vat != "" && net_price != "") {
            if ($(this).attr('data-index')) {
                var i = $(this).attr('data-index');
                extra[i]['description'] = description;
                extra[i]['vat'] = vat;
                extra[i]['net_price'] = net_price;
                $('#update_extra').html('Add Extra');
                $('#update_extra').attr('id', 'add_extra');
            } else {
                extra.push({ description: description, vat: vat, net_price: net_price });
            }
            $('#description_extra').val('');
            $('.vat_extra:checked').attr('checked', 'false');
            $('#net_price_extra').val('');
            setExtraInTable();
        }
    });
    $('#select_colour').change(function() {
        console.log($('#select_colour').val())
        $('#colour_1').hide();
        $('#colour_11').hide();
        $('#colour_2').hide();
        $('#colour_22').hide();
        $('#colour_3').hide();
        $('#colour_33').hide();
        value = $('#select_colour').val();
        if (value == 1) {
            $('#colour_1').show();
            $('#colour_11').show();
        } else if (value == 2) {
            $('#colour_1').show();
            $('#colour_11').show();
            $('#colour_2').show();
            $('#colour_22').show();
        } else if (value == 3) {
            $('#colour_1').show();
            $('#colour_11').show();
            $('#colour_2').show();
            $('#colour_22').show();
            $('#colour_3').show();
            $('#colour_33').show();
        } else if (value == "CMYK") {
            $('#colour_1').hide();
            $('#colour_11').hide();
            $('#colour_2').hide();
            $('#colour_22').hide();
            $('#colour_33').hide();
            $('#colour_3').hide();
        } else {

        }
    });

    $("body").on("click", "#submitBtn", function() {
        var isValidate = true;
        $("#jobForm .required").each(function() {
            if (this.value == "") {
                isValidate = false;
                $(this).css("border", "2px dashed #CC0000");
                $(this).parent().parent().find('.text-muted').html('Please Enter');
                // <span class="form-text text-muted">Please enter your address</span>

            } else {
                $(this).css("border", "");
            }
        });
        console.log()
        if (isValidate && $('#customer_id').val() != '') {
            var post_data = new FormData($('#jobForm')[0]);
            var printing = $('#printingForm').serializeArray();
            post_data.append('printing', JSON.stringify(printing));
            post_data.append('extra', JSON.stringify(extra));
            ajax_request('/admin/jobs/print_job_sheets/submit', post_data, submit_response, null);
        } else {
            toast_error("Please enter all required fields");
        }
    });
    $("body").on("click", "#submitCalenderBtn", function() {
        var isValidate = true;
        $("#jobForm .required").each(function() {
            if (this.value == "") {
                isValidate = false;
                $(this).css("border", "2px dashed #CC0000");
                $(this).parent().parent().find('.text-muted').html('Please Enter');
                // <span class="form-text text-muted">Please enter your address</span>

            } else {
                $(this).css("border", "");
            }
        });
        console.log(new FormData($('#jobForm')[0]))
        if (isValidate && $('#customer_id').val() != '') {
            var post_data = new FormData($('#jobForm')[0]);
            post_data.append('extra', JSON.stringify(extra));
            ajax_request('/admin/jobs/calendars/submit', post_data, submit_calender_response, null);
        } else {
            toast_error("Please enter all required fields");
        }
    });
    $("body").on("click", "#submitWebJobBtn", function() {
        var isValidate = true;
        $("#jobForm .required").each(function() {
            if (this.value == "") {
                isValidate = false;
                $(this).css("border", "2px dashed #CC0000");
                $(this).parent().parent().find('.text-muted').html('Please Enter');
                // <span class="form-text text-muted">Please enter your address</span>

            } else {
                $(this).css("border", "");
            }
        });
        console.log()
        if (isValidate && $('#customer_id').val() != '') {
            var post_data = new FormData($('#jobForm')[0]);
            var web = $('#webForm').serializeArray();
            post_data.append('web', JSON.stringify(web));
            post_data.append('extra', JSON.stringify(extra));
            ajax_request('/admin/jobs/web_job_sheets/submit', post_data, submit_web_response, null);
        } else {
            toast_error("Please enter all required fields");
        }
    });
    $("body").on("click", "#submitOtherBtn", function() {
        var isValidate = true;
        $("#jobForm .required").each(function() {
            if (this.value == "") {
                isValidate = false;
                $(this).css("border", "2px dashed #CC0000");
                $(this).parent().parent().find('.text-muted').html('Please Enter');
                // <span class="form-text text-muted">Please enter your address</span>

            } else {
                $(this).css("border", "");
            }
        });
        console.log(new FormData($('#jobForm')[0]))
        if (isValidate && $('#customer_id').val() != '') {
            var post_data = new FormData($('#jobForm')[0]);
            post_data.append('extra', JSON.stringify(extra));
            ajax_request('/admin/jobs/other_job_sheets/submit', post_data, submit_other_response, null);
        } else {
            toast_error("Please enter all required fields");
        }
    });

});



function submit_calender_response(result) {
    if (result.status == "success") {
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/jobs/calendars"
        }, 1200)
    } else {
        toast_error(result.message);
    }
}

function submit_other_response(result) {
    if (result.status == "success") {
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/jobs/other_job_sheets"
        }, 1200)
    } else {
        toast_error(result.message);
    }
}

function submit_response(result) {
    if (result.status == "success") {
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/jobs/print_job_sheets"
        }, 1200)
    } else {
        toast_error(result.message);
    }
}

function submit_web_response(result) {
    if (result.status == "success") {
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/jobs/web_job_sheets"
        }, 1200)
    } else {
        toast_error(result.message);
    }
}

function setExtraInTable() {
    var table = $('#datatable1 tbody');
    table.html('');
    $(extra).each(function(index, data) {
        table.append("<tr><td>" + data.description + "</td><td>" + data.vat + "</td><td>" + data.net_price + "</td>" +
            "<td><a class='fa_style' data-index='" + index + "' onclick='editExtra(this)'><i class='fa fa-pencil' style='color:black;'></i></a>" +
            "<a class='fa_style'  data-index='" + index + "' onclick='deleteExtra(this)'><i class='fa fa-trash' style='color:black;'></i></a></td></tr>");
    });
    $('#table_row').show();
}

function editExtra(event) {
    var index = $(event).attr('data-index');
    $('#description_extra').val(extra[index]['description']);
    $('.vat_extra').filter(function() {
        return this.value == extra[index]['vat'];
    }).prop("checked", "true");
    $('#net_price_extra').val(extra[index]['net_price']);
    $('#add_extra').html('Update Extra');
    $('#add_extra').attr('data-index', index);
    $('#add_extra').attr('id', 'update_extra');

}

function deleteExtra(event) {
    var index = $(event).attr('data-index')
    console.log(extra);
    extra.splice(index, 1);
    this.setExtraInTable();
}