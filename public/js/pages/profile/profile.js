//getStaffData();
var table = '';
init();

function init() {
    table = $("#staffDT").DataTable({
        scrollY: "50vh",
        scrollX: !0,
        scrollCollapse: !0,
    })
}


$("body").on("click", "#submitStaff", function () {
    var isValidate = true;
    $("#staffForm .required").each(function () {
        console.log(this.value);
        if (this.value == "") {
            isValidate = false;
            $(this).css("border", "2px dashed #CC0000" );
            $(this).parent().parent().find('.text-muted').html('Please Enter');
            // <span class="form-text text-muted">Please enter your address</span>

        } else {
            $(this).css("border", "");
        }
    });
       
    if (isValidate ==true) {
        if($('#password').val() != $('#re-password').val()){
        toast_error("Passwords does not match");
        }else{
        var post_data = new FormData($('#staffForm')[0]);
        ajax_request('/admin/profile/store',post_data,submit_response ,null);
        }
    }else{
        toast_error("Please enter all required fields");
    }
});
$("#re-password").blur(function(){
    if($('#password').val() != $('#re-password').val()){
        toast_error("Passwords does not match");
    }
});
$(".date").blur(function(){
   var from = new Date($('#startDate').val());
    var to = new Date($('#endDate').val());
    if (from > to) {
        toast_error("end DATE must be equal or greater than start DATE.");
        $('#startDate').val('');
        $('#endDate').val('');
    }else{
        console.log("success");
    }
});
function submit_response(result){
    if(result.status=="success"){
        toast_success(result.message);
        setTimeout(function() {
            window.location = "/admin/profile/edit/"+result.id
        },1200)
    }else{
        toast_error(result.message);
    }
}
function getStaffData() {
    var post_data = {};
    ajax_request('/admin/profile/getStaffData', post_data, getStaffDetails, null);
}

function getStaffDetails(result) {
    console.log(result['data'])
    var html = '';
    table.destroy();
    $(result['data']).each(function (index, data) {
        html += '<tr>' +
            '<td>' + (index + 1) + '</td>' +
            '<td>' + data.loginName + '</td>' +
            '<td>' + data.firstName +' '+  data.lastName +'</td>' +
            '<td>' + data.startDate + '</td>' +
            '<td>' + data.endDate + '</td>' +
            '<td>' +
            '<a onclick="editCustomer('+data.staffId +')" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">' +
            '<span class="svg-icon svg-icon-md">' +
            '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' +
            '</span>' +
            '</a>' +
            '<a onclick="deleteCustomer('+data.staffId +')" class="btn btn-sm btn-clean btn-icon" title="Delete">' +
            '<span class="svg-icon svg-icon-md">' +
            '<i class="far fa-trash-alt"></i>' +
            '</span>' +
            '</a>' +
            '</td>' +
            '</tr>';
    });
    $('.tbody').html(html);
    init();
}

function editCustomer(id){
    window.location.href = "/admin/profile/edit/"+id;
}

function deleteCustomer(id){
    var post_data =  new FormData();
    post_data.append('id',id);
    deleteswal('/admin/profile/delete', post_data, deleteResponse, null);
}
function deleteResponse(result){
    if (result.status == "success") {
        toast_success(result.message);
        getStaffData();

    } else {
        toast_error(result.message);
    }
}
