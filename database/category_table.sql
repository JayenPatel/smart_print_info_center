-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2021 at 03:36 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_print`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL COMMENT 'Web-for Web job\r\ncalender -for calender job\r\nother - for other job\r\nprint -for print job',
  `status` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `type`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Rubber Stamp', 'other', 1, '2021-07-23 11:48:35', '2021-07-24 07:34:24', NULL),
(4, 'Plastic Bag', 'other', 1, '2021-07-23 11:48:35', '2021-07-24 07:33:43', NULL),
(5, 'Lamination', 'other', 1, '2021-07-23 11:48:35', '2021-07-23 11:48:35', NULL),
(6, 'Binding', 'other', 1, '2021-07-23 11:48:35', '2021-07-23 11:48:35', NULL),
(7, 'Copy A4', 'other', 1, '2021-07-23 13:29:03', '2021-07-23 13:29:03', NULL),
(8, 'Take Away Leaflet', 'print', 1, '2021-07-23 13:54:09', '2021-07-23 13:54:09', NULL),
(9, 'Table Menu', 'print', 1, '2021-07-23 13:54:25', '2021-07-24 07:21:49', NULL),
(10, 'Business Card', 'print', 1, '2021-07-23 13:56:12', '2021-07-23 13:56:12', NULL),
(11, 'Wall World', 'calender', 1, '2021-07-23 14:12:48', '2021-07-24 07:21:31', NULL),
(12, 'Wall Asia', 'calender', 1, '2021-07-23 14:13:59', '2021-07-23 14:13:59', NULL),
(13, 'Wall Bangla', 'calender', 1, '2021-07-23 14:15:22', '2021-07-23 14:15:22', NULL),
(14, 'Desk World', 'calender', 1, '2021-07-23 14:16:33', '2021-07-23 14:16:33', NULL),
(15, 'HTML (static)', 'web', 1, '2021-07-23 14:20:47', '2021-07-24 06:58:17', NULL),
(16, 'Flash (static)', 'web', 1, '2021-07-23 14:25:29', '2021-07-24 06:53:17', NULL),
(17, 'Banner', 'other', 1, '2021-07-23 14:26:35', '2021-07-23 14:26:35', NULL),
(18, 'CD', 'calender', 1, '2021-07-23 14:27:40', '2021-07-23 14:27:40', NULL),
(19, 'Booklet', 'print', 1, '2021-07-23 14:28:41', '2021-07-23 14:28:41', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
