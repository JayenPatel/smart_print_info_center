-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2021 at 05:37 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_print`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `accountId` int(11) NOT NULL,
  `customerId` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `creditNo` int(11) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL COMMENT '0 for active 1 for deactive',
  `createdDate` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `modifyBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `alternate_address`
--

CREATE TABLE `alternate_address` (
  `id` int(151) NOT NULL,
  `address1` text DEFAULT NULL,
  `address2` text DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `area` text DEFAULT NULL,
  `postCode` varchar(255) DEFAULT NULL,
  `addrs1` text DEFAULT NULL,
  `addrs2` text DEFAULT NULL,
  `area2` text DEFAULT NULL,
  `country2` varchar(255) DEFAULT NULL,
  `town2` varchar(255) DEFAULT NULL,
  `postCode2` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alternate_address`
--

INSERT INTO `alternate_address` (`id`, `address1`, `address2`, `town`, `country`, `area`, `postCode`, `addrs1`, `addrs2`, `area2`, `country2`, `town2`, `postCode2`) VALUES
(1, 'aaaaaaaaaaaaaaaaaa', 'sad', 'sadsa', 'India', 'asdsad', '131123', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'sadsa', 'sad', 'sadsa', 'India', 'asdsad', '131123', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'test', 'tset', 'test', 'India', 'test', '362002', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'test', 'tset', 'test', 'India', 'test', '362002', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'test', 'tset', 'test', 'India', 'test', '362002', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'test', 'tset', 'test', 'India', 'test', '362002', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'test', 'tset', 'test', 'India', 'test', '362002', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'test', 'tset', 'test', 'India', 'test', '362002', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'test', 'tset', 'test', 'India', 'test', '362002', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'test', '123', '123', 'India', '123', '123', NULL, NULL, NULL, NULL, NULL, NULL),
(11, '1', '1', '1', 'Falkland Islands (Islas Malvinas)', '1', '1', '2', '2', '12', NULL, '2', '12'),
(12, '1', '1', '1', 'Ethiopia', '1', '1', '2', '2', '12', NULL, '2', '13'),
(13, 'address 1', 'address line 1 2', 'amd', 'Afghanistan', 'test area 1', '3620000', 'address line 1 2', 'address line 1 2', 'address line 1 2', NULL, 'address line 1 2', '36200000'),
(14, 'address 1', 'address line 1 2', 'amd', 'Afghanistan', 'test area 1', '3620000', 'address line 1 2', 'address line 1 2', 'address line 1 2', NULL, 'address line 1 2', '36200000'),
(15, 'address 1', 'address line 1 2', 'amd', 'Afghanistan', 'test area 1', '3620000', 'address line 1 2', 'address line 1 2', 'address line 1 2', NULL, 'address line 1 2', '36200000'),
(16, 'address 1', 'address line 1 2', 'amd', 'Afghanistan', 'test area 1', '3620000', 'address line 1 2', 'address line 1 2', 'address line 1 2', NULL, 'address line 1 2', '36200000'),
(17, 'address 1', 'address line 1 2', 'amd', 'Afghanistan', 'test area 1', '3620000', 'address line 1 2', 'address line 1 2', 'address line 1 2', NULL, 'address line 1 2', '36200000'),
(18, 'address 1', 'address 1', 'address 1', 'India', 'address 1', 'wc2n5du', 'address 1', 'address 1', 'address 1', NULL, 'address 1', 'wc2n5du');

-- --------------------------------------------------------

--
-- Table structure for table `bussiness_type`
--

CREATE TABLE `bussiness_type` (
  `typeId` int(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bussiness_type`
--

INSERT INTO `bussiness_type` (`typeId`, `name`) VALUES
(1, 'IT'),
(2, 'Computer');

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE `calendar` (
  `job_id` varchar(5) NOT NULL,
  `envelope_qty` smallint(255) DEFAULT NULL,
  `logo` tinyint(4) DEFAULT NULL,
  `menu_on_back` tinyint(4) DEFAULT NULL,
  `comments` varchar(4356) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `calendar`
--

INSERT INTO `calendar` (`job_id`, `envelope_qty`, `logo`, `menu_on_back`, `comments`) VALUES
('10C', 10, 1, 0, ''),
('2C', 1, 1, 0, '5'),
('6C', 14, 1, 0, '32'),
('6Q', 14, 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` smallint(11) NOT NULL,
  `reg_date` varchar(19) DEFAULT NULL,
  `customer_status` varchar(7) DEFAULT NULL,
  `business_type` varchar(20) DEFAULT NULL,
  `business_name` varchar(36) DEFAULT NULL,
  `first_name` varchar(18) DEFAULT NULL,
  `surname` varchar(12) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `fax` varchar(13) DEFAULT NULL,
  `mobile` varchar(12) DEFAULT NULL,
  `email` varchar(26) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `address1` varchar(100) DEFAULT NULL,
  `address2` varchar(15) DEFAULT NULL,
  `area` varchar(10) DEFAULT NULL,
  `city` varchar(23) DEFAULT NULL,
  `county` varchar(19) DEFAULT NULL,
  `postcode` varchar(9) DEFAULT NULL,
  `comments` varchar(133) DEFAULT NULL,
  `alternate_address` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `delivery_address1` varchar(22) DEFAULT NULL,
  `delivery_address2` varchar(15) DEFAULT NULL,
  `delivery_area` varchar(18) DEFAULT NULL,
  `delivery_city` varchar(19) DEFAULT NULL,
  `delivery_county` varchar(15) DEFAULT NULL,
  `delivery_postcode` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `reg_date`, `customer_status`, `business_type`, `business_name`, `first_name`, `surname`, `telephone`, `fax`, `mobile`, `email`, `web`, `address1`, `address2`, `area`, `city`, `county`, `postcode`, `comments`, `alternate_address`, `status`, `delivery_address1`, `delivery_address2`, `delivery_area`, `delivery_city`, `delivery_county`, `delivery_postcode`) VALUES
(2, NULL, 'Active', 'test bussiness', 'tset name', '123', '123', '123', '123', '1234567890', 'shineinfosoft33@gmail.com', 'test website', '123', '123', '123', '123', 'India', '362002', 'test', '10', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, 'Active', 'test bussiness', 'hdsfhj', 'bfhsdbd', 'bfhdbjfd', 'bdhsfbj', 'fsjhbjdf', 'bdhfbs', 'fshjb', 'dsfbh', 'bfhds', 'bfjhdsb', 'bfdhjsbf', 'bfdshjbf', 'Fiji', 'bdhsj', 'dfbsj', '12', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, 'Active', 'test bussiness', 'testcust', 'test cust', 'test cust', '1234567890', 'test', '1234567890', 'shineinfosoft33@gmail.com', 'testcust.com', 'JBL Multi Trade, Westminster,    London, Greater London', 'test cust', '1234567890', '1234567890', 'India', 'WC2N5DU', 'test', '18', 1, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `extra`
--

CREATE TABLE `extra` (
  `extra_id` smallint(6) NOT NULL,
  `job_id` varchar(5) DEFAULT NULL,
  `description` varchar(12) DEFAULT NULL,
  `vat` decimal(3,1) DEFAULT NULL,
  `net_price` decimal(8,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `extra`
--

INSERT INTO `extra` (`extra_id`, `job_id`, `description`, `vat`, `net_price`) VALUES
(118, '1050C', 'menu on back', '17.5', '50.0000'),
(120, '1055C', 'Menu on back', '17.5', '50.0000'),
(121, '1057C', 'Menu on back', '17.5', '50.0000'),
(122, '1060C', 'Menu on back', '17.5', '30.0000'),
(123, '1207W', '12321', '17.5', '1556.0000'),
(124, '1015P', 'delivery', '17.5', '15.0000'),
(125, '1015P', 'Menu on back', '0.0', '23.0000'),
(126, '1P', '22', '17.5', '33.0000'),
(127, '1P', '2', '17.5', '43.0000'),
(136, '7P', '10', '15.0', '10.0000'),
(137, '8P', '10', '15.0', '10.0000'),
(138, '10C', '10', '17.5', '10.0000'),
(139, '9P', '12', '15.0', '12.0000'),
(143, '10P', '14', '15.0', '14.0000');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` smallint(6) NOT NULL,
  `final` tinyint(4) DEFAULT 0,
  `customer_id` smallint(6) DEFAULT NULL,
  `invoice_date` varchar(19) DEFAULT NULL,
  `net_delivery_price` decimal(6,4) DEFAULT 0.0000,
  `delivery_vat` decimal(3,1) DEFAULT 0.0,
  `deposit` decimal(5,4) DEFAULT 0.0000,
  `discount` decimal(7,4) DEFAULT 0.0000,
  `show_discount` tinyint(4) DEFAULT 0,
  `paid_date` datetime DEFAULT NULL,
  `paid` tinyint(4) DEFAULT 0,
  `comments` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `final`, `customer_id`, `invoice_date`, `net_delivery_price`, `delivery_vat`, `deposit`, `discount`, `show_discount`, `paid_date`, `paid`, `comments`) VALUES
(2, 0, 804, '2021-02-23 00:00:00', NULL, '17.5', '0.0000', '0.0000', 1, NULL, 0, NULL),
(6, 0, 803, '2021-02-23 00:00:00', NULL, '17.5', '0.0000', NULL, 1, NULL, 0, NULL),
(7, 0, 4, '2021-04-02 00:00:00', '10.0000', '17.5', '0.0000', '10.0000', 0, NULL, 0, '10');

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `job_id` varchar(5) NOT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `invoice_id` varchar(4) DEFAULT NULL,
  `customer_id` smallint(12) DEFAULT NULL,
  `reg_date` datetime DEFAULT NULL,
  `type` varchar(34) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `description` varchar(245) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `category` varchar(17) DEFAULT NULL,
  `quantity` mediumint(9) DEFAULT NULL,
  `reprint_id` varchar(5) DEFAULT NULL,
  `colours` varchar(134) DEFAULT NULL,
  `colour1` varchar(2345) DEFAULT NULL,
  `colour2` varchar(435) DEFAULT NULL,
  `colour3` varchar(4565) DEFAULT NULL,
  `net_price` decimal(8,4) DEFAULT NULL,
  `vat` decimal(3,1) DEFAULT NULL,
  
  `to_delivery_address` tinyint(4) DEFAULT NULL,
  `order_taken_by` varchar(15) DEFAULT NULL,
  `last_updated_by` varchar(15) DEFAULT NULL,
  `last_updated` varchar(19) DEFAULT NULL,
  `comments` varchar(5443) DEFAULT NULL,
  `prog_type` tinyint(4) DEFAULT NULL,
  `prog_proof` tinyint(4) DEFAULT NULL,
  `prog_confirmed` tinyint(4) DEFAULT NULL,
  `prog_film` tinyint(4) DEFAULT NULL,
  `prog_printed` tinyint(4) DEFAULT NULL,
  `prog_finished` tinyint(4) DEFAULT NULL,
  `prog_delivered` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`job_id`, `job_title`, `invoice_id`, `customer_id`, `reg_date`, `type`, `description`, `status`, `category`, `quantity`, `reprint_id`, `colours`, `colour1`, `colour2`, `colour3`, `net_price`, `vat`, `to_delivery_address`, `order_taken_by`, `last_updated_by`, `last_updated`, `comments`, `prog_type`, `prog_proof`, `prog_confirmed`, `prog_film`, `prog_printed`, `prog_finished`, `prog_delivered`) VALUES
('10C', 'test', NULL, 4, '2021-04-01 06:30:55', 'C', 'test testtestteststset', 'A', 'Bill Books 50 set', NULL, NULL, '1', '10', NULL, NULL, '100.0000', '17.5', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('10P', 'trestetettt', NULL, 4, '2021-04-03 06:09:35', 'P', '14', 'A', 'test print', 14, NULL, '1', '#67ae37', '#000000', '#000000', '14.0000', '15.0', NULL, '1', NULL, NULL, '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('10X', 'other job', NULL, 4, '2021-04-01 07:54:31', 'X', '10', 'A', 'Bill Books 50 set', 10, NULL, NULL, NULL, NULL, NULL, '10.0000', '17.5', NULL, '1', NULL, NULL, '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2C', NULL, NULL, 800, '2021-02-15 12:41:05', 'C', 'ss', 'A', 'Bill Books 50 set', 4, NULL, '1', 'black', NULL, NULL, '3.0000', '0.0', NULL, '1', NULL, NULL, NULL, 1, 1, 0, 0, 0, 0, 0),
('3W', NULL, '2', 804, '2021-02-15 12:41:54', 'W', 'as', 'D', 'Bill Books 50 set', 2, NULL, NULL, NULL, NULL, NULL, '2.0000', '0.0', NULL, '1', NULL, NULL, '22', 1, 1, 1, 1, 1, 1, 1),
('4X', NULL, '2', 804, '2021-02-15 12:43:52', 'X', 's', 'A', 'Bill Books 50 set', 34, NULL, NULL, NULL, NULL, NULL, '43.0000', '0.0', NULL, '1', NULL, NULL, 'd', 1, 1, 1, 1, 0, 0, 0),
('5X', NULL, NULL, 803, '2021-02-15 12:44:25', 'X', 'ds', 'A', 'Bill Books 50 set', 4, NULL, NULL, NULL, NULL, NULL, '3.0000', '0.0', NULL, '1', NULL, NULL, 'fddf', 1, 1, 1, 1, NULL, NULL, NULL),
('6C', NULL, NULL, 803, '2021-02-15 12:46:43', 'C', 'saa', 'A', 'Bill Books 50 set', 7, NULL, '1', 'sa', NULL, NULL, '32.0000', '0.0', NULL, '1', NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, NULL),
('6Q', NULL, '6', 803, '2021-02-15 12:46:43', 'C', 'saa', 'A', 'Bill Books 50 set', 7, NULL, '1', 'sa', NULL, NULL, '32.0000', '0.0', NULL, '1', NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, NULL),
('7P', 'print job 1', NULL, 4, '2021-04-01 08:26:04', 'P', 'test', 'A', 'test print', 10, NULL, '1', NULL, NULL, NULL, '10.0000', '17.5', NULL, '1', NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('8P', 'test print 2', NULL, 4, '2021-04-01 08:55:09', 'P', 'test', 'A', 'test print', 10, NULL, '1', '10', NULL, NULL, '10.0000', '17.5', NULL, '1', NULL, NULL, '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('9P', 'color test print job', NULL, 4, '2021-04-01 10:46:26', 'P', 'test', 'A', 'test print', 12, NULL, '1', '#ff0000', NULL, NULL, '12.0000', '15.0', NULL, '1', NULL, NULL, '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_12_07_061053_create_staff_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `otherjob`
--

CREATE TABLE `otherjob` (
  `job_id` varchar(52) NOT NULL,
  `comment` varchar(6043) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `otherjob`
--

INSERT INTO `otherjob` (`job_id`, `comment`) VALUES
('10X', '10'),
('4X', 'sd'),
('5X', 'd');

-- --------------------------------------------------------

--
-- Table structure for table `printing`
--

CREATE TABLE `printing` (
  `job_id` varchar(5) NOT NULL,
  `design_folds` varchar(43) DEFAULT '0',
  `design_comments` varchar(5532) DEFAULT '',
  `note` varchar(255) DEFAULT NULL,
  `design_folded_pages` varchar(105) DEFAULT '0',
  `design_unfolded_size` varchar(143) DEFAULT '',
  `design_unfolded_pages` varchar(1145) DEFAULT '0',
  `design_folded_size` varchar(10) DEFAULT '',
  `design_artwork` tinyint(4) DEFAULT 0,
  `design_logo` tinyint(4) DEFAULT 0,
  `design_typesetting` tinyint(4) DEFAULT 0,
  `design_scan` tinyint(4) DEFAULT 0,
  `design_photography` tinyint(4) DEFAULT 0,
  `design_stockphotos` tinyint(4) DEFAULT 0,
  `design_orientation` varchar(234) DEFAULT '',
  `design_double_side` varchar(547) DEFAULT '',
  `prepress_no_of_up` varchar(234) NOT NULL,
  `prepress_setupsize` varchar(234) DEFAULT '',
  `prepress_emailto` varchar(234) DEFAULT '',
  `prepress_setup_type` varchar(234) DEFAULT '',
  `prepress_cd` tinyint(234) DEFAULT 0,
  `prepress_email` tinyint(234) DEFAULT 0,
  `prepress_dig_or_film` varchar(234) DEFAULT '',
  `prepress_platform` varchar(234) DEFAULT '',
  `prepress_where` varchar(234) DEFAULT '',
  `prepress_orientation` varchar(234) DEFAULT '',
  `press_no_of_plates` varchar(234) DEFAULT '',
  `press_machine_run` varchar(234) DEFAULT '',
  `press_comments` varchar(24) DEFAULT '',
  `press_printwhere` varchar(234) DEFAULT '',
  `press_makeready` tinyint(4) DEFAULT 0,
  `paper_thickness` varchar(234) DEFAULT '',
  `paper_quantity` varchar(234) DEFAULT '',
  `paper_comments` varchar(2342) DEFAULT '',
  `paper_colour` varchar(234) DEFAULT '',
  `paper_size` varchar(2342) DEFAULT '',
  `paper_type` varchar(203) DEFAULT '',
  `paper_brand` varchar(152) DEFAULT '',
  `finish_comments` text DEFAULT '',
  `finish_binding` varchar(111) DEFAULT '',
  `finish_finish` varchar(16) DEFAULT '',
  `finish_delivery` varchar(11) DEFAULT '',
  `finish_cutting` tinyint(4) DEFAULT 0,
  `finish_creasing` tinyint(4) DEFAULT 0,
  `finish_folding` tinyint(4) DEFAULT 0,
  `finish_perforation` tinyint(4) DEFAULT 0,
  `finish_numbering` tinyint(4) DEFAULT 0,
  `finish_boxing` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `printing`
--

INSERT INTO `printing` (`job_id`, `design_folds`, `design_comments`, `design_folded_pages`, `design_unfolded_size`, `design_unfolded_pages`, `design_folded_size`, `design_artwork`, `design_logo`, `design_typesetting`, `design_scan`, `design_photography`, `design_stockphotos`, `design_orientation`, `design_double_side`, `prepress_no_of_up`, `prepress_setupsize`, `prepress_emailto`, `prepress_setup_type`, `prepress_cd`, `prepress_email`, `prepress_dig_or_film`, `prepress_platform`, `prepress_where`, `prepress_orientation`, `press_no_of_plates`, `press_machine_run`, `press_comments`, `press_printwhere`, `press_makeready`, `paper_thickness`, `paper_quantity`, `paper_comments`, `paper_colour`, `paper_size`, `paper_type`, `paper_brand`, `finish_comments`, `finish_binding`, `finish_finish`, `finish_delivery`, `finish_cutting`, `finish_creasing`, `finish_folding`, `finish_perforation`, `finish_numbering`, `finish_boxing`) VALUES
('10P', '', '', '', '1', '0', '1', 1, 0, 1, 0, 1, 0, 'Portrait', 'Single', '0', '', 'jaydeeptank90@gmail.com', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', 'Cluster', '', 0, 0, 0, 0, 0, 0),
('1P', '', 'test  ', '12', '1', '0', 'Not Folded', 1, 1, 0, 1, 0, 1, 'Portrait', 'Single', '0', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0),
('7P', '', '', '', '4', '0', '5', 1, 0, 0, 0, 1, 1, 'Portrait', 'Single', '0', '', '', '', 1, 0, 'Digital', 'Mac', '', 'Portrait', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', 'Cutting', '', 1, 0, 0, 0, 0, 1),
('8P', '10', '', '10', '1', '0', '1', 1, 0, 0, 0, 1, 0, 'Portrait', 'Single', '0', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', 'test', '', 'Cutting', '', 0, 0, 0, 0, 0, 0),
('9P', '', '', '', '1', '0', '1', 1, 0, 1, 0, 1, 0, 'Portrait', 'Single', '0', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', 'Cluster', '', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `roleId` tinyint(4) NOT NULL,
  `roleName` varchar(13) DEFAULT NULL,
  `description` varchar(0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleId`, `roleName`, `description`) VALUES
(1, 'administrator', ''),
(2, 'manager', ''),
(3, 'secretary', ''),
(4, 'designer', ''),
(5, 'printer', ''),
(6, 'finisher', '');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `role_id` tinyint(4) DEFAULT NULL,
  `permission_id` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 101),
(1, 102),
(1, 103),
(1, 104),
(1, 105),
(1, 106),
(1, 107),
(1, 108),
(2, 1),
(2, 2),
(2, 101),
(2, 102),
(2, 103),
(2, 104),
(2, 105),
(2, 106),
(3, 101),
(3, 106),
(4, 102),
(4, 103);

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `setting_id` int(11) NOT NULL,
  `vat_setting` varchar(100) DEFAULT NULL,
  `createdDate` date DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `modifyDate` date DEFAULT NULL,
  `modifyBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`setting_id`, `vat_setting`, `createdDate`, `createdBy`, `modifyDate`, `modifyBy`) VALUES
(1, '15', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staffId` int(11) NOT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `loginName` varchar(255) DEFAULT NULL,
  `password` varchar(550) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `roleId` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 for active 1 for deactive',
  `createdDate` date DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `modifyDate` date DEFAULT NULL,
  `modifyBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staffId`, `firstName`, `lastName`, `loginName`, `password`, `startDate`, `endDate`, `comment`, `profile_pic`, `roleId`, `status`, `createdDate`, `createdBy`, `modifyDate`, `modifyBy`) VALUES
(1, 'Bansi', 'Patel', 'bansis', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-01', '2021-05-06', NULL, '', '1,2', '0', '2021-01-18', 1, '2021-03-10', 1),
(2, 'Pradip', 'Kor', 'pdkor', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04', '2021-02-02', NULL, '', '1', '0', '2021-01-18', 1, '2021-01-18', 1),
(6, 'jaydeep', 'tank', 'jdtank', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-01', '2021-04-10', 'test', '1615788634.jpg', '5', '0', '2021-03-10', 1, '2021-03-15', 1),
(7, 'designer', '1', 'designer', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-01', '2021-04-10', 'test', '1615803937.jpg', '4', '0', '2021-03-15', 1, NULL, NULL),
(8, 'printer', 'printer 1', 'printer', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-01', '2021-04-10', 'test', '1615803972.jpg', '5', '0', '2021-03-15', 1, NULL, NULL),
(9, 'finisher', 'finisher 1', 'finisher', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-01', '2021-04-10', 'test', '1615804037.jpg', '6', '0', '2021-03-15', 1, NULL, NULL),
(10, 'secretary', 'secretary 1', 'secretary', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-01', '2021-04-10', 'tset', '1615804072.png', '3', '0', '2021-03-15', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_role`
--

CREATE TABLE `staff_role` (
  `staff_id` smallint(6) DEFAULT NULL,
  `role_id` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff_role`
--

INSERT INTO `staff_role` (`staff_id`, `role_id`) VALUES
(101, 2),
(102, 4),
(103, 6),
(104, 1),
(104, 4),
(105, 5),
(106, 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` smallint(6) NOT NULL,
  `reg_date` varchar(19) DEFAULT NULL,
  `supplier_status` varchar(7) DEFAULT NULL,
  `business_type` varchar(20) DEFAULT NULL,
  `business_name` varchar(36) DEFAULT NULL,
  `first_name` varchar(18) DEFAULT NULL,
  `surname` varchar(12) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `fax` varchar(13) DEFAULT NULL,
  `mobile` varchar(12) DEFAULT NULL,
  `email` varchar(26) DEFAULT NULL,
  `web` varchar(0) DEFAULT NULL,
  `address1` varchar(36) DEFAULT NULL,
  `address2` varchar(15) DEFAULT NULL,
  `area` varchar(10) DEFAULT NULL,
  `city` varchar(23) DEFAULT NULL,
  `county` varchar(19) DEFAULT NULL,
  `postcode` varchar(9) DEFAULT NULL,
  `comments` varchar(133) DEFAULT NULL,
  `alternate_address` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `delivery_address1` varchar(22) DEFAULT NULL,
  `delivery_address2` varchar(15) DEFAULT NULL,
  `delivery_area` varchar(18) DEFAULT NULL,
  `delivery_city` varchar(19) DEFAULT NULL,
  `delivery_county` varchar(15) DEFAULT NULL,
  `delivery_postcode` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `reg_date`, `supplier_status`, `business_type`, `business_name`, `first_name`, `surname`, `telephone`, `fax`, `mobile`, `email`, `web`, `address1`, `address2`, `area`, `city`, `county`, `postcode`, `comments`, `alternate_address`, `status`, `delivery_address1`, `delivery_address2`, `delivery_area`, `delivery_city`, `delivery_county`, `delivery_postcode`) VALUES
(763, '2005-09-09 12:08:00', 'Passive', 'Indian Restaurant', 'Blue Ginger', 'Mustak', 'X', '01235821335', '', '', '', '', '6 High Street', '', '', 'Steventon', 'Oxfordshire', 'OX13 6RX', '', '', NULL, '59 Ambrook Road', '', 'Whitley', 'Reading', 'Berkshire', 'RG2 8SJ                                           '),
(771, '2005-04-10 17:29:00', 'Active', 'Indian Restaurant', 'The Rose Valley', 'Mizan', 'X', '01483572572', '', '07949008780', '', '', '50 Chertsey Street', '', '', 'Guilford', 'Surrey', 'GU1 4HD', '', '', NULL, '', '', '', '', '', ''),
(781, '2005-02-11 17:19:00', 'Active', 'Mini Cab', 'Heathrow Express Cars', 'Abdul', 'Mehraban', '02089974449', '', '07950881503', '', '', '11 Royal Parade', '', '', 'London', 'London', 'W5 1ET', '', '', NULL, '', '', '', '', '', ''),
(792, '', 'Active', 'Indian Restaurant', 'Shenfield Tandoori', 'K', 'Rahman', '01277234087', '', '07706156696', '', '', '152 Hutton Road', '', '', 'Shenfield', 'Essex', 'CM15 8NL', '', '', NULL, '', '', '', '', '', ''),
(793, '', 'Active', 'Indian Restaurant', 'Ali Raj', 'Abdul', 'Rashid', '01423521627', NULL, '07765862614', NULL, NULL, '7 Cheltenham Crescent', NULL, NULL, 'Harrogate', 'American Samoa', 'HG1 1DH', NULL, '', 1, '68 Upper Rushton Road', '', '', 'Bradford', 'Yorkshire', 'BD3 7LQ                                           '),
(796, '', 'Active', 'Indian Restaurant', 'Curry Delight', 'Motin', 'X', '02085739900', '', '', '', '', '1 North Parade', 'North Hyde Road', '', 'Hayes', 'Middlesex', 'UB3 4JA', '', '', NULL, '', '', '', '', '', ''),
(797, '', 'Active', 'Indian Restaurant', 'Cafe Sekander', 'X', 'Ali', '01517244300', NULL, NULL, NULL, NULL, '165 Allerton Road', 'Mossley Hill', NULL, 'Liverpool', 'Albania', 'L18 6HG', NULL, '', 1, '', '', '', '', '', ''),
(798, NULL, 'Active', 'test jd 123', 'jd test', 'jd', 'tank', '1234567890', NULL, '9638334075', 'jdtank90@gmail.com', NULL, '123456', '456789', 'junagadh', 'junagadh', 'Afghanistan', '362002', 'test', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `task_description` text DEFAULT NULL,
  `task_assign_to` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `modifyBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `task_name`, `task_description`, `task_assign_to`, `created_at`, `createdBy`, `updated_at`, `modifyBy`) VALUES
(3, 'testign', 'testing', 1, '2021-03-10 07:01:27', 1, NULL, NULL),
(4, 'please complete job required', 'complete job required', 2, '2021-03-10 07:02:16', 1, '2021-03-10 07:03:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1-Active, 2-Inactive, 3-InProgress, 4-Done, 5-ReOpen',
  `assign_to` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `web`
--

CREATE TABLE `web` (
  `job_id` varchar(5) NOT NULL,
  `url` varchar(532) DEFAULT NULL,
  `name_expiry_date` varchar(1911) DEFAULT NULL,
  `domain_tag` varchar(634) DEFAULT NULL,
  `hosting_company` varchar(632) DEFAULT NULL,
  `accoun_type` varchar(532) DEFAULT NULL,
  `account_username` varchar(433) DEFAULT NULL,
  `account_password` int(255) DEFAULT NULL,
  `ftp` varchar(2152) NOT NULL,
  `ftp_address` varchar(3435) DEFAULT NULL,
  `ftp_username` varchar(448) DEFAULT NULL,
  `p3_server_address` varchar(634) DEFAULT NULL,
  `p3_username` varchar(622) DEFAULT NULL,
  `expired` tinyint(43) DEFAULT NULL,
  `comments` varchar(823) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`accountId`);

--
-- Indexes for table `alternate_address`
--
ALTER TABLE `alternate_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bussiness_type`
--
ALTER TABLE `bussiness_type`
  ADD PRIMARY KEY (`typeId`);

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `extra`
--
ALTER TABLE `extra`
  ADD PRIMARY KEY (`extra_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otherjob`
--
ALTER TABLE `otherjob`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `printing`
--
ALTER TABLE `printing`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staffId`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web`
--
ALTER TABLE `web`
  ADD PRIMARY KEY (`job_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alternate_address`
--
ALTER TABLE `alternate_address`
  MODIFY `id` int(151) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `bussiness_type`
--
ALTER TABLE `bussiness_type`
  MODIFY `typeId` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` smallint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `extra`
--
ALTER TABLE `extra`
  MODIFY `extra_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `roleId` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staffId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `supplier_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=799;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
