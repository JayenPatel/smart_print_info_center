-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2021 at 06:51 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_print`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `accountId` int(11) NOT NULL,
  `customerId` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `creditNo` int(11) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL COMMENT '0 for active 1 for deactive',
  `createdDate` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `modifyBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `alternate_address`
--

CREATE TABLE `alternate_address` (
  `id` int(151) NOT NULL,
  `address1` text DEFAULT NULL,
  `address2` text DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `area` text DEFAULT NULL,
  `postCode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alternate_address`
--

INSERT INTO `alternate_address` (`id`, `address1`, `address2`, `town`, `country`, `area`, `postCode`) VALUES
(1, 'aaaaaaaaaaaaaaaaaa', 'sad', 'sadsa', 'India', 'asdsad', '131123'),
(2, 'sadsa', 'sad', 'sadsa', 'India', 'asdsad', '131123');

-- --------------------------------------------------------

--
-- Table structure for table `bussiness_type`
--

CREATE TABLE `bussiness_type` (
  `typeId` int(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bussiness_type`
--

INSERT INTO `bussiness_type` (`typeId`, `name`) VALUES
(1, 'IT'),
(2, 'Computer');

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE `calendar` (
  `job_id` varchar(5) NOT NULL,
  `envelope_qty` smallint(255) DEFAULT NULL,
  `logo` tinyint(4) DEFAULT NULL,
  `menu_on_back` tinyint(4) DEFAULT NULL,
  `comments` varchar(4356) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `calendar`
--

INSERT INTO `calendar` (`job_id`, `envelope_qty`, `logo`, `menu_on_back`, `comments`) VALUES
('2C', 1, 1, 0, '5'),
('6C', 14, 1, 0, '32'),
('6Q', 14, 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` smallint(6) NOT NULL,
  `reg_date` varchar(19) DEFAULT NULL,
  `customer_status` varchar(7) DEFAULT NULL,
  `business_type` varchar(20) DEFAULT NULL,
  `business_name` varchar(36) DEFAULT NULL,
  `first_name` varchar(18) DEFAULT NULL,
  `surname` varchar(12) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `fax` varchar(13) DEFAULT NULL,
  `mobile` varchar(12) DEFAULT NULL,
  `email` varchar(26) DEFAULT NULL,
  `web` varchar(0) DEFAULT NULL,
  `address1` varchar(36) DEFAULT NULL,
  `address2` varchar(15) DEFAULT NULL,
  `area` varchar(10) DEFAULT NULL,
  `city` varchar(23) DEFAULT NULL,
  `county` varchar(19) DEFAULT NULL,
  `postcode` varchar(9) DEFAULT NULL,
  `comments` varchar(133) DEFAULT NULL,
  `alternate_address` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `delivery_address1` varchar(22) DEFAULT NULL,
  `delivery_address2` varchar(15) DEFAULT NULL,
  `delivery_area` varchar(18) DEFAULT NULL,
  `delivery_city` varchar(19) DEFAULT NULL,
  `delivery_county` varchar(15) DEFAULT NULL,
  `delivery_postcode` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `reg_date`, `customer_status`, `business_type`, `business_name`, `first_name`, `surname`, `telephone`, `fax`, `mobile`, `email`, `web`, `address1`, `address2`, `area`, `city`, `county`, `postcode`, `comments`, `alternate_address`, `status`, `delivery_address1`, `delivery_address2`, `delivery_area`, `delivery_city`, `delivery_county`, `delivery_postcode`) VALUES
(1, '', 'Active', 'Indian Restaurant', 'Asha', 'Faruk/Monir', 'Monir', '01189272711', '', '', '', '', '6 Hudson Road Woodley', '', '', 'Reading', 'Berkshire', 'RG5 4EW', '', '', NULL, '', '', '', '', '', ''),
(2, '', 'Passive', 'Indian Restaurant', 'Light of Bengal', 'Hafiz', '', '01202391181', '', '', '', '', '269 Holenhurst Road', '', '', 'Bournemouth', '', 'BH8 8BZ', '', '', NULL, '', '', '', '', '', ''),
(3, '', 'Passive', 'Indian Restaurant', 'Haweli Blackburn', 'Harun', '', '01254670265', '', '', '', '', '32 - 34 Higher Eanam', '', '', 'Blackburn', '', 'BB1 3AZ', '', '', NULL, '', '', '', '', '', ''),
(4, '', 'Passive', 'Indian Restaurant', 'Punjab Balti', 'Tariq', 'Mizer', '01386 422010', '', '', '', '', '53 high Street', '', '', 'Evesham', 'Worcs', 'WR11 4DA', '', '', NULL, '', '', '', '', '', ''),
(5, '', 'Passive', 'Indian Restaurant', 'Mohan', 'Uddin', 'S.', '01438 726419', '', '', '', '', '116 High Street', '', '', 'Stevenage Old Town', 'Hertfordshire', 'SG1 3DW', '', '', NULL, '', '', '', '', '', ''),
(6, '', 'Passive', 'Indian Restaurant', 'New Megna', 'Mumin', '', '01442 249777', '', '', '', '', '175 / 177 London Road', '', '', 'Apsley Hemel', 'Hertfordshire', 'HP3 9SQ', '', '', NULL, '', '', '', '', '', ''),
(7, '', 'Passive', 'Indian Restaurant', 'Kathmandu 34', 'Ram', 'A', '01491 574422', '', '07787748398', '', '', '34 Reading Road', '', '', 'Henley on Thames', 'Oxfordshire', 'RG9 1AG', '', '', NULL, '', '', '', '', '', ''),
(8, '', 'Passive', 'Thai Restaurant', 'Mae Nam', 'Andrew', 'Smith', '01491 574659', '', '', '', '', '53 - 55 Reading Road', '', '', 'Henley on Thames', 'Oxfordshire', 'RG9 1AB', '', '', NULL, '', '', '', '', '', ''),
(9, '', 'EOT', 'Indian Restaruant', 'Rajput', 'Ruman', '', '01491 872796', '', '', '', '', 'The Laurels High Street', '', '', 'Goring on', 'Oxfordshire', 'RG8 9AT', '', '', NULL, '', '', '', '', '', ''),
(10, '', 'Passive', 'Indian Restaurant', 'Raj Gate', 'Nazrul', '', '01525 40635', '', '', '', '', '8 Bedford Street', '', '', 'Ampthill', 'Bedfordshire', 'MK45 2NB', '', '', NULL, '', '', '', '', '', ''),
(11, '', 'Passive', 'Indian Restaurant', 'Shipa', 'Islam', '', '01634 570119', '', '', '', '', '113 Canterbury Street', '', '', 'Gillingham', 'Kent', 'ME7 5TS', '', '', NULL, '', '', '', '', '', ''),
(12, '', 'Acive', 'Indian Restaurant', 'Tandoori Knights', 'Ashraf', 'Manager', '01664566460', '', '', '', '', '33 Burton Street', '', '', 'Melton Mowbray', 'Leicester', 'LE13 1AF', '', '', NULL, '', '', '', '', '', ''),
(13, '', 'Passive', 'Indian Restaurant', 'Tripti', 'Joynal', '', '01753 882557', '', '', '', '', '29 Market Place', '', '', 'Chalfont St.Peter', 'Buckinghamshire', 'SL9 9DU', '', '', NULL, '', '', '', '', '', ''),
(14, '', 'Passive', 'Indian Restaurant', 'Karma Innovation', 'Koddus', '', '01792 477848', '', '', '', '', '3 Victoria Road', '', '', 'Swansea', '', 'SA1 3NE', '', '', NULL, '', '', '', '', '', ''),
(15, '', 'Passive', 'Indian Restaurant', 'Koh I Noor', 'Rahman', '', '01895 239686', '', '', '', '', '230 High St', '', '', 'Uxbridge', 'Middlesex', 'UB8 1LD', '', '', NULL, '', '', '', '', '', ''),
(16, '', 'Passive', 'Indian Restaurant', 'Dinajpur', 'Ali', '', '01908 376234', '', '', '', '', '41 Aylesbury Street', '', '', 'Fenny Stratford', '', 'MK2 2BQ', '', '', NULL, '', '', '', '', '', ''),
(17, '', 'Passive', 'Indian Restaurant', 'Essence of India', 'Jalil', 'A', '01920 484779', '', '', '', '', '1A East Street', '', '', 'Ware', 'Hertfordshire', 'SG12 9HJ', 'home 01920484266', '', NULL, '89 Queens Road', '', '', 'Ware', 'Hertfordshire', 'SG12 7DW                                          '),
(18, '', 'Passive', 'Indian Restaurant', 'Bushey Spice', 'Shamim', 'Mohammed', '01923222429', '', '', '', '', '435 Bushey Mill Lane', '', '', 'Bushey', 'Hertfordshire', 'WD2 2AN', '', '', NULL, '', '', '', '', '', ''),
(19, '', 'Passive', 'Indian Restaurant', 'Red Rose', 'Ujjal', 'Ahmed', '01923 254900', '', '07960253784', '', '', '9 Chalk Hill', '', '', 'Bushey', 'Hertfordshire', 'WD19 4BL', 'partner  Mr.Shohid', '', NULL, '', '', '', '', '', ''),
(20, '', 'Passive', 'Indian Restaurant', 'Raj Garden', 'Siddique', 'Rahman', '01923 493939', '', '07957195988', '', '', '219 Park Avenue', '', '', 'Bushey', 'Hertfordshire', 'WD2 2DQ', 'home tel: 01582726678', '', NULL, '', '', '', '', '', ''),
(21, '', 'EOT', 'Indian Restaurant', 'Shanti', 'Harun', '', '01923 827856', '', '', '', '', '48 High St', '', '', 'Northwood', 'Middlesex', 'XX66 77K', '', '', NULL, '', '', '', '', '', ''),
(22, '', 'Passive', 'Indian Restaurant', 'Maharaja', 'Ali/Kalam', '', '01923835825', '', '', '', '', '24 Green Lane', '', '', 'Northwood', 'Middlesex', 'HA6 2QB', '', '', NULL, '', '', '', '', '', ''),
(23, '', 'Passive', 'Pizza', 'Bella Pizza', 'Ronni', '', '01926 885554', '', '', '', '', '48 High Street', '', '', 'Leamington Spa', '', 'CV31 1LW', '', '', NULL, '', '', '', '', '', ''),
(24, '', 'Passive', 'Indian Restaurant', 'Ali Taj', 'Ali', '', '01946 693085', '', '', '', '', '34-35 Tangier Street', '', '', 'Whitehaven', 'Cumbria', 'CA28 7UZ', '', '', NULL, '', '', '', '', '', ''),
(25, '', 'Passive', 'Indian Restaurant', 'Curry Paradise', 'Mohammed', 'Ahyah', '01993702187', '', '07863129043', '', '', '39 High Street', '', '', 'Witney', 'Oxfordshire', 'OX28 6HP', 'home 01993779696\r\n\r\nconact Raju @ Restaurant', '', NULL, '31 Moorland close', '', '', 'Witney', 'Oxfordshire', 'OX28 6LN                                          '),
(26, '', 'Passive', 'Indian Restaurant', 'Buckingham Balti', 'Suleman', '', '020 7', '', '', '', '', '42 Buckingham Palace Road', '', '', '', 'London', 'SW1W 0RE', '', '', NULL, '', '', '', '', '', ''),
(27, '', 'Passive', 'Indian Restaurant', 'Goa', 'Mohammed', 'Kadir', '02072232169', '', '07859070165', '', '', '344 Battersea Park Road', '', '', 'London', 'London', 'SW11 3BY', 'Zaman Home   02079317418', '', NULL, '', '', '', '', '', ''),
(28, '', 'Passive', 'Indian Restaurant', 'Suruchi', 'Turaj', 'Miah', '020 7241 5213', '', '', '', '', '82 Mildmay Park', '', '', 'Islington', 'London', 'N1 4PR', 'Mo: 07956895399', '', NULL, '', '', '', '', '', ''),
(29, '', 'Passive', 'Indian Restaurant', 'Everest', 'Bari', 'K', '02072623853', '', '', '', '', '41 Craven Road', '', '', 'Lancester Gate', 'London', 'W2 3BX', '', '', NULL, '', '', '', '', '', ''),
(30, '', 'Passive', 'Indian Restaurant', 'Chillies', 'Jubair', '', '020 7266 3445', '', '', '', '', '412 Harrow Road', '', '', '', 'London', 'W9 2HU', '', '', NULL, '', '', '', '', '', ''),
(31, '', 'Passive', 'Indian Restaurant', 'Sitara', 'Raj', 'Singmaal', '020 7281 0649', '', '07956262330', '', '', '784 Holloway Road', '', '', 'London', 'London', 'N19 3JH', 'home:', '', NULL, '3 Grange Avenue', '', 'north finchley', '', 'London', 'N12 8DN                                           '),
(32, '', 'EOT', 'Indian Restaurant', 'Café Bombay', 'Darius', 'Darius', '020 7289 2023', '', '', '', '', '370 Harrow Road', '', '', '', 'London', 'W9 2HU', '', '', NULL, '', '', '', '', '', ''),
(33, '', 'Passive', 'Indian Restaurant', 'Panshi', 'Aminur', 'Rahman', '020 7328 1226', '', '', '', '', '31 Malvern Road', '', '', 'London', 'London', 'NW6 5PS', 'Ekram', '', NULL, '', '', '', '', '', ''),
(34, '', 'Passive', 'Indian Restaurant', 'Café Villa', 'Azad', '', '020 7328 2727', '', '', '', '', '215 Shirland Road', '', '', '', 'London', 'W9 3JP', '', '', NULL, '', '', '', '', '', ''),
(35, '', 'Passive', 'Indian Restaurant', 'Raj Moni', 'Kalam', '', '020 7354 1270', '', '', '', '', '279 Upper Street', '', '', 'Islington', 'Londoon', 'N1 2TZ', '', '', NULL, '', '', '', '', '', ''),
(36, '', 'Passive', 'Indian Restaurant', 'Classic Tandoori', 'Monir', 'Hussain', '020 7372 8828', '', '079551715047', '', '', '218 Belsize Road', '', '', 'London', 'London', 'NW6 4DJ', 'Home tel 02082062931', '', NULL, '', '', '', '', '', ''),
(37, '', 'Passive', 'Indian Restaurant', 'Tandoori Garden', 'Suleeman', '', '020 7381 1069', '', '', '', '', '98 Lillie Road', '', '', 'Fulham', 'London', 'SW6 7SR', '', '', NULL, '', '', '', '', '', ''),
(38, '', 'Passive', 'Indian Restaurant', 'Chameli Indian Restaurant', 'Eddy', 'A', '020 7482 2833', '', '', '', '', '56 Chetwynd Road', '', '', 'London', 'London', 'NW5 1DJ', '', '', NULL, '', '', '', '', '', ''),
(39, '', 'Passive', 'Indian Restaurant', 'Tandoori of Chelsea', 'Nazir', '', '020 7589 7749', '', '', '', '', '153 Fulham Road', '', '', 'Chelsea', 'London', 'SW3 6SN', '', '', NULL, '', '', '', '', '', ''),
(40, '', 'Passive', 'Indian Restaurant', 'Raj of India', 'Hakim', 'A', '02076029112', '', '', '', '', '46 Shepherds Bush Road', '', '', 'London', 'London', 'W6 7PJ', 'cotact Mr.Hoque', '', NULL, '', '', '', '', '', ''),
(41, '', 'Passive', 'Indian Restaurant', 'Jomuna', 'Kader', '', '020 7630 0238', '', '', '', '', '74 Wilton Road', '', '', '', 'London', 'SW1V 1DE', '', '', NULL, '', '', '', '', '', ''),
(42, '', 'Passive', 'Indian Restaurant', 'Neel Akash', 'Jaheed', '', '020 7637 0050', '', '', '', '', '93 Charlotte Street', '', '', '', 'London', 'W1T 4PY', '', '', NULL, '', '', '', '', '', ''),
(43, '', 'Passive', 'Indian Restaurant', 'Paprika', 'Shajid', '', '020 7703 3149', '', '', '', '', '39 Denmark Hill', '', '', '', 'London', 'SE5 8RS', '', '', NULL, '', '', '', '', '', ''),
(44, '', 'Passive', 'Indian Restaurant', 'Durbar', 'Shamim', 'Syed', '02077271947', '', '', '', '', '24 Hereford Road', '', '', 'London', 'London', 'W2 4AA', '', '', NULL, '', '', '', '', '', ''),
(45, '', 'Passive', 'Indian Restaurant', 'Taste of Bengal', 'Amir', '', '020 7732 8331', '', '', '', '', '172 New Cross Road', '', '', '', 'London', 'SE14 5AA', '', '', NULL, '', '', '', '', '', ''),
(46, '', 'Passive', 'Indian Restaurant', 'India Cottage', 'Islam', 'Islam', '02077362521', '', '', '', '', '67 Fulham High Street', '', '', '', 'London', 'SW6 3JJ', '', '', NULL, '', '', '', '', '', ''),
(47, '', 'Passive', 'Indian Restaurant', 'Spice of Hampstead', 'Khan', '', '020 7794 5922', '', '', '', '', '448 Finchley Road', '', '', 'Childs Hill', 'London', 'NW2 2HY', '', '', NULL, '', '', '', '', '', ''),
(48, '', 'Passive', 'Indian Restaurant', 'Gandhi\'s Restaurant', 'Azad', 'Azad', '02078316208', '', '07946577441', '', '', '35 Grays Inn Road', '', '', 'Holborn', 'London', 'WC1X 8PP', '', '', NULL, '', '', '', '', '', ''),
(49, '', 'Passive', 'Indian Restaurant', 'Shekara', 'Kanti', '', '020 7834 0722', '', '', '', '', '3 Lower Grosvenor Place', '', '', '', 'London', 'SW1W 0EJ', '', '', NULL, '', '', '', '', '', ''),
(50, '', 'Passive', 'Indian Restaurant', 'Maharaja of India', 'Shalim', '', '02079303044', '', '', '', '', '19A Charing Cross Road', '', '', '', 'London', 'WC2H 0ES', '', '', NULL, '', '', '', '', '', ''),
(51, '', 'Passive', 'Indian Restaurant', 'Lahoria', 'Ricky', '', '02082061129', '', '', '', '', '274 Kingsbury Road', '', '', '', 'London', 'NW9 0BY', '', '', NULL, '', '', '', '', '', ''),
(52, '', 'Passive', 'Indian Restaurant', 'Bombay Tandoori', 'Shamim', '', '020 8340 1714', '', '', '', '', '50 Topsfield Parade', '', '', 'Tottenham Lane', 'London', 'N8 8PT', '', '', NULL, '', '', '', '', '', ''),
(53, '', 'Passive', 'Indian Restaurant', 'East India', 'Numan', 'Khan', '020 8399 0617', '', '', '', '', '1 Brighton Road', '', '', 'Surbiton', 'Surrey', 'KT6 5LX', '', '', NULL, '', '', '', '', '', ''),
(54, '', 'Passive', 'Indian Restaurant', 'Vintage', 'Manager', '', '020 8427 7960', '', '', '', '', '207 Station Road', '', '', 'Harrow', 'Middlesex', 'HA1 2TP', '', '', NULL, '', '', '', '', '', ''),
(55, '', 'Passive', 'Indian Restaurant', 'Darjeeling Inn', 'Rai', 'Rai', '020 85757236', '', '', '', '', '468 Lady Margaret Road', '', '', 'Southall', 'Middlesex', 'UB1 2NW', 'home tel 02085747289', '', NULL, '', '', '', '', '', ''),
(56, '', 'Passive', 'Indian Restaurant', 'Drumstick', 'Winsteen', '', '020 8646 8661', '', '', '', '', '259 London Road', '', '', 'Mitcham', 'Surrey', 'CR4 3NH', '', '', NULL, '', '', '', '', '', ''),
(57, '', 'Passive', 'Indian Restaurant', 'Haweli Feltham', 'Khalique', '', '02087510716', '', '', '', '', '1 Parkfield Parade High Street', '', '', 'Fel', 'Middlesex', 'TW13 4HJ', '', '', NULL, '', '', '', '', '', ''),
(58, '', 'Passive', 'Indian Restaurant', 'Ma Goa', 'Kapoor', '', '020 8780 1767', '', '', '', '', '244 Upper Richmond Road', '', '', '', 'London', 'SW15 6TG', '', '', NULL, '', '', '', '', '', ''),
(59, '', 'Passive', 'Indian Restaurant', 'Willesden Spice', 'Nassar', '', '020 8830 3914', '', '', '', '', '373 High Road', '', '', 'Willesden', 'London', 'NW10 2JR', '', '', NULL, '', '', '', '', '', ''),
(60, '', 'Passive', 'Indian Restaurant', 'Ruhits', 'Islam', '', '020 8830 4222', '', '', '', '', '4 Sidmouth Parade Sidmouth Road', '', '', '', 'London', 'NW2 5HG', '', '', NULL, '', '', '', '', '', ''),
(61, '', 'Passive', 'Indian Restaurant', 'Millennium', 'Amir', 'X', '02088450001', '', '076863103678', '', '', '249 Yeading Lane', '', '', 'Hayes', 'Middlesex', 'UB4 9AD', '', '', NULL, '', '', '', '', '', ''),
(62, '', 'Passive', 'Indian Restaurant', 'Taj Mahal', 'Tarif', 'Miah', '020 8845 7779', '', '07876500432', '', '', '4 Willow Tree Lane', '', '', 'Yeading, Hayes', 'Middlesex', 'UB4 9BB', '', '', NULL, '', '', '', '', '', ''),
(63, '', 'EOT', 'Indian Restaurant', 'Raj of Bengal', 'Hakim', '', '020 8848 4818', '', '', '', '', '630 Uxbridge Road', '', '', 'Hayes', 'Middlesex', 'UB4 0RY', '', '', NULL, '', '', '', '', '', ''),
(65, '', 'Passive', 'Indian Restaurant', 'Gurkha Tandoori', 'Bickay', '', '02088903474', '', '', '', '', '78 Bedfont Lane', '', '', 'Feltham', 'Middlesex', 'TW14 9BP', '', '', NULL, '', '', '', '', '', ''),
(66, '', 'Passive', 'Indian Restaurant', 'Golden Tandoori', 'Subhan', 'A', '02088961397', '', '', '', '', '122 Churchfield Road', '', '', 'London', 'London', 'W3 6BY', '', '', NULL, '', '', '', '', '', ''),
(67, '', 'Passive', 'Indian Restaurant', 'Tandoori Express', 'Jamal', 'Miah', '02089605274', '', '07903010614', '', '', '16 Station Terrace', '', '', 'Kensal Rise', 'London', 'NW10 5RX', '', '', NULL, '', '', '', '', '', ''),
(68, '', 'Passive', 'Thai Restaurant', 'Flying Elephant', 'Choudhury', '', '02089609044', '', '', '', '', '37 Chamberlayne Road', '', '', '', 'London', 'NW10 3NB', '', '', NULL, '', '', '', '', '', ''),
(69, '', 'Passive', 'Indian Restaurant', 'Bawarchi', 'Iqbal', 'X', '02089609608', '', '07947279058', '', '', '68 Chamberlayne Road', '', '', 'London', 'London', 'NW10 3JJ', '', '', NULL, '', '', '', '', '', ''),
(70, '', 'Passive', 'Indian Restaurant', 'Curry Nights', 'Ali/Islam', '', '020 8964 5725', '', '', '', '', '58 Chamberlayne Road', '', '', 'Kensal Rise', 'London', 'NW10 3JE', '', '', NULL, '', '', '', '', '', ''),
(71, '', 'EOT', 'Chinese Restaurant', 'Mandarin Chef', 'Manager', '', '020 8965 0237', '', '', '', '', '110 High Street', '', '', 'Harlesden', 'London', 'NW10 4SL', '', '', NULL, '', '', '', '', '', ''),
(72, '', 'Passive', 'Indian Restaurant', 'The Familey lebanese', 'Ali', 'Haj Ali', '02089689053', '', '', '', '', '359 Harrow Road', '', '', 'Maida Vale', 'London', 'W9 3NA', '', '', NULL, '', '', '', '', '', ''),
(73, '', 'Passive', 'Indian Restaurant', 'Khas Tandoori', 'Jamshed', '', '02089692537', '', '', '', '', '39 Chamberlayne Road', '', '', 'Kensal Rise', 'London', 'NW10 3NB', '', '', NULL, '', '', '', '', '', ''),
(74, '', 'Passive', 'Indian Restaurant', 'The Raj', 'Choudhury', '', '020 8969 2543', '', '', '', '', '37 Chamberlayne Road', '', '', 'Kensal Green', 'London', 'NW10 3NB', '', '', NULL, '', '', '', '', '', ''),
(75, '', 'Passive', 'Indian Restaurant', 'Meghna Tandoori', 'Iqbal', '', '02089912122', '', '', '', '', '2 Abbey Parade', '', '', 'Hanger Lane', 'London', 'W5 1EE', '', '', NULL, '', '', '', '', '', ''),
(77, '', 'Passive', 'Indian Restaurant', 'Sunrise', 'Choudhury', '', '02380 268383', '', '', '', '', '95A Winchester Road', '', '', 'Chandlers Ford', 'Hampshire', 'SO53 2SJ', '', '', NULL, '', '', '', '', '', ''),
(78, '', 'Passive', 'Indian Restaurant', 'Moonlight', 'Choudhury', '', '02380 643626', '', '', '', '', '31 High Street', '', '', 'Eastleigh', 'Hampshire', 'SP50 5LF', '', '', NULL, '', '', '', '', '', ''),
(79, '', 'Passive', 'Indian Restaurant', 'Misaki', 'Choudhury', '', '020 8964 3939', '', '', '', '', '43 Chamberlayne Road', '', '', 'Kensal Green', 'London', 'NW10 3NB', '', '', NULL, '', '', '', '', '', ''),
(80, '', 'EOT', 'Indian Restaurant', 'Raj', 'Inam', '', '020 8965 6036', '', '', '', '', '146 High Street', '', '', 'Harlesden', 'London', 'NW10 4SP', 'Previous Name:  Raj Balti', '', NULL, '', '', '', '', '', ''),
(100, '', 'Passive', 'Indian Restaurant', 'Pipasha Tandoori Restaurant', 'Abdur Rahim', 'Khan', '01428712080', '', '', '', '', 'Crossways', '', '', 'Churt Nr. Farnham', 'Surrey', 'GU10 2JA', '', '', NULL, '', '', '', '', '', ''),
(101, '', 'Passive', 'Indian Restaurant', 'Standard Indian', 'Ahsan', 'Choudhury', '01215612048', '', '', '', '', '2 High Street', '', '', 'Black Heath', 'West Milddlands', 'B65 0DT', '', '', NULL, '', '', '', '', '', ''),
(102, '', 'Passive', 'Office', 'Reddy Siddiqui', 'Fozia', 'Muddassir', '02088111188', '02088111189', '', '', '', 'Park View  183  189 The Vale', '', '', 'London', 'London', 'W3 7RW', '', '', NULL, '', '', '', '', '', ''),
(103, '', 'Passive', 'Indian Restaurant', 'Bagicha', 'Miah', 'R', '1865554895', '', '', '', '', '15 North Parade', '', '', 'Oxford', 'Oxfordshire', 'OX2 6LX', '', '', NULL, '', '', '', '', '', ''),
(104, '', 'Passive', 'Indian Restaurant', 'Lee Tandoori', 'Abu', 'Taher', '02392551408', '', '', '', '', '4 Flower Buildings  Marine Parde', '', '', 'Lee on Solent', 'Hampshire', 'PO13 9LB', '', '', NULL, '', '', '', '', '', ''),
(105, '', 'Passive', 'Indian Restaurant', 'The Asian Grill', 'Abdul', 'Kadir', '01213547491', '', '', '', '', '91 Park Road', '', '', 'Sutton Coldfield', 'West Midlands', 'B73 6BT', '', '', NULL, 'SNS Newsagents', '69 Park Road', '', 'Sutton Coldfield', 'West Midlands', 'B73 6BX                                           '),
(106, '', 'Passive', 'Indian Restaurant', 'The Sunam', 'Basar', 'A', '01253352572', '', '', '', '', '93/99 Red Bank Road  Bispham', '', '', 'Blackpool', 'Lancashire', 'FY2 9HZ', '', '', NULL, '', '', '', '', '', ''),
(107, '', 'Passive', 'Indian Restaurant', 'Ali Raj', 'Rahel', 'Ahmed', '01306631664', '', '07796808603', '', '', 'Park Gate Road  Newdigate', '', '', 'Dorking', 'Surrey', 'RH5 5DZ', '', '', NULL, '', '', '', '', '', ''),
(108, '', 'Passive', 'Indian Restaurant', 'Tandoori Nite', 'Rahman', 'S', '01843 848244', '', '', '', '', '64 Station Road', '', '', 'Birchington', 'Kent', 'CT7 9RA', '', '', NULL, '', '', '', '', '', ''),
(109, '', 'EOT', 'Indian Restaurant', 'Porthcawl Tandoori', 'Belal', 'Chowdhury', '01656788193', '', '', '', '', '108 John Street', '', '', 'Porthcawl', 'Mid Galmorgan', 'CF36 3DT', '', '', NULL, '', '', '', '', '', ''),
(110, '', 'Passive', 'Indian Restaurant', 'Royal Tandoori', 'Arif', '', '02086513500', '', '', '', '', '226 Addington Road  Selsdon', '', '', 'South Croydon', 'Surrey', 'CR2 8DL', '', '', NULL, '', '', '', '', '', ''),
(112, '', 'Passive', 'Indian Restaurant', 'The Royal Tandoori', 'Arif', '', '01883626200', '', '', '', '', '209 Godstone Road', '', '', 'Whyteleafe', 'Surrey', 'CR3 0EL', '', '', NULL, '', '', '', '', '', ''),
(113, '', 'Passive', 'Indian Restaurant', 'Spice of Life', 'Sarwar', 'Sarwar', '01933411122', '', '', '', '', '169 Wellingborough Road', '', '', 'Rushden', 'Northamptonshire', 'NN10 9SZ', '', '', NULL, '', '', '', '', '', ''),
(114, '', 'Passive', 'Indian Restaurant', 'Eastern Spice', 'Sarwar', '', '01933650044', '', '', '', '', '56 High Street', '', '', 'Irthlingborough', 'Northants', 'NN9 5TN', '', '', NULL, '', '', '', '', '', ''),
(115, '', 'Passive', 'Indian Restaurant', 'Tandoori Club', 'Sunu', 'Miah', '02089053233', '', '', '', '', '4a Broadfield Parade  Glengall Road', '', '', 'Edgeware', 'middlesex', 'HA8 8TE', '', '', NULL, '', '', '', '', '', ''),
(116, '', 'Passive', 'Indian Restaurant', 'Khans Restaurant', 'Bazlu', 'Khan', '01227741442', '', '', '', '', '8 William Street', '', '', 'Herne Bay', 'Kent', 'CT6 5EJ', '', '', NULL, '', '', '', '', '', ''),
(117, '', 'Passive', 'Indian Restaurant', 'The Purbani', 'Koyes', 'Ahmed', '0190527402', '', '07984888193', '', '', '27 The Tything', '', '', 'Worcester', 'Worcestershire', 'WR1 1JL', '', '', NULL, '', '', '', '', '', ''),
(118, '', 'Passive', 'Indian Restaurant', 'Balti Kitchen', 'F Rahman', 'Ghuri', '01625503070', '', '', '', '', '22 Park Green', '', '', 'Macclesfield', 'Cheshire', 'SK11 7NA', '', '', NULL, '', '', '', '', '', ''),
(119, '', 'Passive', 'Indian Restaurant', 'Balti Naz', 'Foyazur', 'Rahman', '02085605900', '', '', '', '', '185 South Ealing Road', '', '', 'Ealing', 'London', 'W5 4RH', '', '', NULL, '', '', '', '', '', ''),
(120, '', 'Passive', 'Indian Restaurant', 'Shappi of Ashford', 'Ahmed', 'K', '01784258389', '', '07944283664', '', '', '7 Church Parade  Church Road', '', '', 'Ashford', 'Middlesex', 'TW15 2TX', '', '', NULL, '', '', '', '', '', ''),
(121, '', 'Passive', 'Indian Restaurant', 'Akash Tandoori', 'Barik', 'Miah', '01904 633550', '', '', '', '', '10 North Street', '', '', 'York', 'Yorkshire', 'YO1 6JD', '', '', NULL, '', '', '', '', '', ''),
(122, '', 'Passive', 'Indian Restaurant', 'Maharaja Tandoori', 'Zia', 'Matin', '01708725533', '', '', '', '', '40 Victoria Road', '', '', 'Romford', 'Essex', 'RM1 2JH', '', '', NULL, '', '', '', '', '', ''),
(123, '', 'Passive', 'Indian Restaurant', 'Jewel in the Crown', 'Jewel', 'M Rahman', '02380512205', '', '', '', '', '303 Shirley Road', '', '', 'Southampton', 'Hampshire', 'SO15 3HU', '', '', NULL, '', '', '', '', '', ''),
(124, '', 'Passive', 'Indian Restaurant', 'Jewel of India', 'Jewel', 'M Rahman', '02380466400', '', '', '', '', 'unit 3  Town Hill Way  West End', '', '', 'Southapton', 'Hampshire', 'SO18 3RA', '', '', NULL, '', '', '', '', '', ''),
(125, '', 'Passive', 'Indian Restaurant', 'Eastern Promise', 'Nurul', 'Amin', '01323832533', '', '07796945766', '', '', 'London House  Gardner Street', '', '', 'Herstmonceux', 'East Sussex', 'BN27 4LB', '', '', NULL, '', '', '', '', '', ''),
(126, '', 'Passive', 'Indian Restaurant', 'Bonani Tandoori', 'Mahsuk', 'Miah', '01424772677', '', '', '', '', '42b High Street', '', '', 'Batle', 'East Sussex', 'TN33 0EE', '', '', NULL, '', '', '', '', '', ''),
(127, '', 'Passive', 'Indian Restaurant', 'Bengal Tandoori', 'Rofu', 'Miah', '01903774365', '', '07776203177', '', '', '116 Downs way', '', '', 'East Preston', 'West Sussex', 'BN16 1AF', '', '', NULL, '', '', '', '', '', ''),
(128, '', 'Passive', 'Indian Restaurant', 'Papadoms', 'Shokat', 'Ali', '01327702010', '', '', '', '', '22A High Street', '', '', 'Daventry', 'Northants', 'NN11 4HU', '', '', NULL, '', '', '', '', '', ''),
(129, '', 'Passive', 'Indian Restaurant', 'Balti Garden', 'Mukta', 'Miah', '01803290087', '', '', '', '', '23 Abbey Road', '', '', 'Torquay', 'S. Devon', 'TQ2 5NF', '', '', NULL, '', '', '', '', '', ''),
(130, '', 'Passive', 'Indian Restaurant', 'Raja Tandoori', 'Asif', 'Khan', '01418893123', '', '07739151230', '', '', '57 Old Sneddon Street', '', '', 'Paisley', '', 'PA3 2AN', '', '', NULL, '', '', '', '', '', ''),
(131, '', 'Passive', 'Indian Restaurant', 'Shalimar Restaurant', 'Iraq', '', '02392251565', '', '', '', '', '9Hambledon Parade Hambledon Rd', '', '', 'Waterlooville', 'Hampshire', 'PO7 6XE', '', '', NULL, '', '', '', '', '', ''),
(132, '', 'Active', 'Indian Restaurant', 'Royal Balti House', 'Nurul', 'Islam', '01204573515', '', '', '', '', '78 Lower Market Street  Farnworth', '', '', 'Bolton', 'Lancashire', 'BL4 7NY', '', '', NULL, '7 Glenlea Drive', '', 'East Didsbury', 'Manchester', 'Lancashire', 'M20 5QJ                                           '),
(134, '', 'Passive', 'Indian Restaurant', 'The Lodge Restaurant', 'Imam', 'Uddin', '01543483334', '', '', '', '', 'The Lodge Cottage Birmingham Rd', '', '', 'Lichfield', 'staffordshire', 'WS14 0LQ', '', '', NULL, '', '', '', '', '', ''),
(135, '', 'Passive', 'Indian Restaurant', 'Streetly Balti', 'Imam', 'Uddin', '01213532224', '', '', '', '', '188 Chester Rd  Streetly', '', '', 'Sutton Coldfield', '', 'B74 3NA', '', '', NULL, '', '', '', '', '', ''),
(136, '', 'Passive', 'Indian Restaurant', 'Kismet', 'Azad', '', '01912972028', '', '', '', '', '177 Whitley Road', '', '', ' Whitley Bay', '', 'NE26 2DN', '', '', NULL, '', '', '', '', '', ''),
(137, '', 'Passive', 'Indian Restaurant', 'Viceroy Restaurant', 'Mohibur', 'Rahman', '01935421758', '', '07776222717', '', '', '100 Middle Street', '', '', 'Yeovil', 'Somerset', 'BA20 1NE', '', '', NULL, '', '', '', '', '', ''),
(138, '', 'EOT', 'Indian Restaurant', 'New Shapla Tandoori', 'Karim', '', '01923224224', '', '', '', '', '105 Longspring', '', '', 'St. Albans', 'Herdfordshire', 'WD24 6PU', '', '', NULL, '', '', '', '', '', ''),
(139, '', 'Passive', 'Indian Restaurant', 'The Paradise', 'Ali', 'S.', '01730265162', '', '', '', '', '23 Lavant Street', '', '', 'Petersfield', 'Hampshire', 'GU32 3EL', '', '', NULL, '', '', '', '', '', ''),
(141, '', 'EOT', 'Indian Restaurant', 'Millennium', 'Abdul', 'Haye', '01913843672', '', '', '', '', '30 Front Street', '', '', 'Framwellgate Moor', 'Durham', 'DH1 5EE', '', '', NULL, '', '', '', '', '', ''),
(142, '', 'Passive', 'Indian Restaurant', 'Mahabharat', 'Nurul', 'Islam', '01296713611', '', '07968 772425', '', '', '25 Market Square', '', '', 'Winslow', 'Buckinghampshire', 'MK18 3AB', '', '', NULL, '', '', '', '', '', ''),
(143, '', 'Passive', 'Indian Restaurant', 'Bengal Balti', 'Bari', '', '01625 612779', '', '', '', '', '55 Old Park Lane', '', '', 'Macclesfield', 'Cheshire', 'SK11 6TX', '', '', NULL, '', '', '', '', '', ''),
(144, '', 'Passive', 'Indian Restaurant', 'Shapla Tandoori', 'Moklis', 'Miah', '01570 422076', '', '', '', '', '8 College Street', '', '', 'Lampeter', 'Caredigion', 'SA48 7DY', '', '', NULL, '', '', '', '', '', ''),
(145, '', 'Passive', 'Indian Restaurant', 'The Prachee', 'Rob', '', '01617986500', '', '', '', '', '12a Whittaker Lane', '', '', 'Prestwitch', 'Manchester', 'M25 1FX', '', '', NULL, '', '', '', '', '', ''),
(146, '', 'Passive', 'Indian Restaurant', 'The Light of Bengal', 'Sirazul', 'Islam', '01616244600', '', '', '', '', '114 Union Street', '', '', 'Oldham', 'Lancashire', 'OL1 1DU', '', '', NULL, '', '', '', '', '', ''),
(147, '', 'Active', 'Indian Restaurant', 'Zam Zam', 'Ismail', 'Hussain', '01616822220', '', '', '', '', '390 Holinwood Avenue', '', '', 'Manchester', 'Lancashire', 'M40 0JD', '', '', NULL, '', '', '', '', '', ''),
(148, '', 'Passive', 'Indian Restaurant', 'Akash Restaurant', 'Somru', 'Miah', '02088640621', '', '', '', '', '145 Greenford Road', '', '', 'Sudbury Hill', 'Middx', 'HA1 3QN', 'contact: Deip', '', NULL, '', '', '', '', '', ''),
(149, '', 'Passive', 'Indian Restaurant', 'Taj Tandoori', 'Mujib', 'Rahman', '01670819292', '', '07980542059', '', '', '72A station Road', '', '', 'Ashington', 'Northumberland', 'NE63 8RN', 'home & Fax 01670854022', '', NULL, '35 Black Dean', '', '', 'Ashington', 'Northumberland', 'NE63 8TL                                          '),
(150, '', 'Passive', 'Indian Restaurant', 'Miilennium Restaurant', 'Abdul', 'Haye', '01913843672', '', '', '', '', '30 Front Street', '', '', 'Framwellgate Moore', 'Durham', 'DH1 5EE', '', '', NULL, '', '', '', '', '', ''),
(151, '', 'Passive', 'Indian Restaurant', 'Taj Balti', 'Layas', 'Miah', '01267234243', '', '', '', '', '119 Priory Street', '', '', 'Carmarthen', '', 'SA31 1NB', '', '', NULL, '', '', '', '', '', ''),
(152, '', 'Passive', 'Indian Restaurant', 'Bengal Dynasty', 'Monchab', 'Ali', '01244 830455', '', '07966426907', '', '', '104A Chester Road East  Ssotton', '', '', 'Deeside', 'Flintshire', 'CH5 1QD', '', '', NULL, '', '', '', '', '', ''),
(153, '', 'Passive', 'Indian Restaurant', 'Passage to India', 'Jubed', 'Ali', '01517096441', '', '', '', '', '76 Bold Street', '', '', 'Liverpool', '', 'L1 4HR', '', '', NULL, '', '', '', '', '', ''),
(154, '', 'Passive', 'Indian Restaurant', 'Shaan Restaurant', 'Oyes', 'Zaman', '01993779339', '', '', '', '', '12 Corn Street', '', '', 'Witney', 'Oxon', 'OX28 6BL', 'contact: Iqbal', '', NULL, '', '', '', '', '', ''),
(155, '', 'Passive', 'Indian Restaurant', 'Warlingham Tandoori', 'Malek', 'Hussain', '01883622362', '', '07747190875', '', '', '14 The Green', '', '', 'Warlingham', 'Surrey', 'CR6 9NA', '', '', NULL, '', '', '', '', '', ''),
(156, '', 'Passive', 'Indian Restaurant', 'Trishna Tandoori', 'Ali', 'Ali', '01377252905', '', '', '', '', '1 George Street', '', '', 'Driffield', 'Yorkshire', 'YO25 6RA', 'home tel : 01132720996\r\n\r\nnaphew - shalim', '', NULL, 'Nessa Villa', '', 'Gpisy Lane', 'Leeds', '', 'LS11 5TP                                          '),
(157, '', 'Passive', 'Indian Restaurant', 'Bahar Tandoori', 'Bahar', '', '01983200378', '', '', '', '', '44 High Street', '', '', 'Cowes', 'Isle of Wight', 'PO30 7RR', '', '', NULL, '', '', '', '', '', ''),
(158, '', 'Passive', 'Indian Restaurant', 'Mah Mortgage & Finance', 'Habib', 'M A', '02476687952', '', '07930490389', '', '', '522 Stoney Stanton Raod', '', '', 'Coventry', '', 'CV6 5FS', '', '', NULL, '', '', '', '', '', ''),
(159, '', 'Active', 'Indian Restaurant', 'Khas Tandoori', 'Ashraf', 'Rahman', '02077042279', '', '07956367642', '', '', '41 Newington Green Road', '', '', 'London', 'London', 'N1 4QT', '', '', NULL, '55 Canning Road', '', '', 'London', 'London', 'N5 2JR                                            '),
(188, '', 'Passive', 'Indian Restaurant', 'Monsoon Bay', 'Bahar', 'Bahar', '(01895) 431 653', '', '07931558385', '', '', '150 High Street', '', '', 'West Drayton', 'Middlesex', 'UB7 7BD', '', '', NULL, '', '', '', '', '', ''),
(189, '', 'Passive', 'Printing Company', 'Smart Print Ltd', 'Misba', 'Rahman', '020 8838 2747', '020 8838 6009', '', '', '', '189 High Street', '', '', 'London', 'London', 'NW10 4TE', '', '', NULL, '', '', '', '', '', ''),
(190, '', 'Passive', 'Indian Restaurant', 'Tandoori Palace', 'Sikander', 'Sikander', '02085721242', '', '07956368117', '', '', '441 Great West Road', '', '', 'Hounslow', 'Middx', 'TW5  0BY', '', '', NULL, '', '', '', '', '', ''),
(191, '', 'Passive', 'Shops', 'Reina Kurosawa Flowers', 'Re: Jean', 'Garrett', '020 73732528', '', '', '', '', '34 Chesterton Square', '', '', 'London', '', 'W8 6PH', 'referred by jean garrett', '', NULL, '', '', '', '', '', ''),
(192, '', 'Passive', 'Indian Restaurant', 'Spice of India', 'Junu', 'Junu', '02088941010', '02088940919', '07956994504', '', '', '27 High Street', '', '', 'Whitton', 'Middx', 'TW2  7LB', 'branch @ 2Wharf Rd, Ash Vale, Surrey  GU12  5AZ', '', NULL, '', '', '', '', '', ''),
(193, '', 'Passive', 'Indian Restaurant', 'Spice of India', 'Mizan', 'Junu', '01252313638', '', '', '', '', '2 Wharf Road', '', '', 'Ash Vale', 'Surrey', 'GU12 5AZ', 'branch @ 27 High st, Whitton, Middx TW2 7LB', '', NULL, '', '', '', '', '', ''),
(194, '', 'Passive', 'Indian Restaurant', 'Le Cinnamon', 'Jiblu', 'Jiblu', '020 7602 8899', '', '', '', '', '158A  Shepherds Bush Road', '', '', 'London', '', 'W6 7PB', '', '', NULL, '', '', '', '', '', ''),
(196, '', 'Passive', 'Office', 'Transport Network ltd', 'Halyna', 'Khor', '02086069977', '02086069541', '', '', '', '25 Mill House  W P Business Centre', '', '', 'Southall', 'Middx', 'UB2 4NJ', '', '', NULL, '', '', '', '', '', ''),
(197, '', 'Passive', 'Indian Restaurant', 'Claye Chef', 'Khosla', 'khosla', '020 8861 5772', '', '07801803553', '', '', '391 High Road', '', '', 'Harrow', 'Middlesex', 'HA3 6EL', '', '', NULL, '', '', '', '', '', ''),
(198, '', 'Passive', 'Indian Restaurant', 'Anarkali', 'Rafique', 'Rafique', '02087481760', '', '07961095640', '', '', '303 King Street', '', '', 'London', '', 'W6 9NH', '', '', NULL, '', '', '', '', '', ''),
(199, '', 'EOT', 'Indian Restaurant', 'Jaipur Restaurant', 'B', 'Rashid', '02075885043', '02075885045', '07899928560', '', '', '45 London Wall', '', '', 'London', 'London', 'EC2M 5TE', 'branch: Maharaja WC2H', '', NULL, '', '', '', '', '', ''),
(200, '', 'Passive', 'Indian Restaurant', 'Cinnamon Spice', 'Ahmed', 'Khan', '02079350212', '02074868976', '', '', '', '12 / 14 Glentworth Street', '', '', 'London', 'London', 'NW1 5PG', 'branch: Spic of Hampstead mr.khan', '', NULL, '', '', '', '', '', ''),
(201, '', 'Passive', 'Indian Restaurant', 'The Guru Balti', 'Zohir', 'Ahmed', '01483724440', '', '07887641655', '', '', '48 Westfield Road', '', '', 'Old Woking', 'Surrey', 'GU22 9NG', '', '', NULL, '', '', '', '', '', ''),
(202, '', 'Passive', 'Indian Restaurant', 'Essence of India', 'Fozlu', 'Fozlu', '01279441187', '01279434296', '07855779064', '', '', 'Oasis Hotel 2 Hart Road', '', '', 'Old Harlow', 'Essex', 'CM19 0LH', 'Mr.Goufar\r\nAbdul Salam', '', NULL, '140 Arkwright', '', '', 'Harlow', 'Essex', 'CM20 3LZ                                          '),
(203, '', 'Passive', 'Indian Restaurant', 'Ruthin Tandoori', 'Waris', 'Ullah', '01824707666', '', '077734880324', '', '', 'Grosgenor Building Wynnstay Road', '', '', 'Ruthin', 'Denbighshire', 'LL15 1AS', '', '', NULL, '', '', '', '', '', ''),
(205, '', 'Passive', 'Office', 'London Tigers', 'Mesba', 'Ahmed', '02072893395', '02072665281', '07956353615', '', '', '1st floor WECH Communty Centre', '', '', 'London', 'London', 'W9 3RS', '', '', NULL, '', '', '', '', '', ''),
(206, '', 'Passive', 'Office', 'W9 Dental Health', 'Khoury', 'Khoury', '02072896222', '', '', '', '', '176 Shirland Road', '', '', 'London', 'London', 'W9 3JE', '', '', NULL, '', '', '', '', '', ''),
(207, '', 'Passive', 'Indian Restaurant', 'Maida Vale Tandoori', 'Sheraz', 'x', '02072664616', '', '', '', '', '49 Chippenham Road', '', '', 'London', 'London', 'W9 2AH', '', '', NULL, '', '', '', '', '', ''),
(208, '', 'Passive', 'Indian Restaurant', 'Shalimar Garden', 'M', 'Ambiah', '02077069303', '', '07734257858', '', '', '42 gloucester Terrace', '', '', 'London', 'London', 'W2 3DA', '', '', NULL, '', '', '', '', '', ''),
(209, '', 'Passive', 'Thai Restaurant', 'Thai Pagoda', 'Monica', 'Choudhury', '02083989952', '', '07903496775', '', '', '7 Station Approach', '', '', 'Esher', 'Surrey', 'KT10 0SP', 'home tel 01483832434', '', NULL, '', '', '', '', '', ''),
(210, '', 'Passive', 'Builders, Decorators', 'Thomas', 'A', 'Thomas', '02085341602', '', '07812094638', '', '', '53 Vaughan Road', '', '', 'London', 'London', 'E15 4AA', '', '', NULL, '', '', '', '', '', ''),
(211, '', 'Passive', 'Trade Printer', 'HKH Design', 'Khosru', 'K', '02089027576', '02089036388', '07979267585', '', '', '78 Whitton Avenue East', '', '', 'Greenford', 'Middx', 'UB6 0PX', 'Signum delivery : Ruth , Floatin Earth, unit 14\r\n21 Wadswort road, Parevale, UB6 7JD', '', NULL, '', '', '', '', '', ''),
(212, '', 'Passive', 'Office', 'Ryan Estates', 'Imran', 'Akhtar', '02089023902', '02089022249', '', '', '', '2 Neeld Parade', '', '', 'Wembley', 'Middx', 'HA9 6QU', 're: tunji', '', NULL, '', '', '', '', '', ''),
(213, '', 'Passive', 'Office', 'Astl & Co', 'Rahman', 'Rahman', '01582582900', '01582582990', '', '', '', 'Unit 4-6  117 Leagrave Road', '', '', 'Luton', 'Bedfordshire', 'LU3 1JR', '', '', NULL, '', '', '', '', '', ''),
(214, '', 'Passive', 'Indian Restaurant', 'Palace Tandoori', 'Shalim', 'A', '01895431193', '', '07980602895', '', '', '113 Station Road', '', '', 'West Drayton', 'Middx', 'UB7 7LT', '', '', NULL, '', '', '', '', '', ''),
(215, '', 'Passive', 'Indian Restaurant', 'Curry Garden', 'Opuo', 'Choudhury', '01923770622', '', '07736646288', '', '', '141 Uxbridge Road', '', '', 'Rickmansworth', 'Herts', 'WD3 2DW', '108 Morgan Close, Luton LU4 9GN', '', NULL, '', '', '', '', '', ''),
(216, '', 'Passive', 'Indian Restaurant', 'Great Eastern Tandoori', 'Rohim', 'A', '02078283366', '', '', '', '', '30 Winchester Street', '', '', 'London', 'London', 'SW1V 4NE', 'home: 02089628736', '', NULL, '', '', '', '', '', ''),
(217, '', 'Passive', 'Office', 'GPRD', 'Choudhury', 'Jamshed', '02089332951', '02089332952', '07956895399', '', '', '31 Lindsay Drive', '', '', 'Kenton', 'Middx', 'HA3 0TA', '', '', NULL, '', '', '', '', '', ''),
(218, '', 'Passive', 'Builders, Decorators', 'UK Property Maintenance', 'Abou', 'Ziad', '02072661628', '02086707415', '07850808647', '', '', '3 Giles Coppice', '', '', 'London', 'London', 'SE19 1XF', '', '', NULL, '', '', '', '', '', ''),
(219, '', 'Passive', 'Indian Restaurant', 'Millbank Spice', 'Rahim', 'A', '02076301444', '02076308400', '07958042876', '', '', '34- 38 Vauxhall Bridge Road', '', '', 'London', 'London', 'SW1V 2RY', '', '', NULL, '', '', '', '', '', ''),
(220, '', 'Passive', 'Indian Restaurant', 'Pride of India', 'Mohin', 'U', '01438725846', '', '', '', '', '123/125 High Street', '', '', 'Old Stevenage', 'Herts', 'SG1 3HS', '', '', NULL, '', '', '', '', '', ''),
(221, '', 'Passive', 'Indian Restaurant', 'Basmati', 'Luthfur', 'Ullah', '01792641685', '', '07796565000', '', '', '293 Llangyfelach Road', '', '', 'Swanesa', 'Wales', 'SA5 9LG', 'home tel 01792643711', '', NULL, '43 odo Street', '', 'Hafod', 'Swansea', 'Wales', 'SA1 2LS                                           '),
(222, '', 'Passive', 'Indian Restaurant', 'Temptations', 'Adhikari', 'B', '02079355864', '', '07796853494', '', '', '43 chiltern Street', '', '', 'London', 'London', 'W1U 6LS', '', '', NULL, '', '', '', '', '', ''),
(223, '', 'EOT', 'Indian Restaurant', 'Khans', 'Shaheen', 'A', '01895435121', '', '07930957982', '', '', '62 Swan Road', '', '', 'West Drayton', 'Middx', 'UB7 7JZ', '', '', NULL, '', '', '', '', '', ''),
(224, '', 'EOT', 'Indian Restaurant', 'Raj Brasserie', 'Iqbal', 'A', '02089604978', '', '', '', '', '68 ChamberlayneRoad', '', '', 'London', 'London', 'NW10 3JJ', '', '', NULL, '', '', '', '', '', ''),
(225, '', 'Passive', 'Indian Restaurant', 'Shampa', 'Hannan', 'A', '01227272717', '', '07985117306', '', '', '24 High Street', '', '', 'Whistable', 'Kent', 'CT5 1BQ', '', '', NULL, '', '', '', '', '', ''),
(226, '', 'Passive', 'Indian Restaurant', 'Star of India', 'Rukon', 'MD', '01462743062', '', '07961985080', '', '', '68 High Street', '', '', 'Baldock', 'Herts', 'SG7 5NS', '', '', NULL, '', '', '', '', '', ''),
(227, '', 'Passive', 'Indian Restaurant', 'Tandoori Nights', 'Hoque', 'A', '02088440364', '', '07903731575', '', '', '232 Uxbridge Road  Airman Parade', '', '', 'Feltham', 'Middx', 'TW13 5DL', '', '', NULL, '', '', '', '', '', ''),
(228, '', 'Passive', 'Office', 'Bengali Workers Association', 'Rahman', 'S', '02073887313', '02073878731', '', '', '', 'Surma Centre 1 Robert Street', '', '', 'London', 'London', 'NW1 3JU', '', '', NULL, '', '', '', '', '', ''),
(229, '', 'Passive', 'Indian Restaurant', 'Anarkali', 'Mosabir', 'A', '01895420973', '', '', '', '', '107 Station Road', '', '', 'West Drayton', 'Middx', 'UB7 7LT', '', '', NULL, '', '', '', '', '', ''),
(231, '', 'Passive', 'Chinese Restaurant', 'Panda', 'Kalee', 'A', '02089618796', '', '07798517817', '', '', '18 Park Parade', '', '', 'London', 'London', 'NW10 4JH', 'contact  Ms May', '', NULL, '', '', '', '', '', ''),
(232, '', 'Passive', 'Builders, Decorators', 'M M Electrical', 'Mike', 'M', '07860515376', '', '07956256901', '', '', '32 Linden Avenue', '', '', 'London', 'London', 'NW10 5RE', '', '', NULL, '', '', '', '', '', ''),
(233, '', 'Passive', 'Office', 'Al-Jazeera', 'Mohammed', 'Jaber', '02078204837', '02078209831', '07985555432', '', '', 'Westminster Tower 7th Floor', '', '', 'London', 'London', 'SE1 7SL', 'Khurram Nadeem -accounts', '', NULL, '', '', '', '', '', ''),
(234, '', 'Passive', 'Indian Restaurant', 'Shiva', 'Pall', 'Pall', '01608617455', '', '07956094810', '', '', '83 Great Stone Road', '', '', 'Stretford', 'Manchester', 'M32 8 GR', 'N', '', NULL, '', '', '', '', '', ''),
(235, '', 'Passive', 'Office', 'Habibi', 'Nuruddin', 'a', '0209609203', '02089609223', '', '', '', '1c Greyhound road', '', '', 'London', 'London', 'NW10 5QH', '', '', NULL, '', '', '', '', '', ''),
(236, '', 'Passive', 'Indian Restaurant', 'Rajgate Restaurant', 'Misba', 'Rahman', '02076248123', '', '', '', '', '74 Kingsgate Road', '', '', 'London', 'London', 'NW6 4TE', 'pricing as raj nw10', '', NULL, '', '', '', '', '', ''),
(237, '', 'Passive', 'Indian Restaurant', 'Apurba Restaurant', 'Ashraf', 'M', '01664410554', '', '', '', '', '38 Leicester Street', '', '', 'Leicester', 'Leicestershire', 'LE13 0PP', '', '', NULL, '6 Adcock Close', '', 'Milton mowbray', '', 'Leicestershire', 'LE13 1UU                                          '),
(238, '', 'Passive', 'Indian Restaurant', 'Hounslow Brasserie', 'Vipin', 'Nayar', '02085705535', '', '07712579438', '', '', '47/49 Spring Grove Road', '', '', 'Hounslow', 'Middx', 'TW3 4BD', '', '', NULL, '', '', '', '', '', ''),
(239, '', 'Passive', 'Indian Restaurant', 'Kent Curry', 'Abu', 'M', '01322225129', '', '', '', '', '56 Hythe Street', '', '', 'Dartford', 'Kent', 'DA1 1BX', '', '', NULL, '', '', '', '', '', ''),
(240, '', 'Passive', 'Office', 'Adde Telecom ltd', 'Abdi', 'M', '02088381745', '02088381745', '07930477957', '', '', '148 High Street', '', '', 'London', 'London', 'NW10 4SP', '', '', NULL, '', '', '', '', '', ''),
(241, '', 'EOT', 'Indian Restaurant', 'Jewel of Bengal', 'Leil', 'Miah', '02084455213', '', '', '', '', '1-3 Oakleigh Road North', '', '', 'london', 'London', 'N20', '', '', NULL, '', '', '', '', '', ''),
(242, '', 'Passive', 'Office', 'Marylebone Bangladesh Society', 'Mesbah', 'Uddin', '02077247427', '02076169740', '', '', '', '19 Samford Street', '', '', 'London', 'London', 'NW8 8ER', '', '', NULL, '', '', '', '', '', ''),
(243, '', 'Passive', 'Indian Restaurant', 'Shappi Tandoori', 'Syed/Bashir', 'a', '01932771441', '', '07790482618', '', '', '135 Vicarage Road', '', '', 'Sunbury on Thames', 'Middx', 'TW16 7QB', '', '', NULL, '', '', '', '', '', ''),
(244, '', 'Passive', 'Indian Restaurant', 'Blue Lagoon', 'Mr. Ahmed', 'A', '02084201594', '', '', '', '', '5 The Parade Prestwick Rd', '', '', 'South Oxhey', 'Hertfordshire', 'WD19 6ED', '', '', NULL, '', '', '', '', '', ''),
(245, '', 'Passive', 'Indian Restaurant', 'The Akash Restaurant', 'Islam', 'N', '01249653358', '', '', '', '', '19 The Bridge', '', '', 'Chippenham', 'Wiltshire', 'SN15 1HA', '', '', NULL, '', '', '', '', '', ''),
(246, '', 'Passive', 'Indian Restaurant', 'India Garden', 'Sadik', 'Ullah', '01444246141', '', '', '', '', '199 Lower Church Road', '', '', 'Burgess Hill', 'West Sussex', 'RH15 9AA', '', '', NULL, '', '', '', '', '', ''),
(247, '', 'Passive', 'Indian Restaurant', 'Taste Of India', 'Kalu', 'Miah', '01263711484', '', '', '', '', '31 Bull Street', '', '', 'Holt', 'Norfolk', 'NR25 6HP', '', '', NULL, '', '', '', '', '', ''),
(248, '', 'Passive', 'Indian Restaurant', 'Indian Spice', 'Mosabir', 'Mr', '02082082424', '02082081145', '', '', '', '1C Humber Road', '', '', 'Dollis Hill', 'London', 'NW2 6EG', '', '', NULL, '', '', '', '', '', ''),
(249, '', 'Passive', 'Indian Restaurant', 'Morecombe Tandoori', 'Quadir', 'M', '01524832633', '', '', '', '', '47 Marine Road West', '', '', 'Morecombe', 'Lancs', 'LA3 1BZ', '', '', NULL, '7 Hynburn Close', '', '', 'Morecambe', 'Lancashire', 'LA3 3SQ                                           '),
(250, '', 'Passive', 'Indian Restaurant', 'Bengal Spices', 'Anwar', 'A', '01293571007', '', '', '', '', '71 Gales Drive', '', '', 'Crawley', 'West Sussex', 'RH10 1QA', '', '', NULL, '', '', '', '', '', ''),
(251, '', 'Passive', 'Indian Restaurant', 'Mitali Tandoori', 'Lilu', 'Miah', '01254381054', '', '', '', '', '17-19 Warner Street', '', '', 'Accrington', 'Lancs', 'BB5 1HN', '', '', NULL, '', '', '', '', '', ''),
(252, '', 'Passive', 'Indian Restaurant', 'Eastern Spice', 'Syed', 'Mukarram', '01642712661', '', '', '', '', '46 High Street', '', '', 'Stokesley', 'Middlesborough', 'TS9 5AX', '', '', NULL, '', '', '', '', '', ''),
(253, '', 'Passive', 'Indian Restaurant', 'Sitar', 'Atar', 'Miah', '01442242609', '', '', '', '', '218 St Johns Road', '', '', 'Hemel Hempstead', 'Herts', 'HP1 1QQ', '', '', NULL, '', '', '', '', '', ''),
(254, '', 'Passive', 'Indian Restaurant', 'Parkgate Tandoori', 'Miah', 'A', '01489583153', '', '', '', '', '51 Duncan Road', '', '', 'Southampton', 'Hampshire', 'SO31 7BX', '', '', NULL, '', '', '', '', '', ''),
(255, '', 'Passive', 'Indian Restaurant', 'Taj Mahal Restaurant', 'Mubin', 'Chowdhury', '02087690704', '', '', '', '', '11A  Leigham Court  Avenue', '', '', 'Streatham Hill', 'London', 'SW16 2ND', '', '', NULL, '', '', '', '', '', ''),
(256, '', 'Passive', 'Indian Restaurant', 'Taj Tandoori', 'Lokon', 'Miah', '01753521674', '', '', '', '', '7 Upton Lea Parade', '', '', 'Slough', 'Berkshire', 'SL2 5JU', '', '', NULL, '', '', '', '', '', ''),
(257, '', 'Passive', 'Indian Restaurant', 'Bombay Spice', 'Miah', 'Mr', '01438314699', '', '', '', '', '77-79 High Street', '', '', 'Stevenage', 'Hertfordshire', 'SG1 3HR', '', '', NULL, '', '', '', '', '', ''),
(258, '', 'Passive', 'Indian Restaurant', 'Garam Massala', 'Rashid', 'M', '01614884001', '', '', '', '', '18 Station Road', '', '', 'Cheadle Hulme', 'Cheshire', 'SK8 5AE', '', '', NULL, '', '', '', '', '', ''),
(259, '', 'Passive', 'Indian Restaurant', 'Red Rose of Byfleet', 'Miah', 'S', '01932355559', '', '', '', '', '148 High Road', '', '', 'Byfleet', 'Surrey', 'KT14 7RG', '', '', NULL, '', '', '', '', '', ''),
(260, '', 'Passive', 'Indian Restaurant', 'Prince of India', 'Islam', 'Islam', '01303276298', '', '', '', '', '23 Risborough Lane', '', '', 'Folkstone', 'Kent', 'CT19 4JH', '', '', NULL, '', '', '', '', '', ''),
(261, '', 'Passive', 'Indian Restaurant', 'Sultan', 'Naz', 'Naz', '01249655017', '', '', '', '', '5 Bath Road', '', '', 'Melksham', 'Wiltshire', 'SN12 6LL', '', '', NULL, '', '', '', '', '', ''),
(262, '', 'Passive', 'Indian Restaurant', 'Khusboo Balti', 'Jitu', 'Miah', '01902677164', '', '', '', '', '643 Birmingham New Road', '', '', 'Coseley', 'West Midlands', 'WV14 9JL', '', '', NULL, '', '', '', '', '', ''),
(263, '', 'Passive', 'Indian Restaurant', 'New India', 'Paul', 'L', '02085464097', '', '', '', '', '60 Coombe Road', '', '', 'Kiingston', 'Surrey', 'KT2 7AE', '', '', NULL, '', '', '', '', '', ''),
(264, '', 'Passive', 'Indian Restaurant', 'Nabab', 'Jewell', 'Miah', '01983523276', '', '', '', '', '84 Upper St James Street', '', '', 'Newport', 'Isle of Wight', 'PO30 1LQ', '', '', NULL, '', '', '', '', '', ''),
(265, '', 'Passive', 'Indian Restaurant', 'Babur Empire', 'Shaukat', 'Ali', '02074311841', '', '', '', '', '12 Mill Lane', '', '', 'West Hampstead', 'London', 'NW6 1NR', '', '', NULL, '', '', '', '', '', ''),
(266, '', 'Passive', 'Indian Restaurant', 'Taj Mahal', 'Mijanur\r\nMizanur', 'Rahman', '01869246640', '', '', '', '', '8 The Causeway', '', '', 'Bicester', 'Oxfordshire', 'OX6 7AW', '', '', NULL, '', '', '', '', '', ''),
(267, '', 'Passive', 'Indian Restaurant', 'Elizabethan Spice', 'Babul', 'M', '01273495272', '', '', '', '', 'High Street', '', '', 'Henfield', 'West Sussex', 'BN5 9EQ', '', '', NULL, '', '', '', '', '', ''),
(268, '', 'Passive', 'Indian Restaurant', 'Rajputana', 'Lal', 'Miah', '01792895883', '', '', '', '', '44 High Street', '', '', 'Gorseinon', 'Swanesa', 'SA4 4BT', '', '', NULL, '', '', '', '', '', ''),
(269, '', 'Passive', 'Indian Restaurant', 'Mughal Dynasty', 'Chowdhury', 'M', '01622763360', '', '', '', '', '14 London Road', '', '', 'Maidstone', 'Kent', 'ME16 8QL', '', '', NULL, '', '', '', '', '', ''),
(270, '', 'Passive', 'Indian Restaurant', 'Whitley Bay', 'Aftar', 'Hussain', '01912529483', '', '', '', '', '40 Esplanade', '', '', 'Whitley Bay', 'Tyne & Wear', 'NE26 2AE', '', '', NULL, '', '', '', '', '', ''),
(271, '', 'Passive', 'Indian Restaurant', 'Naima', 'Rouf', 'A', '01953456800', '', '', '', '', '11 Exchange Street', '', '', 'Attleborough', 'Norfolk', 'NR17 2AB', '', '', NULL, '', '', '', '', '', ''),
(272, '', 'Passive', 'Indian Restaurant', 'New Bekash', 'Habib', 'Koyes', '01327350056', '', '07919992130', '', '', '100 Walting Street East', '', '', 'Towcester', 'Northamptonshire', 'NN12 7BT', '', '', NULL, '', '', '', '', '', ''),
(273, '', 'Passive', 'Indian Restaurant', 'Strand Tandoori', 'Rashid', 'H', '02074972213', '', '', '', '', '45 Bedford Street', '', '', 'London', 'London', 'WC2 9HA', '', '', NULL, '', '', '', '', '', ''),
(274, '', 'Passive', 'Indian Restaurant', 'Rose of India', 'Mohamed', 'Aslam', '01387248666', '', '', '', '', '50 Bucculuch Street', '', '', 'Dumfries', 'Scotland', 'DD1 2AH', '', '', NULL, '', '', '', '', '', ''),
(275, '', 'Passive', 'Indian Restaurant', 'Jitar Restaurant', 'Janu', 'Miah', '01204572302', '', '', '', '', '31 Market Street', '', '', 'Little Liver', 'Bolton', 'B23 1HH', '', '', NULL, '', '', '', '', '', ''),
(277, '', 'Passive', 'Indian Restaurant', 'Planet  Aroma', 'Helim', 'Helim', '01495225671', '', '', '', '', '67 High Street', '', '', 'Blackwood', 'Gwent', 'NP12 1BA', '', '', NULL, '', '', '', '', '', ''),
(278, '', 'Passive', 'Indian Restaurant', 'India Cottage', 'Kadir', 'Abdul', '01782575448', '', '', '', '', '52 Market Place', '', '', 'Stoke on Trent', 'Staffordshire', 'ST6 4AR', '', '', NULL, '', '', '', '', '', ''),
(279, '', 'Passive', 'Indian Restaurant', 'Taj Mahal Indian Restaurant', 'Bardsley', 'D', '01624674741', '', '', '', '', '3 Esplanade Lane', '', '', 'Douglas', 'Isle of Man', 'IM2 4LP', '', '', NULL, '', '', '', '', '', ''),
(280, '', 'Passive', 'Indian Restaurant', 'Champan', 'Hannan', 'Miah', '01483893684', '', '07771567269', '', '', 'High Street', '', '', 'Guildford', 'Surrey', 'GU5 0HB', '', '', NULL, '', '', '', '', '', ''),
(281, '', 'Passive', 'Indian Restaurant', 'New Shapla Tandoori', 'Rahman', 'A', '01923224224', '', '', '', '', '105 Longspring', '', '', 'North Watford', 'Hertfordshire', 'WD24 6PU', '', '', NULL, '', '', '', '', '', ''),
(282, '', 'Passive', 'Indian Restaurant', 'Dalchini', 'Enamul', 'Mumin', '01708739292', '', '', '', '', '206 Main Road', '', '', 'Gidea Park', 'Essex', 'RM2 5HA', '', '', NULL, '', '', '', '', '', '');
INSERT INTO `customer` (`customer_id`, `reg_date`, `customer_status`, `business_type`, `business_name`, `first_name`, `surname`, `telephone`, `fax`, `mobile`, `email`, `web`, `address1`, `address2`, `area`, `city`, `county`, `postcode`, `comments`, `alternate_address`, `status`, `delivery_address1`, `delivery_address2`, `delivery_area`, `delivery_city`, `delivery_county`, `delivery_postcode`) VALUES
(283, '', 'Passive', 'Indian Restaurant', 'The Royal Cuisine', 'Ajmal', 'Hussain', '01892540863', '', '07737752074', '', '', '111 St Johns Road', '', '', 'Tunbridge Wells', 'Kent', 'TN4 9TU', '', '', NULL, '111a St Johns Road', '', '', 'Tunbridge Wells', 'Kent', 'TN4 9TU                                           '),
(284, '', 'Passive', 'Indian Restaurant', 'Nawab Balti', 'Ali', 'Ali', '01613689917', '', '', '', '', '160-164 Market Street', '', '', 'Hyde', 'Manchester', 'SK14 1EX', '', '', NULL, '', '', '', '', '', ''),
(285, '', 'Passive', 'Indian Restaurant', 'Spicy Hut', 'Shabir', 'Shabir', '01612486200', '', '', '', '', '35 Wilmslow Road', '', '', 'Rusholme', 'Manchester', 'M14 5TB', '', '', NULL, '', '', '', '', '', ''),
(286, '', 'Passive', 'Shops', 'Fiqi Halal', 'Abdishik', 'a', '02089611763', '', '07960917096', '', '', '121 High Street', '', '', 'Harlesden', 'London', 'NW10 4TR', '', '', NULL, '', '', '', '', '', ''),
(287, '', 'Passive', 'Indian Restaurant', 'Abbey Tandoori', 'Hussain', 'A', '0958696434', '', '', '', '', '38 Jubilee Road', '', '', 'Gosforth', 'Newcastle-upon-Tyne', 'NE3 3UR', '', '', NULL, '', '', '', '', '', ''),
(288, '', 'Passive', 'Indian Restaurant', 'Motihara', 'Khan', 'Khan', '01534619130', '', '', '', '', '37 Lanmotte Street', '', '', 'St. Hellier', 'Jersey (CI)', 'JE2 4SJ', '', '', NULL, '', '', '', '', '', ''),
(289, '', 'EOT', 'Indian Restaurant', 'Manzil', 'Ali', 'Ali', '01428607272', '', '', '', '', 'Head Alley Road', '', '', 'Hindhead', 'Surrey', 'GU26 6LD', '', '', NULL, '', '', '', '', '', ''),
(290, '', 'Passive', 'Indian Restaurant', 'Zarin', 'Kapoor', 'x', '02088319016', '', '07961850647', '', '', '100 Harington Road West', '', '', 'Feltham', 'Middx', 'TW14 0JJ', '', '', NULL, '', '', '', '', '', ''),
(291, '', 'Passive', 'Indian Restaurant', 'Shuhel', 'Jetu', 'Jetu', '01619470658', '', '', '', '', '48 Old Church Street', '', '', 'Newton Heath', 'Manchester', 'M40 21F', '', '', NULL, '', '', '', '', '', ''),
(292, '', 'Passive', 'Indian Restaurant', 'Herb & Spice', 'Ahmed', 'Ahmed', '01613435961', '', '', '', '', '99 Foudry Street', '', '', 'Duckinfield', 'Cheshire', 'SK15 5PN', '', '', NULL, '', '', '', '', '', ''),
(293, '', 'EOT', 'Indian Restaurant', 'The Spice', 'Ahmed', 'Ahmed', '02085467524', '', '', '', '', '168 London Road', '', '', 'Kingston-upon-Thames', 'Surrey', 'KT2 6QW', '', '', NULL, '', '', '', '', '', ''),
(294, '', 'Passive', 'Indian Restaurant', 'Woodhurst Tandoori', 'Islam', 'Islam', '02083616879', '', '', '', '', '219 Woodhurst Road', '', '', 'Fiern Barnet', 'London', 'N12 9BE', '', '', NULL, '', '', '', '', '', ''),
(295, '', 'Passive', 'Other (supplier)', 'UK Premium Foods', 'Ali', 'Ahmed', '01222455588', '', '', '', '', 'Unit 16', '', '', 'Dyndill St.', 'Cardiff', 'CF10 4BG', '', '', NULL, '', '', '', '', '', ''),
(296, '', 'EOT', 'Indian Restaurant', 'Chillies', 'Ali', 'Ali', '01992636841', '', '', '', '', '1 Manocroft Parade', '', '', 'Cheshunt', 'Hertfordshire', 'EN8 9LP', '', '', NULL, '', '', '', '', '', ''),
(297, '', 'Passive', 'Indian Restaurant', 'Vinodan Tandoori', 'Uddin', 'Ala', '01303265834', '', '', '', '', '74 High Street', '', '', 'Hythe', 'Kent', 'CT21 5AL', '', '', NULL, '', '', '', '', '', ''),
(298, '', 'Passive', 'Indian Restaurant', 'Naseeb', 'Patel', 'Abdul', '01772562082', '', '', '', '', '121 Church Street', '', '', 'Preston', 'Lancshire', 'PR1 3BT', '', '', NULL, '', '', '', '', '', ''),
(299, '', 'Passive', 'Indian Restaurant', 'Khyber Pass', 'Jalal', 'Jalal', '01483797509', '', '', '', '', '12 Lower Guilford Road', '', '', 'Knaphill', 'Surrey', 'GU21 2EG', '', '', NULL, '', '', '', '', '', ''),
(300, '', 'Passive', 'Indian Restaurant', 'Viceroy', 'Rahman', 'Rahman', '02086503445', '', '', '', '', '49 Beckenham Road', '', '', 'Beckenham', 'Kent', 'BR3 4PR', '', '', NULL, '', '', '', '', '', ''),
(301, '', 'Passive', 'shops', 'Pauli Frames', 'Ricado', 'A', '02089613023', '02089613023', '07930375334', '', '', 'unit14 Acton Business centre', '', '', 'London', 'London', 'NW10 6TD', '', '', NULL, '', '', '', '', '', ''),
(302, '', 'Passive', 'Indian Restaurant', 'Hussain Brothers', 'Somru', 'Miah', '01616272210', '', '', '', '', '251 Featherstall Road', '', '', 'Oldham', 'Lancashire', 'OL1 2NG', '', '', NULL, '', '', '', '', '', ''),
(303, '', 'Passive', 'Indian Restaurant', 'Taste of Bengal', 'Manager', 'Manager', '01513469676', '', '', '', '', '59 Seaview Road', '', '', 'Liverpool', 'Merseyside', 'CH45 4QW', '', '', NULL, '', '', '', '', '', ''),
(304, '', 'Passive', 'Indian Restaurant', 'Nazmins Balti', 'Miah', 'Miah', '02089462219', '', '', '', '', '398 Gareth Lane', '', '', 'London', 'London', 'SW18 4HB', '', '', NULL, '', '', '', '', '', ''),
(305, '', 'Passive', 'Indian Restaurant', 'Alban Tandoori', 'Shahid', 'Choudhury', '01727862111', '', '', '', '', '145 Victoria Street', '', '', 'St Albans', 'Hertfordshire', 'AL1 3TA', '', '', NULL, '', '', '', '', '', ''),
(306, '', 'Passive', 'Indian Restaurant', 'Ginger', 'Manager', 'Manager', '02079081990', '', '', '', '', '115 Westbourne Grove', '', '', 'London', 'London', 'W2 4UP', '', '', NULL, '', '', '', '', '', ''),
(309, '', 'Passive', 'Indian Restaurant', 'Jaipur', 'Iqbal', 'Iqbal', '02089467954', '', '', '', '', '403 Durnsford Road', '', '', 'Wimbledon Park', 'London', 'SW19 8EE', '', '', NULL, '', '', '', '', '', ''),
(310, '', 'Passive', 'Indian Restaurant', 'Shahe', 'Humayun', 'Chowdhury', '01912851801', '', '07961450254', '', '', '30 Wansbeck Road South', '', '', 'Newcastle-upon Tyne', 'Tyne & Wear', 'NE3 3HQ', '', '', NULL, '3 Windsor Court', '', 'Kingston Park', 'Newcastle Upon Tyne', 'Tyne and Wear', 'NE3 2YJ                                           '),
(311, '', 'Passive', 'Indian Restaurant', 'Ruislip Tandoori', 'Manager', 'Manager', '01895632859', '', '', '', '', '115 High Street', '', '', 'Ruislip', 'Midlesex', 'HA4 8JN', '', '', NULL, '', '', '', '', '', ''),
(312, '', 'Passive', 'Indian Restaurant', 'Khan restaurant', 'Khan', 'Khan', '01642817457', '', '', '', '', '349 Linthorp Road', '', '', 'Middlesborough', 'Cleveland', 'TS5 9AB', '', '', NULL, '', '', '', '', '', ''),
(313, '', 'EOT', 'Other (finance)', 'City Link Limited', 'Miah', 'S', '02073753111', '', '', '', '', '68 Bricklane First Floor', '', '', 'London', 'London', 'E1 6RL', '', '', NULL, '', '', '', '', '', ''),
(314, '', 'Passive', 'Other (printing)', 'RK Graphics', 'Jim', 'Sagar', '02087955080', '', '', '', '', 'Trinity House', '', '', 'Wembley', 'Middlesex', 'HA0 1SU', '', '', NULL, '', '', '', '', '', ''),
(315, '', 'Passive', 'Other (laundry)', 'Steamline Laundry', 'Manager', 'Manager', '02084524546', '', '', '', '', '40 Waterloo Road', '', '', 'Staples Corner', 'London', 'NW2 7UH', '', '', NULL, '', '', '', '', '', ''),
(316, '', 'Passive', 'Indian Restaurant', 'Pabna', 'Ali', 'Ali', '01782271464', '', '', '', '', '10 Thorndyke Street', '', '', 'Stoke on trent', 'Staffordshire', 'ST1 4PP', '', '', NULL, '', '', '', '', '', ''),
(317, '', 'Passive', 'Indian Restaurant', 'Al Mina', 'Ali', 'Ali', '01213603903', '', '', '', '', '140 Becon Road', '', '', 'Birmingham', 'West Midlands', 'B43 7BN', '', '', NULL, '', '', '', '', '', ''),
(318, '', 'Passive', 'Indian Restaurant', 'Mizan Balti', 'Ahmed', 'Ahmed', '01213585999', '', '', '', '', '611 Walshall Road', '', '', 'Birmingham', 'West Midlands', 'B42 1EH', '', '', NULL, '', '', '', '', '', ''),
(319, '', 'Passive', 'Indian Restaurant', 'Supper Style', 'Nazim', 'Aslam', '07989580493', '', '', '', '', '26 Hardwick Street', '', '', 'Ashton Under Lyne', 'Lancashire', 'OL7 OAJ', '', '', NULL, '', '', '', '', '', ''),
(320, '', 'Passive', 'Indian Restaurant', 'Shapla', 'Don', 'Don', '01616256004', '', '', '', '', '157 Waterloo Street', '', '', 'Oldham', 'Lancashire', 'OL4 1EN', '', '', NULL, '', '', '', '', '', ''),
(321, '', 'Passive', 'Indian Restaurant', 'Taj', 'Taj', 'Mohamed', '01202603060', '', '', '', '', '218 Wareham Road', '', '', 'Wimbourne', 'Dorset', 'BH21 3LN', '', '', NULL, '', '', '', '', '', ''),
(322, '', 'Passive', 'Indian Restaurant', 'Chester Tandoori', 'Noor', 'Noor', '01244347410', '', '', '', '', '39-45 Brook Street', '', '', 'Chester', 'Cheshire', 'CH1 3DZ', '', '', NULL, '', '', '', '', '', ''),
(323, '', 'Passive', 'Indian Restaurant', 'India Chef', 'Abdul', 'Noor', '011426660552', '', '', '', '', '176 Crookes', '', '', 'Sheffield', 'South Yorkshire', 'S10 1UH', '', '', NULL, '', '', '', '', '', ''),
(326, '', 'Passive', 'Indian Restaurant', 'Bombay Kitchen', 'Abid', 'Manager', '02084511114', '', '', '', '', '185 Church Road', '', '', 'London', 'London', 'NW10 9EE', '', '', NULL, '', '', '', '', '', ''),
(327, '', 'Passive', 'Indian Restaurant', 'Curry Centre', 'Mustakim', 'Mustakim', '02089521883', '', '', '', '', '13 The Quadrant', '', '', 'Edgware', 'Middlesex', 'HA8 7LU', '', '', NULL, '', '', '', '', '', ''),
(328, '', 'Passive', 'Other (supplier)', 'Taj Tandoori', 'Manager', 'Manager', '02086468661', '', '', '', '', '259 London Road', '', '', 'Mitcham', 'Surrey', 'CR4 3NH', '', '', NULL, '', '', '', '', '', ''),
(329, '', 'Passive', 'Indian Restaurant', 'Spice Garden', 'Shah', 'Hussain', '02089925131', '', '', '', '', '47 Acton High Street', '', '', 'London', 'London', 'W3 6ND', '', '', NULL, '', '', '', '', '', ''),
(330, '', 'Passive', 'Other (travel agent)', 'High Class', 'Bobby', 'Bobby', '02089331000', '', '', '', '', '246 High Street', '', '', 'Harlesden', 'London', 'NW10 4TD', 'contact  Paul', '', NULL, '', '', '', '', '', ''),
(331, '', 'EOT', 'Indian Restaurant', 'Raja Tandoori', 'Ashraf', 'Ashraf', '01476567294', '', '', '', '', '7 Westgate', '', '', 'Grantham', 'Lincolnshire', 'NG31 6LG', '', '', NULL, '', '', '', '', '', ''),
(332, '', 'Passive', 'Other (specify)', 'Tribal Gathering', 'Bryan', 'Reeves', '02072216650', '', '', '', '', '1 Westbourne Grove Mews', '', '', 'Notting Hill', 'London', 'W11 2RU', '', '', NULL, '', '', '', '', '', ''),
(333, '', 'Passive', 'Indian Restaurant', 'Greenford Tandoori', 'Mohibur', 'Rahman', '02085783505', '', '07939251366', '', '', '311 Ruislip Road East', '', '', 'Greenford', 'Middlesex', 'UB6 9RL', 're: Durbar (Price list)', '', NULL, '', '', '', '', '', ''),
(334, '', 'Passive', 'Indian Restaurant', 'Ganges', 'Ahmed', 'Ahmed', '01825713287', '', '', '', '', '22 High Street', '', '', 'Nutley', 'East Sussex', 'TN22 3NG', '', '', NULL, '', '', '', '', '', ''),
(335, '', 'Passive', 'Chinese Restaurant', 'Garden Chinese', 'Tang', 'Tang', '02089681333', '', '', '', '', 'Unit 1 Westmoorland House', '', '', '80 Scrubs Lane', 'London', 'NW10 6RE', '', '', NULL, '', '', '', '', '', ''),
(336, '', 'Passive', 'Indian Restaurant', 'Moonlight', 'Manager', 'Manager', '01488685252', '', '', '', '', '43/43A High Street', '', '', 'Hungerford', 'Berkshire', 'RG17 0NE', '', '', NULL, '', '', '', '', '', ''),
(337, '', 'Passive', 'Indian Restaurant', 'Royal Club', 'Tappha', 'Tappha', '02085783268', '', '', '', '', '116-118 Ruislip Road', '', '', 'Greenford', 'Middlesex', 'UB6 9RU', '', '', NULL, '', '', '', '', '', ''),
(338, '', 'Passive', 'Other (medical)', 'Queens', 'Mohamed', 'Mohamed', '02072292900', '', '', '', '', '44 Queensway', '', '', 'London', 'London', 'W2 3RS', '', '', NULL, '', '', '', '', '', ''),
(339, '', 'Passive', 'Caffee', 'The Cedar', 'George', 'George', '02089642011', '', '', '', '', '65 Fernhead Road', '', '', 'Maida Vale', 'London', 'W9 3EY', '', '', NULL, '', '', '', '', '', ''),
(340, '', 'Passive', 'Other (specify)', 'Concise Ltd', 'Emily', 'Emily', '02089644446', '', '', '', '', 'Unit 32 Pall Mall Deposite', '', '', '124 Barlby Road', 'London', 'W10 6BL', '', '', NULL, '', '', '', '', '', ''),
(342, '', 'Passive', 'Other (specify)', 'R T Studio', 'Robert', 'Klimek', '02088381785', '', '07712175425', '', '', '16 Tubbs Road', '', '', 'Harlesden', 'London', 'NW10 4RE', '', '', NULL, '', '', '', '', '', ''),
(343, '', 'Passive', 'Indian Restaurant', 'Jewel in the Crown', 'Abdul', 'Muhit', '01387264183', '', '0411279118', '', '', '50 St Micheal Street', '', '', 'Dumfries', 'Dumfries & Galloway', 'DG1 2QF', '', '', NULL, '', '', '', '', '', ''),
(344, '', 'Passive', 'Indian Restaurant', 'Polash', 'Manager', 'Manager', '01758613884', '', '', '', '', '28 Penlan Street', '', '', 'Pwllheli', 'Gwynedd', 'LL53 5DE', '', '', NULL, '', '', '', '', '', ''),
(345, '', 'Passive', 'Indian Restaurant', 'Saffron Balti House', 'Joynal', 'Joynal', '01914566098', '01914562519', '', '', '', '86 Ocean Road', '', '', 'South Shields', 'Tyne and Wear', 'NE33 2JD', '', '', NULL, '', '', '', '', '', ''),
(346, '', 'Passive', 'Indian Restaurant', 'Taj Mahal', 'Faruk', 'Bhuiya', '01189894237', '', '', '', '', '25 Denmark Street', '', '', 'Wokingham', 'Berkshire', 'RG40 2AY', '', '', NULL, '', '', '', '', '', ''),
(347, '', 'Passive', 'Indian Restaurant', 'Rangamati', 'Faruk', 'B', '01189774526', '', '', '', '', '424 Reading Road', '', '', 'Wokingham', 'Berkshire', 'RG41 5EP', '', '', NULL, '', '', '', '', '', ''),
(348, '', 'Passive', 'Indian Restaurant', 'The Polash', 'Khalique', 'A', '01702294721', '', '', '', '', '84-86 West Road', '', '', 'Shoeburyness', 'Essex', 'SS3 9DS', '', '', NULL, '', '', '', '', '', ''),
(349, '', 'Passive', 'Indian Restaurant', 'Polash', 'Khalique', 'A', '01621782233', '', '', '', '', '169 Station Road', '', '', 'Burnham-on-Crouch', 'Essex', 'CM0 8HJ', '', '', NULL, '', '', '', '', '', ''),
(350, '', 'Passive', 'Indian Restaurant', 'Raj of India', 'Manju', 'Miah', '01375377993', '', '', '', '', '9 Orset Road', '', '', 'Grays', 'Essex', 'RM17 5DD', '', '', NULL, '', '', '', '', '', ''),
(351, '', 'Passive', 'Indian Restaurant', 'Sylhet Food Store', 'Aziz', 'Aziz', '01274744295', '01274744295', '', '', '', '2 Mavis Street', '', '', 'Bradford', 'West Yorkshire', 'BD3 9DR', '', '', NULL, '', '', '', '', '', ''),
(352, '', 'Passive', 'Indian Restaurant', 'Taste of Bengal', 'Aziz', 'Aziz', '01274618308', '', '', '', '', '79 Bradford Road', '', '', 'Bradford', 'West Yorkshire', 'BD10 9LB', '', '', NULL, '', '', '', '', '', ''),
(353, '', 'Passive', 'Indian Restaurant', 'Moghul Restaurant', 'Aziz', 'Aziz', '01132590530', '', '', '', '', '8/9 The Green', '', '', 'Horsforth, Leeds', 'West Yorkshire', 'LS18 5JB', '', '', NULL, '', '', '', '', '', ''),
(354, '', 'Passive', 'Indian Restaurant', 'Mogul Balti', 'Aziz', 'Aziz', '01943864403', '', '', '', '', '111 Main Street', '', '', 'Ilkley', 'West Yorkshire', 'LS29 7JN', '', '', NULL, '', '', '', '', '', ''),
(355, '', 'Passive', 'Indian Restaurant', 'New Bilash', 'Shuheb', 'Shuheb', '01582766474', '', '', '', '', '2 Grove Road', '', '', 'Harpenden', 'Hertfordshire', 'AL5 1PX', '', '', NULL, '', '', '', '', '', ''),
(356, '', 'Passive', 'Indian Restaurant', 'Royal Spice', 'Chowdhury', 'A', '01443773143', '', '', '', '', '136 Bute Street', '', '', 'Theorchy', 'Mid Glamorgan', 'CF42 6BB', '', '', NULL, '', '', '', '', '', ''),
(357, '', 'Passive', 'Indian Restaurant', 'Sissinghurst Tandoori', 'Rahman', 'Rahman', '01580712894', '', '', '', '', 'the Street', '', '', 'Cranbrook', 'Kent', 'TN17 2JJ', '', '', NULL, '', '', '', '', '', ''),
(358, '', 'Passive', 'Indian Restaurant', 'Raj of India', 'Jalal', 'Uddin', '01703236996', '', '', '', '', '186 Bitterne Road', '', '', 'Southampton', 'Hampshire', 'SO18 1BE', '', '', NULL, '', '', '', '', '', ''),
(359, '', 'Passive', 'Indian Restaurant', 'Raj of India', 'Sarwar', 'Ahmed', '01322341528', '', '', '', '', '294 Erith Road', '', '', 'Bexleyheath', 'Kent', 'DA7 6HJ', '', '', NULL, '', '', '', '', '', ''),
(360, '', 'Passive', 'Indian Restaurant', 'The Meghna', 'Fozor', 'Ali', '01604409464', '', '', '', '', '55 Dairyground Road', '', '', 'Bramhall', 'Cheshire', 'SK7 2QW', '', '', NULL, '', '', '', '', '', ''),
(361, '', 'Passive', 'Indian Restaurant', 'Royal Tandoori', 'Manager', 'Manager', '02089040011', '', '', '', '', '769 Harrow Road', '', '', 'Sudbury', 'Middlesex', 'HA0 2LW', '', '', NULL, '', '', '', '', '', ''),
(362, '', 'Passive', 'Indian Restaurant', 'Diss Tandoori', 'Jalal', 'A', '01379651685', '', '07833961636', '', '', '1-3 Shelfanger Road', '', '', 'Diss', 'Norfolk', 'IP22 4EH', '', '', NULL, '', '', '', '', '', ''),
(363, '', 'Passive', 'Shop', 'The Pretex', 'Jim', 'Jim', '02089656047', '', '07984916120', '', '', '3 High Street', '', '', 'London', 'London', 'NW10 4NA', '', '', NULL, '', '', '', '', '', ''),
(364, '', 'Passive', 'Indian Restaurant', 'Spice Hut', 'Salam /Kader', 'Salam', '01727864338', '', '', '', '', '9/10 The Quadrant Sherwood Road', '', '', 'St. Albans', 'Hartfordshire', 'AL4 9RA', '', '', NULL, '', '', '', '', '', ''),
(365, '', 'Passive', 'Indian Restaurant', 'Bridlington Tandoori', 'Harun', 'Rashid', '01262400014', '', '', '', '', '124 St Johns Street', '', '', 'Bridlington', 'Humberside', 'YO16 5JS', '', '', NULL, '', '', '', '', '', ''),
(366, '', 'Passive', 'Indian Restaurant', 'Curry Inn', 'Rahim', 'A', '02088920879', '', '', '', '', '34 Crown Road', '', '', 'Twickenham', ' Greater London', 'TW1 3EH', '', '', NULL, '', '', '', '', '', ''),
(367, '', 'EOT', 'Indian Restaurant', 'Spice Valley', 'Manager', 'Manager', '02083408404', '', '', '', '', '25 Aylmer Parade', '', '', 'London', 'Greater London', 'N2 0PE', '', '', NULL, '', '', '', '', '', ''),
(368, '', 'Passive', 'Indian Restaurant', 'Morley Indian Take Away', 'Manager', 'Manager', '02074018685', '', '', '', '', '69 Morley Street', '', '', 'London', 'Greater London', 'SE1 7QZ', '', '', NULL, '', '', '', '', '', ''),
(369, '', 'Passive', 'Indian Restaurant', 'Jhopri', 'Manager', 'Manager', '02082450090', '', '', '', '', '25 The Broadway', '', '', 'Souhtgate', 'London', 'N14 6PH', '', '', NULL, '', '', '', '', '', ''),
(370, '', 'Passive', 'Office', 'Accident Claims', 'Manager', 'Manager', '0208896123', '', '', '', '', '67 Churchfield Road', '', '', 'Acton', 'London', 'W3 6AX', '', '', NULL, '', '', '', '', '', ''),
(371, '', 'Passive', 'Indian Restaurant', 'Boys Café', 'Manager', 'Manager', '02089699132', '', '', '', '', '615 Harrow Road', '', '', 'London', 'Greater London', 'W10 4RA', '', '', NULL, '', '', '', '', '', ''),
(372, '', 'Passive', 'Indian Restaurant', 'Bali', 'Manager', 'Manager', '02072629100', '', '', '', '', '101 Edgware Road', '', '', 'Marble Arch', 'London', 'W2 2HX', '', '', NULL, '', '', '', '', '', ''),
(373, '', 'EOT', 'Indian Restaurant', 'Spice House', 'Manager', 'Manager', '02089604137', '', '', '', '', '9 Kilburn Lane', '', '', 'London', 'Greater London', 'W10 4AE', '', '', NULL, '', '', '', '', '', ''),
(374, '', 'Passive', 'Indian Restaurant', 'Tikka Massala', 'Manager', 'x', '02082028999', '', '', '', '', '198 The Broadway', '', '', 'London', 'London', 'NW9 7EE', '', '', NULL, '', '', '', '', '', ''),
(375, '', 'Passive', 'Indian Restaurant', 'Neasden Tandoori', 'Manager', 'Manager', '02084500201', '', '', '', '', '286 Neasden Lane', '', '', 'Neasden', 'London', 'NW10', '', '', NULL, '', '', '', '', '', ''),
(376, '', 'Passive', 'Indian Restaurant', 'Curry House', 'Rahman', 'Rahman', '01704550302', '', '', '', '', '3 Crown Buildings', '', '', 'Birkdale', 'Lancashire', 'PR8 3BY', '', '', NULL, '', '', '', '', '', ''),
(377, '', 'Passive', 'Indian Restaurant', 'Millon', 'Manager', 'Manager', '01616206445', '', '', '', '', 'Westwood Buisiness Centre', '', '', 'Oldham', 'Lancashire', 'OL9 6HN', '', '', NULL, '', '', '', '', '', ''),
(378, '', 'EOT', 'Indian Restaurant', 'Cafe Taj', 'Manager', 'Manager', '02084280003', '', '', '', '', '26 Hallowes Crecsent', '', '', 'South Oxhey', 'Hertfordshire', 'WD1 6NT', '', '', NULL, '', '', '', '', '', ''),
(379, '', 'Passive', 'Indian Restaurant', 'Taj Of India', 'Miah', 'Miah', '02083684249', '', '', '', '', '347 Bowes Road', '', '', 'Arnos Grove', 'London', 'N11 1AA', '', '', NULL, '', '', '', '', '', ''),
(380, '', 'Passive', 'Indian Restaurant', 'Alpona', 'Manager', 'Manager', '02083988797', '', '', '', '', '2  Summer Road', '', '', 'Thames Ditton', 'Surrey', 'KT7 0QQ', '', '', NULL, '', '', '', '', '', ''),
(381, '', 'Passive', 'Indian Restaurant', 'Spice Of India', 'Surath', 'Ali', '01279424162', '', '', '', '', '6 Cecil Court', '', '', 'Harlow', 'Essex', 'CM18 7QR', 'Azor Ali home tel 01279 450747\r\n5 Pittmans Field, Harlow  CM20 3LA', '', NULL, '5 pittman field', '', '', 'Harlow', 'Essex', 'CM20 3LA                                          '),
(382, '', 'EOT', 'Indian Restaurant', 'L. M Tandoori', 'Manager', 'Manager', '02087429163', '', '', '', '', '242 Goldhawk Roaad', '', '', 'Shepherds Bush', 'London', 'W12 9PE', '', '', NULL, '', '', '', '', '', ''),
(383, '', 'Passive', 'Indian Restaurant', 'Kathmundu Valley', 'Kandel', 'N', '02088710240', '', '', '', '', '5 West Hill', '', '', 'Wandsworth', 'London', 'SW18 1RB', '', '', NULL, '', '', '', '', '', ''),
(384, '', 'Passive', 'Indian Restaurant', 'Jewel in the Crown', 'Asab', 'Ali', '012392827787', '', '', '', '', '60 Osborn Road', '', '', 'Portsmouth', 'Hampshire', 'PO5 3LU', '', '', NULL, '', '', '', '', '', ''),
(385, '', 'Passive', 'Indian Restaurant', 'White Castle', 'Rahman', 'Rahman', '02087865801', '', '', '', '', '234-236 Chessington Road', '', '', 'West Ewell', 'Surrey', 'KT19 9XF', '', '', NULL, '', '', '', '', '', ''),
(386, '', 'Passive', 'Indian Restaurant', 'Shah Jahan', 'Ahmed', 'Ahmed', '0152945313', '', '07074342245', '', '', '19 Market Place', '', '', 'Sleaford', 'Lincolnshire', 'NG34 7SR', '', '', NULL, '', '', '', '', '', ''),
(387, '', 'Passive', 'Indian Restaurant', 'Onamika', 'Samad', 'Samad', '01322347733', '', '', '', '', '294 Erith  Road', '', '', 'Barn hurst', 'Kent', 'DA7 6HJ', '', '', NULL, '', '', '', '', '', ''),
(388, '', 'Passive', 'Indian Restaurant', 'Lal Qila', 'Shahed', 'Chowdhury', '01670734262', '', '', '', '', 'Dudley Lane', '', '', 'Cramlington', 'Northumberland', 'NE223 6UW', '', '', NULL, '', '', '', '', '', ''),
(389, '', 'Passive', 'Indian Restaurant', 'The Royal Indian', 'Mokbul', 'Ali', '01189780179', '', '07710653133', '', '', '72 Peach Street', '', '', 'Wokingham', 'Berkshire', 'RG40 1XH', '', '', NULL, '', '', '', '', '', ''),
(390, '', 'Passive', 'Indian Restaurant', 'Adil Restaurant', 'Rashid', 'Rashid', '01214490335', '', '', '', '', '148-150 Stoney Lane', '', '', 'Sparkbrooke', 'Warwickshire', 'B12 8AJ', '', '', NULL, '', '', '', '', '', ''),
(391, '', 'Passive', 'Indian Restaurant', 'Royal Bengal Tandoori', 'Uddin', 'S', '01536510747', '', '', '', '', '64 Rockingham Road', '', '', 'Kettering', 'Northamptonshire', 'NN16 8JT', '', '', NULL, '', '', '', '', '', ''),
(392, '', 'Passive', 'Indian Restaurant', 'New Tripti', 'Joynal', 'Joynal', '02088529891', '', '07957390888', '', '', '318 Lee High Road', '', '', 'London', 'London', 'SE15 5PJ', 'Home tel 01923820967', '', NULL, '', '', '', '', '', ''),
(393, '', 'Passive', 'Indian Restaurant', 'New Diwanee Khas', 'Anwar', 'Miah', '0127623500', '', '', '', '', '413 London Road', '', '', 'Camberly', 'Surrey', 'GU15 3HZ', '', '', NULL, '', '', '', '', '', ''),
(394, '', 'Passive', 'Indian Restaurant', 'Laboni', 'Chowdhury', 'Chowdhury', '01263821120', '', '07930561791', '', '', '40 Cromer Road', '', '', 'Sheringham', 'Norfolk', 'NR26 8RR', '', '', NULL, '', '', '', '', '', ''),
(395, '', 'Passive', 'Indian Restaurant', 'Standard', 'Jahangir', 'Jahangir', '01243376669', '', '', '', '', '45 High Street', '', '', 'Emsworth', 'Hampshire', 'PO10 7AL', '', '', NULL, '', '', '', '', '', ''),
(396, '', 'Passive', 'Indian Restaurant', 'Goa Balti', 'Hussain', 'Hussain', '01264850850', '', '', '', '', 'Everleigh Filling Station', '', '', 'Marlborough', 'Wiltshire', 'SN8 3EY', '', '', NULL, '', '', '', '', '', ''),
(397, '', 'Passive', 'Indian Restaurant', 'Tiffins Tandoori', 'Manju', 'Miah', '01865372245', '', '07919274383', '', '', '63 High Street', '', '', 'Kidlington', 'Oxfordshire', 'OX5 2DN', '', '', NULL, '', '', '', '', '', ''),
(398, '', 'Passive', 'Indian Restaurant', 'Tabak Restaurant', 'Harun', 'Khan', '01782412121', '', '07976058443', '', '', '29 Liverpool Road', '', '', 'Stoke on Trent', 'Staffordshire', 'ST4 1AW', '', '', NULL, '', '', '', '', '', ''),
(399, '', 'Passive', 'Indian Restaurant', 'Guru Tandoori', 'Miah', 'Miah', '02083460306', '', '', '', '', '12 Long Lane', '', '', 'London', 'London', 'N3 2PT', '', '', NULL, '', '', '', '', '', ''),
(400, '', 'Passive', 'Indian Restaurant', 'Lal Vagh', 'Kabul', 'Kabul', '01543262697', '', '', '', '', '9 Bird Street', '', '', 'Lichfield', 'Staffordshire', 'WS13 6PW', '', '', NULL, '', '', '', '', '', ''),
(401, '', 'Passive', 'Indian Restaurant', 'The Taj', 'Ali', 'Ali', '01689819119', '', '07957914478', '', '', '10 High Street', '', '', 'Orpington', 'Kent', 'BR6 7BB', '', '', NULL, '', '', '', '', '', ''),
(402, '', 'Passive', 'Indian Restaurant', 'Taste of India', 'Rahman', 'A', '01304240122', '', '', '', '', '322 London Road', '', '', 'Dover', 'Kent', 'CT17 0SX', '', '', NULL, '', '', '', '', '', ''),
(403, '', 'Passive', 'Other (specify)', 'K2 Frozen Foods', 'Masum', 'Ahmed', '02392690398', '', '07764318507', '', '', '29/31 New Road', '', '', 'Portsmouth', 'Hampshire', 'PO2 7QN', '', '', NULL, '', '', '', '', '', ''),
(404, '', 'Passive', 'Indian Restaurant', 'Standard', 'Manager', 'Manager', '01923240031', '', '', '', '', '208 Watford Road', '', '', 'Watford', 'Hertfordshire', 'WD3 3DD', '', '', NULL, '', '', '', '', '', ''),
(405, '', 'Passive', 'Indian Restaurant', 'Rumney Balti', 'Ali', 'Ali', '02920794413', '', '', '', '', '198 New Road', '', '', 'Cardiff', 'South Glamorgan', 'CF3 8BN', '', '', NULL, '', '', '', '', '', ''),
(406, '', 'Passive', 'Indian Restaurant', 'Royal Bengal', 'Abdul', 'Majid', '01604602873', '', '', '', '', '46 St Marys Road', '', '', 'Market Harborough', 'Northamptonshire', 'LE16 7DU', '', '', NULL, '', '', '', '', '', ''),
(407, '', 'Passive', 'Indian Restaurant', 'Standard Tandoori', 'Shofar', 'Uddin', '02076071637', '', '', '', '', '87 Holloway Road', '', '', 'London', 'London', 'N7 8LT', '', '', NULL, '', '', '', '', '', ''),
(408, '', 'Passive', 'Indian Restaurant', 'Bombay Inn', 'Hakim', 'A', '02086717372', '', '', '', '', '220 Brixton Hill', '', '', 'Brixton', 'London', 'SW2 1HE', '', '', NULL, '', '', '', '', '', ''),
(409, '', 'Passive', 'Indian Restaurant', 'Rupali Indian', 'Anis', 'Anis', '01307464480', '', '', '', '', '3 Queen Street', '', '', 'Angus', 'Scotland', 'DD8 3AJ', '', '', NULL, '', '', '', '', '', ''),
(410, '', 'Passive', 'Indian Restaurant', 'Golden Gate', 'Zavid', 'Zavid', '02083373479', '', '07932667949', '', '', '18 Stonecote Hill', '', '', 'Sutton', 'Surrey', 'SM3 9HE', '', '', NULL, '', '', '', '', '', ''),
(411, '', 'Passive', 'Indian Restaurant', 'Spice Tandoori', 'Jamal', 'Miah', '01234363299', '', '', '', '', '84 High Street', '', '', 'Clapham', 'Bedfordshire', 'MK41 6BW', '', '', NULL, '', '', '', '', '', ''),
(412, '', 'Passive', 'Indian Restaurant', 'Bearsted Tandoori', 'Shafiul', 'Alam', '01622730409', '', '', '', '', '7 Motepark Shopping Centre', '', '', 'Bearsted', 'Kent', 'ME15 8LH', '', '', NULL, '', '', '', '', '', ''),
(413, '', 'Passive', 'Indian Restaurant', 'Bengal Diner', 'Laek', 'Chowdhury', '01622756094', '', '', '', '', '98 King Street', '', '', 'Maidstone', 'Kent', 'ME14 1BH', '', '', NULL, '', '', '', '', '', ''),
(414, '', 'Passive', 'Indian Restaurant', 'Royal Tandoori', 'Miah', 'Miah', '01517336816', '', '', '', '', '453 Smihtdown Road', '', '', 'Liverpool', 'Merseyside', 'L15 3JL', '', '', NULL, '', '', '', '', '', ''),
(415, '', 'Passive', 'Shop', 'Jai Electronics', 'V Mehta', 'V Mehta', '02089655080', '02089612924', '', '', '', '155 High Street', '', '', 'London', 'London', 'NW10 4TR', '', '', NULL, '', '', '', '', '', ''),
(416, '', 'Passive', 'Indian Restaurant', 'Sheeshmahal', 'Khalil', 'Khalil', '01753655398', '', '', '', '', '18 High Street', '', '', 'Iver', 'Buckinghamshire', 'SL0 9NG', '', '', NULL, '', '', '', '', '', ''),
(417, '', 'Passive', 'Indian Restaurant', 'Pabna Tandoori', 'Mohammed', 'Mohammed', '01538381156', '', '', '', '', '16-18 Ashbourne Road', '', '', 'Leek', 'Staffordshire', 'ST13 5AS', '', '', NULL, '', '', '', '', '', ''),
(418, '', 'Passive', 'Indian Restaurant', 'Harefield Balti', 'Munim', 'Choudhury', '01895825844', '', '07958528717', '', '', '40 High Street', '', '', 'Harefield', 'Middlesex', 'UB9 6BU', 'halal shop: 020 7723 3475', '', NULL, '', '', '', '', '', ''),
(419, '', 'Passive', 'Office', 'Lawson Soliictors', 'Efuru', 'Obua', '02089630700', '', '07931215050', '', '', '91/93 High Street', '', '', 'Harlesden', 'London', 'NW10 4NT', 'N', '', NULL, '', '', '', '', '', ''),
(420, '', 'Passive', 'Indian Restaurant', 'India Royal', 'Raju', 'Raju', '01795536033', '', '07931767810', '', '', '16 East Street', '', '', 'Faversham', 'Kent', 'ME13 8AD', 'N', '', NULL, '', '', '', '', '', ''),
(421, '', 'Passive', 'Indian Restaurant', 'Mughal Dynasty', 'Kamal', 'Uddin', '01933626351', '', '', '', '', '33 Brook Street', '', '', 'Raunds', 'Northamptonshire', 'NN9 6LL', 'N', '', NULL, '', '', '', '', '', ''),
(422, '', 'Passive', 'Office', 'Talbots', 'Manager', 'Manager', '02089651066', '', '', '', '', '189 Hight Street', '', '', 'Harlesden', 'London', 'NW10 4TE', 'N', '', NULL, '', '', '', '', '', ''),
(423, '', 'Passive', 'Indian Restaurant', 'Delhi Durbar', 'Y', 'Miah', '01494783121', '', '', '', '', '131 Broad Street', '', '', 'Chesham', 'Buckinghamshire', 'HP5 3EF', 'N', '', NULL, '178 White Hill', '', '', 'Chesham', 'Buckinghamshire', 'HP5 1AZ                                           '),
(424, '', 'EOT', 'Indian Restaurant', 'Lebu\'s', 'Lebu', 'Aziz', '01461205520', '', '', '', '', '118 High Street', '', '', 'Annan', 'Dumfrieshire', 'DG12 6DP', 'N', '', NULL, '', '', '', '', '', ''),
(425, '', 'Passive', 'Indian Restaurant', 'Thai Kitchen', 'Bayo', 'Bayo', '02082081111', '', '07904407777', '', '', '273 Neasden Lane', '', '', 'Neasden', 'London', 'NW10 1QJ', 'N', '', NULL, '', '', '', '', '', ''),
(426, '', 'Passive', 'Indian Restaurant', 'Pilgrims Korai', 'Sajid', 'Miah', '01277372140', '', '', '', '', '499 Ongar Road', '', '', 'Brentwood', 'Essex', 'CM15 9JP', 'N', '', NULL, '', '', '', '', '', ''),
(427, '', 'Passive', 'Other (specify)', 'O Encontro', 'Paul', 'Paul', '02088382889', '', '', '', '', '214 High Street', '', '', 'Harlesden', 'London', 'NW10 4SY', 'N', '', NULL, '', '', '', '', '', ''),
(428, '', 'Passive', 'Indian Restaurant', 'Raj', 'Abdul', 'Hoque', '02074390035', '', '', '', '', '72 Berwick Street', '', '', 'London', 'London', 'W1F 8TD', 'N', '', NULL, '', '', '', '', '', ''),
(429, '', 'Passive', 'Indian Restaurant', 'Chadnis', 'Koyes', 'Islam', '01296658484', '', '', '', '', '43/45 High Street', '', '', 'Waddesdon', 'Aylesbury', 'HP18 0JB', 'N', '', NULL, '', '', '', '', '', ''),
(430, '', 'Passive', 'Indian Restaurant', 'Rickshaw Tandoori', 'Manager', 'Manager', '02089693286', '', '', '', '', '8 Chamberlayne Road', '', '', 'London', 'London', 'NW10 3JD', 'N', '', NULL, '', '', '', '', '', ''),
(432, '', 'Passive', 'Indian Restaurant', 'Khyber Pass', 'Shakil', 'Shakil', '01483764710', '', '', '', '', '18 The Broadway', '', '', 'Woking', 'Surrey', 'GU21 5AP', '', '', NULL, '', '', '', '', '', ''),
(433, '', 'Passive', 'Indian Restaurant', 'Les Spice', 'Rashid', 'Rashid', '01932242600', '', '', '', '', '2 Station Approach', '', '', 'Shepperton', 'Middlesex', 'TW17 8AR', 'N', '', NULL, '', '', '', '', '', ''),
(434, '', 'Passive', 'Indian Restaurant', 'Penlan', 'Foyaz', 'Foyaz', '01792793849', '', '', '', '', '53 Crwys Terrace', '', '', 'Penlan', 'Swansea', 'SA5 9AH', 'N', '', NULL, '', '', '', '', '', ''),
(435, '', 'Passive', 'Indian Restaurant', 'Bengal Lancer', 'Fotik', 'Miah', '01252315012', '', '', '', '', '354 High Street', '', '', 'Aldershot', 'Hants', 'GU12 4LU', 'N', '', NULL, '', '', '', '', '', ''),
(437, '', 'Passive', 'Indian Restaurant', 'Khans', 'Manager', 'Manager', '02077275420', '', '', '', '', '13-15 Westbourne Grove', '', '', 'London', 'London', 'W2 4UA', 'N', '', NULL, '', '', '', '', '', ''),
(438, '', 'Passive', 'Indian Restaurant', 'Spice Plaza', 'Manager', 'Manager', '02077940228', '', '', '', '', '212 West End Lane', '', '', 'West Hampstead', 'London', 'NW6 1UU', 'N', '', NULL, '', '', '', '', '', ''),
(439, '', 'Passive', 'Indian Restaurant', 'Lebanese Kitchen', 'Manager', 'Manager', '02089618281', '', '', '', '', 'School Road', '', '', 'Park Royal', 'London', 'NW10 6TD', 'N', '', NULL, '', '', '', '', '', ''),
(440, '', 'Passive', 'Indian Restaurant', 'Vama', 'Manager', 'Manager', '02075658501', '', '', '', '', '438 Kings Road', '', '', 'Chelsea', 'London', 'SW10 0LJ', 'N', '', NULL, '', '', '', '', '', ''),
(441, '', 'Passive', 'Indian Restaurant', 'Verandah', 'Manager', 'Manager', '02088468555', '', '', '', '', '199 Castelnau', '', '', 'Barnes', 'London', 'SW13 9ER', 'N', '', NULL, '', '', '', '', '', ''),
(442, '', 'Passive', 'Indian Restaurant', 'Tandoori Centre', 'Manager', 'Manager', '02089924418', '', '', '', '', '15 Chuchfield Road', '', '', 'Acton', 'London', 'W3 6BD', 'N', '', NULL, '', '', '', '', '', ''),
(443, '', 'Passive', 'Indian Restaurant', 'Vhujon', 'Manager', 'Manager', '02074312052', '', '', '', '', '90 Fortune Green Road', '', '', 'West Hampstead', 'London', 'NW6 1DS', 'N', '', NULL, '', '', '', '', '', ''),
(444, '', 'Passive', 'Indian Restaurant', 'Shujon', 'Rahman', 'Rahman', '02084727048', '', '', '', '', '308 Barking Road', '', '', 'East Ham', 'London', 'E6 3BA', 'N', '', NULL, '', '', '', '', '', ''),
(445, '', 'Passive', 'Indian Restaurant', 'Shafique', 'Manager', 'Manager', '01245422569', '', '', '', '', '30 the Green', '', '', 'Chelmsford', 'Essex', 'CM1 3DU', 'N', '', NULL, '', '', '', '', '', ''),
(446, '', 'Passive', 'Indian Restaurant', 'Le Taj', 'Manager', 'Manager', '01276683668', '', '', '', '', '16 Park Street', '', '', 'Camberly', 'Surrey', 'GU15 3PL', 'N', '', NULL, '', '', '', '', '', ''),
(447, '', 'Passive', 'Indian Restaurant', 'Akbar Tandoori', 'Manager', 'Manager', '02088817902', '', '', '', '', '13 Salisbury Road', '', '', 'Wood Green', 'London', 'N22 6NL', 'N', '', NULL, '', '', '', '', '', ''),
(448, '', 'Passive', 'Indian Restaurant', 'New Delhi', 'Manager', 'Manager', '01375373183', '', '', '', '', '57 Orsett Road', '', '', 'Grays', 'Essex', 'RM17 5HJ', 'N', '', NULL, '', '', '', '', '', ''),
(449, '', 'Passive', 'Indian Restaurant', 'Bombay Palace', 'Manager', 'Manager', '01494531595', '', '', '', '', '6 Crendon Street', '', '', 'High Wycombe', 'Buckinghamshire', 'HP13 7LW', 'N', '', NULL, '', '', '', '', '', ''),
(450, '', 'Passive', 'Indian Restaurant', 'Eastern Delight', 'Manager', 'Manager', '02088687566', '', '', '', '', '15 Lovelane', '', '', 'Pinner', 'Middlesex', 'HA5 2EE', 'N', '', NULL, '', '', '', '', '', ''),
(451, '', 'Passive', 'Indian Restaurant', 'Eastern Night', 'Abdul', 'Ali', '02084294848', '', '', '', '', '6 Black Horse Parade', '', '', 'Pinner', 'Middlesex', 'HA5 2EN', 'N', '', NULL, '', '', '', '', '', ''),
(452, '', 'EOT', 'Indian Restaurant', 'Prince Tandoori', 'Manager', 'Manager', '01702391515', '', '', '', '', '288 London Road', '', '', 'West Cliff on Sea', 'Essex', 'SS0 7JJ', 'N', '', NULL, '', '', '', '', '', ''),
(453, '', 'Passive', 'Indian Restaurant', 'India Nights', 'Manager', 'Manager', '02087785251', '', '', '', '', '79 High Street', '', '', 'Penge', 'London', 'SE20 7HW', 'N', '', NULL, '', '', '', '', '', ''),
(454, '', 'Passive', 'Indian Restaurant', 'Sylhet Tandoori', 'Manager', 'Manager', '02083851001', '', '', '', '', '314 Preston Road', '', '', 'Harrow', 'Middlesex', 'HA3 0QH', 'N', '', NULL, '', '', '', '', '', ''),
(455, '', 'Passive', 'Indian Restaurant', 'Curry Express', 'Manager', 'Manager', '02087408316', '', '', '', '', '5 Pavilion Parade', '', '', 'Wood Lane', 'London', 'W12 0HQ', 'N', '', NULL, '', '', '', '', '', ''),
(456, '', 'Passive', 'Indian Restaurant', 'Halema', 'Humayun', 'Humayun', '01727830562', '', '', '', '', '256 Hatfield Road', '', '', 'St. Albans', 'Hertfordshire', 'AL1 4UN', 'N', '', NULL, '', '', '', '', '', ''),
(457, '', 'Passive', 'Indian Restaurant', 'Poppadom', 'Manager', 'Manager', '02088784577', '', '', '', '', '69A Sheen Lane', '', '', 'Mortlake', 'London', 'SW14 8AD', 'N', '', NULL, '', '', '', '', '', ''),
(458, '', 'Passive', 'Indian Restaurant', 'Bengal Lancer', 'Manager', 'Manager', '01554749199', '', '', '', '', '43 Murray Street', '', '', 'Llanelli', 'Dyfed', 'SA15 1BQ', 'N', '', NULL, '', '', '', '', '', ''),
(459, '', 'Passive', 'Indian Restaurant', 'Darjeeling Tandoori', 'Manager', 'Manager', '01895679300', '', '', '', '', '89 High Road', '', '', 'Ickenham', 'Middlesex', 'UB10 8LH', 'N', '', NULL, '', '', '', '', '', ''),
(460, '', 'Passive', 'Indian Restaurant', 'Anarkali', 'Rahman', 'A', '01792650549', '', '', '', '', '80 St. Helens Road', '', '', 'Swansea', 'South Glamorgan', 'SA1 4BQ', 'N', '', NULL, '', '', '', '', '', ''),
(461, '', 'Passive', 'Indian Restaurant', 'Hatch End Tandoori', 'Manager', 'Manager', '02084289781', '', '', '', '', '282-284 Uxbridge Road', '', '', 'Hatch End', 'Middlesex', 'HA5 4HS', 'N', '', NULL, '', '', '', '', '', ''),
(462, '', 'Passive', 'Indian Restaurant', 'Bua Thai', 'Manager', 'Manager', '02085631101', '', '', '', '', '138-140 King Street', '', '', 'Hammersmith', 'London', 'W6 0QU', 'N', '', NULL, '', '', '', '', '', ''),
(463, '', 'Passive', 'Indian Restaurant', 'Suhag', 'Manager', 'Manager', '01614283496', '', '', '', '', '345 Edgeley Road', '', '', 'Stockport', 'Cheshire', 'SK3 0RJ', 'N', '', NULL, '', '', '', '', '', ''),
(464, '', 'Passive', 'Indian Restaurant', 'Suhag', 'Manager', 'Manager', '01706643187', '', '', '', '', '185 Yorkshire Street', '', '', 'Rochdale', 'Lancashire', 'OL12 0DR', 'N', '', NULL, '', '', '', '', '', ''),
(465, '', 'Passive', 'Indian Restaurant', 'The Cottage', 'Manager', 'Manager', '02073724863', '', '', '', '', '30 Salusbury Road', '', '', 'Queens Park', 'London', 'NW6 6NL', 'N', '', NULL, '', '', '', '', '', ''),
(466, '', 'Passive', 'Indian Restaurant', 'Sonargoan Restaurant', 'Manager', 'Manager', '02089504156', '', '', '', '', '55 High Road', '', '', 'Bushey Heath', 'Hertfordshire', 'WD23 1EE', 'N', '', NULL, '', '', '', '', '', ''),
(467, '', 'Passive', 'Indian Restaurant', 'Verandah', 'Manager', 'Manager', '01554759561', '', '', '', '', '73 St Helens Road', '', '', 'Swansea', 'South Galmorgan', 'SA1 4BG', 'N', '', NULL, '', '', '', '', '', ''),
(468, '', 'Passive', 'Indian Restaurant', 'Purbani', 'Manager', 'Manager', '02089894174', '', '', '', '', '153 High Street', '', '', 'Wanstead', 'Essex', 'E11 2RL', 'N', '', NULL, '', '', '', '', '', ''),
(469, '', 'Passive', 'Indian Restaurant', 'Shapla', 'Manager', 'Manager', '02089973635', '', '', '', '', '44 Bilton Road', '', '', 'Greenford', 'Middlesex', 'UB6 7DH', 'N', '', NULL, '', '', '', '', '', ''),
(470, '', 'Passive', 'Indian Restaurant', 'Bengal Pride', 'Manager', 'Manager', '02083610005', '', '', '', '', '10 Halliwick Court Parade', '', '', 'Friern Barnet', 'London', 'N12 0NB', 'N', '', NULL, '', '', '', '', '', ''),
(471, '', 'Passive', 'Indian Restaurant', 'Rajdoot', 'Manager', 'Manager', '01895634656', '', '', '', '', '59 Windmill Hill', '', '', 'Ruislip Manor', 'Middlesex', 'HA4 8PU', '', '', NULL, '', '', '', '', '', ''),
(472, '', 'EOT', 'Indian Restaurant', 'Genie\'s', 'Manager', 'Manager', '02078345000', '', '', '', '', '68 Uxbridge Road', '', '', 'Shepherd\'s Bush', 'London', 'W12 8LP', 'N', '', NULL, '', '', '', '', '', ''),
(473, '', 'Passive', 'Indian Restaurant', 'Monsoon', 'Manager', 'Manager', '02086921588', '', '', '', '', '338 New Cross Road', '', '', 'London', 'London', 'SE14 6AG', 'N', '', NULL, '', '', '', '', '', ''),
(474, '', 'Passive', 'Indian Restaurant', 'Curry King', 'Miah', 'Miah', '02084557783', '', '', '', '', '31 Market Place', '', '', 'Hampstead Garden', 'London', 'NW11 6JY', 'N', '', NULL, '', '', '', '', '', ''),
(475, '', 'Passive', 'Indian Restaurant', 'Niwala', 'Manager', 'Manager', '02073726075', '', '', '', '', '182 Broadhurst Gardens', '', '', 'West Hampstead', 'London', 'NW6 3AY', 'N', '', NULL, '', '', '', '', '', ''),
(476, '', 'Passive', 'Indian Restaurant', 'St  Albans Tandoori', 'Zunel', 'Ahmed', '01727868648', NULL, NULL, NULL, NULL, '44-46 St  Peters Street', NULL, NULL, 'St Albans', 'Argentina', 'AL1 3NF', 'N', '', 1, '', '', '', '', '', ''),
(477, '', 'Passive', 'Indian Restaurant', 'Spicy King', 'Khan', 'Khan', '02088107171', '', '', '', '', '10 Castle Hill Parade', '', '', 'West Ealing', 'London', 'W13 8JP', 'contact Karim', '', NULL, '', '', '', '', '', ''),
(478, '', 'Passive', 'Indian Restaurant', 'Bengal Restaurant', 'Manager', 'Manager', '02072291640', '', '', '', '', '62 Porchester Road', '', '', 'Bayswater', 'London', 'W2 6ET', 'N', '', NULL, '', '', '', '', '', ''),
(479, '', 'Passive', 'Indian Restaurant', 'Monsoon', 'Manager', 'Manager', '02089921133', '', '', '', '', '307 Uxbridge Road', '', '', 'Acton', 'London', 'W3 9QU', 'N', '', NULL, '', '', '', '', '', ''),
(480, '', 'Passive', 'Thai Restaurant', 'Thai Tai', 'Tarine', 'Tarine', '02089608844', '', '', '', '', '536 Harrow Road', '', '', 'London', 'London', 'W9 3QF', '', '', NULL, '', '', '', '', '', ''),
(481, '', 'Passive', 'Indian Restaurant', 'Regency', 'Monayem', 'Khan', '01843831412', '', '', '', '', '9 Station Road', '', '', 'Westgate on Sea', 'Kent', 'CT8 8RB', 'N', '', NULL, '', '', '', '', '', ''),
(482, '', 'Passive', 'Indian Restaurant', 'Slough Tandoori', 'Sami', 'Sami', '01753516411', '', '07957318208', '', '', '30 Chalvey Road East', '', '', 'Slough', 'Berkshire', 'SL1 2LU', 'N', '', NULL, '', '', '', '', '', ''),
(484, '', 'Passive', 'Office', 'Fushi Ltd', 'Ranish', 'Jansari', '02088962545', '02088962545', '07971204112', '', '', '32D Rosemount Road', '', '', 'London', 'London', 'W3 9LY', '', '', NULL, '', '', '', '', '', ''),
(485, '', 'EOT', 'Indian Restaurant', 'Verandah', 'Manager', 'Manager', '01554759561', '', '', '', '', '20 Market Street', '', '', 'Llanelli', 'Dyfed', 'SA11 5YD', 'N', '', NULL, '', '', '', '', '', ''),
(486, '', 'Passive', 'Office', 'D.P.D Consultancy Services', 'Daniel', 'Djaba', '02089617738', '', '', '', '', 'P.O. Box 30316', '', '', 'London', 'London', 'NW10 2ZF', 'N', '', NULL, '', '', '', '', '', ''),
(487, '', 'Passive', 'Indian Restaurant', 'Jomuna', 'Halaith', 'Hossain', '01217645588', '', '07779327213', '', '', '1159 Warwick Road', '', '', 'Birmingham', 'West Midlands', 'B27 6RG', 'N', '', NULL, '', '', '', '', '', ''),
(488, '', 'Passive', 'Other (specify)', 'Furness Stores', 'Mazeh', 'Mazeh', '02084531841', '', '', '', '', '8 Furness Road', '', '', 'Harlesden', 'London', 'NW10 4PP', 'M', '', NULL, '', '', '', '', '', ''),
(489, '', 'Passive', 'Indian Restaurant', 'Chillies', 'Manager', 'Manager', '02087418200', '', '', '', '', '127b Brackenbury Road', '', '', 'London', 'London', 'W6 0BQ', 'M', '', NULL, '', '', '', '', '', ''),
(490, '', 'Passive', 'Indian Restaurant', 'Maurya', 'Sundip', 'Sundip', '02084160770', '', '07970074697', '', '', '16 Church Road', '', '', 'Stanmore', 'Middlesex', 'HA7 4AR', 'Fax at sundip office 02088610268\r\nOffice tel: 02088613232\r\nsundip office Angel Care  4 Ambassador House  Wolsely Road Harrow  HA3 5RT', '', NULL, '', '', '', '', '', ''),
(491, '', 'Passive', 'Other (specify)', 'Notting Hill Fine Art', 'R', 'Best', '07944691119', '', '', '', '', '28 Powis Terrace', '', '', 'Off Westboune Park Road', 'London', 'W11 1JH', 'M', '', NULL, '', '', '', '', '', ''),
(492, '', 'EOT', 'Indian Restaurant', 'Tava', 'Manager', 'Manager', '02085450900', '', '', '', '', '325 Haydons Road', '', '', 'Wimbledon', 'London', 'SW19 8LA', 'M', '', NULL, '', '', '', '', '', ''),
(493, '', 'Passive', 'Other (specify)', 'Pace private Hire', 'Manager', 'Manager', '02089978997', '', '', '', '', '8 The Avenue', '', '', 'West Ealing', 'London', 'W13 8PH', 'M', '', NULL, '', '', '', '', '', ''),
(494, '', 'Passive', 'Indian Restaurant', 'Cottage Tandoori', 'Manager', 'Manager', '01903743605', '', '', '', '', '25 West Street', '', '', 'Storrington', 'West Sussex', 'RH20 4DZ', 'M', '', NULL, '', '', '', '', '', ''),
(495, '', 'Passive', 'Indian Restaurant', 'Elysium', 'Manager', 'Manager', '02075823682', '', '', '', '', '46 Brixton Road', '', '', 'London', 'London', 'SW9 6BT', 'M', '', NULL, '', '', '', '', '', ''),
(496, '', 'Passive', 'Indian Restaurant', 'Gandhi\'s Restaurant', 'Manager', 'Manager', '02077359015', '', '', '', '', '347A Kennington Road', '', '', 'London', 'London', 'SE11 4QE', 'M', '', NULL, '', '', '', '', '', ''),
(497, '', 'Passive', 'Indian Restaurant', 'Imperial Tandoori', 'Manager', 'Manager', '02079284153', '', '', '', '', '48 Kennington Road', '', '', 'London', 'London', 'SE1 7BL', 'M', '', NULL, '', '', '', '', '', ''),
(498, '', 'Passive', 'Indian Restaurant', 'Jholpai', 'Manager', 'Manager', '01932354807', '', '', '', '', '2-3 Parvis Road', '', '', 'West Byfleet', 'Surrey', 'KT14 6LP', 'M', '', NULL, '', '', '', '', '', ''),
(499, '', 'Passive', 'Indian Restaurant', 'Lemon Grass', 'Manager', 'Manager', '01628777723', '', '', '', '', '17 Shifford Crescent', '', '', 'Maidenhead', 'Berkshire', 'SL6 7UA', 'M', '', NULL, '', '', '', '', '', ''),
(500, '', 'Passive', 'Indian Restaurant', 'Lily Tandoori', 'Manager', 'Manager', '02073851922', '', '', '', '', '86C Lillie Road', '', '', 'Fulham', 'London', 'SW6 1TL', 'M', '', NULL, '', '', '', '', '', ''),
(501, '', 'EOT', 'Indian Restaurant', 'Tandoori Raj', 'Manager', 'Manager', '02083408404', '', '', '', '', '25 Aylmer Parade', '', '', 'London', 'London', 'N2 0PE', 'M', '', NULL, '', '', '', '', '', ''),
(502, '', 'Passive', 'Indian Restaurant', 'Bekash Tandoori', 'Mukith', 'Mukith', '01908562249', '', '07717281178', '', '', '50 High Street', '', '', 'Stoney Stratford', 'Buckinghamshire', 'MK11 1AQ', 'home tel: 01908261113', '', NULL, '', '', '', '', '', ''),
(503, '', 'Passive', 'Indian Restaurant', 'Akash Tandoori', 'Manager', 'Manager', '02086475592', '02086696911', '', '', '', '145 Stafford Road', '', '', 'Wallington', 'Surrey', 'SM6 9BN', 'K', '', NULL, '', '', '', '', '', ''),
(504, '', 'Passive', 'Indian Restaurant', 'Mela restaurant', 'Manager', 'Manager', '000000', '', '', '', '', '8 Market street', '', '', 'Dartford', 'Kent', 'DA1 1ET', '', '', NULL, '', '', '', '', '', ''),
(505, '', 'Passive', 'Indian Restaurant', 'Cochin', 'Mahmmed', 'Ali', '01442233777', '01442231777', '07736787708', '', '', '61 High Street', '', '', 'Hemel Hempstead', 'Hertfordshire', 'HP1 3AF', '', '', NULL, '', '', '', '', '', ''),
(506, '', 'Passive', 'Indian Restaurant', 'Nadias Takeaway', 'Noor', 'Noor', '01235760557', '', '', '', '', '30 Wallingford Street', '', '', 'Wantage', 'Oxfordshire', 'OX12 8AX', 'home tel: 01865250278', '', NULL, '46 Mill Lane', '', '', 'Old Marston', 'Oxfordshire', 'OX3 0QA                                           '),
(507, '', 'Passive', 'Office', 'Impressions', 'Ivan', 'Garcia', '02089054554', '', '07900885371', '', '', '35 Tintagel drive', '', '', 'Stanmore', 'Middlesex', 'HA7 4SR', 'Re Nuria', '', NULL, '', '', '', '', '', ''),
(508, '', 'Passive', 'Office', 'Escuela de Baile', 'Nuria', 'Garcia', '02089054554', '', '07765878078', '', '', '35 Tintagel Drive', '', '', 'Stanmore', 'Middlesex', 'HA7 4SR', '', '', NULL, '', '', '', '', '', ''),
(509, '', 'Passive', 'Indian Restaurant', 'Balti Stan', 'Harun', 'Harun', '01254 871700', '', '07966269554', '', '', '217-219 Whalley Road', '', '', 'Accrington', 'Lancashire', 'BB5 5HD', '', '', NULL, '', '', '', '', '', ''),
(510, '', 'Passive', 'Office', 'Centrust Homes', 'Manager', 'Manager', '02089022036', '', '', '', '', '2 neeld Parade', '', '', 'Wembley', 'Middlesex', 'HA9 6QU', '', '', NULL, '', '', '', '', '', ''),
(511, '', 'Passive', 'Indian Restaurant', 'Mumtaj', 'Manager', 'Manager', '01727 843691', '', '', '', '', '115 London Road', '', '', 'St. Albans', 'Hertfordshire', 'AL1 1LR', '', '', NULL, '', '', '', '', '', ''),
(512, '', 'Passive', 'Indian Restaurant', 'The Mogul', 'Manager', 'Manager', '01799516611', '', '', '', '', '21 High Street', '', '', 'Saffron Walden', 'Essex', 'CB10 1AT', 're Lill Miah day of raj NW7', '', NULL, '', '', '', '', '', ''),
(513, '', 'Passive', 'Indian Restaurant', 'Nimmi Authentic Indian Cuisine', 'Rouf', 'Rouf', '01842761260', '01842761497', '', '', '', '17 White Hart Street', '', '', 'Thetford', 'Norfolk', 'IP24 1AA', '', '', NULL, 'Euro House', '', 'Mill Lane', 'Theford', 'Norfolk', 'IP24 3BZ                                          '),
(514, '', 'Passive', 'Indian Restaurant', 'Little India', 'Ashraf', 'Miah', '02380886688', '', '07792415623', '', '', '37 Rumbridge Street', '', '', 'Southampton', 'Hampshire', 'SO40 9DQ', '', '', NULL, '', '', '', '', '', ''),
(515, '', 'Passive', 'Indian Restaurant', 'Bombay Spice', 'Subeyr', 'Rahman', '02073816123', '', '', '', '', '26 Filmer Road', '', '', 'London', 'London', 'SW6 7BW', 'Re  Abu BAKER', '', NULL, '', '', '', '', '', ''),
(516, '', 'Passive', 'Indian Restaurant', 'Forest Tandoori', 'Shalim', 'Shalim', '02085206085', '', '07958131422', '', '', '102 Wood Street', '', '', 'London', 'London', 'E17 3HX', 'Re Rahim @ Millbank spice', '', NULL, '', '', '', '', '', ''),
(517, '', 'Passive', 'Indian Restaurant', 'New Balti Tandoori', 'Lahkon', 'Lahkon', '02084583480', '', '', '', '', '22 North End Road', '', '', 'London', 'London', 'NW11 7PT', '', '', NULL, '', '', '', '', '', ''),
(518, '', 'Passive', 'Indian Restaurant', 'Curry Garden', 'Zia', 'Zia', '01442877867', '', '07808157034', '', '', '29 High Street', '', '', 'Berkhamsted', 'Hertfordshire', 'HP4 2BX', '', '', NULL, '79 Conway Road', '', '', 'Luton', 'Bedfordshire', 'LU4 8JB                                           '),
(519, '', 'Passive', 'Indian Restaurant', 'The Akash Tandoori', 'Afsor', 'Ali', '01594827770', '', '', '', '', '92 High Street', '', '', 'Cinderford', 'Gloucestershire', 'GL14 2SZ', 'home tel: 01594827214', '', NULL, '', '', '', '', '', '');
INSERT INTO `customer` (`customer_id`, `reg_date`, `customer_status`, `business_type`, `business_name`, `first_name`, `surname`, `telephone`, `fax`, `mobile`, `email`, `web`, `address1`, `address2`, `area`, `city`, `county`, `postcode`, `comments`, `alternate_address`, `status`, `delivery_address1`, `delivery_address2`, `delivery_area`, `delivery_city`, `delivery_county`, `delivery_postcode`) VALUES
(520, '', 'Passive', 'Indian Restaurant', 'Rasal Brasserie Ltd', 'Shahab', 'Uddin', '01923778722', '', '07957373919', '', '', '173a High Street', '', '', 'Rickmansworth', 'Hertfordshire', 'WD3 1AY', '', '', NULL, '115 Uxbridge Road', '', '', 'Rickmansworth', 'Hertfordshire', 'WD3 7DN                                           '),
(521, '', 'Passive', 'Indian Restaurant', 'Ascot Spices', 'Amin', 'Amin', '01344623000', '', '', '', '', '29 High Street', '', '', 'Ascot', 'Berkshire', 'SL5 7HG', '', '', NULL, '53 Liddell Way', '', '', 'Ascot', 'Berkshire', 'SL5 9UU                                           '),
(522, '', 'Passive', 'Indian Restaurant', 'Bangkok & Bombay Brasserie', 'A M  (Babu)', 'Choudhury', '01494 872172', '', '08703333636', '', '', '8 Dean Way', '', '', 'Chalfont St. Giles', 'Buckinghamshire', 'HP8 4JH', '', '', NULL, '', '', '', '', '', ''),
(523, '', 'Passive', 'Indian Restaurant', 'The Ganges', 'Khalid', 'Ahmed', '02077234096', '', '07904572072', '', '', '101 Praed Street', '', '', 'London', 'London', 'W2 1NT', 'old mobile: 07956238480', '', NULL, '', '', '', '', '', ''),
(533, '', 'Passive', 'Indian Restaurant', 'Taj Cuisine', 'Abul', 'Monsur', '01634686636', '', '07958544461', '', '', '1 Sherwood House  Walderslade Centre', '', '', 'Chatham', 'Kent', 'ME5 9UD', '', '', NULL, '', '', '', '', '', ''),
(534, '', 'Passive', 'Indian Restaurant', 'Raj Vogue', 'Mohammed', 'Ali', '01992641297', '', '07960305344', '', '', '48 High Street', '', '', 'Cheshunt', 'Hertfordshire', 'EN8 0AQ', 'contact Torun\r\n155 Wellesley Road, Ilford, IG1 4LN', '', NULL, '', '', '', '', '', ''),
(535, '', 'Passive', 'Indian Restaurant', 'Dilshad', 'Faruk', 'Faruk', '01543570264', '', '07958497716', '', '', '1 Ashworth House  Cannock Road', '', '', 'Connock', 'Staffordshire', 'WS11 2DZ', '', '', NULL, '', '', '', '', '', ''),
(536, '', 'Passive', 'Indian Restaurant', 'Shard End Balti', 'Choudhury', 'Choudhury', '01217491907', '', '07931780360', '', '', '23 Heathway', '', '', 'Birmingham', 'Warwickshire', 'B34 6QU', '', '', NULL, '', '', '', '', '', ''),
(537, '', 'Passive', 'Indian Restaurant', 'Balti Nites', 'Choudhury', 'Choudhury', '01213845777', '', '07931780360', '', '', '244 Wheel Wright Road', '', '', 'Birmingham', 'Warwickshire', 'B24 8EH', 're Shard End mr choudhury', '', NULL, '', '', '', '', '', ''),
(538, '', 'Passive', 'Indian Restaurant', 'Balti Mahal', 'Choudhury', 'Choudhury', '01213293331', '', '07931780360', '', '', 'unit 14  Norris Way  Riland Ind Est', '', '', 'Birmingham', 'Warwickshire', 'B75 7BB', 'Re Shard End  mr choudhury', '', NULL, '', '', '', '', '', ''),
(539, '', 'Passive', 'Indian Restaurant', 'Shabagh Tandoori & Balti', 'C', 'C', '01568614500', '', '07956945316', '', '', '16 Burgess Street', '', '', 'Leominster', 'Hertfordshire', 'HR6 8DE', 'shard end', '', NULL, '', '', '', '', '', ''),
(540, '', 'Passive', 'Indian Restaurant', 'The Bombay Palace', 'Abdul', 'Rob', '01905613969', '', '07815107014', '', '', '38 The Tything', '', '', 'Worcester', 'Worcestershire', 'WR1 1JL', '', '', NULL, '9 Templefield gardens', '', 'Bordesley Village', 'Birmingham', '', 'B9 4NY                                            '),
(541, '', 'Passive', 'Indian Restaurant', 'The Curry Garden', 'Uddin', 'Uddin', '01933314121', '', '07815725422', '', '', '24 Church Street', '', '', 'Rushden', 'Northamptonshire', 'NN10 9YT', '', '', NULL, '', '', '', '', '', ''),
(542, '', 'Passive', 'Indian Restaurant', 'Baltizer', 'Khandakar', 'Khandakar', '01322551122', '', '', '', '', '5 Mill Row  High Street', '', '', 'Bexley', 'Kent', 'DA5 1LA', '', '', NULL, '', '', '', '', '', ''),
(543, '', 'Passive', 'Indian Restaurant', 'Verandah', 'Koddus', 'Ali', '01554759561', '', '07974295321', '', '', '1 Hall Street', '', '', 'Llanelli', 'Carmadenshire', 'SA15 3BB', 'home tel: 01792 418234', '', NULL, '21 Mirador Crecent', '', 'Uplands', 'Swansea', '', 'SA2 0QX                                           '),
(544, '', 'Passive', 'Indian Restaurant', 'Regal Handi', 'Chunu', 'Miah', '02088939291', '', '07903992814', '', '', '21 The Green', '', '', 'Twickenham', 'Middlesex', 'TW2 5AA', 'Nov 2003\r\nBill Books ncr (100 set) x 200 books £350 +VAT', '', NULL, '', '', '', '', '', ''),
(545, '', 'Passive', 'Indian Restaurant', 'Bay of Bengal', 'Ahia', 'Miah', '02082989233', '', '', '', '', '258 Shearwood Park Avenue', '', '', 'Sidcup', 'Kent', 'DA15 9JN', '', '', NULL, '', '', '', '', '', ''),
(546, '', 'Passive', 'Indian Restaurant', 'The Shimla Palace', 'Sukanta', 'Paul', '01889881325', '', '07855808469', '', '', 'Cromwell House  Wolseley Bridge', '', '', 'Stafford', 'Staffordshire', 'ST17 0XS', '', '', NULL, 'Midland Lighting', '', 'Cromwell House', 'Wolseley Bridge', 'Stafford', 'ST17 0XS                                          '),
(547, '', 'Passive', 'Indian Restaurant', 'Grange Town Tandoori', 'Kowser', 'Miah', '01915655984', '', '', '', '', '1 Stockton Terrace', '', '', 'Sunderland', 'Durham', 'SR2 9RQ', '', '', NULL, '229 Queen Alexandra Rd', '', '', 'Sunderland', '', 'SR3 1XF                                           '),
(548, '', 'Passive', 'Indian Restaurant', 'Elacihi Tandoori', 'Atiqur', 'Rahman', '01458833966', '', '07766220229', '', '', '62 High Street', '', '', 'Glastonbury', 'Somerset', 'BA6 9DY', '', '', NULL, '', '', '', '', '', ''),
(549, '', 'Passive', 'Indian Restaurant', 'The Hasina', 'S Miah', 'Smiah', '01702511590', '', '07866922342', '', '', '358 Rayleigh Road', '', '', 'Leigh on Sea', 'Essex', 'SS9 5PU', 'Contact mr Ashik (Sharma)', '', NULL, '', '', '', '', '', ''),
(550, '', 'Passive', 'Indian Restaurant', 'Curry Nights', 'Mannan', 'Abdul', '02087513300', '', '', '', '', '4 Hamilton Parade  Feltham Hill Road', '', '', 'Feltham', 'Middlesex', 'TW13 4PJ', 'Re: hoque  (tandoori nights)', '', NULL, '', '', '', '', '', ''),
(551, '', 'Passive', 'Indian Restaurant', 'Pasha Indian Cuisine', 'Nurul', 'Haque', '01905426327', '', '07957107034', '', '', '56 St. Johns', '', '', 'Worcester', '', 'WR2 5AJ', '', '', NULL, '', '', '', '', '', ''),
(552, '', 'Passive', 'Indian Restaurant', 'Paramount Tandoori & Balti', 'Lacky', 'Miah', '02920708778', '', '07817226171', '', '', '5 Station Approach', '', '', 'Penarth', 'Glamorganshire', 'CF64 3EE', '', '', NULL, '25 Beresford', '', 'Roath', 'Cardiff', '', 'CF24 1RA                                          '),
(553, '', 'Passive', 'Indian Restaurant', 'Victoria Indian Cuisine', 'Mezanur', 'Rahman', '02078344686', '', '', '', '', '318 Vauxhall Bridge Road', '', '', 'London', 'Londoon', 'SW1V 1AA', '', '', NULL, '', '', '', '', '', ''),
(554, '', 'Passive', 'Indian Restaurant', 'India Garden', 'Nadim', 'Aziz', '01529302521', '', '', '', '', '19 Market Place', '', '', 'Sleaford', 'Lincolnshire', 'NG34 7SR', '', '', NULL, '', '', '', '', '', ''),
(557, '', 'Passive', 'Office', 'Independent Mortgage Specialists Ltd', 'Mesba', 'Ahmed', '07971131147', '02084521551', '07956 353615', '', '', '37 The Circle North Circular Road', '', '', 'London', 'London', 'NW2 7QR', '', '', NULL, '', '', '', '', '', ''),
(558, '', 'Passive', 'Indian Restaurant', 'Curry Hut', 'Nurul', 'Haque', '0190521945', '', '07957107034', '', '', '45A Upper Tything', '', '', 'Worcester', 'Worcestershire', 'WR1 1JT', 're: Pasha mr nurul haque', '', NULL, '', '', '', '', '', ''),
(559, '', 'Passive', 'Indian Restaurant', 'Nashaa Indian Restaurant', 'Rahim', 'Rahim', '02476366344', '', '', '', '', '154 Longford Road', '', '', 'Coventry', '', 'CV6 6DR', 're: millbank spice', '', NULL, '', '', '', '', '', ''),
(560, '', 'Passive', 'Indian Restaurant', 'The Gandhi', 'Habib / naz', 'Rahman', '01803522529', '', '', '', '', '4A Parkside Road', '', '', 'Paignton', 'Devonshire', 'TQ4 6AE', 'price rate re: millbank Spice', '', NULL, '', '', '', '', '', ''),
(561, '', 'Passive', 'Indian Restaurant', 'Burgh Heath Tandoori', 'Tipu', 'Sultan', '01737371716', '', '07974311362', '', '', '1 The Parade  Brighton Road', '', '', 'Burgh Heath', 'Surrey', 'KT20 6AT', '', '', NULL, '21 Walton Court', '', 'Boundary Road', 'Woking', 'Surrey', 'GU21 5EB                                          '),
(562, '', 'Passive', 'Indian Restaurant', 'Crendon Indian Cuisine', 'Ahmed', 'Ahmed', '01844201900', '', '07786016020', '', '', '11 The Square', '', '', 'Long Crendon', 'Buckinghamshire', 'HP18 9AA', '', '', NULL, '', '', '', '', '', ''),
(563, '', 'Passive', 'Indian Restaurant', 'The Gate of India', 'Meraz', 'Meraz', '01202717068', '', '07813955131', '', '', '54 Commercial Road', '', '', 'Pool', 'Dorset', 'BH14 0JT', '', '', NULL, 'Fabric Shop', '', '52 Commercial Road', 'Pool', 'Dorset', 'BH14 0JT                                          '),
(564, '', 'Passive', 'Indian Restaurant', 'Samrat Restaurant', 'Horouf', 'Horouf', '02089978923', '', '07870658617', '', '', '52 Pitshanger Lane', '', '', 'London', 'London', 'W5 1QY', '', '', NULL, '', '', '', '', '', ''),
(565, '', 'Passive', 'Indian Restaurant', 'Balti Court', 'Kawsarul', 'Gani', '01213824200', '', '07815753489', '', '', 'Stuart Court  90 gravelly Lane', '', '', 'Birmingham', '', 'B23 6LR', 'Home Tel: 01213732269', '', NULL, '10 Mona Road', '', 'Erdington', 'Birmingham', '', 'B23 6UL                                           '),
(566, '', 'Passive', 'Indian Restaurant', 'Bengal Dynasty', 'Manchab', 'Ali', '01492875928', '01492878445', '07966426907', '', '', '1 North Parade', '', '', 'Llandudno', '', 'LL30 2LP', '', '', NULL, '', '', '', '', '', ''),
(567, '', 'EOT', 'Indian Restaurant', 'Shobab Indian Cuisine', 'Hannan', 'Hannan', '01227272325', '', '', '', '', '40 Oxford Street', '', '', 'Whistable', 'Kent', 'CT5 1DG', 'branch Shampa', '', NULL, '', '', '', '', '', ''),
(568, '', 'Passive', 'Indian Restaurant', 'Meah\'s', 'Zia', 'Rahman', '01582454504', '', '', '', '', '102 Park Street', '', '', 'Luton', 'Bedfordshire', 'LU1 3EY', '', '', NULL, '', '', '', '', '', ''),
(569, '', 'Passive', 'Indian Restaurant', 'Bombay', 'Rofu', 'Miah', '01243 864038', '', '', '', '', '31A Station Road', '', '', 'Bognor Regis', 'Sussex', 'PO21 1QD', '', '', NULL, '', '', '', '', '', ''),
(570, '', 'Passive', 'Indian Restaurant', 'The Saeed Balti House', 'Abdul', 'Goni', '01452506122', '', '', '', '', '36 Seymour Road', '', '', 'Gloucester', 'Gloucestershire', 'GL1 5PT', 'get postcode', '', NULL, '22 Howard Street', '', '', 'Gloucester', 'Gloucestershire', 'GL1 4US                                           '),
(571, '', 'Passive', 'Indian Restaurant', 'Bipash Tandoori Restaurant', 'Kahir', 'Gofur', '01707372491', '', '07919577420', '', '', '41 Gole Green Lane  Woodhall Parade', '', '', 'Welwyn Garden City', 'Hertfordshire', 'AL7 3PP', '', '', NULL, '140 Arkwrights', '', '', 'Harlow', 'Essex', 'CM20 3LZ                                          '),
(572, '', 'Passive', 'Indian Restaurant', 'Indian Expess', 'Juberul', 'Khan', '01617944141', '', '', '', '', '133 Partington Lane', '', '', 'Manchester', '', 'M27 0NS', '', '', NULL, '', '', '', '', '', ''),
(573, '', 'Passive', 'Indian Restaurant', 'Al Mina', 'Shamim', 'Ahmed', '01614069595', '', '', '', '', '144 Higher Bents Lane', '', '', 'Stockport', '', 'SK6 2LU', '', '', NULL, '', '', '', '', '', ''),
(574, '', 'Passive', 'Indian Restaurant', 'Pakeeza Indian Takeaway', 'Mohammed / Hussain', 'Hussain', '02083570303', '', '07956565243', '', '', '351 Kingsbury Road', '', '', 'London', 'London', 'NW9', '', '', NULL, '', '', '', '', '', ''),
(575, '', 'Passive', 'Indian Restaurant', 'Dine India Takeaway', 'Abdul', 'Motin', '01923236411', '', '', '', '', '274a St Albans Road', '', '', 'Watford', 'Hertfordshire', 'WD2 5PE', 'Re : Anil chopra (01923 354990)\r\n110 Langley Way, Watford WD17 3EE', '', NULL, '', '', '', '', '', ''),
(576, '', 'Passive', 'Indian Restaurant', 'Sitar Indian Cuisine', 'Rafiqul', 'Choudhury', '01461205520', '', '07830216913', '', '', '118 High street', '', '', 'Annan', 'Dumfreisshire', 'DG12 6DP', '', '', NULL, '', '', '', '', '', ''),
(577, '', 'Passive', 'Indian Restaurant', 'Curry Land', 'Rashid', 'Ahmed', '01903243138', '', '', '', '', '239 Tarring Road', '', '', 'Worthing', 'West Sussex', 'BN11 4HW', '', '', NULL, '', '', '', '', '', ''),
(578, '', 'Passive', 'Indian Restaurant', 'Shahi Balti', 'Amran', 'Aslam', '01873859201', '01633262265', '07966543404', '', '', '5 Mill Street', '', '', 'Abergavenny', 'Monmouthshire', 'NP7 5HE', 'home tel: 01633258611', '', NULL, '21 Harrow Road', '', 'Newport', 'Gwent', '', 'NP19 0BU                                          '),
(579, '', 'Passive', 'Indian Restaurant', 'Mehek', 'Shalim', 'Rashid', '02075885043', '', '07899928560', '', '', '45 London Wall', '', '', 'London', 'London', 'EC2M 5TE', '', '', NULL, '', '', '', '', '', ''),
(580, '', 'Passive', 'Indian Restaurant', 'Kabul Restaurant', 'Ahmedi', 'Ahmedi', '02085716878', '', '07957913713', '', '', '65 The Broadway', '', '', 'Southall', 'Middlesex', 'UB1 1LB', 'Home Address 117 Mollison Way, Edgware, \r\nHA8 5QU', '', NULL, '', '', '', '', '', ''),
(581, '', 'Passive', 'Office', 'The Latin Quarter', 'Nuria', 'Garcia', '02073830567', '02073830567', '07765878078', '', '', '87 Hampstead Road', '', '', 'London', 'London', 'NW1 2PL', '', '', NULL, '', '', '', '', '', ''),
(582, '', 'Passive', 'Indian Restaurant', 'Mela Indian Cuisine', 'Mahbub', 'Mahbub', '02072813555', '', '07710223897', '', '', '94 Stroud Green Road', '', '', 'London', 'London', 'N4 3EN', 'Re: Ferdus', '', NULL, '', '', '', '', '', ''),
(583, '', 'Passive', 'Indian Restaurant', 'Days of the Raj', 'Sunu', 'Miah', '02089063363', '', '', '', '', '123 The Broadway', '', '', 'London', 'London', 'NW7 3TG', 'Re: Lil Miah  mob  07974227474', '', NULL, '', '', '', '', '', ''),
(584, '', 'Passive', 'Thai Restaurant', 'Sang Thai', 'Zaffar', 'Wahab', '01306889053', '', '', '', '', '274 High Street', '', '', 'Dorking', 'Surrey', 'RH4 1QT', '', '', NULL, '', '', '', '', '', ''),
(585, '', 'Passive', 'Shops', 'New al madina Grocers', 'Munim', 'Munim', '02077233475', '', '', '', '', '57 Church Street', '', '', 'London', 'London', 'NW8 8EU', '', '', NULL, '', '', '', '', '', ''),
(586, '', 'Passive', 'Office', 'Shalim', 'Shalim', 'Shalim', '02077239355', '', '07986660659', '', '', '26 Cherwell House Church Street', '', '', 'London', 'London', 'NW8 8PT', '', '', NULL, '', '', '', '', '', ''),
(587, '', 'Passive', 'Indian Restaurant', 'Kensington Tandoori', 'Ahad', 'Ahad', '02079376182', '02079379450', '', '', '', '1 Abingdon Road', '', '', 'London', 'London', 'W8 6AH', '', '', NULL, '', '', '', '', '', ''),
(588, '', 'Passive', 'Office', 'Mountain Lake ltd', 'Lee', 'Baldry', '02088386289', '', '07881555338', '', '', '22 Longstone Avenue', '', '', 'London', 'London', 'NW10 3TU', '', '', NULL, '', '', '', '', '', ''),
(589, '', 'Passive', 'Office', 'Callbourne Ltd', 'Fozia', 'Muddassir', '02088111100', '020881189', '', '', '', 'Park View 183 -189 The Vale', '', '', 'London', 'London', 'W3 7RW', '', '', NULL, '', '', '', '', '', ''),
(590, '', 'Passive', 'Office', 'Park View London Ltd', 'Fozia', 'Muddasir', '02088111188', '02088111189', '', '', '', 'Park View  183-189 The Vale', '', '', 'London', 'London', 'W3 7RW', '', '', NULL, '', '', '', '', '', ''),
(591, '', 'Passive', 'Office', 'Reddy Siddique Information Systems', 'Fozia', 'Muddasir', '02088111188', '02088111189', '', '', '', 'Park View  183 - 189 The Vale', '', '', 'London', 'London', 'W3 7RW', '', '', NULL, '', '', '', '', '', ''),
(592, '', 'Passive', 'Office', 'Reddy Siddiqui Management Ltd', 'Fozia', 'Muddasir', '02088111188', '02088111189', '', '', '', 'Park View  183 - 189 The Vale', '', '', 'London', 'London', 'W3 7RW', '', '', NULL, '', '', '', '', '', ''),
(593, '', 'Passive', 'Office', 'Reddy Siddiqui Management Service', 'Fozia', 'Muddassir', '02088111188', '02088111189', '', '', '', 'Park View  183 - 189 The Vale', '', '', 'London', 'London', 'W3 7RW', '', '', NULL, '', '', '', '', '', ''),
(594, '', 'Passive', 'Indian Restaurant', 'Bombay Dreams', 'Kaptan', 'Miah', '02084417424', '', '000000', '', '', '251B East Barnet Road', '', '', 'East Barnet', 'Hertfordshire', 'EN4 8SS', '', '', NULL, '', '', '', '', '', ''),
(595, '', 'Passive', 'Indian Restaurant', 'Noor Jahan 2', 'Manager', 'Manager', '0207402332', '02074025885', '', '', '', '26 Sussex Place', '', '', 'London', 'London', 'W2 2TH', '', '', NULL, '', '', '', '', '', ''),
(596, '', 'Passive', 'Indian Restaurant', 'Anikas', 'Afzol', 'Hussain', '01895270543', '', '', '', '', '12 Hercies Road', '', '', 'Hillingdon', 'Middlesex', 'UB10 9NA', '2 colour A4 50k @ £770 inc delivery', '', NULL, '', '', '', '', '', ''),
(597, '', 'Passive', 'Office', 'Waterman Black', 'Augustine', 'Filson', '02087953225', '02087952393', '07956337119', '', '', '602A High Road', '', '', 'Wembley', 'Middlesex', 'HA0 2AF', '', '', NULL, '', '', '', '', '', ''),
(598, '', 'Passive', 'Office', 'Sugar & Spice Co', 'Kannan', 'Sivanathan', '02082043899', '02082061282', '07742606655', '', '', '26 Berkeley Road', '', '', 'London', 'London', 'NW9 9DG', '', '', NULL, '', '', '', '', '', ''),
(599, '', 'Passive', 'Indian Restaurant', 'Quality', 'Moinul  Ali', 'Ali', '01279443099', '', '07985193351', '', '', 'unit 15 Horsecroft Place', '', '', 'Harlow', 'Essex', 'CM19 5BU', 'ali mobile 07862738167\r\nmoinul home 02088091802', '', NULL, '', '', '', '', '', ''),
(600, '', 'Passive', 'Indian Restaurant', 'Bishaal Tandoori', 'Kahir', 'Kahir', '01279412999', '', '07919577420', '', '', '6 Staple Tye', '', '', 'Harlow', 'Essex', 'CM18 7LX', '', '', NULL, '', '', '', '', '', ''),
(601, '', 'Passive', 'Shops', 'Med Food', 'Manager', 'Manager', '07887567110', '', '07887567110', '', '', '91 Rothesay Avenue', '', '', 'Greenford', 'Middlesex', 'UB6 0DB', 're: Jamal printer', '', NULL, '', '', '', '', '', ''),
(602, '', 'Passive', 'Shops', 'Green Valley London Ltd', 'Moussa', 'Motairek', '07774840430', '', '07774840430', '', '', '35 John Aird Court  Harrow Road', '', '', 'London', 'London', 'W2 1UY', 're Ali', '', NULL, '', '', '', '', '', ''),
(603, '', 'Passive', 'Indian Restaurant', 'Great India', 'Rahim', 'Rahim', '02077305638', '', '', '', '', '79 Lower Sloane Street', '', '', 'London', 'London', 'SW1W 8DA', 'branch of Great Eastern', '', NULL, '', '', '', '', '', ''),
(604, '', 'Passive', 'Indian Restaurant', 'Curry Mahal', 'Choudhury', 'Choudhury', '01225789666', '', '07751544401', '', '', '6 Georges Road', '', '', 'Bath', 'Somerset', 'BA1 6EY', '', '', NULL, '', '', '', '', '', ''),
(605, '', 'Passive', 'Office', 'UK Hand Car  Wash', 'Musa', 'Musa', '07814812156', '', '', '', '', 'M1 Yard  Ellesmere Avenue', '', '', 'London', 'London', 'NW7 3EX', '', '', NULL, '', '', '', '', '', ''),
(606, '', 'Passive', 'Indian Restaurant', 'Mahaan', 'Shammim', 'Shammim', '01895252725', '', '', '', '', '29 High Road', '', '', 'Uxbridge', 'Middlesex', 'UB8 2HL', 'home 02088483688', '', NULL, '', '', '', '', '', ''),
(608, '', 'Passive', 'Indian Restaurant', 'Curry Leaf', 'Khan', 'Khan', '07931196139', '', '07931196139', '', '', '44 New House Park', '', '', 'St Albans', 'Hertfordshire', 'AL1 1UJ', 'Home 310 High Street  London Colney  St Albans AL2 1HW', '', NULL, '', '', '', '', '', ''),
(609, '', 'Passive', 'Indian Restaurant', 'Massala Tandoori', 'Salim', 'Salim', '02084291615', '', '', '', '', '208 Whitby Road', '', '', 'South Ruislip', 'Middlesex', 'HA4 9DY', '', '', NULL, '', '', '', '', '', ''),
(610, '', 'Passive', 'Indian Restaurant', 'Five Ways', 'Mohammed', 'Yahya', '01608811011', '', '', '', '', 'Sturt Road', '', '', 'Charlbury', 'Oxfordshire', 'OX7 3AB', 'contact : Mr.Rafi', '', NULL, '', '', '', '', '', ''),
(611, '', 'Active', 'Office', 'Apex Dental Care', 'Khosravi', 'Khosravi', '02089592299', '02089060494', '07770565335', '', '', '647 Watford Way', '', '', 'London', 'London', 'NW7 3JR', 'Mr Khosravi   mobile 07944401051\r\nmob 0781097362', '', NULL, '', '', '', '', '', ''),
(612, '', 'Passive', 'Indian Restaurant', 'Fatima Indian', 'Wahid', 'Wahid', '02087491323', '', '', '', '', '253 Wood Lane', '', '', 'London', 'London', 'W12 0HL', '', '', NULL, '', '', '', '', '', ''),
(613, '', 'Passive', 'Indian Restaurant', 'Chatkhara Tandoori', 'Adil', 'Adil', '02086822519', '', '07734186679', '', '', '15 Upper Tooting Road', '', '', 'London', 'London', 'SW17 7TS', '', '', NULL, '', '', '', '', '', ''),
(615, '', 'Passive', 'Office', 'O Elbacha', 'Osama', 'Albasha', '02084531777', '02089650156', '07799363940', '', '', '11 Johosons Way  Coronation Road', '', '', 'London', 'London', 'NW10 7PF', '', '', NULL, '', '', '', '', '', ''),
(616, '', 'Passive', 'Indian Restaurant', 'Iver Curry & Tandoori', 'Raju', 'Raju', '01753630303', '', '07968192827', '', '', '26 High Street', '', '', 'Iver', 'Buckinghamshire', 'SL0 9NG', '', '', NULL, '', '', '', '', '', ''),
(617, '', 'Passive', 'Indian Restaurant', 'Taj Mahal', 'Forid', 'Uddin', '02088902347', '', '', '', '', '8 Cavendish Terrace  High Street', '', '', 'Feltham', 'Middlesex', 'TW13 4HE', '', '', NULL, '', '', '', '', '', ''),
(618, '', 'EOT', 'Indian Restaurant', 'Curry & Kebab House', 'Vicky', 'Vicky', '01', '', '07984416696', '', '', '4 Wingate Parade', '', '', 'Swindon', 'Gloucestershire', 'SN1 2PN', '', '', NULL, '', '', '', '', '', ''),
(619, '', 'Passive', 'Indian Restaurant', 'Memories of India', 'Islam', 'Islam', '02075896450', '02075844438', '07930474738', '', '', '18 Gloucester Road', '', '', 'London', 'London', 'SW7 4RB', '', '', NULL, '', '', '', '', '', ''),
(620, '', 'Passive', 'Indian Restaurant', 'Little India', 'Islam', 'Islam', '02075843476', '', '07930474738', '', '', '32 Gloucester Road', '', '', 'London', 'London', 'SW7 4RB', 'Mrs Islam mobile: 07956 495054', '', NULL, '', '', '', '', '', ''),
(621, '', 'Passive', 'Indian Restaurant', 'Imperial Spice', 'Khalid', 'Khalid', '02084446567', '', '07832219581', '', '', '470 Long Lane', '', '', 'London', 'London', 'N2 8JL', '', '', NULL, '', '', '', '', '', ''),
(622, '', 'Passive', 'Indian Restaurant', 'Davids Deli', 'Zakaria', 'Zakaria', '02074338383', '', '', '', '', '341 West end Lane', '', '', 'London', 'London', 'NW6 1RS', '', '', NULL, '', '', '', '', '', ''),
(623, '', 'Passive', 'Office', 'Relax Print', 'Jamal', 'Jamal', '02', '', '', '', '', '189 High Street', '', '', 'London', 'London', 'NW10 4TE', 'Trade Price', '', NULL, '', '', '', '', '', ''),
(624, '', 'Passive', 'Builders, Decorators', 'P A Design ltd', 'Abass', 'Kheirallah', '02089603521', '', '07788914110', '', '', '1070 Harrow Road', '', '', 'London', 'London', 'NW10 5NL', '', '', NULL, '', '', '', '', '', ''),
(625, '', 'Passive', 'Builders, Decorators', 'P A Design ltd', 'Paul', 'Crawford', '02076020625', '', '07977161636', '', '', '53 Chatsworth Court  Pembrok Road', '', '', 'London', 'London', 'W8 6DH', '', '', NULL, '', '', '', '', '', ''),
(626, '', 'Passive', 'Indian Restaurant', 'Chutneys', 'Abdul', 'Rahman', '01895255777', '', '07747078005', '', '', '26 Cowley Road', '', '', 'Uxbridge', 'Middlesex', 'UB8 2LT', '', '', NULL, '', '', '', '', '', ''),
(627, '', 'Passive', 'Builders, Decorators', 'Floor of Choice', 'Ali', 'Issa', '02089654950', '02089654951', '07958379576', '', '', '10 Furness Road', '', '', 'London', 'London', 'NW10 4PP', '', '', NULL, '', '', '', '', '', ''),
(628, '', 'Passive', 'Wholesale', 'Rahims Continental Food Ltd', 'Rahim', 'Rahim', '02084530133', '02084530133', '07880750033', '', '', '27B Minerva Road', '', '', 'London', 'London', 'NW10 6HH', '', '', NULL, '', '', '', '', '', ''),
(629, '', 'Passive', 'Office', 'Faith & Victory Church', 'Moryas', 'Moryas', '02084517218', '', '07947172487', '', '', 'Priory Road', '', '', 'London', 'London', 'N8 8RD', '', '', NULL, '', '', '', '', '', ''),
(630, '', 'Passive', 'Office', 'Craven Park Dental Practice', 'Khosravi', 'Khosravi', '02089653605', '', '07770565335', '', '', '2 Craven Park', '', '', 'London', 'London', 'NW10 8SY', '', '', NULL, '', '', '', '', '', ''),
(636, '', 'Passive', 'Indian Restaurant', 'Spice Route', 'Manager', 'Manager', '02072582', '02076224596', '', '', '', '120 Battersea Park Road', '', '', 'London', 'London', 'SW11 4LY', '', '', NULL, '', '', '', '', '', ''),
(637, '', 'Passive', 'Indian Restaurant', 'Saravanas', 'Kumar', 'Manager', '02084594900', '02084593600', '', '', '', '77-79 Dudden Hill Lane', '', '', 'London', 'London', 'NW10 1BB', '', '', NULL, '', '', '', '', '', ''),
(638, '', 'Passive', 'Indian Restaurant', 'Cinnamon House Caterers', 'Kannan', 'Sivanathan', '02082061282', '', '07742606655', '', '', '26 Berkeley Road', '', '', 'London', 'London', 'NW9 9DG', '', '', NULL, '', '', '', '', '', ''),
(639, '', 'Passive', 'Office', 'Scleroderma Society', 'Kim', 'K', '02089614912', '', '', '', '', '3 Caple Road', '', '', 'London', 'London', 'NW10 8AB', 'kim 02089654094', '', NULL, '', '', '', '', '', ''),
(640, '', 'Passive', 'Indian Restaurant', 'De Ja Vu', 'Manager', 'Manager', '01992626229', '', '', '', '', '19 High Street', '', '', 'Cheshunt', 'Hertfordshire', 'EN8 0BX', '', '', NULL, '', '', '', '', '', ''),
(641, '', 'Passive', 'Caffee', 'Edwards Bakery', 'Emill', 'Ali', '02084593001', '', '07946399216', '', '', '269 High Road', '', '', 'London', 'London', 'NW10 xxx', '', '', NULL, '', '', '', '', '', ''),
(642, '', 'Passive', 'Indian Restaurant', 'Akash Restaurant', 'Ashraf', 'Ashraf', '02079783363', '', '07958212568', '', '', '70 Northcote Road', '', '', 'London', 'London', 'SW11 6QL', '', '', NULL, '', '', '', '', '', ''),
(643, '', 'Passive', 'Indian Restaurant', 'Tamannas Restaurant', 'Shamim', 'Shamim', '01784420720', '', '', '', '', '15A Station Road', '', '', 'Ashford', 'Middlesex', 'TW15 2UP', '', '', NULL, '', '', '', '', '', ''),
(644, '', 'Passive', 'Indian Restaurant', 'Cinnamon', 'Mukhith', 'Khan', '01753860004', '', '07984806733', '', '', '3 Lynwood Parade  St Lukes Road', '', '', 'Old Windsor', 'Berkshire', 'SL4 2QX', '', '', NULL, '', '', '', '', '', ''),
(645, '', 'EOT', 'Office', 'Mr Box ltd', 'Jane', 'Billing', '08007839885', '', '', '', '', 'Glebe Cottage  Ahsbocking Road', '', '', 'Ipswich', '', 'IP6 9RP', '', '', NULL, '', '', '', '', '', ''),
(646, '', 'Passive', 'Indian Restaurant', 'Elachi', 'Badrul', 'Islam', '01895624443', '', '07903865317', '', '', '52 Victoria Road', '', '', 'Ruislip Manor', 'Middlesex', 'HA4 0AG', 'Badrul  home 02089983959', '', NULL, '', '', '', '', '', ''),
(647, '', 'Passive', 'Indian Restaurant', 'Zeera', 'Angur', 'Angur', '02089970210', '', '', '', '', '10 Queens Parade  Queens Drive', '', '', 'London', 'London', 'W5 3HU', '', '', NULL, '', '', '', '', '', ''),
(648, '', 'Passive', 'Builders, Decorators', 'Neto Building', 'Neto', 'Neto', '07729062620', '', '', '', '', '137 High Street', '', '', 'London', 'London', 'Nw10 4TR', '', '', NULL, '', '', '', '', '', ''),
(649, '', 'EOT', 'Office', 'London Worldwide Trading Ltd', 'Bari', 'Bari', '02089655501', '02089655502', '07973634591', '', '', '175 Hillside', '', '', 'London', 'London', 'NW10 8LL', '', '', NULL, '', '', '', '', '', ''),
(652, '', 'Passive', 'Office', 'BSS Associates Ltd', 'BP', 'Bhattacharya', '02077313973', '02077313720', '07973849271', '', '', '1Putney Bridge Approach', '', '', 'London', 'London', 'SW6 3JD', 'Riverbank House (part of address)', '', NULL, '', '', '', '', '', ''),
(653, '', 'EOT', 'Indian Restaurant', 'Bengal Tiger', 'Kutub', 'Uddin', '02089606744', '', '07958683412', '', '', '171 Ladbroke Grove', '', '', 'London', 'London', 'W10 6HJ', '', '', NULL, '', '', '', '', '', ''),
(654, '', 'Passive', 'Indian Restaurant', 'Paras Nepalese Restaurant', 'Ahkari', 'A', '02088401226', '02088404907', '07977580076', '', '', '23 Boston Parade  Boston Road', '', '', 'London', 'London', 'W7 2DG', '', '', NULL, '', '', '', '', '', ''),
(655, '', 'Passive', 'Office', 'RDW Scenery Construction Ltd', 'Christine', 'x', '02089654413', '02089654414', '', '', '', 'Unit A  genesis  Rainsford Road', '', '', 'London', 'London', 'NW10 7RS', 'Boss Mr.Wilson\r\nother contact  Shara', '', NULL, '', '', '', '', '', ''),
(656, '', 'Passive', 'Chinese Restaurant', 'Chinese Express', 'Manager', 'Manager', '02084510783', '', '', '', '', '259 High Road', '', '', 'London', 'London', 'NW10 2RX', '', '', NULL, '', '', '', '', '', ''),
(657, '', 'Passive', 'Indian Restaurant', 'Shameems Restaurant', 'Badrul', 'X', '02089024337', '', '07956439706', '', '', '19 Wembley Hill Road', '', '', 'Wembley', 'Middlesex', 'HA9 8AF', '', '', NULL, '', '', '', '', '', ''),
(658, '', 'Passive', 'Indian Restaurant', 'Spice Grills', 'S Bhatti', 'Bhatti', '02073288889', '', '07832257591', '', '', '59 Salusbury Road', '', '', 'London', 'London', 'NW6 6NJ', 'Home tel 02085745465', '', NULL, '', '', '', '', '', ''),
(659, '', 'Passive', 'Indian Restaurant', 'Raj Mahal', 'Manager', 'Manager', '02088458141', '', '', '', '', '207 Ealing Road', '', '', 'Northolt', 'Middlesex', 'UB5 5HS', '', '', NULL, '', '', '', '', '', ''),
(660, '', 'Passive', 'Hair Salon', 'Beckham Salon', 'Wafik', 'ZamZam', '02072587887', '', '078101100124', '', '', '38 Crawford Street', '', '', 'London', 'London', 'W1H 1JH', '', '', NULL, '', '', '', '', '', ''),
(661, '', 'Passive', 'Indian Restaurant', 'Haiqas', 'Manager', 'A', '02087487345', '', '', '', '', '291 King Street', '', '', 'London', 'London', 'W6 9NH', '', '', NULL, '', '', '', '', '', ''),
(662, '', 'Passive', 'Indian Restaurant', 'Bayleaf', 'Zila', 'Miah', '02076256969', '', '07930284101', '', '', '1 Peel Precinct  Peel Road', '', '', 'London', 'London', 'NW6 5RE', '', '', NULL, '', '', '', '', '', ''),
(663, '', 'Passive', 'French Restaurant', 'La Riviera', 'Thalia', 'Troisi', '02088925815', '', '', '', '', '39 Heath Road', '', '', 'Twickenham', 'Middlesex', 'TW1 4AW', 'Design by Erin Sorensen 07831 566359', '', NULL, '', '', '', '', '', ''),
(664, '', 'Passive', 'Indian Restaurant', 'Paprika', 'Manager', 'Manager', '02072263113', '', '', '', '', '142 Balls Pond Road', '', '', 'London', 'London', 'N1 4AD', '', '', NULL, '', '', '', '', '', ''),
(665, '', 'Passive', 'Indian Restaurant', 'Red Rose', 'A', 'A', '02072726655', '', '', '', '', '597 Holloway Road', '', '', 'London', 'London', 'N19 4DJ', '', '', NULL, '', '', '', '', '', ''),
(666, '', 'Passive', 'Office', 'Andy Loos ltd', 'Pat', 'Barnett', '01905345821', '', '', '', '', 'Brick Barns Farm  Evesham Road', '', '', 'Worcester', '', 'WR7 4QR', '', '', NULL, '', '', '', '', '', ''),
(667, '', 'Passive', 'Indian Restaurant', 'Bengal Berties', 'Jalal', 'Jalal', '02083438332', '', '', '', '', '355 Ballards Lane', '', '', 'London', 'London', 'N12 8LJ', '', '', NULL, '', '', '', '', '', ''),
(668, '', 'Passive', 'Indian Restaurant', 'Shamrat Restaurant', 'Parves', 'Taher', '02073283831', '', '', '', '', '312 Kilburn High Road', '', '', 'London', 'London', 'NW6 2DG', 'home tel 02084500994', '', NULL, '', '', '', '', '', ''),
(669, '', 'Passive', 'Indian Restaurant', 'Monsoon Restaurant', 'K Miah', 'K Miah', '07971626640', '', '07971626640', '', '', 'The Cliff', '', '', 'Sheddar', 'Somerset', 'BS27 3QE', '', '', NULL, '9 Stiling Close', '', '', 'Highbridge', 'Somerset', 'TA9 3LT                                           '),
(670, '', 'Passive', 'Indian Restaurant', 'Café Masala', 'Sam', 'Hoque', '07830291509', '', '', '', '', '113 Kew Road', '', '', 'Richmond', 'Middlesex', 'TW9 2PN', '', '', NULL, '', '', '', '', '', ''),
(671, '', 'Passive', 'Indian Restaurant', 'Bollywood Restaurant', 'Monir', 'X', '01992583430', '01992586380', '', '', '', '97 Fore Street', '', '', 'Hertford', 'Hertfordshire', 'SG14 1AS', '', '', NULL, '', '', '', '', '', ''),
(672, '', 'Passive', 'Indian Restaurant', 'Herbs & Spices', 'Ahmed', 'X', '0138437552', '', '', '', '', '84 Hagley Road', '', '', 'Stourbridge', 'West Midlands', 'DY8 1QU', '', '', NULL, '', '', '', '', '', ''),
(673, '', 'Passive', 'Indian Restaurant', 'The Raj Poot', 'Akthar', 'X', '0146053110', '', '', '', '', '12 Silver Street', '', '', 'Illminister', 'Somerset', 'TA1 0DJ', '', '', NULL, '', '', '', '', '', ''),
(674, '', 'Passive', 'Indian Restaurant', 'Cumin Restaurant', 'Manager', 'X', '02077510446', '', '', '', '', '839 Fulham Road', '', '', 'London', 'London', 'SW6 5HQ', '', '', NULL, '44 Beards Road', '', '', 'Ashford', 'Middlesex', 'TW15 1TD                                          '),
(675, '', 'Passive', 'Indian Restaurant', 'Lal Bagh', 'Manager', 'x', '02088780010', '', '', '', '', '467 Upper Richmond Road West', '', '', 'London', 'London', 'SW14 7PU', '', '', NULL, '', '', '', '', '', ''),
(677, '', 'Passive', 'Indian Restaurant', 'Chamon Restaurant', 'Andy', 'Hussain', '01562701094', '', '', '', '', '507 Bristol Road', '', '', 'Birmingham', 'West Midlands', 'B29 6AO', '______________ -  labels printed  - 140904 - ____________________ 677', '', NULL, '', '', '', '', '', ''),
(678, '', 'Passive', 'Indian Restaurant', 'Ruchi', 'Manager', 'x', '01628771502', '', '', '', '', '41A Wootton Way', '', '', 'Maidenhead', 'Berkshire', 'SL6 4QZ', '', '', NULL, '', '', '', '', '', ''),
(679, '', 'Passive', 'Indian Restaurant', 'Spice Balti', 'Manager', 'x', '01269845777', '', '', '', '', 'Unit 4 The Square', '', '', 'Swansea', 'Wales', 'SA14 6NT', '', '', NULL, '43 odo Street', '', 'Hafod', 'Swansea', 'Wales', 'SA1 2LS                                           '),
(680, '', 'Passive', 'Indian Restaurant', 'Zafran', 'Sham', 'x', '01792813705', '', '', '', '', '215 New Road', '', '', 'Skewen', 'West Glamorganshire', 'SA10 6EW', '', '', NULL, '', '', '', '', '', ''),
(681, '', 'Passive', 'Indian Restaurant', 'Bombay Spice', 'Manager', 'x', '01494444453', '', '', '', '', '5 Cross Court  Plomer Green Avenue', '', '', 'High Wycombe', 'Buckinghamshire', 'HP13 5UW', '', '', NULL, '', '', '', '', '', ''),
(682, '', 'Passive', 'Indian Restaurant', 'Sunargor Restaurant', 'Million', 'x', '01455285900', '', '', '', '', '44B Main Street', '', '', 'Broughton Ashley', 'Leicestershire', 'LE9 6RE', '', '', NULL, '', '', '', '', '', ''),
(683, '', 'Passive', 'Printer', 'Sonex Printers', 'Suran', 'x', '02086514814', '', '', '', '', '30 Albatross Gardens', '', '', 'Selsdon', 'Surrey', 'CR2 8QX', '', '', NULL, '', '', '', '', '', ''),
(684, '', 'Passive', 'Indian Restaurant', 'Spice Republic', 'Bulbul', 'Ali', '02086131234', '', '07960786786', '', '', '499 Lordship Lane', '', '', 'London', 'London', 'SE22 8JY', '', '', NULL, '', '', '', '', '', ''),
(685, '', 'Passive', 'Indian Restaurant', 'Salma  Takeaway', 'Max', 'x', '01253305621', '', '07861224292', '', '', '63 White Gate Drive', '', '', 'Blackpool', 'Lancashire', 'FY3 9DF', '', '', NULL, '', '', '', '', '', ''),
(686, '', 'Passive', 'Indian Restaurant', 'Spice Takeaway', 'Farooq', 'x', '01159623555', '', '', '', '', '459 Westdale Lane', '', '', 'Mapperley', 'Nottinghamshire', 'NG3 6DH', '', '', NULL, '', '', '', '', '', ''),
(687, '', 'Passive', 'Indian Restaurant', 'Indian Valley', 'Lobir', 'Miah', '00', '', '', '', '', '952 London Road', '', '', 'Leigh on Sea', 'Essex', 'SS9 3NF', '', '', NULL, '', '', '', '', '', ''),
(688, '', 'Passive', 'Indian Restaurant', 'Spice of India', 'Mokul', 'x', '02088741777', '', '', '', '', '3 Granville Road', '', '', 'London', 'London', 'SW18 5SB', '', '', NULL, '', '', '', '', '', ''),
(689, '', 'Passive', 'Indian Restaurant', 'Bipasha restaurant', 'Ray', 'x', '02085304650', '', '', '', '', '12 Nightingale Lane', '', '', 'London', 'London', 'E11 2HE', 'H: 02085304650', '', NULL, '', '', '', '', '', ''),
(690, '', 'Passive', 'Indian Restaurant', 'Saffron Restaurant', 'Shundor', 'Miah', '01708734560', '', '07956395223', '', '', '256 Rushgreen Road', '', '', 'Romford', 'Essex', 'RM7 0LA', '', '', NULL, '', '', '', '', '', ''),
(691, '', 'Passive', 'Indian Restaurant', 'Saffron Restaurant', 'Manager', 'x', '01604630800', '', '', '', '', '21 Castilian Street', '', '', 'Northampton', 'Northamptonshire', 'NN1 1JS', '', '', NULL, '', '', '', '', '', ''),
(692, '', 'Passive', 'Indian Restaurant', 'Grapes Tandoori', 'Manager', 'x', '02085691911', '', '', '', '', '524 Uxbridge Road', '', '', 'Hayes', 'Middlesex', 'UB4 0SA', '', '', NULL, '', '', '', '', '', ''),
(693, '', 'Passive', 'Indian Restaurant', 'Chillies Takeaway', 'Manager', 'x', '01895257799', '', '', '', '', '55A Cowley Road', '', '', 'Uxbridge', 'Middlesex', 'UB', '', '', NULL, '', '', '', '', '', ''),
(694, '', 'Passive', 'Office', 'The Friends of Bangladesh', 'Pervez', 'Ahmed', '02083609046', '', '', '', '', '178 South Lodge Drive', '', '', 'London', 'London', 'N14 4XN', '', '', NULL, '', '', '', '', '', ''),
(695, '', 'Passive', 'Indian Restaurant', 'India Spice', 'Afroz', 'Ali', '02088615993', '', '', '', '', '126 Pinner Road', '', '', 'Harrow', 'Middlesex', 'HA1 4JD', '', '', NULL, '', '', '', '', '', ''),
(696, '', 'Passive', 'Indian Restaurant', 'Zeera', 'Shaful', 'Islam', '01895811911', '', '07903076669', '', '', '163 Ryefield Avenue', '', '', 'Hillingdon', 'Middlesex', 'UB10 9DA', '', '', NULL, '', '', '', '', '', ''),
(697, '', 'Passive', 'Indian Restaurant', 'Raj of India', 'R', 'Ali', '01780753556', '', '07947350029', '', '', '2 All Saints Street', '', '', 'Stamford', 'Lincolnshire', 'PE9 2PA', '', '', NULL, '', '', '', '', '', ''),
(698, '', 'Passive', 'Indian Restaurant', 'Highland Balti', 'Abdul', 'Bari', '01663766686', '', '', '', '', '123 Buxton Road', '', '', 'Stockport', 'Cheshire', 'SK6 8DX', '', '', NULL, '', '', '', '', '', ''),
(699, '', 'Passive', 'Indian Restaurant', 'Royal Balti', 'Abdul', 'Latif', '01952415122', '', '07830251108', '', '', '1 Charlton Street', '', '', 'Telford', 'Shropshire', 'TF1 1EF', 'Home tel 01215555078', '', NULL, '', '', '', '', '', ''),
(700, '', 'Passive', 'Office', 'Brotherhood of The Cross', 'Sister Ronki', 'x', '02073721686', '', '', '', '', '4 Bond House  Rupert Road', '', '', 'London', 'London', 'NW6 5DL', '200 x Cal with own image £800 +VAT', '', NULL, '', '', '', '', '', ''),
(701, '', 'Passive', 'Indian Restaurant', 'Gulens Restaurant', 'x', 'x', '02084690972', '', '', '', '', '175 Brockley Road', '', '', 'London', 'London', 'SE4 2RS', '', '', NULL, '', '', '', '', '', ''),
(702, '', 'Passive', 'Indian Restaurant', 'Chulmleigh Tandoori', 'Shamim', 'Ahmed', '01769580497', '', '', '', '', 'Cooper House  Fore Street', '', '', 'Chulmleigh', 'Devon', 'Ex 18 7BR', '', '', NULL, '', '', '', '', '', ''),
(703, '', 'Passive', 'Indian Restaurant', 'Ahmed Tandoori', 'Noor', 'x', '02089590519', '', '07949117784', '', '', '153 Hale Lane', '', '', 'Edgware', 'Middlesex', 'HA8 9QW', '', '', NULL, '', '', '', '', '', ''),
(704, '', 'Passive', 'Indian Restaurant', 'Shapla Restaurant', 'Imran', 'Hussain', '01142720831', '', '07966050578', '', '', '28 Cumberland Street', '', '', 'Sheffield', 'Yorkshire', 'S1 4PT', '', '', NULL, '', '', '', '', '', ''),
(705, '', 'Passive', 'Indian Restaurant', 'Palace Tandoori', 'Shiraj', 'x', '02085637905', '', '', '', '', '95A Fullham Palace Road', '', '', 'London', 'London', 'W6 8JA', '', '', NULL, '', '', '', '', '', ''),
(706, '', 'Passive', 'Indian Restaurant', 'Eastern Experience', 'Shamim', 'X', 'x', '', '07838254836', '', '', 'Unit 7  Aspects Leisure Park', '', '', 'Bristol', 'Somerset', 'BS15 9LA', '', '', NULL, '', '', '', '', '', ''),
(707, '', 'Passive', 'Indian Restaurant', 'India Cottage', 'S', 'Miah', '01792830208', '', '07961933430', '', '', '69 Herbert Street', '', '', 'Swansea', 'Wales', 'SA8 4ED', '', '', NULL, '', '', '', '', '', ''),
(708, '', 'Passive', 'Indian Restaurant', 'Alis Tandoori', 'Rour', 'Ali', '01239621069', '', '07976361044', '', '', '2 Finchs Sq', '', '', 'Cardighan', 'West Wales', 'SA43 1BS', '', '', NULL, '', '', '', '', '', ''),
(709, '', 'Passive', 'Indian Restaurant', 'Gondho Raj', 'Munin', 'X', '01708746704', '', '', '', '', '42A London Road', '', '', 'Romford', 'Essex', 'RM7 9RB', '', '', NULL, '', '', '', '', '', ''),
(710, '', 'Passive', 'Indian Restaurant', 'Spice Grill', 'Ashard', 'Fida', '02088111919', '', '07956836211', '', '', '4 Shepherds Bush Road', '', '', 'London', 'London', 'W6 7PJ', '', '', NULL, '', '', '', '', '', ''),
(711, '', 'Passive', 'Office', 'Kansaras', 'Indera', 'x', '02082057921', '', '07968837681', '', '', '15 Pear Close', '', '', 'London', 'London', 'NW9 0LJ', '', '', NULL, '', '', '', '', '', ''),
(712, '', 'Passive', 'Pizza', 'The Pizzeria', 'Raj', 'x', '01993841513', '', '07837387681', '', '', '12 Alvescot Road', '', '', 'Carterton', 'Oxfordshire', 'OX', '', '', NULL, '', '', '', '', '', ''),
(713, '', 'Passive', 'Indian Restaurant', 'Agrah Restaurant', 'Kamal', 'x', '02088761799', '', '07931566824', '', '', '105 Sheen Lane', '', '', 'London', 'London', 'SW14 8AE', 'cont Mr Pavel home 02087480331 mo 07903814793\r\nKamal home 02077318134', '', NULL, '', '', '', '', '', ''),
(714, '', 'Passive', 'Indian Restaurant', 'Naaz Restaurant', 'Fazal', 'Ali', '01772626695', '', '', '', '', 'Club Street', '', '', 'Preston', 'Lancashire', 'PR5 6FN', 'con tel 01254681593', '', NULL, '', '', '', '', '', ''),
(715, '', 'Passive', 'Indian Restaurant', 'Depa Restaurant', 'Pintu', 'x', '02073870613', '', '', '', '', '4 Leigh Street', '', '', 'London', 'London', 'WC1H 9EW', 'con Mr.Mitu 01603616937  mo 07958134151', '', NULL, '', '', '', '', '', ''),
(716, '', 'Passive', 'Indian Restaurant', 'Shapla Restaurant', 'T Hossain', 'x', '01444232493', '', '', '', '', '226 London Road', '', '', 'Burgess Hill', 'West Sussex', 'RH15 9QR', '', '', NULL, '', '', '', '', '', ''),
(717, '', 'Passive', 'Indian Restaurant', 'The Viceroy', 'Shahin', 'x', '01923672121', '', '07949495092', '', '', '85 Old Watford Road', '', '', 'Bricketwood', 'Hertfordshire', 'AL2 3UN', '', '', NULL, '', '', '', '', '', ''),
(718, '', 'Passive', 'Office', 'Kitchen Odour  Solution', 'Neil', 'Verner', '08702419986', '08702420615', '', '', '', 'Metic House  Ripley Drive', '', '', 'Normanton', 'West Yorkshire', 'WF6 1QT', '', '', NULL, '', '', '', '', '', ''),
(719, '', 'Passive', 'Indian Restaurant', 'Jewel Tandoori', 'Mohith', 'Miah', '01702421003', '', '07958128155', '', '', '538 Rayleigh Road', '', '', 'Leigh on Sea', 'Essex', 'SS9 5HX', '', '', NULL, '', '', '', '', '', ''),
(720, '', 'Passive', 'Indian Restaurant', 'Masum Takeaway', 'Rakib', 'x', '02084496863', '', '07944540901', '', '', '18 Lytton Road', '', '', 'New Barnet', 'Hertfordshire', 'EN5 5BY', '', '', NULL, '99 Victoria Road', '', '', '', 'London', 'N9 9SU                                            '),
(721, '', 'Passive', 'Indian Restaurant', 'Samins Curry And Tandoori', 'Burhan', 'Uddin', '01273455558', '', '07980832278', '', '', '10 Middle Road', '', '', 'Shoreham By Sea', 'West Sussex', 'BN43 6GA', 'Qumar Uddin  mob 07773107899', '', NULL, '', '', '', '', '', ''),
(722, '', 'Passive', 'Indian Restaurant', 'Peppers Balti', 'Abdul', 'Latif', '01952606555', '', '07830251108', '', '', '109A Trench Road', '', '', 'Telford', 'Shropshire', 'TF2 7DP', 'home tel 01215555078', '', NULL, '21 Priory Close', '', '', 'Smethwick', 'Warwickshire', 'B66 3SL                                           '),
(723, '', 'Passive', 'Indian Restaurant', 'Dipali Restaurant', 'Mujahid', 'Choudhury', '02088862221', '', '07956806153', '', '', '82 Aldermans Hill', '', '', 'London', 'London', 'N13 4PP', '', '', NULL, '', '', '', '', '', ''),
(724, '', 'Passive', 'Indian Restaurant', 'New Bengal', 'Kobir', 'M A', '02392521822', '', '', '', '', '28 Gomer Lane', '', '', 'Gosport', 'Hampshire', 'PO12 2SA', '', '', NULL, '', '', '', '', '', ''),
(725, '', 'Passive', 'Indian Restaurant', 'Pasha Balti', 'Amran', 'Aslam', '01633257939', '', '07966543404', '', '', '2 Constance Street', '', '', 'Newport', 'South Wales', 'NP19 7DB', 'Re Shahi Balti', '', NULL, '', '', '', '', '', ''),
(726, '', 'Passive', 'Indian Restaurant', 'Shahs Palace', 'Griff', 'x', '01563529870', '', '07774242999', '', '', '25 John Finnie Street', '', '', 'Kilmarnock', 'Scotland', 'KA1 1DD', '', '', NULL, '', '', '', '', '', ''),
(727, '', 'Passive', 'Indian Restaurant', 'Annarpurna Restaurant', 'Sharma', 'x', '02088939977', '', '', '', '', '231 Powder Mill Lane', '', '', 'Whitton', 'Middlesex', 'TW2 6EH', '', '', NULL, '', '', '', '', '', ''),
(728, '', 'Passive', 'Indian Restaurant', 'The City Raj', 'Turaj', 'x', '02072363663', '', '', '', '', '41 Farringdon Street', '', '', 'London', 'London', 'EC4A 4AN', 'Branch Suruchi', '', NULL, '', '', '', '', '', ''),
(729, '', 'Passive', 'Indian Restaurant', 'Panahar Restaurant', 'Shoeb', 'Choudhury', '01375676566', '', '07939930230', '', '', '18 King Street', '', '', 'Stanford-Le-Hope', 'Essex', 'SS17 0HL', '', '', NULL, '', '', '', '', '', ''),
(730, '', 'Passive', 'Office', 'Trio Graphics', 'Seils', 'Whybra', '01452302858', '', '', '', '', 'Unit 2  Shepherd Road', '', '', 'Gloucester', 'Gloucestershire', 'GL2 5EL', '', '', NULL, '', '', '', '', '', ''),
(731, '', 'Passive', 'Office Estate Agents', 'Harrisons', 'Brian', 'D\'Brass', '02089519458', '02089519459', '07766342050', '', '', '15 Park Parade', '', '', 'London', 'London', 'NW10 4JH', '', '', NULL, '', '', '', '', '', ''),
(732, '', 'Passive', 'Indian Restaurant', 'Standard Bengal', 'Manager', 'x', '02084591999', '', '', '', '', '33 High Road', '', '', 'London', 'London', 'NW10 2TE', '', '', NULL, '', '', '', '', '', ''),
(733, '', 'Passive', 'Indian Restaurant', 'Kings Cross Tandoori', 'Manager', 'x', '02072780506', '', '', '', '', '341 Grays Inn Road', '', '', 'London', 'London', 'WC1', '', '', NULL, '', '', '', '', '', ''),
(734, '', 'Passive', 'Indian Restaurant', 'Grove Spice', 'Jitu', 'x', '02088516588', '', '07940593515', '', '', '101 Chinbrook Road', '', '', 'London', 'London', 'SE12 9QL', '', '', NULL, '', '', '', '', '', ''),
(737, '', 'Passive', 'Office', 'Asian Television Group', 'Bharath', 'Patal', '02089652100', '02089652102', '', '', '', '28  Metro Centre Britannia Way', '', '', 'London', 'London', 'NW10 7PA', '', '', NULL, '', '', '', '', '', ''),
(738, '', 'Passive', 'Office', 'SFH', 'Jamil', 'Khan', '01707274444', '01707274443', '07795231585', '', '', 'Unit 5 The IO Centre  Hearle Way', '', '', 'Hatfield', 'Hertfordshire', 'AL0 9EW', '', '', NULL, '', '', '', '', '', ''),
(739, '', 'Passive', 'Restaurant', 'Bumbuu', 'x', 'x', '02089653938', '', '', '', '', '39 Station Road', '', '', 'London', 'London', 'NW10 4UP', '', '', NULL, '', '', '', '', '', ''),
(740, '', 'Passive', 'Restaurant', 'La Carpa Doro', 'Shahid', 'x', '02089063494', '', '', '', '', '40 The Broadway', '', '', 'Mill Hill', 'London', 'NW7 3LH', '', '', NULL, '', '', '', '', '', ''),
(741, '', 'Passive', '2', 'Jubraj', 'Angur', 'Miah', '02089938884', NULL, '07956430306', NULL, NULL, '7 Park Parade Gunnersbury Avenue', NULL, NULL, 'London', 'Argentina', 'W3', 'Re Zeera W5', '', 1, '', '', '', '', '', ''),
(742, '', 'Passive', 'Indian Restaurant', 'Garlic Indian Takeaway', 'Anhar', 'Ali', '01895435121', '', '07956418814', '', '', '62 Swan Road', '', '', 'West Drayton', 'Middlesex', 'UB7 7JZ', '', '', NULL, '', '', '', '', '', ''),
(743, '', 'Passive', 'Indian Restaurant', 'Massalla Indian Takeaway', 'Adil', 'Khan', '02089631154', '', '07956982878', '', '', '98 Craven Park Road', '', '', 'London', 'London', 'NW10 4AG', '', '', NULL, '', '', '', '', '', ''),
(744, '', 'Passive', 'Other', 'CASH', 'CASH', 'CASH', 'CASH', '', '', '', '', 'CASH', '', '', 'CASH', 'CASH', 'CASH', 'Only cash jobs. Customer details are not relevant.', '', NULL, '', '', '', '', '', ''),
(745, '', 'Passive', 'Other', 'Club Dance Holidays', 'Robert', 'Thompson', '08702866000', '', '07778299497', '', '', '24-32 Stephenson Way', '', '', 'London', 'London', 'NW1 2HD', '', '', NULL, '', '', '', '', '', ''),
(746, '', 'Passive', 'Indian Restaurant', 'Quality Spice', 'M0niul', 'x', '02085312942', '', '07985193351', '', '', '151 Wadham Road', '', '', 'London', 'London', 'E17 4HU', '', '', NULL, '', '', '', '', '', ''),
(747, '', 'Passive', 'Indian Restaurant', 'Kathmandu Kitchen', 'Nimalama', 'x', '01753681981', '', '07883036807', '', '', 'Arora Pk Hotel Old Bath Road', '', '', 'Poyle', 'Berkshire', 'SL3 0NZ', '', '', NULL, '', '', '', '', '', ''),
(748, '', 'Passive', 'Indian Restaurant', 'Raj', 'Inam', 'Hoque', '02089656036', '', '', '', '', '43 Chamberlayne Road', '', '', 'London', 'London', 'NW10 3NB', '', '', NULL, '', '', '', '', '', ''),
(749, '', 'Passive', 'Indian Restaurant', 'Sylhet', 'Lil', 'Miah', '02088455213', '', '07974227474', '', '', '1 Oakleigh Road North', '', '', 'Whetstone', 'London', 'N20 9HE', '', '', NULL, '', '', '', '', '', ''),
(750, '', 'Passive', 'Indian Restaurant', 'Rajput', 'Ruman', 'Miah', '01932354551', '', '07908764299', '', '', '70 High Road', '', '', 'Byfleet', 'Surrey', 'KT14 7QL', 'home: 01895812338', '', NULL, '', '', '', '', '', ''),
(751, '', 'Passive', 'Indian Restaurant', 'The Bayleaf', 'Abdul', 'Miah', '02084227255', '', '', '', '', '1280 Greenford Road', '', '', 'Greenford', 'Middlesex', 'UB6 0HH', '', '', NULL, '', '', '', '', '', ''),
(752, '', 'Passive', 'Indian Restaurant', 'Bengal Spice', 'M', 'Islam', '02077354865', '', '07957390888', '', '', '353 Kennington Lane', '', '', 'London', 'London', 'SE11 5QY', 'conact Mr.Joynal', '', NULL, '', '', '', '', '', ''),
(753, '', 'Passive', 'Indian Restaurant', 'Khans Restaurant', 'Hafiz', 'x', '02079784455', '', '07956171152', '', '', '159 Lavender Hill', '', '', 'London', 'London', 'SW11 5QH', '', '', NULL, '', '', '', '', '', ''),
(754, '', 'Passive', 'Indian Restaurant', 'Indigo Restaurant', 'Iqbal', 'X', '01908261471', '', '', '', '', '34 High Street', '', '', 'Stoney Stratford', 'Buckinghamshire', 'MK11 1AF', '', '', NULL, '', '', '', '', '', ''),
(755, '', 'Passive', 'Indian Restaurant', 'Taste of Nawab', 'Abdul', 'Rahman', '0208886429', '0208446046', '07984636117', '', '', '97 Colney Hatch Lane', '', '', 'London', 'London', 'N10 1LR', '', '', NULL, '', '', '', '', '', ''),
(762, '2005-03-09 14:10:00', 'Active', 'Indian Restaurant', 'Khyber Pass', 'Ali', 'X', '01932225670', '', '', '', '', '54 Terrace Road', '', '', 'Walton on Thames', 'Surrey', 'KT12 2SA', '', '', NULL, '', '', '', '', '', '');
INSERT INTO `customer` (`customer_id`, `reg_date`, `customer_status`, `business_type`, `business_name`, `first_name`, `surname`, `telephone`, `fax`, `mobile`, `email`, `web`, `address1`, `address2`, `area`, `city`, `county`, `postcode`, `comments`, `alternate_address`, `status`, `delivery_address1`, `delivery_address2`, `delivery_area`, `delivery_city`, `delivery_county`, `delivery_postcode`) VALUES
(763, '2005-09-09 12:08:00', 'Passive', 'Indian Restaurant', 'Blue Ginger', 'Mustak', 'X', '01235821335', '', '', '', '', '6 High Street', '', '', 'Steventon', 'Oxfordshire', 'OX13 6RX', '', '', NULL, '59 Ambrook Road', '', 'Whitley', 'Reading', 'Berkshire', 'RG2 8SJ                                           '),
(770, '2005-03-10 17:07:00', 'Active', 'Indian Restaurant', 'Ryath Indian Restaurant', 'M A', 'Shah', '02077238954', '', '07906213536', '', '', '32 Norfolk Place', '', 'Paddington', 'London', 'London', 'W2 1QH', '', '', NULL, '', '', '', '', '', ''),
(771, '2005-04-10 17:29:00', 'Active', 'Indian Restaurant', 'The Rose Valley', 'Mizan', 'X', '01483572572', '', '07949008780', '', '', '50 Chertsey Street', '', '', 'Guilford', 'Surrey', 'GU1 4HD', '', '', NULL, '', '', '', '', '', ''),
(781, '2005-02-11 17:19:00', 'Active', 'Mini Cab', 'Heathrow Express Cars', 'Abdul', 'Mehraban', '02089974449', '', '07950881503', '', '', '11 Royal Parade', '', '', 'London', 'London', 'W5 1ET', '', '', NULL, '', '', '', '', '', ''),
(782, '2005-05-11 18:23:00', 'Active', 'Office', 'Ital Catering Equipment', 'Isaak', 'X', '02087494832', '', '07956076185', '', '', '91 Old Oak Common Lane', '', '', 'London', 'London', 'W3 7DD', 'cont:Cris', '', NULL, '', '', '', '', '', ''),
(783, '2005-05-11 18:35:00', 'Active', 'Indian Restaurant', 'Blue Ginger', 'Tarif', 'Miah', '01895272888', '', '07876500432', '', '', '5 Dellfied Parade', 'High Street', '', 'Cowley', 'Middlesex', 'UB8 2EN', '', '', NULL, '', '', '', '', '', ''),
(784, '2005-07-11 13:28:00', 'Passive', 'Indian Restaurant', 'Teddington Tandoori', 'Motin', 'Miah', '02089775593', '', '', '', '', '176 High Street', '', '', 'Teddington', 'Middlesex', 'TW11 8HU', '', '', NULL, '', '', '', '', '', ''),
(785, '2005-07-11 17:15:00', 'Active', 'Indian Restaurant', 'Taj Mahal', 'Nazrul', 'Islam', '02087885941', '', '', '', '', '150 Upper Richmond Road', '', '', 'London', 'London', 'SW15 2SW', '', '', NULL, '', '', '', '', '', ''),
(786, '2005-07-11 18:06:00', 'Active', 'Indian Restaurant', 'Emperor of India', 'Azmol', 'Ali', '01628624100', '', '07973111368', '', '', '5 Windsor Road', 'Bray Wick', '', 'Maidenhead', 'Berkshire', 'SL6 1UZ', '', '', NULL, '50 Kingsfied Ave', '', '', 'Harrow', 'Middlesex', 'HA2 6AT                                           '),
(787, '2005-09-11 00:20:00', 'Active', 'Indian Restaurant', 'Bollywood', 'Shokath', 'X', '01622690931', '', '', '', '', '26 Lowerstone Street', '', '', 'Maidstone', 'Kent', 'ME15 6LA', 'Babul 07961563395\r\n9 Frederick St, WC1X 0NF', '', NULL, '', '', '', '', '', ''),
(788, '2005-09-11 03:05:00', 'Active', 'Indian Restaurant', 'Raj Restaurant', 'Raju', 'Ahmed', '01993703430', '', '07958567387', '', '', '74 High Street', '', '', 'Witney', 'Oxfordshire', 'OX28 6HL', 'Home 01993866096', '', NULL, '82 Campion Way', '', '', 'Witney', 'Oxfordshire', 'OX28 1ES                                          '),
(789, '2005-10-11 00:06:00', 'Active', 'Indian Restaurant', 'Saffron', 'Jiblu', 'Rahman', '01923680077', '', '07830817229', '', '', '113 Court Lends Drive', '', '', 'Watford', 'Hertfordshire', 'WD17 4HZ', '', '', NULL, '44 Whitworth House', 'Falmouth Street', '', 'London', 'London', 'SE1 6RW                                           '),
(790, '2005-11-11 00:19:00', 'Active', 'Indian Restaurant', 'Hillside Tandoori', 'Shammim', 'X', '02085085287', '', '', '', '', '128 Church Hill', '', '', 'Loughton', 'Essex', 'IG10 1LH', '', '', NULL, '', '', '', '', '', ''),
(791, '', 'Active', 'Indian Restaurant', 'Joba\'s Takeway', 'Shahed', 'Miah', '0182766338', '', '07969911390', '', '', '81b Bolebridge Street', '', '', 'Tamworth', 'Staffordshire', 'B79 7PD', '', '', NULL, '7 Poplar Drive', '', '', 'Saltley,  B,Ham', 'Warwickshire', 'B8 1QW                                            '),
(792, '', 'Active', 'Indian Restaurant', 'Shenfield Tandoori', 'K', 'Rahman', '01277234087', '', '07706156696', '', '', '152 Hutton Road', '', '', 'Shenfield', 'Essex', 'CM15 8NL', '', '', NULL, '', '', '', '', '', ''),
(793, '', 'Active', 'Indian Restaurant', 'Ali Raj', 'Abdul', 'Rashid', '01423521627', '', '07765862614', '', '', '7 Cheltenham Crescent', '', '', 'Harrogate', 'Yorkshire', 'HG1 1DH', '', '', NULL, '68 Upper Rushton Road', '', '', 'Bradford', 'Yorkshire', 'BD3 7LQ                                           '),
(794, '', 'Active', 'Indian Restaurant', 'Saffron Restaurant', 'M', 'Quadir', '01524417788', '', '07818448544', '', '', '6 Skipton Street', '', '', 'Morecambe', 'Lancashire', 'LA4 4AR', '', '', NULL, '7 Hyndburn Close', '', '', 'Morecambe', 'Lancashire', 'LA3 3SQ                                           '),
(795, '', 'Active', 'Supplier Restaurant', 'Indian Shop', 'S', 'DasGupta', '02201315', '0229510136', '', '', '', 'Via Palermo 3/5', '', '', '20090 Assago (M1)', 'Italy', 'ITALY', '', '', NULL, '', '', '', '', '', ''),
(796, '', 'Active', 'Indian Restaurant', 'Curry Delight', 'Motin', 'X', '02085739900', '', '', '', '', '1 North Parade', 'North Hyde Road', '', 'Hayes', 'Middlesex', 'UB3 4JA', '', '', NULL, '', '', '', '', '', ''),
(797, '', 'Active', 'Indian Restaurant', 'Cafe Sekander', 'X', 'Ali', '01517244300', '', '', '', '', '165 Allerton Road', 'Mossley Hill', '', 'Liverpool', 'Merseyside', 'L18 6HG', '', '', NULL, '', '', '', '', '', ''),
(798, '', 'Active', 'Indian Restaurant', 'Spice of India', 'F R', 'Ghuuri', '01925444442', '', '07818441191', '', '', '110 Bridge Street', '', '', 'Warrington', 'Lancashire', 'WA1 2RU', '', '', NULL, '1 Oakleigh Avenue', '', '', 'Manchester', 'Lincolnshire', 'M19 2WP                                           '),
(800, '2018-02-17 16:25:00', 'Active', 'Indian Restaurant', 'Taste of punjab', 'Ali', 'Butt', '02089609925', '', '', '', '', '316B', 'Ladbroke grove', '', 'London', 'London', 'W10 5NQ', '', '', NULL, '', '', '', '', '', ''),
(801, '2018-06-07 19:35:00', 'Active', 'Indian Restaurant', 'Misbah', 'A', 'Ali', '07737944514', '', '', 'misbah@smartprintltd.co.uk', '', '1a', 'Honeywood rd', '', 'London', 'London', 'NW10 4UU', '', '', NULL, '', '', '', 'London', 'London', 'NW10 4UU                                          '),
(802, '2018-08-15 23:27:00', 'Active', 'IT', 'Test 1', 'Razeen', 'Abdal-Rahman', '02088382747', '', '', '', '', '1a', 'Honeywood rd', '', 'London', 'London', 'NW10 4UU', '', '', NULL, '', '', '', '', '', ''),
(803, '2018-08-16 16:13:00', 'Active', '2', 'Arbab', 'Muhammad', 'Arbab', '07918258080', NULL, NULL, 'accounts@sttelco.co.uk', NULL, '47 - 49 Park Royal Road', NULL, NULL, 'London', 'Antigua and Barbuda', 'NW10 7LQ', NULL, '', 1, '', '', '', 'London', 'London', 'NW10 7LQ                                          '),
(804, '2020-12-10 10:24:00', 'Active', 'Builder', 'Test', 'Test', 'Test', '123456789', '', '', '', '', 'Gtest', '', '', 'Test', 'Italy', '1211', '', '', NULL, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `extra`
--

CREATE TABLE `extra` (
  `extra_id` smallint(6) NOT NULL,
  `job_id` varchar(5) DEFAULT NULL,
  `description` varchar(12) DEFAULT NULL,
  `vat` decimal(3,1) DEFAULT NULL,
  `net_price` decimal(8,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `extra`
--

INSERT INTO `extra` (`extra_id`, `job_id`, `description`, `vat`, `net_price`) VALUES
(118, '1050C', 'menu on back', '17.5', '50.0000'),
(120, '1055C', 'Menu on back', '17.5', '50.0000'),
(121, '1057C', 'Menu on back', '17.5', '50.0000'),
(122, '1060C', 'Menu on back', '17.5', '30.0000'),
(123, '1207W', '12321', '17.5', '1556.0000'),
(124, '1015P', 'delivery', '17.5', '15.0000'),
(125, '1015P', 'Menu on back', '0.0', '23.0000'),
(126, '1P', '22', '17.5', '33.0000'),
(127, '1P', '2', '17.5', '43.0000');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` smallint(6) NOT NULL,
  `final` tinyint(4) DEFAULT 0,
  `customer_id` smallint(6) DEFAULT NULL,
  `invoice_date` varchar(19) DEFAULT NULL,
  `net_delivery_price` decimal(6,4) DEFAULT 0.0000,
  `delivery_vat` decimal(3,1) DEFAULT 0.0,
  `deposit` decimal(5,4) DEFAULT 0.0000,
  `discount` decimal(7,4) DEFAULT 0.0000,
  `show_discount` tinyint(4) DEFAULT 0,
  `paid_date` datetime DEFAULT NULL,
  `paid` tinyint(4) DEFAULT 0,
  `comments` varchar(0) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `final`, `customer_id`, `invoice_date`, `net_delivery_price`, `delivery_vat`, `deposit`, `discount`, `show_discount`, `paid_date`, `paid`, `comments`) VALUES
(2, 0, 804, '2021-02-23 00:00:00', NULL, '17.5', '0.0000', '0.0000', 1, NULL, 0, NULL),
(6, 0, 803, '2021-02-23 00:00:00', NULL, '17.5', '0.0000', NULL, 1, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `job_id` varchar(5) NOT NULL,
  `invoice_id` varchar(4) DEFAULT NULL,
  `customer_id` smallint(12) DEFAULT NULL,
  `reg_date` datetime DEFAULT NULL,
  `type` varchar(34) DEFAULT NULL,
  `description` varchar(245) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `category` varchar(17) DEFAULT NULL,
  `quantity` mediumint(9) DEFAULT NULL,
  `reprint_id` varchar(5) DEFAULT NULL,
  `colours` varchar(134) DEFAULT NULL,
  `colour1` varchar(2345) DEFAULT NULL,
  `colour2` varchar(435) DEFAULT NULL,
  `colour3` varchar(4565) DEFAULT NULL,
  `net_price` decimal(8,4) DEFAULT NULL,
  `vat` decimal(3,1) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `to_delivery_address` tinyint(4) DEFAULT NULL,
  `order_taken_by` varchar(15) DEFAULT NULL,
  `last_updated_by` varchar(15) DEFAULT NULL,
  `last_updated` varchar(19) DEFAULT NULL,
  `comments` varchar(5443) DEFAULT NULL,
  `prog_type` tinyint(4) DEFAULT NULL,
  `prog_proof` tinyint(4) DEFAULT NULL,
  `prog_confirmed` tinyint(4) DEFAULT NULL,
  `prog_film` tinyint(4) DEFAULT NULL,
  `prog_printed` tinyint(4) DEFAULT NULL,
  `prog_finished` tinyint(4) DEFAULT NULL,
  `prog_delivered` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`job_id`, `invoice_id`, `customer_id`, `reg_date`, `type`, `description`, `status`, `category`, `quantity`, `reprint_id`, `colours`, `colour1`, `colour2`, `colour3`, `net_price`, `vat`, `to_delivery_address`, `order_taken_by`, `last_updated_by`, `last_updated`, `comments`, `prog_type`, `prog_proof`, `prog_confirmed`, `prog_film`, `prog_printed`, `prog_finished`, `prog_delivered`) VALUES
('1P', NULL, 803, '2021-02-06 14:01:21', 'P', 'Bill Books 50 set', 'A', 'Bill Books 50 set', 234, NULL, '1', 'black', NULL, NULL, '32.0000', '0.0', NULL, '1', NULL, NULL, 'for test', 0, 0, 0, 0, 0, 0, 0),
('2C', NULL, 800, '2021-02-15 12:41:05', 'C', 'ss', 'A', 'Bill Books 50 set', 4, NULL, '1', 'black', NULL, NULL, '3.0000', '0.0', NULL, '1', NULL, NULL, NULL, 1, 1, 0, 0, 0, 0, 0),
('3W', '2', 804, '2021-02-15 12:41:54', 'W', 'as', 'D', 'Bill Books 50 set', 2, NULL, NULL, NULL, NULL, NULL, '2.0000', '0.0', NULL, '1', NULL, NULL, '22', 1, 1, 1, 1, 1, 1, 1),
('4X', '2', 804, '2021-02-15 12:43:52', 'X', 's', 'A', 'Bill Books 50 set', 34, NULL, NULL, NULL, NULL, NULL, '43.0000', '0.0', NULL, '1', NULL, NULL, 'd', 1, 1, 1, 1, 0, 0, 0),
('5X', NULL, 803, '2021-02-15 12:44:25', 'X', 'ds', 'A', 'Bill Books 50 set', 4, NULL, NULL, NULL, NULL, NULL, '3.0000', '0.0', NULL, '1', NULL, NULL, 'fddf', 1, 1, 1, 1, NULL, NULL, NULL),
('6C', NULL, 803, '2021-02-15 12:46:43', 'C', 'saa', 'A', 'Bill Books 50 set', 7, NULL, '1', 'sa', NULL, NULL, '32.0000', '0.0', NULL, '1', NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, NULL),
('6Q', '6', 803, '2021-02-15 12:46:43', 'C', 'saa', 'A', 'Bill Books 50 set', 7, NULL, '1', 'sa', NULL, NULL, '32.0000', '0.0', NULL, '1', NULL, NULL, NULL, 1, 1, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_12_07_061053_create_staff_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `otherjob`
--

CREATE TABLE `otherjob` (
  `job_id` varchar(52) NOT NULL,
  `comment` varchar(6043) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `otherjob`
--

INSERT INTO `otherjob` (`job_id`, `comment`) VALUES
('4X', 'sd'),
('5X', 'd');

-- --------------------------------------------------------

--
-- Table structure for table `printing`
--

CREATE TABLE `printing` (
  `job_id` varchar(5) NOT NULL,
  `design_folds` varchar(43) DEFAULT '0',
  `design_comments` varchar(5532) DEFAULT '',
  `note` varchar(255) DEFAULT NULL,
  `design_folded_pages` varchar(105) DEFAULT '0',
  `design_unfolded_size` varchar(143) DEFAULT '',
  `design_unfolded_pages` varchar(1145) DEFAULT '0',
  `design_folded_size` varchar(10) DEFAULT '',
  `design_artwork` tinyint(4) DEFAULT 0,
  `design_logo` tinyint(4) DEFAULT 0,
  `design_typesetting` tinyint(4) DEFAULT 0,
  `design_scan` tinyint(4) DEFAULT 0,
  `design_photography` tinyint(4) DEFAULT 0,
  `design_stockphotos` tinyint(4) DEFAULT 0,
  `design_orientation` varchar(234) DEFAULT '',
  `design_double_side` varchar(547) DEFAULT '',
  `prepress_no_of_up` varchar(234) NOT NULL,
  `prepress_setupsize` varchar(234) DEFAULT '',
  `prepress_emailto` varchar(234) DEFAULT '',
  `prepress_setup_type` varchar(234) DEFAULT '',
  `prepress_cd` tinyint(234) DEFAULT 0,
  `prepress_email` tinyint(234) DEFAULT 0,
  `prepress_dig_or_film` varchar(234) DEFAULT '',
  `prepress_platform` varchar(234) DEFAULT '',
  `prepress_where` varchar(234) DEFAULT '',
  `prepress_orientation` varchar(234) DEFAULT '',
  `press_no_of_plates` varchar(234) DEFAULT '',
  `press_machine_run` varchar(234) DEFAULT '',
  `press_comments` varchar(24) DEFAULT '',
  `press_printwhere` varchar(234) DEFAULT '',
  `press_makeready` tinyint(4) DEFAULT 0,
  `paper_thickness` varchar(234) DEFAULT '',
  `paper_quantity` varchar(234) DEFAULT '',
  `paper_comments` varchar(2342) DEFAULT '',
  `paper_colour` varchar(234) DEFAULT '',
  `paper_size` varchar(2342) DEFAULT '',
  `paper_type` varchar(203) DEFAULT '',
  `paper_brand` varchar(152) DEFAULT '',
  `finish_comments` text DEFAULT '',
  `finish_binding` varchar(111) DEFAULT '',
  `finish_finish` varchar(16) DEFAULT '',
  `finish_delivery` varchar(11) DEFAULT '',
  `finish_cutting` tinyint(4) DEFAULT 0,
  `finish_creasing` tinyint(4) DEFAULT 0,
  `finish_folding` tinyint(4) DEFAULT 0,
  `finish_perforation` tinyint(4) DEFAULT 0,
  `finish_numbering` tinyint(4) DEFAULT 0,
  `finish_boxing` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `printing`
--

INSERT INTO `printing` (`job_id`, `design_folds`, `design_comments`, `design_folded_pages`, `design_unfolded_size`, `design_unfolded_pages`, `design_folded_size`, `design_artwork`, `design_logo`, `design_typesetting`, `design_scan`, `design_photography`, `design_stockphotos`, `design_orientation`, `design_double_side`, `prepress_no_of_up`, `prepress_setupsize`, `prepress_emailto`, `prepress_setup_type`, `prepress_cd`, `prepress_email`, `prepress_dig_or_film`, `prepress_platform`, `prepress_where`, `prepress_orientation`, `press_no_of_plates`, `press_machine_run`, `press_comments`, `press_printwhere`, `press_makeready`, `paper_thickness`, `paper_quantity`, `paper_comments`, `paper_colour`, `paper_size`, `paper_type`, `paper_brand`, `finish_comments`, `finish_binding`, `finish_finish`, `finish_delivery`, `finish_cutting`, `finish_creasing`, `finish_folding`, `finish_perforation`, `finish_numbering`, `finish_boxing`) VALUES
('1P', '', 'test  ', '12', '1', '0', 'Not Folded', 1, 1, 0, 1, 0, 1, 'Portrait', 'Single', '0', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `roleId` tinyint(4) NOT NULL,
  `roleName` varchar(13) DEFAULT NULL,
  `description` varchar(0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleId`, `roleName`, `description`) VALUES
(1, 'administrator', ''),
(2, 'manager', ''),
(3, 'secretary', ''),
(4, 'designer', ''),
(5, 'printer', ''),
(6, 'finisher', '');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `role_id` tinyint(4) DEFAULT NULL,
  `permission_id` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 101),
(1, 102),
(1, 103),
(1, 104),
(1, 105),
(1, 106),
(1, 107),
(1, 108),
(2, 1),
(2, 2),
(2, 101),
(2, 102),
(2, 103),
(2, 104),
(2, 105),
(2, 106),
(3, 101),
(3, 106),
(4, 102),
(4, 103);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staffId` int(11) NOT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `loginName` varchar(255) DEFAULT NULL,
  `password` varchar(550) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `roleId` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 for active 1 for deactive',
  `createdDate` date DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `modifyDate` date DEFAULT NULL,
  `modifyBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staffId`, `firstName`, `lastName`, `loginName`, `password`, `startDate`, `endDate`, `comment`, `roleId`, `status`, `createdDate`, `createdBy`, `modifyDate`, `modifyBy`) VALUES
(1, 'Bansi', 'Patel', 'bansis', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-01', '2021-05-06', NULL, '1', '0', '2021-01-18', 1, '2021-01-18', 1),
(2, 'Pradip', 'Kor', 'pdkor', 'e10adc3949ba59abbe56e057f20f883e', '2021-01-04', '2021-02-02', NULL, '1', '0', '2021-01-18', 1, '2021-01-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff_role`
--

CREATE TABLE `staff_role` (
  `staff_id` smallint(6) DEFAULT NULL,
  `role_id` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff_role`
--

INSERT INTO `staff_role` (`staff_id`, `role_id`) VALUES
(101, 2),
(102, 4),
(103, 6),
(104, 1),
(104, 4),
(105, 5),
(106, 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` smallint(6) NOT NULL,
  `reg_date` varchar(19) DEFAULT NULL,
  `supplier_status` varchar(7) DEFAULT NULL,
  `business_type` varchar(20) DEFAULT NULL,
  `business_name` varchar(36) DEFAULT NULL,
  `first_name` varchar(18) DEFAULT NULL,
  `surname` varchar(12) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `fax` varchar(13) DEFAULT NULL,
  `mobile` varchar(12) DEFAULT NULL,
  `email` varchar(26) DEFAULT NULL,
  `web` varchar(0) DEFAULT NULL,
  `address1` varchar(36) DEFAULT NULL,
  `address2` varchar(15) DEFAULT NULL,
  `area` varchar(10) DEFAULT NULL,
  `city` varchar(23) DEFAULT NULL,
  `county` varchar(19) DEFAULT NULL,
  `postcode` varchar(9) DEFAULT NULL,
  `comments` varchar(133) DEFAULT NULL,
  `alternate_address` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `delivery_address1` varchar(22) DEFAULT NULL,
  `delivery_address2` varchar(15) DEFAULT NULL,
  `delivery_area` varchar(18) DEFAULT NULL,
  `delivery_city` varchar(19) DEFAULT NULL,
  `delivery_county` varchar(15) DEFAULT NULL,
  `delivery_postcode` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `reg_date`, `supplier_status`, `business_type`, `business_name`, `first_name`, `surname`, `telephone`, `fax`, `mobile`, `email`, `web`, `address1`, `address2`, `area`, `city`, `county`, `postcode`, `comments`, `alternate_address`, `status`, `delivery_address1`, `delivery_address2`, `delivery_area`, `delivery_city`, `delivery_county`, `delivery_postcode`) VALUES
(763, '2005-09-09 12:08:00', 'Passive', 'Indian Restaurant', 'Blue Ginger', 'Mustak', 'X', '01235821335', '', '', '', '', '6 High Street', '', '', 'Steventon', 'Oxfordshire', 'OX13 6RX', '', '', NULL, '59 Ambrook Road', '', 'Whitley', 'Reading', 'Berkshire', 'RG2 8SJ                                           '),
(771, '2005-04-10 17:29:00', 'Active', 'Indian Restaurant', 'The Rose Valley', 'Mizan', 'X', '01483572572', '', '07949008780', '', '', '50 Chertsey Street', '', '', 'Guilford', 'Surrey', 'GU1 4HD', '', '', NULL, '', '', '', '', '', ''),
(781, '2005-02-11 17:19:00', 'Active', 'Mini Cab', 'Heathrow Express Cars', 'Abdul', 'Mehraban', '02089974449', '', '07950881503', '', '', '11 Royal Parade', '', '', 'London', 'London', 'W5 1ET', '', '', NULL, '', '', '', '', '', ''),
(792, '', 'Active', 'Indian Restaurant', 'Shenfield Tandoori', 'K', 'Rahman', '01277234087', '', '07706156696', '', '', '152 Hutton Road', '', '', 'Shenfield', 'Essex', 'CM15 8NL', '', '', NULL, '', '', '', '', '', ''),
(793, '', 'Active', 'Indian Restaurant', 'Ali Raj', 'Abdul', 'Rashid', '01423521627', NULL, '07765862614', NULL, NULL, '7 Cheltenham Crescent', NULL, NULL, 'Harrogate', 'American Samoa', 'HG1 1DH', NULL, '', 1, '68 Upper Rushton Road', '', '', 'Bradford', 'Yorkshire', 'BD3 7LQ                                           '),
(796, '', 'Active', 'Indian Restaurant', 'Curry Delight', 'Motin', 'X', '02085739900', '', '', '', '', '1 North Parade', 'North Hyde Road', '', 'Hayes', 'Middlesex', 'UB3 4JA', '', '', NULL, '', '', '', '', '', ''),
(797, '', 'Active', 'Indian Restaurant', 'Cafe Sekander', 'X', 'Ali', '01517244300', NULL, NULL, NULL, NULL, '165 Allerton Road', 'Mossley Hill', NULL, 'Liverpool', 'Albania', 'L18 6HG', NULL, '', 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `web`
--

CREATE TABLE `web` (
  `job_id` varchar(5) NOT NULL,
  `url` varchar(532) DEFAULT NULL,
  `name_expiry_date` varchar(1911) DEFAULT NULL,
  `domain_tag` varchar(634) DEFAULT NULL,
  `hosting_company` varchar(632) DEFAULT NULL,
  `accoun_type` varchar(532) DEFAULT NULL,
  `account_username` varchar(433) DEFAULT NULL,
  `account_password` int(255) DEFAULT NULL,
  `ftp` varchar(2152) NOT NULL,
  `ftp_address` varchar(3435) DEFAULT NULL,
  `ftp_username` varchar(448) DEFAULT NULL,
  `p3_server_address` varchar(634) DEFAULT NULL,
  `p3_username` varchar(622) DEFAULT NULL,
  `expired` tinyint(43) DEFAULT NULL,
  `comments` varchar(823) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`accountId`);

--
-- Indexes for table `alternate_address`
--
ALTER TABLE `alternate_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bussiness_type`
--
ALTER TABLE `bussiness_type`
  ADD PRIMARY KEY (`typeId`);

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `extra`
--
ALTER TABLE `extra`
  ADD PRIMARY KEY (`extra_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otherjob`
--
ALTER TABLE `otherjob`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `printing`
--
ALTER TABLE `printing`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staffId`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `web`
--
ALTER TABLE `web`
  ADD PRIMARY KEY (`job_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alternate_address`
--
ALTER TABLE `alternate_address`
  MODIFY `id` int(151) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bussiness_type`
--
ALTER TABLE `bussiness_type`
  MODIFY `typeId` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `extra`
--
ALTER TABLE `extra`
  MODIFY `extra_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `roleId` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staffId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
