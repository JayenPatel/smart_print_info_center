-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2021 at 11:59 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_print`
--

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `permited_roles` varchar(255) NOT NULL DEFAULT '1' COMMENT 'Role IDs in comma seperated format',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1- active 0- inactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` varchar(255) DEFAULT NULL COMMENT 'For softdelete purpose'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_name`, `permited_roles`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'dashboard', '2,3,4,5,6,1', 1, '2021-07-26 05:59:45', '2021-07-26 08:37:55', NULL),
(2, 'customers', '1,2,3', 1, '2021-07-26 05:59:45', '2021-07-26 06:02:16', NULL),
(3, 'financies', '1,2,3', 1, '2021-07-26 05:59:45', '2021-07-26 06:02:38', NULL),
(4, 'jobs', '1,2,4', 1, '2021-07-26 05:59:45', '2021-07-26 06:01:50', NULL),
(5, 'suppliers', '1,2,3,4,5,6', 1, '2021-07-26 05:59:45', '2021-07-26 05:59:45', NULL),
(6, 'staffs', '1,2', 1, '2021-07-26 05:59:45', '2021-07-26 06:01:50', NULL),
(7, 'tasks', '1,2,3,4,5', 1, '2021-07-26 05:59:45', '2021-07-26 06:03:16', NULL),
(8, 'reports', '1,2,3,4,5,6', 1, '2021-07-26 05:59:45', '2021-07-26 05:59:45', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
