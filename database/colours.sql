-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2021 at 03:26 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_print`
--

-- --------------------------------------------------------

--
-- Table structure for table `colours`
--

CREATE TABLE `colours` (
  `id` int(11) NOT NULL,
  `colour` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `colours`
--

INSERT INTO `colours` (`id`, `colour`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'red', 'print', '2021-07-24 07:48:24', '2021-07-24 07:48:24', NULL),
(2, 'red', 'other', '2021-07-24 07:48:24', '2021-07-24 07:48:24', NULL),
(3, 'red', 'web', '2021-07-24 07:48:24', '2021-07-24 07:48:24', NULL),
(4, 'red', 'calender', '2021-07-24 07:48:24', '2021-07-24 07:55:50', NULL),
(5, 'yellow', 'print', '2021-07-24 09:16:29', '2021-07-24 09:29:03', NULL),
(6, 'pink', 'print', '2021-07-24 09:55:51', '2021-07-24 09:59:47', NULL),
(7, 'yellow', 'calender', '2021-07-24 10:11:14', '2021-07-24 10:16:06', NULL),
(8, 'pink', 'calender', '2021-07-24 10:13:52', '2021-07-24 10:17:22', NULL),
(9, 'yellow', 'other', '2021-07-24 10:22:28', '2021-07-24 10:22:28', NULL),
(10, 'pink', 'other', '2021-07-24 10:22:40', '2021-07-24 10:23:03', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `colours`
--
ALTER TABLE `colours`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `colours`
--
ALTER TABLE `colours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
