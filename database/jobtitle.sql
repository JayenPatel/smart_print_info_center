-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2021 at 03:27 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_print`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobtitle`
--

CREATE TABLE `jobtitle` (
  `id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobtitle`
--

INSERT INTO `jobtitle` (`id`, `job_title`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'New Traditional title', 'print', '2021-07-24 11:52:56', '2021-07-24 12:15:46', NULL),
(2, 'Print litho', 'print', '2021-07-24 11:54:33', '2021-07-24 11:54:33', NULL),
(3, 'Demo print Job title', 'other', '2021-07-24 12:01:30', '2021-07-24 12:22:55', NULL),
(4, 'Other Job Title', 'other', '2021-07-24 12:15:05', '2021-07-24 12:15:05', NULL),
(5, 'Calender Job title', 'calender', '2021-07-24 12:21:54', '2021-07-24 12:22:51', NULL),
(6, 'New Testing calender Jobtitle', 'calender', '2021-07-24 12:22:23', '2021-07-24 12:22:23', NULL),
(7, 'test print', 'calender', '2021-07-24 12:24:38', '2021-07-24 12:24:38', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobtitle`
--
ALTER TABLE `jobtitle`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobtitle`
--
ALTER TABLE `jobtitle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
