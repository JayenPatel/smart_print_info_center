<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

    Route::namespace('Admin')->group(function () {
    // Login
    Route::group(['middleware' => ['CheckLogin']], function () {
        Route::get('/', 'LoginAndRegisterController@Login')->name('admin.login');
        Route::post('admin/doLogin', 'LoginAndRegisterController@doLogin')->name('admin.doLogin');

    });

    Route::group(['middleware' => ['checkuser']], function () {

        // Dashboard
        Route::get('admin/dashboard', 'DashboardController@index')->name('admin.dashboard');
        Route::post('admin/getDashboardData', 'DashboardController@getDashboardData')->name('admin.getDashboardData');
        //Header

        // Route::get('', 'HeaderController@printJob')->name('admin.header.print-job');
        Route::get('admin/print-job', 'HeaderController@printJob')->name('admin.header.print-job');
        Route::get('admin/calendar', 'HeaderController@calendar')->name('admin.header.calendar');
        Route::get('admin/other-job', 'HeaderController@otherJob')->name('admin.header.other-job');
        Route::get('admin/web-job', 'HeaderController@webJob')->name('admin.header.web-job');
        Route::get('admin/quotes', 'HeaderController@quotes')->name('admin.header.quotes');
        Route::post('admin/update', 'HeaderController@update')->name('admin.header.update');
        Route::get('admin/exportJobs/{type}', 'HeaderController@exportJobs')->name('admin.header.export');
        Route::get('admin/exportJobsPDF/{type}', 'HeaderController@exportJobsPDF')->name('admin.header.exporPDF');
        Route::get('admin/exportothers/{type}', 'HeaderController@exportothers')->name('admin.header.exportother');
        Route::get('admin/exportothersPDF/{type}', 'HeaderController@exportothersPDF')->name('admin.header.exportothersPDF');
    });
});

// Finance
Route::namespace('Admin\FinanciesControllers')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {

        //Invoice
        Route::get('admin/financies/invoices', 'invoiceController@index')->name('admin.invoices.list');
        Route::get('admin/financies/invoices/add', 'invoiceController@create')->name('admin.invoices.add');
        Route::get('admin/financies/invoices/add/{id}', 'invoiceController@create')->name('admin.invoices.addWithJob');
       
        Route::post('admin/financies/invoices/delete', 'invoiceController@delete')->name('admin.invoices.delete');
       
        Route::get('admin/financies/invoices/bill', 'invoiceController@bill')->name('admin.invoices.bill');
 
      
        Route::get('admin/financies/invoices/edit/{id}', 'invoiceController@edit')->name('admin.invoices.edit');
        Route::post('admin/financies/invoices/customerData', 'invoiceController@customerData')->name('admin.invoices.customerData');
        Route::post('admin/financies/invoices/addInvoice', 'invoiceController@addInvoice')->name('admin.invoices.addInvoice');
        Route::get('admin/financies/invoices/exportData', 'invoiceController@exportData')->name('admin.invoices.exportData');
        
        // Route::get('admin/financies/invoices/exportData', 'invoiceController@invoiceData')->name('admin.invoices.invoiceData');
        // Route::get('admin/financies/invoices/pdf', 'invoiceController@pdf')->name('admin.invoices.pdf');
        //Accounts
        Route::get('admin/financies/accounts', 'AccountController@create')->name('admin.accounts.list');
        Route::get('admin/financies/accounts/add', 'AccountController@create')->name('admin.accounts.add');
        Route::get('admin/financies/accounts/edit', 'AccountController@edit')->name('admin.accounts.edit');
    });
});

//Customer
Route::namespace('Admin\CustomersController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {

        Route::get('admin/customers/list', 'CustomerController@index')->name('admin.customers.list');
        Route::get('admin/customers/add', 'CustomerController@create')->name('admin.customers.create');
        Route::get('admin/customers/edit/{id}', 'CustomerController@edit')->name('admin.customers.edit');
        Route::post('admin/customers/getCustomerData', 'CustomerController@getCustomerData')->name('admin.customers.lists');
        Route::post('admin/customers/delete', 'CustomerController@delete')->name('admin.customers.delete');
        //add
        Route::post('admin/customers/store', 'CustomerController@store')->name('admin.customers.store');
        //add alternative address
        Route::get('admin/customers/address', 'CustomerController@Address')->name('admin.customers.address');
    });
});


//Jobs
Route::namespace('Admin\JobsController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {
        //Calendar
        Route::get('admin/jobs/calendars', 'CalendarSheetController@index')->name('admin.jobs.calendar.list');
        Route::get('admin/jobs/calendars/add', 'CalendarSheetController@create')->name('admin.jobs.calendar.add');
        Route::get('admin/jobs/calendars/edit/{id}', 'CalendarSheetController@edit')->name('admin.jobs.calendar.edit');
        
        Route::post('admin/jobs/calendars/delete', 'CalendarSheetController@delete')->name('admin.jobs.calendar.delete');
        Route::post('admin/jobs/calendars/submit', 'CalendarSheetController@submit')->name('admin.jobs.calendar.submit');

        //Other job sheet
        Route::get('admin/jobs/other_job_sheets', 'OtherJobSheetController@index')->name('admin.jobs.other_job_sheets.list');
        Route::get('admin/jobs/other_job_sheets/add', 'OtherJobSheetController@create')->name('admin.jobs.other_job_sheets.add');
       
        Route::post('admin/jobs/other_job_sheets/delete', 'OtherJobSheetController@delete')->name('admin.jobs.other_job_sheets.delete');
        Route::post('admin/jobs/other_job_sheets/submit', 'OtherJobSheetController@submit')->name('admin.jobs.other_job_sheets.submit');
        Route::get('admin/jobs/other_job_sheets/edit/{id}', 'OtherJobSheetController@edit')->name('admin.jobs.other_job_sheets.edit');

        //Print job sheet
        Route::get('admin/jobs/print_job_sheets', 'PrintJobSheetController@index')->name('admin.jobs.print_job_sheets.list');
        Route::get('admin/jobs/print_job_sheets/add', 'PrintJobSheetController@create')->name('admin.jobs.print_job_sheets.add');
        Route::post('admin/jobs/print_job_sheets/submit', 'PrintJobSheetController@submit')->name('admin.jobs.print_job_sheets.submit');
         Route::get('admin/jobs/print_job_sheets/edit/{id}', 'PrintJobSheetController@edit')->name('admin.jobs.print_job_sheets.edit');
         Route::post('admin/jobs/print_job_sheets/delete', 'PrintJobSheetController@delete')->name('admin.jobs.print_job_sheets.delete');
         Route::post('admin/financies/invoices/customerprintData', 'PrintJobSheetController@customerprintData')->name('admin.invoices.customerprintData');
        // Route::post('admin/jobs/print_job_sheets/delete', 'PrintJobSheetController@delete')->name('admin.jobs.print_job_sheets.delete');
        
        
        Route::post('admin/job/category', 'CalendarSheetController@addcategory');
        Route::post('admin/job/removecategory', 'CalendarSheetController@removecategory');

        Route::post('admin/job/colour', 'CalendarSheetController@addcolour');
        Route::post('admin/job/removecolour', 'CalendarSheetController@removecolour');

        Route::post('admin/job/jobtitle', 'PrintJobSheetController@addjobtitle');
        Route::post('admin/job/removejobtitle', 'PrintJobSheetController@removejobtitle');
       
        //Web job sheet
        Route::get('admin/jobs/web_job_sheets', 'WebJobSheetController@index')->name('admin.jobs.web_job_sheets.list');
        Route::get('admin/jobs/web_job_sheets/add', 'WebJobSheetController@create')->name('admin.jobs.web_job_sheets.add');
        Route::post('admin/jobs/web_job_sheets/submit', 'WebJobSheetController@submit')->name('admin.jobs.web_job_sheets.submit');
        Route::post('admin/jobs/web_job_sheets/delete', 'CalendarSheetController@delete')->name('admin.jobs.web_job_sheets.delete');
       
        Route::get('admin/jobs/web_job_sheets/edit/{id}', 'WebJobSheetController@edit')->name('admin.jobs.web_job_sheets.edit');
    });
});
// Suppliers
Route::namespace('Admin\SuppliersController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {
        Route::get('admin/suppliers/list', 'SupplierController@index')->name('admin.suppliers.list');
        Route::get('admin/suppliers/add', 'SupplierController@create')->name('admin.suppliers.create');
        Route::get('admin/suppliers/edit/{id}', 'SupplierController@edit')->name('admin.suppliers.edit');
        Route::post('admin/suppliers/getSupplierData', 'SupplierController@getSupplierData')->name('admin.suppliers.lists');
        Route::post('admin/suppliers/delete', 'SupplierController@delete')->name('admin.suppliers.delete');
        //add
        Route::post('admin/suppliers/store', 'SupplierController@store')->name('admin.suppliers.store');


    });
});
// Staff
Route::namespace('Admin\StaffsController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {

        Route::get('admin/staffs', 'StaffController@index')->name('admin.staffs.list');
        Route::get('admin/staffs/add', 'StaffController@create')->name('admin.staffs.create');
        Route::get('admin/staffs/edit/{id}', 'StaffController@edit')->name('admin.staffs.edit');
        Route::post('admin/staffs/store', 'StaffController@store')->name('admin.staffs.store');
        Route::post('admin/staffs/delete', 'StaffController@delete')->name('admin.staffs.delete');
        Route::post('admin/staffs/getStaffData', 'StaffController@getStaffData')->name('admin.staff.lists');

    });
});
// Staff
Route::namespace('Admin\RoleController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {

        Route::get('admin/role_permission/{role_id}', 'RoleController@index')->name('admin.roles.permission');
        Route::post('admin/role_permission/{role_id}', 'RoleController@store');

    });
});
// Task
Route::namespace('Admin\TasksController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {
        Route::get('admin/tasks', 'TaskController@index')->name('admin.tasks.list');
        Route::get('admin/tasks/add', 'TaskController@create')->name('admin.tasks.create');
        Route::post('admin/tasks/store', 'TaskController@store')->name('admin.tasks.store');
        Route::post('admin/tasks/getTaskData', 'TaskController@getTaskData')->name('admin.tasks.lists');
        Route::get('admin/tasks/edit/{id}', 'TaskController@edit')->name('admin.tasks.edit');
        Route::post('admin/tasks/delete', 'TaskController@delete')->name('admin.tasks.delete');
    });
});


// Profile
Route::namespace('Admin\ProfileController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {
        Route::post('admin/profile/store', 'ProfileController@store')->name('admin.profile.store');
        Route::get('admin/profile/edit/{id}', 'ProfileController@edit')->name('admin.profile.edit');
    });
});

// Product
Route::namespace('Admin\ProductController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {
        Route::get('admin/products', 'ProductController@index')->name('admin.products.list');
        Route::get('admin/products/add', 'ProductController@create')->name('admin.products.create');
        Route::post('admin/products/store', 'ProductController@store')->name('admin.products.store');
        Route::post('admin/products/getProductData', 'ProductController@getProductData')->name('admin.products.lists');
        Route::get('admin/products/edit/{id}', 'ProductController@edit')->name('admin.products.edit');
        Route::post('admin/products/delete', 'ProductController@delete')->name('admin.products.delete');
    });
});

// Product
Route::namespace('Admin\ReportsController')->group(function () {
    Route::group(['middleware' => ['checkuser']], function () {
        Route::get('admin/reports/user', 'ReportController@userreport')->name('admin.reports.user.list');
        Route::post('admin/reports/user', 'ReportController@getuserData');
        Route::get('admin/reports/product', 'ReportController@productreport')->name('admin.reports.product.list');
        Route::post('admin/reports/product', 'ReportController@getproductData');
        Route::get('admin/reports/print', 'ReportController@printreport')->name('admin.reports.print.list');
        Route::post('admin/reports/print', 'ReportController@getprintData');
        Route::get('admin/reports/web', 'ReportController@webreport')->name('admin.reports.web.list');
        Route::post('admin/reports/web', 'ReportController@getwebData');
        Route::get('admin/reports/other', 'ReportController@otherreport')->name('admin.reports.other.list');
        Route::post('admin/reports/other', 'ReportController@getotherData');
        Route::get('admin/reports/calender', 'ReportController@calenderreport')->name('admin.reports.calender.list');
        Route::post('admin/reports/calender', 'ReportController@getcalenderData');
        Route::get('admin/reports/supplier', 'ReportController@supplierreport')->name('admin.reports.supplier.list');
        Route::post('admin/reports/supplier', 'ReportController@getsupplierData');
        Route::get('admin/reports/staff', 'ReportController@staffreport')->name('admin.reports.staff.list');
        Route::post('admin/reports/staff', 'ReportController@getstaffData');
    });
});

Route::get('/logout', function () {
    Session::forget('userData');
    if (!Session::has('userData')) {
        return redirect('/');
    }
});

// Clear Cache
Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('log:clear');
    return "Run cmd is fine";
});


// Route::get('test/pdf',function(){
//     // $data['v1'] = "Test one";
//     $pdf = \PDF::loadView('test');

//       // download PDF file with download method
//     $time = "product_".time().".pdf";

//       return $pdf->stream('pdf_file.pdf');
//     //   return $pdf->download($time);
// });


